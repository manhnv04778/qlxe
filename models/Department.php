<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "department".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'name' => Yii::t('user', 'Tên đơn vị'),
            'status' => Yii::t('user', 'Tình trạng'),
        ];
    }

    public static function getDepartment($id = null){
        $query = Department::find()
            ->select(['id', 'name'])
            ->asArray(true)
            ->all();
        $data = [];
        foreach($query as $item){
            $data[$item['id']] = $item['name'];
        }
        if($id) return $data[$id];
        return $data;
    }

    public function search($params){
        $query = Department::find();
        if(!empty($params)){
            if(!empty($params['Department']['name']))
                $query->andWhere(['name' => $params['Department']['name']]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }
}
