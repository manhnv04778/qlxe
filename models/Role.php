<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "role".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'name' => Yii::t('user', 'Tên quyền'),
            'status' => Yii::t('user', 'Tình trạng'),
        ];
    }

    public static function getRole($id = null){
        $query = Role::find()
            ->select(['id', 'name'])
            ->asArray()
            ->all();
        $data = [];
        foreach($query as $item){
            $data[$item['id']] = $item['name'];
        }
        if($id) return $data[$id];
        return $data;
    }

    public function search($params){
        $query = Role::find();
        if(!empty($params)){
            if(!empty($params['Role']['name']))
                $query->andWhere(['name' => $params['Role']['name']]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }
}
