<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "register".
 *
 * @property string $id
 * @property string $car_id
 * @property string $seria_gcn
 * @property string $deparment
 * @property string $created
 * @property string $expire
 * @property string $created_cost
 * @property string $department_cost
 * @property string $number_cost
 * @property integer $money_cost
 * @property string $expire_cost
 *
 * @property Car $car
 */
class Register extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'register';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'created'], 'required'],
            [['car_id', 'money_cost'], 'integer'],
            [['created', 'expire', 'created_cost', 'expire_cost'], 'safe'],
            [['seria_gcn', 'department', 'department_cost', 'number_cost'], 'string', 'max' => 255],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::className(), 'targetAttribute' => ['car_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user in document ', 'ID'),
            'car_id' => Yii::t('user in document ', 'Xe'),
            'seria_gcn' => Yii::t('user in document ', 'Tem kiểm định'),
            'department' => Yii::t('user in document ', 'Đơn vị kiểm đinh'),
            'created' => Yii::t('user in document ', 'Ngày kiểm định'),
            'expire' => Yii::t('user in document ', 'Ngày hết hạn'),
            'created_cost' => Yii::t('user in document ', 'Ngày nộp'),
            'department_cost' => Yii::t('user in document ', 'Nộp tại đơn vị'),
            'number_cost' => Yii::t('user in document ', 'Số biên lai'),
            'money_cost' => Yii::t('user in document ', 'Số tiền nộp'),
            'expire_cost' => Yii::t('user in document ', 'Phí nộp tới thời gian nào?'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['id' => 'car_id']);
    }
    public function search($params){
        $query = Register::find();
        if(!empty($params)){
            if(!empty($params['Register']['name']))
                $query->andWhere(['name' => $params['Register']['name']]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

}
