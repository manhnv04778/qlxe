<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car_approved".
 *
 * @property integer $id
 * @property integer $car_id
 * @property string $date_from
 * @property string $date_to
 * @property integer $status
 * @property integer $document_id
 * @property integer $user_approve_id
 */
class CarApproved extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_approved';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'document_id', 'user_approve_id'], 'required'],
            [['car_id', 'status', 'document_id', 'user_approve_id'], 'integer'],
            [['date_from', 'date_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('car approve', 'ID'),
            'car_id' => Yii::t('car approve', 'Car ID'),
            'date_from' => Yii::t('car approve', 'Date From'),
            'date_to' => Yii::t('car approve', 'Date To'),
            'status' => Yii::t('car approve', 'Status'),
            'document_id' => Yii::t('car approve', 'Document ID'),
            'user_approve_id' => Yii::t('car approve', 'User Approve ID'),
        ];
    }
}
