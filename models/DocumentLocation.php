<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "document_location".
 *
 * @property string $id
 * @property string $document_id
 * @property string $city_id
 * @property string $district_id
 * @property string $address
 * @property string $long
 * @property integer $index
 *
 * @property Document $document
 */
class DocumentLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['document_id', 'city_id', 'district_id'], 'required'],
            [['document_id', 'city_id', 'index'], 'integer'],
            [['address', 'district_id'], 'string', 'max' => 255],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['document_id' => 'id']],
            [['long'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('car in document ', 'ID'),
            'document_id' => Yii::t('car in document ', 'Document ID'),
            'city_id' => Yii::t('car in document ', 'City ID'),
            'district_id' => Yii::t('car in document ', 'District ID'),
            'address' => Yii::t('car in document ', 'Address'),
            'index' => Yii::t('car in document ', 'Index'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getDistrict(){
        return $this->hasOne(District::className(), ['latlng' => 'district_id']);
    }
}
