<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "manufacture".
 *
 * @property integer $id
 * @property string $name
 * @property integer $country_id
 * @property integer $status
 */
class Manufacture extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'status'], 'integer'],
            [['name', 'country_id'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'name' => Yii::t('user', 'Tên hãng'),
            'country_id' => Yii::t('user', 'Quốc gia'),
            'status' => Yii::t('user', 'Tình trạng'),
        ];
    }

    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public function search($params){
        $query = Manufacture::find();
        if(!empty($params)){
            if(!empty($params['Manufacture']['name']))
                $query->andWhere(['name' => $params['Manufacture']['name']]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public static function getDataAll(){
        $query = Manufacture::find()->asArray()->all();

        $data = ArrayHelper::map($query, 'id', 'name');
        return $data;
    }


}
