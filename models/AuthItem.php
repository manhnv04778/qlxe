<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property string $group_name
 * @property string $group
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthRule $ruleName
 * @property AuthItemChild[] $authItemChildren
 * @property AuthItemChild[] $authItemChildren0
 * @property AuthItem[] $children
 * @property AuthItem[] $parents
 * @property AuthItem[] $parents
 * @property AuthItem[] $parents
 * @property AuthItem[] $parents
 */
class AuthItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data','group','group_name'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['rule_name'], 'exist', 'skipOnError' => true, 'targetClass' => AuthRule::className(), 'targetAttribute' => ['rule_name' => 'name']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('document', 'Name'),
            'type' => Yii::t('document', 'Type'),
            'description' => Yii::t('document', 'Vai tr�'),
            'rule_name' => Yii::t('document', 'Rule Name'),
            'data' => Yii::t('document', 'Data'),
            'created_at' => Yii::t('document', 'Created At'),
            'updated_at' => Yii::t('document', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(AuthRule::className(), ['name' => 'rule_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren()
    {
        return $this->hasMany(AuthItemChild::className(), ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren0()
    {
        return $this->hasMany(AuthItemChild::className(), ['child' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'child'])->viaTable('auth_item_child', ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'parent'])->viaTable('auth_item_child', ['child' => 'name']);
    }

    public static function getRole()
    {
        $query = AuthItem::find()->where(['type' => 1])->asArray()->all();
        $data = ArrayHelper::map($query, 'name', 'description');
        return $data;
    }

    public static function getPermission()
    {
        $permissions = AuthItem::find()->where(['type' => 2])->asArray()->all();
        return ArrayHelper::map($permissions, 'name', 'description');
    }

    public static function getPermissionByGroup()
    {
        $permissions = AuthItem::find()->where(['type' => 2])->all();
        $data = [];
        if($permissions){
            foreach ($permissions as $item) {
                $data[$item->group]['group_name'] = $item->group_name;
                $data[$item->group]['data'][$item->name] = $item->description;
            }
        }

        return $data;

    }

    public function search($params){
        $query = AuthItem::find()->where(['type' => 1]);
        if(!empty($params)){
            if(!empty($params['AuthItem']['name']))
                $query->andWhere(['name' => $params['Role']['name']]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }
}
