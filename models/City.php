<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "city".
 *
 * @property string $id
 * @property string $name
 * @property string $name_2
 * @property string $alias
 * @property string $latlng
 * @property string $index
 * @property string $group
 *
 * @property District[] $districts
 * @property DistrictHasPublicLocation[] $districtHasPublicLocations
 * @property Geo[] $geos
 * @property GeoBeta[] $geoBetas
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'latlng', 'index', 'group'], 'required'],
            [['index'], 'integer'],
            [['group'], 'string'],
            [['name', 'name_2', 'alias', 'latlng'], 'string', 'max' => 50],
            [['alias'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('driver in document ', 'ID'),
            'name' => Yii::t('driver in document ', 'Name'),
            'name_2' => Yii::t('driver in document ', 'Name 2'),
            'alias' => Yii::t('driver in document ', 'Alias'),
            'latlng' => Yii::t('driver in document ', 'Latlng'),
            'index' => Yii::t('driver in document ', 'Index'),
            'group' => Yii::t('driver in document ', 'Group'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(District::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrictHasPublicLocations()
    {
        return $this->hasMany(DistrictHasPublicLocation::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeos()
    {
        return $this->hasMany(Geo::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeoBetas()
    {
        return $this->hasMany(GeoBeta::className(), ['city_id' => 'id']);
    }

    public static function getCity(){
        $query = City::find()->orderBy(['name' => SORT_ASC])->asArray()->all();
        $data = ArrayHelper::map($query, 'id', 'name');
        return $data;
    }

    /* DATA
   -------------------------------------------------- */
    /**
     * @param bool|true $cache
     * @return array|mixed|\yii\db\ActiveRecord[]
     */
    public static function getDataAll()
    {
        $cities = City::find()->with([
            'districts' => function ($query) {
                /* @var $query \yii\db\ActiveQuery */
                $query->orderBy(['name' => SORT_ASC])->indexBy('id');
            }
        ])
            ->orderBy('name asc')
            ->indexBy('id')
            ->asArray()
            ->all();
        return $cities;
    }


    /**
     * Trả về 1 mảng dictionary với key và value
     * @param string $key id|alias
     * @return mixed
     */
    public static function getData($key = 'id', $name = 'name')
    {
        return ArrayHelper::map(self::getDataAll(), $key, $name);
    }
}
