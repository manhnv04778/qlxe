<?php

namespace app\models;

use app\modules\user\models\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "register_car".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $driver_id
 * @property string $start
 * @property string $end
 * @property string $date_suggest
 * @property string $content
 * @property string $license
 * @property integer $status
 * @property integer $car_id
 */
class RegisterCar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'register_car';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'driver_id', 'start', 'car_id'], 'required'],
            [['user_id', 'driver_id', 'status','car_id'], 'integer'],
            [['start', 'end', 'date_suggest'], 'safe'],
            [['content'], 'string'],
            [['license'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'user_id' => Yii::t('user', 'User ID'),
            'driver_id' => Yii::t('user', 'Driver ID'),
            'start' => Yii::t('user', 'Start'),
            'end' => Yii::t('user', 'End'),
            'date_suggest' => Yii::t('user', 'Date Suggest'),
            'content' => Yii::t('user', 'Content'),
            'license' => Yii::t('user', 'License'),
            'status' => Yii::t('user', 'Status'),
            'car_id' => Yii::t('user', 'Car ID'),
        ];
    }

    public function search($params){
        $query = RegisterCar::find();
        if(!empty($params)){
            if(!empty($params['RegisterCar']['content']))
                $query->andWhere(['content' => $params['RegisterCar']['content']]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public static function getStatus(){
        $data = [
            '0' => 'Chờ tiếp nhận',
            '1' => 'Đã tiếp nhận',
            '2' => 'Đã duyệt',
            '3' => 'Không tiếp nhận'
        ];
        return $data;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getDriver()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_id']);
    }
}
