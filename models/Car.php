<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car".
 *
 * @property string $id
 * @property string $name
 * @property string $code
 * @property string $seria
 * @property string $seria_machine
 * @property string $type
 * @property string $type_use
 * @property string $color
 * @property string $upholsterer
 * @property integer $user_id
 * @property integer $driver_id
 * @property string $country
 * @property string $manufacture
 * @property string $time_start
 * @property string $time_end
 * @property string $speed
 * @property integer $status
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code','color', 'upholsterer', 'country', 'manufacture'], 'required'],
            [['user_id', 'driver_id', 'status'], 'integer'],
            [['time_start', 'time_end','type','type_use'], 'safe'],
            [['name', 'seria', 'seria_machine', 'country', 'manufacture'], 'string', 'max' => 255],
            [['code', 'color', 'speed'], 'string', 'max' => 50],
            [['upholsterer'], 'string', 'max' => 100],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('document', 'ID'),
            'name' => Yii::t('document', 'Tên xe'),
            'code' => Yii::t('document', 'Biển số'),
            'seria' => Yii::t('document', 'Số khung'),
            'seria_machine' => Yii::t('document', 'Số máy'),
            'type' => Yii::t('document', 'Hình thức sử dụng'),
            'type_use' => Yii::t('document', 'Tình hình sử dụng'),
            'color' => Yii::t('document', 'Màu sắc'),
            'upholsterer' => Yii::t('document', 'Số chỗ'),
            'user_id' => Yii::t('document', 'User ID'),
            'driver_id' => Yii::t('document', 'Driver ID'),
            'country' => Yii::t('document', 'Nước sản xuất'),
            'manufacture' => Yii::t('document', 'Nhà sản xuất'),
            'time_start' => Yii::t('document', 'Time Start'),
            'time_end' => Yii::t('document', 'Time End'),
            'speed' => Yii::t('document', 'Dung tích xe'),
            'status' => Yii::t('document', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegisters()
    {
        return $this->hasMany(Register::className(), ['car_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarLogs()
    {
        return $this->hasMany(CarLog::className(), ['car_id' => 'id']);
    }

    public function getCarLogCount()
    {
        return CarLog::find()->where(['car_id' => $this->id])->count();
    }

    public function getOilSum()
    {
        return Combustible::find()->where(['car_id' => $this->id])->sum('capacity');
    }


    public function search($params){
        $query = Car::find();
        if(!empty($params)){
            if(!empty($params['Car']['name']))
                $query->andWhere(['name' => $params['Car']['name']]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public static function getCar($key = 'id', $name = 'name'){
        $query = Car::find()->asArray()->all();

        $data = ArrayHelper::map($query, $key, $name);
        return $data;
    }

}
