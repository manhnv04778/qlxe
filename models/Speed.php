<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "speed".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class Speed extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'speed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'Mã'),
            'name' => Yii::t('user', 'Dung tích'),
            'status' => Yii::t('user', 'Trạng thái'),
        ];
    }

    public function search($params){
        $query = Speed::find();
        if(!empty($params)){
            if(!empty($params['Speed']['name']))
                $query->andWhere(['name' => $params['Speed']['name']]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public static function getDataAll(){
        $query = Speed::find()->asArray()->all();

        $data = ArrayHelper::map($query, 'id', 'name');
        return $data;
    }
}
