<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "type_car_use".
 *
 * @property string $id
 * @property string $name
 * @property string $desc
 * @property integer $status
 */
class TypeCarUse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_car_use';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['desc'], 'string'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'name' => Yii::t('user', 'Tình trạng'),
            'desc' => Yii::t('user', 'Mô tả'),
            'status' => Yii::t('user', 'Trạng thái'),
        ];
    }

    public function search($params){
        $query = TypeCarUse::find();
        if(!empty($params)){
            if(!empty($params['TypeCarUse']['name']))
                $query->andWhere(['name' => $params['TypeCarUse']['name']]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public static function getDataAll(){
        $query = TypeCarUse::find()->asArray()->all();

        $data = ArrayHelper::map($query, 'id', 'name');
        return $data;
    }
}
