<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "UserInDocumentForm".
 * @property integer $doc_id
 * @property string $full_name
 * @property string $department
 * @property string $position
 * @property string $email
 * @property string $phone
 */
class UserInDocumentForm extends Model
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'department', 'position', 'email', 'phone'], 'string'],
            [['full_name', 'department', 'position', 'email', 'phone', 'doc_id'],'required'],
            [['doc_id'], 'integer'],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'full_name' => Yii::t('document', 'Họ và tên'),
            'department' => Yii::t('document', 'Đơn vị'),
            'position' => Yii::t('document', 'Chức danh'),
            'email' => Yii::t('document', 'Email'),
            'phone' => Yii::t('document', 'Điện thoại'),
        ];
    }

}
