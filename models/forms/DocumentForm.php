<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "document".
 * @property integer $status
 * @property integer $car_id
 * @property integer $driver_id
 * @property integer $modify_user_id
 * @property integer $user_id_approve
 * @property string $approve_note
 * @property string $approve_date
 */
class DocumentForm extends Model
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['approve_note'], 'string'],
            [['approve_note','status', 'car_id', 'driver_id', 'user_id_approve'],'required'],
            [['status', 'car_id', 'driver_id', 'user_id_approve'], 'integer'],
            [['approve_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'approve_note' => Yii::t('document', 'Nội dung duyệt'),
            'status' => Yii::t('document', 'Trạng thái'),
            'car_id' => Yii::t('document', 'Chọn Xe'),
            'driver_id' => Yii::t('document', 'Chọn tài xế'),
            'user_id_approve' => Yii::t('document', 'Người duyệt'),
            'approve_date' => Yii::t('document', 'Ngày duyệt'),

        ];
    }

}
