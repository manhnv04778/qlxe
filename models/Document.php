<?php

namespace app\models;

use app\helpers\DateTimeHelper;
use app\modules\user\models\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "document".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $status
 * @property string $full_name
 * @property integer $department_id
 * @property string $attachment
 * @property integer $from
 * @property string $from_name
 * @property integer $number_user
 * @property string $date_from
 * @property string $date_to
 * @property string $date_from_car
 * @property string $date_to_car
 * @property string $purpose_car
 * @property integer $number_car
 * @property string $date_from_user
 * @property string $date_to_user
 * @property string $place_user
 * @property string $note_document
 * @property integer $car_id
 * @property integer $driver_id
 * @property string $created_date
 * @property integer $created_user_id
 * @property string $modify_date
 * @property integer $modify_user_id
 * @property integer $user_id_approve
 * @property string $approve_note
 * @property string $approve_date
 * @property integer $lenght
 */
class Document extends \yii\db\ActiveRecord
{
    public  $full_name_user=[];
    public  $department_user=[];
    public  $position_user=[];
    public  $email_user=[];
    public  $phone_user=[];
    public  $list_car = [];
    public  $list_driver = [];
    public  $citys = [];
    public  $districts = [];
    public  $address = [];
    public  $long = [];

    public function setActiveValidators($activeValidators)
    {
        $this->activeValidators = $activeValidators;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'lenght', 'full_name', 'department_id'],'required'],
            [['name', 'attachment', 'from_name', 'purpose_car', 'place_user', 'note_document', 'approve_note'], 'string'],
            [['full_name_user', 'department_user', 'position_user', 'email_user', 'phone_user','list_car', 'list_driver'],'each', 'rule'=>['string']],
            [['status', 'department_id', 'from', 'number_user', 'number_car', 'car_id', 'driver_id', 'created_user_id', 'modify_user_id', 'user_id_approve'], 'integer'],
            [['date_from', 'date_to', 'date_from_car', 'date_to_car', 'date_from_user', 'date_to_user', 'created_date', 'modify_date', 'approve_date'], 'safe'],
            ['lenght', 'number'],
            [['code'], 'string', 'max' => 100],
            [['full_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('document', 'ID'),
            'code' => Yii::t('document', 'Mã hồ sơ'),
            'name' => Yii::t('document', 'Tên hồ sơ'),
            'status' => Yii::t('document', 'Trạng thái'),
            'full_name' => Yii::t('document', 'Tên cán bộ'),
            'department_id' => Yii::t('document', 'Đơn vị'),
            'attachment' => Yii::t('document', 'Tệp đính kèm'),
            'from' => Yii::t('document', 'Tỉnh/Thành phố'),
            'from_name' => Yii::t('document', 'Địa chỉ công tác'),
            'number_user' => Yii::t('document', 'Số cán bộ đi công tác'),
            'date_from' => Yii::t('document', 'Thời gian công tác từ ngày'),
            'date_to' => Yii::t('document', 'Công tác đến ngày'),
            'date_from_car' => Yii::t('document', 'Sử dụng xe từ ngày'),
            'date_to_car' => Yii::t('document', 'Sửa dụng xe đến ngày'),
            'purpose_car' => Yii::t('document', 'Mục đích sử dụng xe'),
            'number_car' => Yii::t('document', 'Số ghế ngồi'),
            'date_from_user' => Yii::t('document', 'Thời gian đón cán bộ từ'),
            'date_to_user' => Yii::t('document', 'Thời gian đón cán bộ đến'),
            'place_user' => Yii::t('document', 'Địa điểm đón'),
            'note_document' => Yii::t('document', 'Ghi chú'),
            'car_id' => Yii::t('document', 'Chọn xe'),
            'driver_id' => Yii::t('document', 'Tài xế'),
            'created_date' => Yii::t('document', 'Ngày tạo'),
            'created_user_id' => Yii::t('document', 'Người tạo'),
            'modify_date' => Yii::t('document', 'Ngày sửa'),
            'modify_user_id' => Yii::t('document', 'Người sửa'),
            'user_id_approve' => Yii::t('document', 'Người duyệt'),
            'approve_note' => Yii::t('document', 'Nội dung duyệt'),
            'approve_date' => Yii::t('document', 'Ngày duyệt'),
            'lenght' => Yii::t('document', 'Khoảng cách'),
        ];
    }

    public function search($params = null){
        //echo '<pre>'; print_r($params); echo '</pre>';die;
        $query = Document::find()
            ->orderBy(['created_date' => SORT_DESC]);
        if(!empty($params)){
            if(!empty($params['Document'])) {
                if (!empty($params['Document']['name'])) {
                    $query->andWhere(['like', 'name', $params['Document']['name']]);
                }

                if (!empty($params['Document']['created_from'] && !emtpy($params['Document']['created_to']))) {
                    $date_from = date("Y-m-d", strtotime(strtr($params['Document']['created_from'], '/', '-')));
                    $date_to = date("Y-m-d", strtotime(strtr($params['Document']['created_to'], '/', '-')));
                    $query->andWhere(['between', 'created_date', $date_from, $date_to]);
                }


                if (!empty($params['Document']['code'])) {
                    $query->andWhere(['code' => $params['Document']['code']]);
                }

                if (!empty($params['Document']['from'])) {
                    $query->andWhere(['from' => $params['Document']['from']]);
                }

                if (!empty($params['Document']['status']) || $params['Document']['status'] == "0") {
                    $query->andWhere(['status' => $params['Document']['status']]);
                }
            }

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public function searchSend($params = null){
        $query = Document::find()->where(['in','status', [1,2,3]])->orderBy(['approve_date' => SORT_DESC, 'created_date' => SORT_DESC]);
        if(!empty($params)){
            if(!empty($params['Document']['name'])){
                $query->andWhere(['like', 'name', $params['Document']['name']]);
            }
            if(!empty($params['Document'])) {
                if (!empty($params['Document']['created_from'] && !empty($params['Document']['created_to']))) {
                    $date_from = date("Y-m-d", strtotime(strtr($params['Document']['created_from'], '/', '-')));
                    $date_to = date("Y-m-d", strtotime(strtr($params['Document']['created_to'], '/', '-')));
                    $query->andWhere(['between', 'created_date', $date_from, $date_to]);
                }
            }

            if(!empty($params['Document']['code'])){
                $query->andWhere(['code' => $params['Document']['code']]);
            }

            if(!empty($params['Document']['from'])){
                $query->Where(['from' => $params['Document']['from']]);
            }

            if(!empty($params['Document']['status'])){
                $query->Where(['status' => $params['Document']['status']]);
            }
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public function searchApprove($params){
        $query = Document::find()->where(['status' => 2]);
        if(!empty($params)){
            if(!empty($params['Document']['full_name']))
                $query->andWhere(['content' => $params['Document']['full_name']]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public static function getStatus(){
        $data = [
            '0' => 'Mới tạo',
            '1' => 'Gửi yêu cầu',
            '2' => 'Đã duyệt',
            '3' => 'Từ chối',
            '4' => 'Hủy'
        ];
        return $data;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_user_id']);
    }

    public function getUserApprove()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id_approve']);
    }

    public function getDriver()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_id']);
    }

    public function getDepartment(){
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    public function getCity(){
        return $this->hasOne(City::className(), ['id' => 'from']);
    }

    public function getUserInDoc(){
        return $this->hasMany(UserInDocument::className(),['document_id' => 'id']);
    }

    public function getDocumentHistory(){
        return $this->hasMany(DocumentHistory::className(),['document_id' => 'id']);
    }

    public function getDocumentLocation(){
        return $this->hasMany(DocumentLocation::className(), ['document_id' => 'id']);
    }

    public static function getDocumentLocationByDocId($document_id){
        $str_location = "";
        if(!empty($document_id)){
            $list_location = DocumentLocation::find()->where(['document_id' => $document_id])->orderBy(['index' => SORT_ASC])->all();
            $count_list = count($list_location);
            $i=0;
            if(!empty($list_location)){
                foreach($list_location as $item){
                    $i++;
                    if($i < $count_list){
                        $str_location .= $item->city->name . ' - ';
                    }
                    elseif($i == $count_list){
                        $str_location .= $item->city->name;
                    }
                }
            }
        }
        return $str_location;
    }

    public function insertUserDocument(){

        foreach($this->full_name_user as $key => $value){

            $userInDocument = new UserInDocument();
            $userInDocument->document_id = $this->id;
            $userInDocument->user_name = $value;
            $userInDocument->department = $this->department_user[$key];
            $userInDocument->position = $this->position_user[$key];
            $userInDocument->email = $this->email_user[$key];
            $userInDocument->phone = $this->phone_user[$key];

            $userInDocument->insert();
        }
    }

    public function insertCarInDocument(){

        foreach($this->list_car as $key => $value){

            $carInDocument = new CarInDocument();
            $carInDocument->document_id = $this->id;
            $carInDocument->car_id = $this->list_car[$key];
            $carInDocument->driver_id = $this->list_driver[$key];
            $carInDocument->date_start = DateTimeHelper::getDateTime($this->date_from,'Y-m-d H:i');
            $carInDocument->date_end = DateTimeHelper::getDateTime($this->date_to,'Y-m-d H:i');
            $carInDocument->created = DateTimeHelper::getDateTime('now','Y-m-d H:i');
            $carInDocument->insert();
        }
    }
}
