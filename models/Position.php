<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "position".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class Position extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'name' => Yii::t('user', 'Tên quyền'),
            'status' => Yii::t('user', 'Tình trạng'),
        ];
    }

    public static function getPosition($id = null){
        $query = Position::find()
                    ->select(['id', 'name'])
                    ->asArray()
                    ->all();
        $data = [];
        foreach($query as $item){
            $data[$item['id']] = $item['name'];
        }
        if($id) return $data[$id];
        return $data;
    }

    public function search($params){
        $query = Position::find();
        if(!empty($params)){
            if(!empty($params['Position']['name']))
                $query->andWhere(['name' => $params['Position']['name']]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }
}
