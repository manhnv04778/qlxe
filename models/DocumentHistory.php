<?php

namespace app\models;

use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "document_history".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $document_id
 * @property string $note
 * @property string $created_date
 */
class DocumentHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'document_id'], 'integer'],
            [['note'], 'string'],
            [['created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('document', 'ID'),
            'user_id' => Yii::t('document', 'User ID'),
            'note' => Yii::t('document', 'Note'),
            'created_date' => Yii::t('document', 'Created Date'),
            'document_id' => Yii::t('document', 'Document ID'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
