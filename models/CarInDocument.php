<?php

namespace app\models;

use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "car_in_document".
 *
 * @property integer $id
 * @property integer $car_id
 * @property integer $document_id
 * @property string $created
 * @property string $date_start
 * @property string $date_end
 * @property integer $status
 * @property integer $driver_id
 */
class CarInDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_in_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'document_id', 'created', 'date_start', 'date_end', 'driver_id'], 'required'],
            [['car_id', 'document_id', 'status', 'driver_id'], 'integer'],
            [['created', 'date_start', 'date_end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('car in document ', 'ID'),
            'car_id' => Yii::t('car in document ', 'Car ID'),
            'document_id' => Yii::t('car in document ', 'Document ID'),
            'created' => Yii::t('car in document ', 'Created'),
            'date_start' => Yii::t('car in document ', 'Date Start'),
            'date_end' => Yii::t('car in document ', 'Date End'),
            'status' => Yii::t('car in document ', 'Status'),
            'driver_id' => Yii::t('car in document ', 'Driver ID'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_id']);
    }
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['id' => 'car_id']);
    }
}
