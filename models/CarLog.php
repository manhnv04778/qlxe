<?php

namespace app\models;

use app\modules\user\models\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "car_log".
 *
 * @property integer $id
 * @property string $title
 * @property string $desc
 * @property integer $car_id
 * @property integer $user_id
 * @property integer $driver_id
 * @property string $date_care
 * @property integer $status
 * @property integer $approve_id
 * @property integer $is_complete
 * @property integer $money
 */
class CarLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['desc'], 'string'],
            [['car_id', 'user_id'], 'required'],
            [['car_id', 'user_id', 'driver_id', 'status', 'approve_id', 'is_complete'], 'integer'],
            [['date_care','money'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'title' => Yii::t('user', 'Title'),
            'desc' => Yii::t('user', 'Nội dung bảo dưỡng'),
            'car_id' => Yii::t('user', 'Chọn xe'),
            'user_id' => Yii::t('user', 'Nguời bảo dưỡng'),
            'driver_id' => Yii::t('user', 'Driver ID'),
            'date_care' => Yii::t('user', 'Ngày bảo dưỡng'),
            'status' => Yii::t('user', 'Status'),
            'approve_id' => Yii::t('user', 'Người duyệt'),
            'is_complete' => Yii::t('user', 'Trạng thái'),
            'money' => Yii::t('user', 'Chi phí bảo dưỡng'),
        ];
    }

    public function getUserCare()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['id' => 'car_id']);
    }

    public function getUserApprove()
    {
        return $this->hasOne(User::className(), ['id' => 'approve_id']);
    }

    public function search($params){
        $query = CarLog::find();
        if(!empty($params)){
            if(!empty($params['CarLog']['name']))
                $query->andWhere(['title' => $params['CarLog']['title']]);
            if(!empty($params['CarLog']['car_id']))
                $query->andWhere(['car_id' => $params['CarLog']['car_id']]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public static function getStatusComplete(){
        $data = ['0' => 'Chưa bảo dưỡng xong','1' => 'Đã bảo dưỡng xong'];
        return $data;
    }


}
