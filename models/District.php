<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "district".
 *
 * @property string $id
 * @property string $city_id
 * @property string $name
 * @property string $alias
 * @property string $name_unicode
 * @property string $latlng
 * @property string $index
 *
 * @property City $city
 * @property DistrictHasPublicLocation[] $districtHasPublicLocations
 * @property PublicLocation[] $publicLocations
 */
class District extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'district';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'name', 'alias', 'latlng', 'index'], 'required'],
            [['city_id', 'index'], 'integer'],
            [['name', 'alias', 'latlng','name_unicode'], 'string', 'max' => 50],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('driver in document ', 'ID'),
            'city_id' => Yii::t('driver in document ', 'City ID'),
            'name' => Yii::t('driver in document ', 'Name'),
            'alias' => Yii::t('driver in document ', 'Alias'),
            'latlng' => Yii::t('driver in document ', 'Latlng'),
            'index' => Yii::t('driver in document ', 'Index'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrictHasPublicLocations()
    {
        return $this->hasMany(DistrictHasPublicLocation::className(), ['district_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicLocations()
    {
        return $this->hasMany(PublicLocation::className(), ['id' => 'public_location_id'])->viaTable('district_has_public_location', ['district_id' => 'id']);
    }

    /**
     * Lấy mảng district group by city. key của city và district là id hoặc alias
     * @param string $cityKey id|alias
     * @param string $districtKey id|alias
     * @return array
     */
    public static function getDataGroupByCity($cityKey = 'id', $districtKey = 'id'){
        $cityDataAll = City::getDataAll();

        $data = array();
        foreach($cityDataAll as $city){
            foreach($city['districts'] as $district){
                $data[$city[$cityKey]][$district[$districtKey]] = $district['name'];
            }
        }
        return $data;
    }

    /* DATA
   -------------------------------------------------- */
    /**
     * @param bool|true $cache
     * @return array|mixed|\yii\db\ActiveRecord[]
     */
    public static function getDataAll($city_id)
    {
        $cities = District::find()
                ->where(['city_id' => $city_id])
                ->orderBy('name asc')
                ->indexBy('id')
                ->asArray()
                ->all();
        return $cities;
    }


    /**
     * Trả về 1 mảng dictionary với key và value
     * @param string $key id|alias
     * @return mixed
     */
    public static function getDistrictByCity($city_id, $key = 'latlng', $name = 'name')
    {
        return ArrayHelper::map(self::getDataAll($city_id), $key, $name);
    }
}
