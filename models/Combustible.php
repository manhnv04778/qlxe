<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "combustible".
 *
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property integer $car_id
 * @property integer $capacity
 * @property integer $lenght
 * @property integer $type
 * @property integer $amount
 * @property integer $status
 * @property integer $billing_number
 * @property string $created
 */
class Combustible extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'combustible';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'desc', 'car_id', 'capacity', 'lenght', 'amount', 'billing_number', 'type'], 'required'],
            [['desc', 'type'], 'string'],
            [['car_id', 'capacity', 'lenght', 'amount', 'status', 'billing_number'], 'integer'],
            [['created'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'name' => Yii::t('user', 'Tên quá trình sử dụng nhiên liệu'),
            'desc' => Yii::t('user', 'Mô tả'),
            'car_id' => Yii::t('user', 'Xe sử dụng nhiên liệu'),
            'capacity' => Yii::t('user', 'Thể tích tiêu thụ'),
            'lenght' => Yii::t('user', 'Số km đã đi'),
            'type' => Yii::t('user', 'Loại nhiên liệu'),
            'amount' => Yii::t('user', 'Chi phí'),
            'status' => Yii::t('user', 'Trạng thái'),
            'billing_number' => Yii::t('user', 'Mã hoá đơn'),
            'created' => Yii::t('user', 'Ngày tạo'),
        ];
    }

    public function search($params){
        $query = Combustible::find();
        if(!empty($params)){
            if(!empty($params['Combustible']['name']))
                $query->andWhere(['name' => $params['Combustible']['name']]);

            if(!empty($params['Combustible']['car_id']))
                $query->andWhere(['car_id' => $params['Combustible']['car_id']]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public static function getType()
    {
        return [
            'oil' => 'Dầu',
            'petrol' => 'Xăng'
        ];

    }
    public static function getStatus()
    {
        return [
            '0' => 'Chưa thanh toán',
            '1' => 'Đã thanh toán'
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['id' => 'car_id']);
    }
}
