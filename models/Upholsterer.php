<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "upholsterer".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class Upholsterer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'upholsterer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'name' => Yii::t('user', 'Số chỗ'),
            'status' => Yii::t('user', 'Trạng thái'),
        ];
    }

    public function search($params){
        $query = Upholsterer::find();
        if(!empty($params)){
            if(!empty($params['Upholsterer']['name']))
                $query->andWhere(['name' => $params['Upholsterer']['name']]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public static function getDataAll(){
        $query = Upholsterer::find()->asArray()->all();

        $data = ArrayHelper::map($query, 'id', 'name');
        return $data;
    }
}
