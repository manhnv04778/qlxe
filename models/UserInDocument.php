<?php

namespace app\models;

use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "user_in_document".
 *
 * @property integer $id
 * @property string $user_name
 * @property integer $document_id
 * @property string $department
 * @property string $position
 * @property string $email
 * @property string $phone
 */
class UserInDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_in_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_name', 'document_id', 'department', 'position', 'phone'], 'required'],
            [['document_id'], 'integer'],
            [['user_name', 'department', 'position', 'email', 'phone'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user in document ', 'ID'),
            'user_name' => Yii::t('user in document ', 'User Name'),
            'document_id' => Yii::t('user in document ', 'Document ID'),
            'department' => Yii::t('user in document ', 'Department'),
            'position' => Yii::t('user in document ', 'Position'),
            'email' => Yii::t('user in document ', 'Email'),
            'phone' => Yii::t('user in document ', 'Phone'),
        ];
    }

}
