<?php

namespace app\models;

use app\modules\user\models\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "time_use".
 *
 * @property integer $id
 * @property string $desc
 * @property integer $car_id
 * @property string $content
 * @property integer $user_id
 * @property integer $driver_id
 * @property integer $leader_id
 * @property string $created
 * @property string $start
 * @property string $end
 * @property integer $status
 */
class TimeUse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'time_use';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'user_id', 'driver_id', 'leader_id', 'status'], 'integer'],
            [['content'], 'string'],
            [['created', 'start', 'end'], 'safe'],
            [['desc'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'desc' => Yii::t('user', 'Desc'),
            'car_id' => Yii::t('user', 'Car ID'),
            'content' => Yii::t('user', 'Content'),
            'user_id' => Yii::t('user', 'User ID'),
            'driver_id' => Yii::t('user', 'Driver ID'),
            'leader_id' => Yii::t('user', 'Leader ID'),
            'created' => Yii::t('user', 'Created'),
            'start' => Yii::t('user', 'Start'),
            'end' => Yii::t('user', 'End'),
            'status' => Yii::t('user', 'Status'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getDriver()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_id']);
    }
    public function getLeader()
    {
        return $this->hasOne(User::className(), ['id' => 'leader_id']);
    }

    public static function getStatus(){
        $data = ['0' => 'Ngưng sử dụng','1' => 'Đang sử dụng'];
        return $data;
    }

    public function search($params){
        $query = TimeUse::find();
        if(!empty($params)){
            if(!empty($params['TimeUse']['desc']))
                $query->andWhere(['name' => $params['TimeUse']['desc']]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }
}
