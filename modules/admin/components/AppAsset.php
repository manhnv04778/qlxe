<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\components;

use Yii;
use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
//	public $sourcePath = '@common/css';
//	public $basePath = '@webroot'; // @webroot
//	public $baseUrl = '@web'; // @web
    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\authclient\widgets\AuthChoiceAsset',
        //		'app\components\IEAsset',
    ];

    public $themeFilePath = 'themes/v1/files';

    public function init()
    {

        parent::init();
        $theme = Yii::$app->view->theme->baseUrl;


        //$this->css[] = 'files/css/bootstrap/css/bootstrap.css';
        $this->css[] = 'files/css/Font-Awesome/css/font-awesome.min.css';
        $this->css[] = 'files/css/ionicons/css/ionicons.min.css';
        //$this->css[] = 'files/themes/admin/bower_components/bootstrap/dist/css/bootstrap.min.css';
        $this->css[] = 'files/themes/admin/bower_components/metisMenu/dist/metisMenu.min.css';
        $this->css[] = 'files/themes/admin/dist/css/timeline.css';
        $this->css[] = 'files/themes/admin/dist/css/sb-admin-2.css';
        $this->css[] = 'files/themes/admin/bower_components/morrisjs/morris.css';
        $this->css[] = 'files/themes/admin/bower_components/app-upload.css';

        // load theme

        $this->js[] = $theme.'/files/js/theme.options.js';


        //$this->js[] = '/files/themes/admin/bower_components/bootstrap/dist/js/bootstrap.min.js';

        //$this->js[] = '/files/css/bootstrap/js/bootstrap.min.js';

        $this->js[] = '/files/themes/admin/bower_components/metisMenu/dist/metisMenu.min.js';
        $this->js[] = '/files/themes/admin/bower_components/raphael/raphael-min.js';
        $this->js[] = '/files/themes/admin/bower_components/morrisjs/morris.min.js';

       // $this->js[] = '/files/themes/admin/js/morris-data.js';
        $this->js[] = '/files/themes/admin/dist/js/sb-admin-2.js';
        // custom
        $this->css[] = 'files/css/jasny-bootstrap/dist/css/jasny-bootstrap.min.css';
        $this->js[] = 'files/css/jasny-bootstrap/dist/js/jasny-bootstrap.min.js';

		$this->js[] = '/files/js/bootbox.min.js';
        // custom
        $this->css[] = 'files/css/animate.css'; // animations
        $this->css[] = $theme.'/files/css/admin_style.css';
        $this->js[] = 'files/js/bootstrap-growl/bootstrap-growl.min.js'; // import bootstrap growl
        // load init & common
        $this->js[] = $theme.'/files/js/functions.js';
        $this->js[] = $theme.'/files/js/common.js';

    }
}
