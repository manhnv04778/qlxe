<?php
/**
 * Created by PhpStorm.
 * User: ABC
 * Date: 10/17/14
 * Time: 20:36
 */

namespace app\modules\admin\components;

use Yii;
use yii\helpers\Url;

class Controller extends \app\components\Controller{

    public $menu_parent_selected;
    public $menu_child_selected;
    public $sidebarChildSelected;

    public function init(){
        parent::init();
        $this->layout = '@theme/layouts/sidebar_admin';
        Yii::$app->user->loginUrl = '/admin/admin/login';
        $this->user = Yii::$app->user->identity;
        if(!$this->user){
            $this->redirect(Url::toRoute(['/admin/admin/login']));
        }

    }





} 