<?php

namespace app\modules\admin;


class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\admin\controllers';

    public function init()
    {
        parent::init();
	    // load module config
	    \Yii::configure(\Yii::$app, require(__DIR__ . '/config/config.php'));


//	    // set theme & layout
//	    \Yii::$app->setComponents([
//		    'view' => [
//			    'class' => 'app\modules\admin\components\View',
//			    'theme' => [ // http://www.yiiframework.com/doc-2.0/guide-output-theming.html
//				    'baseUrl' => '@web/themes/admin',
//				    'basePath' => '@app/themes/admin',
//				    'pathMap' => [
//					    '@app/views' => '@app/themes/admin',
//					    '@app/modules/admin/views' => '@app/themes/admin',
//
////					    '@app/modules/admin/views/common' => '@app/themes/admin/common',
////					    '@app/modules/admin/widgets' => '@app/themes/admin/widgets',
//				    ],
//			    ],
//		    ],
//	    ]);

    }
}
