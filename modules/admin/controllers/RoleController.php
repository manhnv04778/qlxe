<?php

namespace app\modules\admin\controllers;


use app\models\AuthAssignment;
use app\models\AuthItem;
use app\models\AuthItemChild;
use app\models\Role;
use app\modules\admin\components\Controller;
use app\modules\admin\models\forms\AddUserForm;
use app\modules\admin\models\forms\RoleForm;
use app\modules\user\models\elasticsearch\UserElasticsearch;
use app\modules\user\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class RoleController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['sync'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
//                    [
//                        'actions' => ['register'],
//                        'allow' => true,
//                        'roles' => ['?'], // chỉ dành cho user chưa đăng nhập
//                    ],
//                    [
//                        'actions' => ['create', 'update'],
//                        'allow' => true,
//                        'roles' => ['@'], // chỉ dành cho user đã đăng nhậpdeny. nếu chưa đăng nhập thì redirect sang trang login.
//                    ],
                    [
                        'actions' => ['sync'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action){
                            return true; //Yii::$app->user->can(AuthManager::ROLE_ADMIN);
                        }
                    ],
//                    [
//                        'actions' => ['create', 'index', 'update'],
//                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
//                        'matchCallback' => function ($rule, $action){
//                            return Yii::$app->user->can(AuthManager::ROLE_AUTHOR);
//                        }
//                    ],
                ],
            ],
        ];
    }

    public function actionIndex(){

        $searchModel = new AuthItem();
        // Check if there is an Editable ajax request
        if(Yii::$app->request->post('hasEditable')){
        }
        $params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => $params
        ]);
    }

    public function actionCreate(){
        $roleForm = new RoleForm();
        $roleForm->scenario = 'create';
        $permissions = AuthItem::getPermissionByGroup();
        if($post = Yii::$app->request->post()){
            $roleForm->load($post);
            if($roleForm->validate()){

                $authItem = new AuthItem();
                $authItem->name = StringHelper::toSEOString($roleForm->description);
                $authItem->description = $roleForm->description;
                $authItem->type = 1;
                $authItem->created_at = time();
                $authItem->insert();

                if(!empty($roleForm->permission)){
                    foreach($roleForm->permission as $item){
                        $authItemChild = new AuthItemChild();
                        $authItemChild->parent = $authItem->name;
                        $authItemChild->child = $item;
                        $authItemChild->insert();
                    }
                }
                Yii::$app->session->setFlash('success', Yii::t('user', 'Thêm thành công vai trò mới'));
                return $this->redirect(['/admin/role/index']);
            }



        }
        return $this->render('create', [
            'roleForm' => $roleForm,
            'permissions' => $permissions,
            'permissinOfRole' => []
        ]);
    }

    public function actionUpdate($name){
        /** @var  $model AuthItem*/
        $model = AuthItem::findOne(['name' => $name]);
        if(!$model) throw new NotFoundHttpException('Vai trò không tồn tại');

        $pR = AuthItemChild::findAll(['parent' => $name]);
        $permissinOfRole = [];
        if($pR){
            foreach ($pR as $item) {
                $permissinOfRole[] = $item->child;
            }
        }

        $permissions = AuthItem::getPermissionByGroup();

        /** @var  $roleForm RoleForm*/
        $roleForm = new RoleForm();
        $roleForm->description = $model->description;

        if($post = Yii::$app->request->post()){
            $roleForm->load($post);
            if($roleForm->validate()){
                $model->description = $roleForm->description;
                $model->updated_at = time();
                $model->update();

                AuthItemChild::deleteAll(['parent' => $model->name]);
                if(!empty($roleForm->permission)){
                    foreach($roleForm->permission as $p){
                        $authItemChild = new AuthItemChild();
                        $authItemChild->parent = $model->name;
                        $authItemChild->child = $p;
                        $authItemChild->insert();
                    }
                }
            }
            Yii::$app->session->setFlash('success', Yii::t('user', 'Cập nhật vai trò thành công'));
            return $this->redirect(['/admin/role/index']);
        }

        return $this->render('update', [
            'roleForm' => $roleForm,
            'permissions' => $permissions,
            'permissinOfRole' => $permissinOfRole
        ]);
    }


    public function actionAddUser()
    {
        $addUserForm = new AddUserForm();
        $addUserForm->scenario = 'create';

        if($post = Yii::$app->request->post()){

            $addUserForm->load($post);
            if($addUserForm->validate()){
                $user = User::findOne(['user_name' => $addUserForm->user_id]);

                $authAssign = new AuthAssignment();
                $authAssign->user_id = $user->id;
                $authAssign->item_name = $addUserForm->item_name;
                $authAssign->created_at = time();
                if($authAssign->validate()){
                    $authAssign->insert();
                }else{
                    echo '<pre>'; print_r($authAssign->errors); echo '</pre>';die;
                }

                Yii::$app->session->setFlash('success', Yii::t('user', 'Cấp vai trò cho user thành công'));
                return $this->redirect(['/admin/role/index']);
            }

        }
        return $this->render('add_user',[
            'addUserForm' => $addUserForm
        ]);
    }

}
