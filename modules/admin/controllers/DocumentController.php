<?php

namespace app\modules\admin\controllers;

use app\helpers\DateTimeHelper;
use app\models\Car;
use app\models\CarApproved;
use app\models\CarInDocument;
use app\models\Document;
use app\models\DocumentHistory;
use app\models\DocumentLocation;
use app\models\UserInDocument;
use app\modules\user\models\User;
use Yii;
use app\modules\admin\components\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\rbac\AuthManager;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;

class DocumentController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className(),
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['sync'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
                    [
                        'actions' => ['sync'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action) {
                            return true; //Yii::$app->user->can(AuthManager::ROLE_ADMIN);
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new Document();
        // Check if there is an Editable ajax request
        if (Yii::$app->request->post('hasEditable')) {
        }
        $params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
        $dataProvider = $searchModel->searchSend(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => $params
        ]);
    }

    public function actionCreate()
    {
        $model = new Document();
        //$model->date_suggest = DateTimeHelper::getDateTime();
        $model->status = 0; //  sau khi thêm thì status = 0 = chờ tiếp nhận , 1 = đã tiếp nhận, 2 = Đã duyệt, 3 = Không tiếp nhận
        $model->created_date = DateTimeHelper::getDateTime();
        if ($post = Yii::$app->request->post()) {
            $model->load($post);
            if ($model->validate()) {
                $model->date_from_car = DateTimeHelper::getDateTime($model->date_from_car, 'Y-m-d h:i');
                $model->date_to_car = DateTimeHelper::getDateTime($model->date_from_car, 'Y-m-d h:i');
                $model->date_from_user = DateTimeHelper::getDateTime($model->date_from_user, 'Y-m-d h:i');
                $model->date_to_user = DateTimeHelper::getDateTime($model->date_to_user, 'Y-m-d h:i');
                $model->date_from = DateTimeHelper::getDateTime($model->date_from, 'Y-m-d h:i');
                $model->date_to = DateTimeHelper::getDateTime($model->date_to, 'Y-m-d h:i');
                $model->insert();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Thêm thành công'));
                return $this->redirect(Url::to(['/admin/document/index']));
            }
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        /** @var  $model Document */
        $model = Document::findOne($id);
        $model->created_date = DateTimeHelper::getDateTime();
        if (!$model) throw new NotFoundHttpException();

        if ($post = Yii::$app->request->post()) {
            $model->load($post);
            $model->date_from_car = DateTimeHelper::getDateTime($model->date_from_car, 'Y-m-d h:i');
            $model->date_to_car = DateTimeHelper::getDateTime($model->date_from_car, 'Y-m-d h:i');
            $model->date_from_user = DateTimeHelper::getDateTime($model->date_from_user, 'Y-m-d h:i');
            $model->date_to_user = DateTimeHelper::getDateTime($model->date_to_user, 'Y-m-d h:i');
            $model->date_from = DateTimeHelper::getDateTime($model->date_from, 'Y-m-d h:i');
            $model->date_to = DateTimeHelper::getDateTime($model->date_to, 'Y-m-d h:i');
            if ($model->validate()) {
                $model->update();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Cập nhật thành công'));
                return $this->redirect(Url::to(['/admin/document/index']));
            }
        }

        return $this->render('update', [
            'model' => $model
        ]);

    }

    public function actionDelete($id)
    {
        /** var $model Document **/
        $model = Document::findOne($id);
        if ($model) {
            $model->delete();
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Xóa thành công'));
            return $this->redirect(Url::to(['/admin/document/index']));
        }
    }

    public function actionApprove($id)
    {
        /** var $model Document **/
        $model = Document::findOne($id);
        if ($model) {
            $model->status = 2;// duyệt yêu cầu
            $model->update();
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Duyệt thành công'));
            //return $this->redirect(Url::to(['/admin/document/index']));
        }
    }

    public function saveDocumentHistory($document_id = null, $note, $user_id){
        /** @var   $documentHis  DocumentHistory*/
        $documentHis = new DocumentHistory();
        $documentHis->document_id = $document_id;
        $documentHis->created_date = DateTimeHelper::getDateTime('now', 'Y-m-d H:i');
        $documentHis->note = $note;
        $documentHis->user_id = $user_id;
        $documentHis->insert();
    }

    public function saveCarInDocument($car_id, $document_id, $date_start, $date_end, $status, $user_approved_id){
        $car_approved = new CarApproved();
        $car_approved->car_id = $car_id;
        $car_approved->document_id = $document_id;
        $car_approved->date_from = DateTimeHelper::getDateTime($date_start, 'Y-m-d H:i');
        $car_approved->date_to = DateTimeHelper::getDateTime($date_end, 'Y-m-d H:i');
        $car_approved->status = $status;
        $car_approved->user_approve_id = $user_approved_id;
        $car_approved->insert();

    }


    public function actionCancel($id)
    {
        $user = Yii::$app->controller->user;
        /** var $model Document **/
        $model = Document::findOne($id);
        if ($model) {
            $model->status = 3;// không duyệt(hủy)
            $model->update();
            $this->saveDocumentHistory($model->id, "Từ chối duyệt hồ sơ thành công", $user->id);
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Từ chối duyệt thành công'));
            return $this->redirect(Url::to(['/admin/document/index']));
        }
    }

    public function actionGetDocument()
    {
        $data = [];
        if ($post = Yii::$app->request->post()) {
            /** @var  $document Document */
            $model = Document::findOne($post['id']);
            if ($model) {
                $data_user_in_doc = $model->userInDoc;
                foreach($data_user_in_doc as $item){
                    $data_user[] = [
                        'id'=>$item->id,
                        'user_name'=> $item->user_name,
                        'document_id'=>$item->document_id,
                        'department'=>$item->department,
                        'position'=>$item->position,
                        'email' => $item->email,
                        'phone' => $item->phone
                    ];
                }

                $data = [
                    'id' => $model->id,
                    'name' => $model->name, // tên đơn
                    'code' => $model->code, //mã hồ sơ
                    'full_name' => $model->full_name, // Tên lãnh đạo
                    'department' => $model->department->name, // tên đơn vị
                    'from' => $model->city->name, // địa điểm theo tỉnh
                    'from_name' => $model->from_name, // tên tỉnh thành phố
                    'number_user' => $model->number_user, // Số lượng người đi công tác
                    'date_from' => !empty($model->date_from) ? DateTimeHelper::getDateTime($model->date_from, 'H:i d/m/Y') : '', // thời gian đi công tác từ
                    'date_to' => !empty($model->date_to) ? DateTimeHelper::getDateTime($model->date_to, 'H:i d/m/Y') : '', /// Thời gian đi công tác đến ngày'
                    'date_from_car' => !empty($model->date_from_car) ? DateTimeHelper::getDateTime($model->date_from_car, 'H:i d/m/Y') : '', // Thời gian nhận xe
                    'date_to_car' => !empty($model->date_to_car) ? DateTimeHelper::getDateTime($model->date_to_car, 'H:i d/m/Y') : '', // Thời gian trả xe
                    'purpose_car' => $model->purpose_car, // mục đích sử dụng xe
                    'number_car' => $model->number_car, //số người đi công tác
                    'date_from_user' => !empty($model->date_from_user) ? DateTimeHelper::getDateTime($model->date_from_user, 'H:i d/m/Y') : '', //thời gian đón cán bộ
                    'date_to_user' => !empty($model->date_to_user) ? DateTimeHelper::getDateTime($model->date_to_user, 'H:i d/m/Y') : '', // thời gian trả cán bộ
                    'place_user' => $model->place_user,  // Địa điểm đón cán bộ
                    'note_document' => $model->note_document, // Ghi chú
                    'created_date' => !empty($model->created_date) ? DateTimeHelper::getDateTime($model->created_date, 'H:i d/m/Y') : '',
                    'data_user' => $data_user
                ];
            }
        }

        echo json_encode($data);
    }

    public function actionGetDocumentApprove()
    {
        $data = [];
        $data_user = [];
        if ($post = Yii::$app->request->post()) {
            /** @var  $document Document */
            $model = Document::findOne($post['id']);
            $cars = Car::find()->where(['status' => 1])->all();
            $drivers = User::find()->where(['is_driver' => 1, 'status' => 1])->all();
            $data_car = [];
            $data_driver = [];
            foreach ($cars as $item) {
                 $data_car[$item->id] = [
                     'id' => $item->id,
                     'name' => $item->upholsterer .' / '.$item->name.' / '.$item->code,
                     'status' => 1
                 ];
            }
            foreach($drivers as $item){
                $data_driver[$item->id] = $item->full_name . ' / '. $item->phone;
            }
            if ($model) {

                $data_user_in_doc = $model->userInDoc;
                foreach($data_user_in_doc as $item){
                    $data_user[] = [
                        'id'=>$item->id,
                        'user_name'=> $item->user_name,
                        'document_id'=>$item->document_id,
                        'department'=>$item->department,
                        'position'=>$item->position,
                        'email' => $item->email,
                        'phone' => $item->phone
                    ];
                }

                $data = [
                    'id' => $model->id,
                    'name' => $model->name, // tên đơn
                    'code' => $model->code, //mã hồ sơ
                    'full_name' => $model->full_name, // Tên lãnh đạo
                    'department' => $model->department->name, // tên đơn vị
                    'from' => $model->city->name, // địa điểm theo tỉnh
                    'from_name' => $model->from_name, // tên tỉnh thành phố
                    'number_user' => $model->number_user, // Số lượng người đi công tác
                    'date_from' => !empty($model->date_from) ? DateTimeHelper::getDateTime($model->date_from, 'H:i d/m/Y') : '', // thời gian đi công tác từ
                    'date_to' => !empty($model->date_to) ? DateTimeHelper::getDateTime($model->date_to, 'H:i d/m/Y') : '', /// Thời gian đi công tác đến ngày'
                    'date_from_car' => !empty($model->date_from_car) ? DateTimeHelper::getDateTime($model->date_from_car, 'H:i d/m/Y') : '', // Thời gian nhận xe
                    'date_to_car' => !empty($model->date_to_car) ? DateTimeHelper::getDateTime($model->date_to_car, 'H:i d/m/Y') : '', // Thời gian trả xe
                    'purpose_car' => $model->purpose_car, // mục đích sử dụng xe
                    'number_car' => $model->number_car, //số người đi công tác
                    'date_from_user' => !empty($model->date_from_user) ? DateTimeHelper::getDateTime($model->date_from_user, 'H:i d/m/Y') : '', //thời gian đón cán bộ
                    'date_to_user' => !empty($model->date_to_user) ? DateTimeHelper::getDateTime($model->date_to_user, 'H:i d/m/Y') : '', // thời gian trả cán bộ
                    'place_user' => $model->place_user,  // Địa điểm đón cán bộ
                    'note_document' => $model->note_document, // Ghi chú
                    'created_date' => !empty($model->created_date) ? DateTimeHelper::getDateTime($model->created_date, 'H:i d/m/Y') : '',
                    'lenght' => $model->lenght, //số km
                    'data_car' => $data_car,
                    'data_driver' => $data_driver,
                    'data_user' => $data_user
                ];
            }
        }

        echo json_encode($data);
    }

//    private function getListCar2($count_user_in_doc = 0 ){
//        $list_xe = Car::find()->all();
//        $list_xe_in_doc = CarInDocument::find()->all();
//
//        foreach($list_xe as $item){
//
//        }
//    }

    private function getListCar($count_user_in_doc = 0){
        $list_xe = Car::find()->all();
        $list_xe_show = null;

        if(!empty($list_xe)){
            ////Lấy danh sách xe thuộc hồ sơ
            //$carInDocuments = CarInDocument::find()->where(['status' => 0])->all(); // status = 0 tức là không bận.
            ////Lấy danh sách xe không thuộc hồ sơ trong bảng xe hồ sơ
            $data_list_car_id = [];
            foreach($list_xe as $item){
                array_push($data_list_car_id,$item->id);
            }
            $list_xe_trong = CarInDocument::find()
                ->where(['not in','car_id', $data_list_car_id])
                ->all();
                //Nếu có
                if (!empty($list_xe_trong))
                {

                }

                //Tìm xe trống trong khoảng giờ

                //lặp trong danh sách xe

            return $list_xe_show;
        }
    }

    public function actionGetCarDriver()
    {
        $data = [];
        if ($post = Yii::$app->request->post()) {
            /** @var  $document Car */
            $data_car = [];
            $data_driver = [];
            $cars = null;
            $drivers = null;
            $cache = Yii::$app->cache;
            if(!empty($cache->get('list_car'))){
                $data_car = $cache->get('list_car');
            }else{
                $cars = Car::find()->where(['status' => 1])->all();
                $cache->set('list_car', $cars);
            }

            if(!empty($cache->get('list_driver'))){
                $data_car = $cache->get('list_driver');
            }else{
                $drivers = User::find()->where(['is_driver' => 1, 'status' => 1])->all();
                $cache->set('list_driver', $drivers);
            }

            if (!empty($cars) && !empty($drivers)) {
                foreach ($cars as $item) {
                    $data_car[$item->id] = $item->upholsterer .' / '.$item->name.' / '.$item->code;
                }
                foreach($drivers as $item){
                    $data_driver[$item->id] = $item->full_name . ' / '. $item->phone;
                }
            }
            $data = [
                'data_car' => $data_car,
                'data_driver' => $data_driver,
            ];
        }
        echo json_encode($data);
    }

    public function actionGetDocumentUpdate()
    {
        $data = [];

        if ($post = Yii::$app->request->post()) {
            /** @var  $document Document */
            if ($post['is_update'] == 0) {
                $model_update = Document::findOne($post['id']);
                if ($model_update) {
                    $data = [
                        'id' => $model_update->id,
                        'name' => $model_update->name, // tên đơn
                        'code' => $model_update->code, //mã hồ sơ
                        'full_name' => $model_update->full_name, // Tên lãnh đạo
                        'department' => $model_update->department_id, // tên đơn vị
                        'from' => $model_update->from, // địa điểm theo tỉnh
                        'from_name' => $model_update->from_name, // tên tỉnh thành phố
                        'number_user' => $model_update->number_user, // Số lượng người đi công tác
                        'date_from' => !empty($model_update->date_from) ? DateTimeHelper::getDateTime($model_update->date_from, 'H:i d/m/Y') : '', // thời gian đi công tác từ
                        'date_to' => !empty($model_update->date_to) ? DateTimeHelper::getDateTime($model_update->date_to, 'H:i d/m/Y') : '', /// Thời gian đi công tác đến ngày'
                        'date_from_car' => !empty($model_update->date_from_car) ? DateTimeHelper::getDateTime($model_update->date_from_car, 'H:i d/m/Y') : '', // Thời gian nhận xe
                        'date_to_car' => !empty($model_update->date_to_car) ? DateTimeHelper::getDateTime($model_update->date_to_car, 'H:i d/m/Y') : '', // Thời gian trả xe
                        'purpose_car' => $model_update->purpose_car, // mục đích sử dụng xe
                        'number_car' => $model_update->number_car, //số người đi công tác
                        'date_from_user' => !empty($model_update->date_from_user) ? DateTimeHelper::getDateTime($model_update->date_from_user, 'H:i d/m/Y') : '', //thời gian đón cán bộ
                        'date_to_user' => !empty($model_update->date_to_user) ? DateTimeHelper::getDateTime($model_update->date_to_user, 'H:i d/m/Y') : '', // thời gian trả cán bộ
                        'place_user' => $model_update->place_user,  // Địa điểm đón cán bộ
                        'note_document' => $model_update->note_document, // Ghi chú
                        'created_date' => !empty($model_update->created_date) ? DateTimeHelper::getDateTime($model_update->created_date, 'H:i d/m/Y') : '',
                        'lenght' => $model_update->lenght,
                    ];
                }
            } elseif ($post['is_update'] == 1) {
                /** @var  $document Document */
                $model_update = new Document();
                $model_update = Document::findOne($post['id']);
                $model_update->name = $post['name']; // tên đơn
                $model_update->full_name = $post['full_name'];
                $model_update->department_id = $post['department_id'];
                $model_update->from = $post['from'];
                $model_update->from_name = $post['from_name'];
                $model_update->number_user = $post['number_user'];
                $model_update->date_from = DateTimeHelper::getDateTime($post['date_from'], 'Y-m-d h:i');
                $model_update->date_to = DateTimeHelper::getDateTime($post['date_to'], 'Y-m-d h:i');
                $model_update->date_from_car = DateTimeHelper::getDateTime($post['date_from_car'], 'Y-m-d h:i');
                $model_update->date_to_car = DateTimeHelper::getDateTime($post['date_to_car'], 'Y-m-d h:i');
                $model_update->purpose_car = $post['purpose_car'];
                $model_update->number_car = $post['number_car'];
                $model_update->date_from_user = DateTimeHelper::getDateTime($post['date_from_user'], 'Y-m-d h:i');
                $model_update->date_to_user = DateTimeHelper::getDateTime($post['date_to_user'], 'Y-m-d h:i');
                $model_update->place_user = $post['place_user'];
                $model_update->note_document = $post['note_document'];
                $model_update->created_date = DateTimeHelper::getDateTime('now', 'Y-m-d h:i');
                $model_update->update();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Duyệt hồ sơ thành công'));
                return $this->redirect(Url::toRoute(['/admin/document']));
            }
        }
        echo json_encode($data);
    }

    public function actionApproveDocument(){
        if($post = Yii::$app->request->post()){
            $user = Yii::$app->controller->user;
            $model = new Document();
            $id = $post['id'];
            $is_approve = $post['is_approve'];
            $note_approve = $post['note_approve'];
            $model = Document::findOne($id);
            if(!empty($model)){
                if($is_approve == 'approved'){
                    $model->list_car = $post['list_car'];
                    $model->list_driver = $post['list_driver'];
                    $model->approve_note = $note_approve;
                    $model->approve_date = DateTimeHelper::getDateTime('now', 'Y-m-d h:i');
                    $model->status = 2; // Đã duyệt
                    if($user){
                        $model->user_id_approve = $user->id;
                    }
                    $model->update();
                    $this->saveDocumentHistory($model->id, 'Duyệt hồ sơ thành công', $user->id);
                    $model->insertCarInDocument();

                    $list_user_in_doc = UserIndocument::find()->where(['document_id' => $model->id])->all();
                    if(!empty($list_user_in_doc)) {
                        foreach ($list_user_in_doc as $item) {
                            Yii::$app->mailer->compose('addEmailSuccessful', ['full_name' => $item->user_name, 'document' => $model, 'list_user' => $list_user_in_doc])
                                ->setFrom([
                                    'duytb1404@gmail.com' => Yii::$app->name
                                ])
                                ->setTo($item->email)
                                ->setSubject('Thông báo về việc đi công tác')
                                ->send();
                        }

                        $driver_in_doc = CarInDocument::find()->where(['document_id' => $model->id])->all();
                        if(!empty($driver_in_doc)) {
                            foreach ($driver_in_doc as $item) {
                                Yii::$app->mailer->compose('addEmailDriverSuccessful', [
                                    'full_name' => $item->user->full_name,
                                    'document' => $model,
                                    'car' => $item->car,
                                    'list_user' => $list_user_in_doc
                                ])
                                    ->setFrom([
                                        'duytb1404@gmail.com' => Yii::$app->name
                                    ])
                                    ->setTo($item->user->email)
                                    ->setSubject('Thông báo về việc nhận xe đưa các cán bộ đi công tác')
                                    ->send();
                            }
                            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Gửi lái xe thành công'));
                        }
                    }

                    Yii::$app->mailer->compose('addEmailSuccessful', ['full_name' => $model->user->full_name, 'document' => $model, 'list_user' => $list_user_in_doc])
                        ->setFrom([
                            'duytb1404@gmail.com' => Yii::$app->name
                        ])
                        ->setTo($model->user->email)
                        ->setSubject('Thông báo về việc đi công tác')
                        ->send();


                }else if($is_approve == 'not_approved'){
                    $model->approve_note = $note_approve;
                    $model->approve_date = DateTimeHelper::getDateTime('now', 'Y-m-d h:i');
                    $model->status = 3; // Từ chối
                    if($user){
                        $model->user_id_approve = $user->id;
                    }
                    $model->update();
                    $this->saveDocumentHistory($model->id, 'Từ chối duyệt hồ sơ thành công', $user->id);
                }

                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Duyệt hồ sơ thành công'));
                return $this->redirect(Url::toRoute(['/admin/document']));
            }else{
                $this->saveDocumentHistory($model->id, 'Duyệt hồ sơ không thành công', $user->id);
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Duyệt hồ sơ không thành công'));
                return $this->redirect(Url::toRoute(['/admin/document']));
            }
        }
    }

    public function actionSuggest($id){
        $model = Document::findOne($id);
        $list_car = CarInDocument::find()->where(['document_id' => $model->id])->all();
        $list_user = UserInDocument::find()->where(['document_id' => $model->id])->all();
        $count = count($list_car);
        $str_car_code = "";
        foreach($list_car as $key => $item){
            if($key < $count - 1){
                $str_car_code .= $item->car->code.', ';
            }else{
                $str_car_code .= $item->car->code.'.';
            }
        }

        $list_address = DocumentLocation::find()->where(['document_id' => $id])->orderBy(['index' => SORT_ASC])->all();
        $count_list = count($list_address);
        $str_address = "";
        foreach($list_address as $key => $item){
            if($key < $count_list - 1){
                $str_address .= $item->district->name . ", ";
            }else{
                $str_address .= $item->district->name;
            }
        }

        return $this->render('suggest',
            [
                'model'=>$model,
                'list_car_code' => $str_car_code,
                'list_car'=>$list_car,
                'list_user'=>$list_user,
                'list_address' => $str_address
            ]
        );
    }

}
