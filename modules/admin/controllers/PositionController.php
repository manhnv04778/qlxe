<?php

namespace app\modules\admin\controllers;


use app\models\Department;
use app\models\Position;
use app\models\Role;
use app\modules\admin\components\Controller;
use app\modules\user\models\elasticsearch\UserElasticsearch;
use app\modules\user\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

class PositionController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['sync'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
//                    [
//                        'actions' => ['register'],
//                        'allow' => true,
//                        'roles' => ['?'], // chỉ dành cho user chưa đăng nhập
//                    ],
//                    [
//                        'actions' => ['create', 'update'],
//                        'allow' => true,
//                        'roles' => ['@'], // chỉ dành cho user đã đăng nhậpdeny. nếu chưa đăng nhập thì redirect sang trang login.
//                    ],
                    [
                        'actions' => ['sync'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action){
                            return true; //Yii::$app->user->can(AuthManager::ROLE_ADMIN);
                        }
                    ],
//                    [
//                        'actions' => ['create', 'index', 'update'],
//                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
//                        'matchCallback' => function ($rule, $action){
//                            return Yii::$app->user->can(AuthManager::ROLE_AUTHOR);
//                        }
//                    ],
                ],
            ],
        ];
    }

    public function actionIndex(){
        $searchModel = new Position();
        // Check if there is an Editable ajax request
        if(Yii::$app->request->post('hasEditable')){
        }
        $params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => $params
        ]);
    }

    public function actionCreate(){
        $model = new Position();
        $model->status = 1;
        if($post = Yii::$app->request->post()){
            $model->load($post);
            if($model->validate()){
                $model->insert();
                $dataProvider = $model->search(Yii::$app->request->queryParams);
                return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $model]);
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id){
        $model = Position::findOne($id);
        if($post = Yii::$app->request->post()) {
            if (!empty($model)) {
                $model->load($post);
                if ($model->validate()) {
                    $model->update();
                    $dataProvider = $model->search(Yii::$app->request->queryParams);
                    return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $model]);
                }
            }
        }
        return $this->render('update', ['model' => $model]);
    }

}
