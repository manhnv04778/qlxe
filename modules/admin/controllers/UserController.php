<?php

namespace app\modules\admin\controllers;


use app\helpers\DateTimeHelper;
use app\modules\admin\components\Controller;
use app\modules\user\models\elasticsearch\UserElasticsearch;
use app\modules\user\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

class UserController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['sync'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
//                    [
//                        'actions' => ['register'],
//                        'allow' => true,
//                        'roles' => ['?'], // chỉ dành cho user chưa đăng nhập
//                    ],
//                    [
//                        'actions' => ['create', 'update'],
//                        'allow' => true,
//                        'roles' => ['@'], // chỉ dành cho user đã đăng nhậpdeny. nếu chưa đăng nhập thì redirect sang trang login.
//                    ],
                    [
                        'actions' => ['sync'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action){
                            return true; //Yii::$app->user->can(AuthManager::ROLE_ADMIN);
                        }
                    ],
//                    [
//                        'actions' => ['create', 'index', 'update'],
//                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
//                        'matchCallback' => function ($rule, $action){
//                            return Yii::$app->user->can(AuthManager::ROLE_AUTHOR);
//                        }
//                    ],
                ],
            ],
        ];
    }
    public function actionIndex(){
        $searchModel = new User();
        //echo '<pre>'; print_r(Yii::$app->request->queryParams); echo '</pre>';die;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       return  $this->render('index',[
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider
            ]);
    }

    public function actionCreate(){
        $model = new User();
        $model->created = DateTimeHelper::getDateTime();
        $model->modify = DateTimeHelper::getDateTime();
        $model->status = 1;
        if($post = Yii::$app->request->post()){
            $model->creat_id = 1;
            $model->load($post);
            if($model->validate()){
                $model->birthday = DateTimeHelper::getDateTime($model->birthday,'Y-m-d h:i');
                $model->access_token = Yii::$app->security->generateRandomString();
                $model->password = Yii::$app->security->generatePasswordHash($model->password);
                $model->insert();
                return $this->redirect(['/admin/user/index']);
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id){
        $model = User::findOne($id);
        $model->modify = DateTimeHelper::getDateTime();
        $model->status = 1;
        if($post = Yii::$app->request->post()){
            $model->creat_id = 1;
            $model->load($post);
            if($model->validate()){
                $model->birthday = DateTimeHelper::getDateTime($model->birthday,'Y-m-d h:i');
                $model->token_key = Yii::$app->security->generateRandomString();
                $model->password = Yii::$app->security->generatePasswordHash($model->password);
                $model->update();
                return $this->render('index');
            }else{
                echo '<pre>'; print_r($model->errors); echo '</pre>';die;
            }
        }
       return $this->render('update', ['model' => $model]);
    }

    public function actionLock($id){
        $model = User::findOne($id);
        if(!empty($model)){
            $model->is_lock = 1;
            $model->update();
        }
        return $this->render('index');
    }

}
