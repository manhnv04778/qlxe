<?php

namespace app\modules\admin\controllers;


use app\helpers\DateTimeHelper;
use app\models\Car;
use app\models\Color;
use app\models\InfoFooter;
use app\models\Speed;
use app\models\Type;
use app\models\TypeCarUse;
use app\models\Upholsterer;
use Yii;
use app\modules\admin\components\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\rbac\AuthManager;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;

class CarController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className(),
            ],
        ];
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['sync'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
                    [
                        'actions' => ['sync'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action){
                            return true; //Yii::$app->user->can(AuthManager::ROLE_ADMIN);
                        }
                    ],
                ],
            ],
        ];
    }


    //================= Index =====================//
    public function actionIndex()
    {
        $searchModel = new Car();
        // Check if there is an Editable ajax request
        if(Yii::$app->request->post('hasEditable')){
        }

        $params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('car/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => $params
        ]);
    }


    //Thêm xe mới
    public function actionCreate()
    {
        $model = new Car();

        if($post = Yii::$app->request->post()){
            $model->load($post);
            if($model->validate()){
                $model->insert();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Thêm thành công xe'));
                return $this->redirect(Url::to(['/admin/car/index']));
            }
        }
        return $this->render('car/create',[
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        /** @var  $model Car*/
        $model = Car::findOne($id);
        if(!$model) throw new NotFoundHttpException();

        if($post = Yii::$app->request->post())
        {
            $model->load($post);
            if($model->validate()){
                $model->update();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Cập nhật thành công thông tin xe'));
                return $this->redirect(Url::to(['/admin/car/index']));
            }
        }

        return $this->render('car/update', [
            'model' => $model
        ]);

    }

    public function actionDelete($id){
        /** var $model Car **/
        $model = Car::findOne($id);
        if($model){
            $model->delete();
            return $this->redirect(Url::to(['/admin/car/index']));
        }
    }

    public function actionGetCar(){
        $data = [];
        if($post = Yii::$app->request->post()){
            /** @var  $car Car*/
            $car = Car::findOne($post['id']);
            if($car){
                $data = [
                    'name' => $car->name,
                    'code' => $car->code,
                    'color' => $car->color->name,
                    'upholsterer' => $car->upholsterer->name,
                    'speed' => $car->speed->name,
                    'type' => $car->type->name,
                    'country' => $car->country->name,
                    'manufacture' => $car->manufacture->name,
                    'time_start' => DateTimeHelper::getDateTime($car->time_start,'d-m-Y'),
                    'time_end' => DateTimeHelper::getDateTime($car->time_end, 'd-m-Y'),
                ];
            }
        }

        echo json_encode($data);
    }



}
