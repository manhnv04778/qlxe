<?php

namespace app\modules\admin\controllers;

use app\components\rbac\AuthManager;
use app\modules\admin\models\forms\AuthForm;
use app\modules\user\models\elasticsearch\UserElasticsearch;
use app\modules\user\models\User;
use stdClass;
use Yii;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\StringHelper;

class AuthController extends \app\modules\admin\components\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create','add-user'],
                'rules' => [
                    [
                        'actions' => ['add-user'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action){
                            return Yii::$app->authManager->checkAccess($this->user->id, AuthManager::ROLE_ADMIN);
                        }
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action){
                            if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isSuper) return true;
                            else return false;
                        }
                    ],
                ],
            ],
        ];
    }



    public function init(){
        parent::init();
    }


    /**
     * list location
     *
     * http://local.tasker.com/admin/auth/create
     * @param int $page
     * @return string
     */
    public function actionCreate()
    {
        $data = AuthManager::getTreeItems();
        $authForm = new AuthForm();
        $authForm->scenario = 'create';
        $auth = Yii::$app->authManager;

        if($post = Yii::$app->request->post()){

            $authForm->load($post);
            if($authForm->validate()){
                $query = (new Query)
                    ->from($auth->itemTable)
                    ->where(['name' => $authForm->name]);
                $queryResult = $query->all($auth->db);
                $item = null;
                if(!$queryResult){
                    if($authForm->type == 1)
                        $item = $auth->createRole($authForm->name);
                    else
                        $item = $auth->createPermission($authForm->name);

                    $item->description = $authForm->description;
                    $auth->add($item);
                }else{
                    $item = new stdClass;
                    $item->name = $authForm->name;
                }
                if(!empty($authForm->parent)){
                    foreach($authForm->parent as $parent){
                        $query = (new Query)
                            ->from($auth->itemChildTable)
                            ->where([
                                'child' => $item->name,
                                'parent' => $parent
                            ]);
                        $queryResult = $query->all($auth->db);
                        if(!$queryResult){
                            //$auth->addChild($parent, $item);
                            $auth->db->createCommand()
                                ->insert($auth->itemChildTable, ['parent' => $parent, 'child' => $item->name])
                                ->execute();
                        }
                    }
                }
            }
            $this->refresh();
        }

        return $this->render('create', [
            'data' => $data,
            'authForm' => $authForm
        ]);

    }

    public function actionAddUser()
    {
        $data = AuthManager::getTreeItems();

        $authForm = new AuthForm();
        $authForm->scenario = 'addUser';

        $auth = Yii::$app->authManager;

        $query = (new Query)
            ->from($auth->assignmentTable);
        $listAssign = $query->all($auth->db);

        if($post = Yii::$app->request->post()){
            $authForm->load($post);
            if($authForm->validate()){
               if(!empty($authForm->parent)){
                   foreach($authForm->parent as $parent){
                       $query = (new Query)
                           ->from($auth->assignmentTable)
                           ->where([
                               'item_name' => $parent,
                               'user_id' => $authForm->user_id
                           ]);
                       $queryResult = $query->all($auth->db);

                       if(!$queryResult){
                           $role = new stdClass();
                           $role->name = $parent;
                           $auth->assign($role,$authForm->user_id);
                       }
                   }
               }
            }else{
                echo '<pre>'; print_r($authForm->errors); echo '</pre>';die;
            }
            $this->refresh();
        }

        return $this->render('add_user', [
            'data' => $data,
            'authForm' => $authForm,
            'listAssign' => $listAssign
        ]);

    }
}
