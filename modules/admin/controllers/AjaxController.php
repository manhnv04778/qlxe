<?php

namespace app\modules\admin\controllers;


use app\models\District;
use app\models\InfoFooter;
use Yii;
use app\modules\admin\components\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\rbac\AuthManager;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\ErrorAction;

class AjaxController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className(),
            ],
        ];
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['sync'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
                    [
                        'actions' => ['sync'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action){
                            return true; //Yii::$app->user->can(AuthManager::ROLE_ADMIN);
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionAutocomplete()
    {
        $q = Yii::$app->request->getQueryParam('q');
        $user = District::find()
            ->where(['like', 'name_unicode', $q])
            ->with(['city' => function($query){
                $query->select(['id','name']);
            }])
            ->orderBy(['city_id' => SORT_DESC])
            ->all();
        $data = [];
        if($user){

            foreach ($user as $item) {
                $data[] = [
                    "id" => $item->id,
                    "key"  => $item->user_name,
                    "text" => $item->user_name,
                ];
            }
        }

        echo json_encode($data);
    }
}
