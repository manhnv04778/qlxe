<?php

namespace app\modules\admin\controllers;


use app\models\Color;
use app\models\InfoFooter;
use app\models\Speed;
use app\models\Type;
use app\models\TypeCarUse;
use app\models\Upholsterer;
use Yii;
use app\modules\admin\components\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\rbac\AuthManager;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;

class CarInfoController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className(),
            ],
        ];
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['sync'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
                    [
                        'actions' => ['sync'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action){
                            return true; //Yii::$app->user->can(AuthManager::ROLE_ADMIN);
                        }
                    ],
                ],
            ],
        ];
    }


    //================= SPEED =====================//
    public function actionIndexSpeed()
    {
        $searchModel = new Speed();
        // Check if there is an Editable ajax request
        if(Yii::$app->request->post('hasEditable')){
        }
        $params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('speed/index_speed', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => $params
        ]);
    }

    public function actionCreateSpeed()
    {
        $model = new Speed();

        if($post = Yii::$app->request->post()){
            $model->load($post);
            if($model->validate()){
                $model->insert();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Thêm thành công dung tích mới'));
                return $this->redirect(Url::to(['/admin/car-info/index-speed']));
            }
        }
        return $this->render('speed/create_speed',[
            'model' => $model
        ]);
    }

    public function actionUpdateSpeed($id)
    {
        /** @var  $model Speed*/
        $model = Speed::findOne($id);
        if(!$model) throw new NotFoundHttpException();

        if($post = Yii::$app->request->post())
        {
            $model->load($post);
            if($model->validate()){
                $model->update();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Cập nhật thành công dung tích'));
                return $this->redirect(Url::to(['/admin/car-info/index-speed']));
            }
        }

        return $this->render('speed/update_speed', [
            'model' => $model
        ]);

    }
    public function actionDeleteSpeed($id){
        /** var $model Speed **/
        $model = Speed::findOne($id);
        if($model){
            $model->delete();
            return $this->redirect(Url::to(['/admin/car-info/index-speed']));
        }
    }


    //================= Type Car Use =====================//
    public function actionIndexUse()
    {
        $searchModel = new TypeCarUse();
        // Check if there is an Editable ajax request
        if(Yii::$app->request->post('hasEditable')){
        }
        $params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('type_use/index_use', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => $params
        ]);
    }

    public function actionCreateUse()
    {
        $model = new TypeCarUse();

        if($post = Yii::$app->request->post()){
            $model->load($post);
            if($model->validate()){
                $model->insert();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Thêm thành công tình trạng sử dụng xe'));
                return $this->redirect(Url::to(['/admin/car-info/index-use']));
            }
        }
        return $this->render('type_use/create_use',[
            'model' => $model
        ]);
    }

    public function actionUpdateUse($id)
    {
        /** @var  $model TypeCarUse*/
        $model = TypeCarUse::findOne($id);
        if(!$model) throw new NotFoundHttpException();

        if($post = Yii::$app->request->post())
        {
            $model->load($post);
            if($model->validate()){
                $model->update();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Cập nhật thành công tình trạng sử dụng xe'));
                return $this->redirect(Url::to(['/admin/car-info/index-use']));
            }
        }

        return $this->render('type_use/update_use', [
            'model' => $model
        ]);

    }
    public function actionDeleteUse($id){
        /** var $model TypeCarUse **/
        $model = TypeCarUse::findOne($id);
        if($model){
            $model->delete();
            return $this->redirect(Url::to(['/admin/car-info/index-use']));
        }
    }


    //Quản lý số chỗ ngồi
    //================= Type Upholsterer =====================//
    public function actionIndexUpholsterer()
    {
        $searchModel = new Upholsterer();
        // Check if there is an Editable ajax request
        if(Yii::$app->request->post('hasEditable')){
        }
        $params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('upholsterer/index_upholsterer', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => $params
        ]);
    }

    public function actionCreateUpholsterer()
    {
        $model = new Upholsterer();

        if($post = Yii::$app->request->post()){
            $model->load($post);
            if($model->validate()){
                $model->insert();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Thêm thành công số chỗ ngồi'));
                return $this->redirect(Url::to(['/admin/car-info/index-upholsterer']));
            }
        }
        return $this->render('upholsterer/create_upholsterer',[
            'model' => $model
        ]);
    }

    public function actionUpdateUpholsterer($id)
    {
        /** @var  $model Upholsterer*/
        $model = Upholsterer::findOne($id);
        if(!$model) throw new NotFoundHttpException();

        if($post = Yii::$app->request->post())
        {
            $model->load($post);
            if($model->validate()){
                $model->update();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Cập nhật thành công số chỗ ngồi'));
                return $this->redirect(Url::to(['/admin/car-info/index-upholsterer']));
            }
        }

        return $this->render('upholsterer/update_upholsterer', [
            'model' => $model
        ]);

    }

    public function actionDeleteUpholsterer($id){
        /** var $model Upholsterer **/
        $model = Upholsterer::findOne($id);
        if($model){
            $model->delete();
            return $this->redirect(Url::to(['/admin/car-info/index-upholsterer']));
        }
    }


    //Quản lý màu sắc
    //================= Color =====================//
    public function actionIndexColor()
    {
        $searchModel = new Color();
        // Check if there is an Editable ajax request
        if(Yii::$app->request->post('hasEditable')){
        }
        $params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('color/index_color', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => $params
        ]);
    }

    public function actionCreateColor()
    {
        $model = new Color();

        if($post = Yii::$app->request->post()){
            $model->load($post);
            if($model->validate()){
                $model->insert();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Thêm thành công màu mới'));
                return $this->redirect(Url::to(['/admin/car-info/index-color']));
            }
        }
        return $this->render('color/create_color',[
            'model' => $model
        ]);
    }

    public function actionUpdateColor($id)
    {
        /** @var  $model Color*/
        $model = Color::findOne($id);
        if(!$model) throw new NotFoundHttpException();

        if($post = Yii::$app->request->post())
        {
            $model->load($post);
            if($model->validate()){
                $model->update();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Cập nhật thành công màu xe'));
                return $this->redirect(Url::to(['/admin/car-info/index-color']));
            }
        }

        return $this->render('color/update_color', [
            'model' => $model
        ]);

    }

    public function actionDeleteColor($id){
        /** var $model Color **/
        $model = Color::findOne($id);
        if($model){
            $model->delete();
            return $this->redirect(Url::to(['/admin/car-info/index-color']));
        }
    }

    //Quản lý Hình thức sử dụng
    //================= Type =====================//
    public function actionIndexType()
    {
        $searchModel = new Type();
        // Check if there is an Editable ajax request
        if(Yii::$app->request->post('hasEditable')){
        }
        $params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('type/index_type', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => $params
        ]);
    }

    public function actionCreateType()
    {
        $model = new Type();

        if($post = Yii::$app->request->post()){
            $model->load($post);
            if($model->validate()){
                $model->insert();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Thêm thành công hình thức sử dụng mới'));
                return $this->redirect(Url::to(['/admin/car-info/index-type']));
            }
        }
        return $this->render('type/create_type',[
            'model' => $model
        ]);
    }

    public function actionUpdateType($id)
    {
        /** @var  $model Type*/
        $model = Type::findOne($id);
        if(!$model) throw new NotFoundHttpException();

        if($post = Yii::$app->request->post())
        {
            $model->load($post);
            if($model->validate()){
                $model->update();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Cập nhật thành công hình thức sử dụng'));
                return $this->redirect(Url::to(['/admin/car-info/index-type']));
            }
        }

        return $this->render('type/update_type', [
            'model' => $model
        ]);

    }

    public function actionDeleteType($id){
        /** var $model Type **/
        $model = Type::findOne($id);
        if($model){
            $model->delete();
            return $this->redirect(Url::to(['/admin/car-info/index-type']));
        }
    }
}
