<?php

namespace app\modules\admin\controllers;


use app\models\Country;
use app\models\InfoFooter;
use app\models\Manufacture;
use app\models\Speed;
use Yii;
use app\modules\admin\components\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\rbac\AuthManager;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;

class CountryController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className(),
            ],
        ];
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['sync'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
                    [
                        'actions' => ['sync'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action){
                            return true; //Yii::$app->user->can(AuthManager::ROLE_ADMIN);
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new Country();
        // Check if there is an Editable ajax request
        if(Yii::$app->request->post('hasEditable')){
        }
        $params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'params' => $params
        ]);
    }

    public function actionCreate()
    {
        $model = new Country();

        if($post = Yii::$app->request->post()){
            $model->load($post);
            if($model->validate()){
                $model->insert();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Thêm thành công dung tích mới'));
                return $this->redirect(Url::to(['/admin/country/index']));
            }
        }
        return $this->render('create',[
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        /** @var  $model Speed*/
        $model = Country::findOne($id);
        if(!$model) throw new NotFoundHttpException();

        if($post = Yii::$app->request->post())
        {
            $model->load($post);
            if($model->validate()){
                $model->update();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Cập nhật thành công dung tích'));
                return $this->redirect(Url::to(['/admin/country/index']));
            }
        }

        return $this->render('update', [
            'model' => $model
        ]);

    }
    public function actionDelete($id){
        /** var $model Speed **/
        $model = Country::findOne($id);
        if($model){
            $model->delete();
            return $this->redirect(Url::to(['/admin/country/index-speed']));
        }
    }
}
