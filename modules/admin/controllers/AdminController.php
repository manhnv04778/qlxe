<?php

namespace app\modules\admin\controllers;


use app\components\Model;
use app\modules\user\models\elasticsearch\UserElasticsearch;
use app\modules\user\models\forms\LoginForm;
use app\modules\user\models\User;
use Curl\Curl;
use Yii;
use app\components\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\rbac\AuthManager;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

class AdminController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'], // chỉ dành cho user chưa đăng nhập
                    ],
                ],
            ],
        ];
    }




    public function actionClear()
    {
        Model::clearCache();

        Yii::$app->session->setFlash('success', Yii::t('app', 'Clear successful'));

        return $this->redirect('/');
    }


    public function actionSetPass($u){

        /** @var  $user User*/
        $user = User::find()->where(['user_name' => $u])->one();

        if($user){

            $user->password = Yii::$app->security->generatePasswordHash(('12345678'));
            $user->update();
        }
    }

    public function actionClearCache()
    {
        if($post = Yii::$app->request->post()){
            $cached = Yii::$app->cache;
            $cached->set($post['cached'],NULL);
        }
        return $this->render('clear_cached');
    }
    /**
     * Trang đăng nhập
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {

        $this->layout = '@theme/layouts/login_admin';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if($post = Yii::$app->request->post()){
            $model->load($post);
            if($model->validate()){

                Yii::$app->user->login($model->getUser(), Yii::$app->params['user']['rememberLogin']);
                //$this->user = Yii::$app->user->identity;
                return $this->redirect(['/admin/site/index']);
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Thoát
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        Yii::$app->session->setFlash('warning', Yii::t('user', 'Đăng xuất thành công'));

        return $this->redirect(['/admin/admin/login']);
    }



}
