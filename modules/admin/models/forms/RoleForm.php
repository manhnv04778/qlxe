<?php
namespace app\modules\admin\models\forms;

use app\models\AuthItem;
use app\modules\user\models\User;
use yii\base\Model;
use Yii;
use yii\helpers\StringHelper;

/**
 * Signup form
 */
class RoleForm extends Model
{
    //public $email;
    public $description;
    public $permission;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'],'required'],
            ['description', 'checkDuplicate', 'on' => 'create'],
            [['permission'], 'safe'],

        ];
    }


    public function attributeLabels()
    {
        return [
            'description' => Yii::t('auth', 'Vai trò'),
            'permission' => Yii::t('auth', 'Quyền của vai trò'),
        ];
    }

    public function checkDuplicate()
    {
        $name = StringHelper::toSEOString($this->description);
        if(AuthItem::find()->where(['name' => $name])->exists()){
            $this->addError('description', Yii::t('user','Vai trò đã tồn tại'));
        }
    }

}
