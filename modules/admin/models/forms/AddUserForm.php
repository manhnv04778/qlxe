<?php
namespace app\modules\admin\models\forms;

use app\models\AuthItem;
use app\modules\user\models\User;
use yii\base\Model;
use Yii;
use yii\helpers\StringHelper;

/**
 * Signup form
 */
class AddUserForm extends Model
{
    //public $email;
    public $user_id;
    public $item_name;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','item_name'],'required'],
            ['user_id', 'checkUser', 'on' => 'create'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('auth', 'User'),
            'item_name' => Yii::t('auth', 'Vai trò'),
        ];
    }
    public function checkUser()
    {

        if(!User::find()->where(['user_name' => $this->user_id])->exists()){
            $this->addError('user_id', Yii::t('user','User không tồn tại'));
        }
    }
}
