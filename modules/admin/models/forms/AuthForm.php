<?php
namespace app\modules\admin\models\forms;

use app\modules\user\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class AuthForm extends Model
{
    //public $email;
    public $username;
    public $user_id;
    public $name;
    public $parent;
    public $type;
    public $description;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','name'],'required', 'on' => 'create'],
            [['username','parent'], 'required', 'on' => 'addUser'],
            [['username'], 'checkUser', 'on' => 'addUser'],
            [['parent'], 'safe'],
            [['user_id'], 'integer'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'username' => Yii::t('auth', 'User Name'),
            'name' => Yii::t('auth', 'Name'),
            'parent' => Yii::t('auth', 'Parent'),
            'type' => Yii::t('auth', 'Type'),

        ];
    }

    public function checkUser(){
        $user = User::find()->where(['username' => $this->username])->one();
        if(!$user){
            $this->addError('username', Yii::t('register', Yii::t('register', 'User không tồn tại')));
        }else{
            $this->user_id = $user->id;
        }

    }

}
