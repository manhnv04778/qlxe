<?php
namespace app\modules\admin\models\forms;

use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class CatGroupForm extends Model
{
    //public $email;
    public $name;
    public $group_id;
    public $lang_id;
    public $parent_id;
    public $position;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id','name'],'required'],
            [['name'], 'string', 'max' => 255],
            [['group_id','lang_id','parent_id','position'], 'integer'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'name' => Yii::t('cat', 'Name'),
            'parent_id' => Yii::t('cat', 'Parent'),
            'group_id' => Yii::t('cat', 'Group'),
            'lang_id' => Yii::t('cat', 'Lang'),
            'position' => Yii::t('cat', 'Position'),

        ];
    }

}
