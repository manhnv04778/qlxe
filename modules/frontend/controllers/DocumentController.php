<?php

namespace app\modules\frontend\controllers;

use app\helpers\DateTimeHelper;
use app\models\Banner;
use app\models\App;
use app\models\Car;
use app\models\CarInDocument;
use app\models\City;
use app\models\District;
use app\models\Document;
use app\models\DocumentHistory;
use app\models\DocumentLocation;
use app\models\forms\UserInDocumentForm;
use app\models\InfoFooter;
use app\models\News;
use app\models\NewsCat;
use app\models\UserInDocument;
use app\modules\frontend\components\Controller;
use app\modules\question\models\Question;
use app\modules\user\models\User;
use app\modules\user\models\UserExtra;
use app\modules\user\models\UserFeed;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;


class DocumentController extends Controller{

	public function behaviors(){
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					//                    'delete' => ['post'],
				],
			],
			// http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['payment'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
				'rules' => [
					//                    [
					//                        'actions' => ['register'],
					//                        'allow' => true,
					//                        'roles' => ['?'], // chỉ dành cho user chưa đăng nhập
					//                    ],
					[
						'actions' => ['payment'],
						'allow' => true,
						'roles' => ['@'], // chỉ dành cho user đã đăng nhập. nếu chưa đăng nhập thì redirect sang trang login.
					],
					//                    [
					//                        'actions' => ['sync'],
					//                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
					//                        'matchCallback' => function ($rule, $action){
					//                            return true; //Yii::$app->user->can(AuthManager::ROLE_ADMIN);
					//                        }
					//                    ],
					//                    [
					//                        'actions' => ['create', 'index', 'update'],
					//                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
					//                        'matchCallback' => function ($rule, $action){
					//                            return Yii::$app->user->can(AuthManager::ROLE_AUTHOR);
					//                        }
					//                    ],
				],
			],
		];
	}

	public function init(){

		parent::init();

	}

	public function actionIndex(){

		$searchModel = new Document();
		$model_update = new Document();
		// Check if there is an Editable ajax request
		if(Yii::$app->request->post('hasEditable')){
		}
		$params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
		$dataProvider = $searchModel->search($params);
		return $this->render('index', [
				'searchModel' => $searchModel,
				'model_update' => $model_update,
				'dataProvider' => $dataProvider,
				'params' => $params
		]);
	}

	public function actionListSend(){
		$searchModel = new Document();
		if(Yii::$app->request->post('hasEditable')){
		}
		$params = !empty(Yii::$app->request->queryParams) ? Yii::$app->request->queryParams : [];
		$dataProvider = $searchModel->searchSend(Yii::$app->request->queryParams);
		return $this->render('list-send', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'params' => $params,
		]);
	}

	public function actionCreate(){
		$user = Yii::$app->controller->user;
		$form_user_doc = new UserInDocumentForm();
		$list_document = Document::find()->all();
		$count_code = count($list_document);
		$count_code++;
		$year = DateTimeHelper::getDateTime('now','Y');
		$model = new Document();
		$model->created_user_id = $user->id;
		$model->code = $year.str_pad($count_code, 4, '0', STR_PAD_LEFT);
		$model->status = 0; //  sau khi thêm thì status = 0 = chờ tiếp nhận , 1 = đã gửi, 2 = Đã duyệt, 3 = Từ chối duyệt, 4 = Hủy
		$model->created_date = DateTimeHelper::getDateTime();
		if($post = Yii::$app->request->post()){
			$model->load($post);
			if($model->validate()){
				$model->date_from_car = DateTimeHelper::getDateTime(str_replace('/','-',$model->date_from_car),'Y-m-d h:i');
				$model->date_to_car = DateTimeHelper::getDateTime(str_replace('/','-',$model->date_to_car),'Y-m-d h:i');
				$model->date_from_user = DateTimeHelper::getDateTime(str_replace('/','-',$model->date_from_user),'Y-m-d h:i');
				$model->date_to_user = DateTimeHelper::getDateTime(str_replace('/','-',$model->date_to_user),'Y-m-d h:i');
				$model->date_from = DateTimeHelper::getDateTime(str_replace('/','-',$model->date_from),'Y-m-d h:i');
				$model->date_to = DateTimeHelper::getDateTime(str_replace('/','-',$model->date_to),'Y-m-d h:i');
				$model->insert();

				//Thêm địa điểm đi công tác
				if(!empty($post['citys'])){
					foreach($post['citys'] as $key => $value){
						$documentLocation = new DocumentLocation();
						$documentLocation->document_id = $model->id;
						$documentLocation->city_id = $value;
						$documentLocation->district_id = $post['districts'][$key];
						$documentLocation->address = $post['address'][$key];
						$documentLocation->index = $key;
						if(!empty($post['long'][$key])) $documentLocation->long = $post['long'][$key];
						if($documentLocation->validate()){
							$documentLocation->insert();
						}
					}
				}

				$this->saveDocumentHistory($model->id,'Tạo hồ sơ thành công',$model->created_user_id);
				if(!empty($post['full_name_user'][0])) {
					$model->full_name_user = $post['full_name_user'];
					$model->department_user = $post['department_user'];
					$model->position_user = $post['position_user'];
					$model->phone_user = $post['phone_user'];
					$model->email_user = $post['email_user'];
				}

				$model->insertUserDocument();
 				Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Tạo hồ sơ thành công'));
				return $this->redirect(Url::to(['/frontend/document/index']));
			}
//			else{
//				echo '<pre>'; print_r($model->errors); echo '</pre>';die;
//			}
		}
		return $this->render('create',[
				'model' => $model,
				'form_user_doc' => $form_user_doc
		]);
	}

	public function saveDocumentHistory($document_id = null, $note, $user_id){
		/** @var   $documentHis  DocumentHistory*/
		$documentHis = new DocumentHistory();

		$documentHis->document_id = $document_id;
		$documentHis->created_date = DateTimeHelper::getDateTime('now', 'Y-m-d H:i');
		$documentHis->note = $note;
		$documentHis->user_id = $user_id;
		$documentHis->insert();
	}

	public function actionUpdate($id)
	{
		/** @var  $model Document*/
		$user = Yii::$app->controller->user;
		$model = Document::findOne($id);
		$model->modify_date = DateTimeHelper::getDateTime();
		$model->modify_user_id = $user->id;
		if(!$model) throw new NotFoundHttpException();

		$documentLocation = DocumentLocation::find()
				->where(['document_id' => $model->id])
				->orderBy(['index' => SORT_ASC])
				->all();

		if($post = Yii::$app->request->post())
		{
			$model->load($post);
			$model->date_from_car = DateTimeHelper::getDateTime($model->date_from_car,'Y-m-d h:i');
			$model->date_to_car = DateTimeHelper::getDateTime($model->date_from_car,'Y-m-d h:i');
			$model->date_from_user = DateTimeHelper::getDateTime($model->date_from_user,'Y-m-d h:i');
			$model->date_to_user = DateTimeHelper::getDateTime($model->date_to_user,'Y-m-d h:i');
			$model->date_from = DateTimeHelper::getDateTime($model->date_from,'Y-m-d h:i');
			$model->date_to = DateTimeHelper::getDateTime($model->date_to,'Y-m-d h:i');
			if($model->validate()){
				$model->update();
				$this->saveDocumentHistory($model->id, 'Cập nhật hồ sơ thành công', $model->modify_user_id);
				Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Cập nhật thành công'));
				return $this->redirect(Url::to(['/frontend/document/index']));
			}
		}

		return $this->render('update', [
			'model' => $model,
			'documentLocation' => $documentLocation
		]);

	}

	public function actionDelete($id){
		/** var $model Document **/
		$user = Yii::$app->controller->user;
		$model = Document::findOne($id);
		if($model){
			$model->status = 4;
			$model->update();
			$this->saveDocumentHistory($model->id, 'Hủy hồ sơ thành công', $user->id);
			Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Hủy thành công'));
			return $this->redirect(Url::to(['/frontend/document/index']));
		}
	}

	public function actionSendDocument(){
		/** var $model Document **/
		$user = Yii::$app->controller->user;
		if($post = Yii::$app->request->post()){
			$model = Document::findOne($post['id']);
			if(!empty($model)){
				if($model->validate()){
					$model->status = 1;
					//$model->created_date = DateTimeHelper::getDateTime();
					$model->update();
					$this->saveDocumentHistory($model->id, 'Gửi hồ sơ thành công', $user->id);
					Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Gửi yêu cầu thành công'));
					return $this->redirect(Url::toRoute(['/frontend/document']));
				}else{
					echo '<pre>'; print_r($model->errors); echo '</pre>';die;
				}
			}
		}
	}

	public function actionGetDocument(){
		$data = [];
		if($post = Yii::$app->request->post()){
			/** @var  $document Document*/
			$model = Document::findOne($post['id']);
			if($model){
				$data_user_in_doc = $model->userInDoc;
				$data_user = [];
				foreach($data_user_in_doc as $item){
					$data_user[] = [
							'id'=>$item->id,
							'user_name'=> $item->user_name,
							'document_id'=>$item->document_id,
							'department'=>$item->department,
							'position'=>$item->position,
							'email' => $item->email,
							'phone' => $item->phone
					];
				}

				$data = [
					'id' => $model->id,
					'name' => $model->name, // tên đơn
					'code' => $model->code, //mã hồ sơ
					'full_name' => $model->full_name, // Tên lãnh đạo
					'department' => $model->department->name, // tên đơn vị
					'from' => '',//$model->city->name, // địa điểm theo tỉnh
					'from_name' => $model->from_name, // tên tỉnh thành phố
					'number_user' => $model->number_user, // Số lượng người đi công tác
					'date_from' =>  !empty($model->date_from) ? DateTimeHelper::getDateTime($model->date_from,'H:i d/m/Y') :  '', // thời gian đi công tác từ
					'date_to' => !empty($model->date_to) ? DateTimeHelper::getDateTime($model->date_to,'H:i d/m/Y') :  '', /// Thời gian đi công tác đến ngày'
					'date_from_car' => !empty($model->date_from_car) ? DateTimeHelper::getDateTime($model->date_from_car,'H:i d/m/Y') :  '', // Thời gian nhận xe
					'date_to_car' => !empty($model->date_to_car) ? DateTimeHelper::getDateTime($model->date_to_car,'H:i d/m/Y') :  '', // Thời gian trả xe
					'purpose_car' => $model->purpose_car, // mục đích sử dụng xe
					'number_car' => $model->number_car, //số người đi công tác
					'date_from_user'=> !empty($model->date_from_user) ? DateTimeHelper::getDateTime($model->date_from_user,'H:i d/m/Y') :  '', //thời gian đón cán bộ
					'date_to_user' => !empty($model->date_to_user) ? DateTimeHelper::getDateTime($model->date_to_user,'H:i d/m/Y') :  '', // thời gian trả cán bộ
					'place_user' => $model->place_user,  // Địa điểm đón cán bộ
					'note_document' => $model->note_document, // Ghi chú
					'created_date' => !empty($model->created_date) ? DateTimeHelper::getDateTime($model->created_date,'H:i d/m/Y') :  '',
					'is_modify' => ($model->status >= 1) ? true : false,
					'data_user' => $data_user
				];
			}
		}

		echo json_encode($data);
	}

	public function actionGetDocumentUpdate(){
		$user = Yii::$app->controller->user;
		$data = [];
		$data_user = [];
		$data_location = [];
		$citys = City::getData();
		$districts = []; //District::getData();

		if($post = Yii::$app->request->post()){
			/** @var  $document Document*/
			if($post['is_update'] == 0) {
				$model_update = Document::findOne($post['id']);
				if ($model_update) {
					$data_user_in_doc = $model_update->userInDoc;
					foreach($data_user_in_doc as $item){
						$data_user[] = [
								'id'=>$item->id,
								'user_name'=> $item->user_name,
								'document_id'=>$item->document_id,
								'department'=>$item->department,
								'position'=>$item->position,
								'email' => $item->email,
								'phone' => $item->phone
						];
					}

					$documentLocation = DocumentLocation::find()
							->where(['document_id' => $model_update->id])
							->orderBy(['index' => SORT_ASC])
							->all();

					foreach($documentLocation as $item){
						$districts = District::getDistrictByCity($item->city_id);
						$data_location[] = [
							'id' => $item->id,
							'city_id' => $item->id,
							'district_id' => $item->district_id,
							'address' => $item->address,
							'long' => $item->long,
							'districts' => $districts
						];
					}

					$data = [
							'id' => $model_update->id,
							'name' => $model_update->name, // tên đơn
							'code' => $model_update->code, //mã hồ sơ
							'full_name' => $model_update->full_name, // Tên lãnh đạo
							'department' => $model_update->department_id, // tên đơn vị
							'from' => $model_update->from, // địa điểm theo tỉnh
							'from_name' => $model_update->from_name, // tên tỉnh thành phố
							'number_user' => $model_update->number_user, // Số lượng người đi công tác
							'date_from' => !empty($model_update->date_from) ? DateTimeHelper::getDateTime($model_update->date_from, 'd/m/Y H:i') : '', // thời gian đi công tác từ
							'date_to' => !empty($model_update->date_to) ? DateTimeHelper::getDateTime($model_update->date_to, 'd/m/Y H:i') : '', /// Thời gian đi công tác đến ngày'
							'date_from_car' => !empty($model_update->date_from_car) ? DateTimeHelper::getDateTime($model_update->date_from_car, 'd/m/Y H:i') : '', // Thời gian nhận xe
							'date_to_car' => !empty($model_update->date_to_car) ? DateTimeHelper::getDateTime($model_update->date_to_car, 'd/m/Y H:i') : '', // Thời gian trả xe
							'purpose_car' => $model_update->purpose_car, // mục đích sử dụng xe
							'number_car' => $model_update->number_car, //số người đi công tác
							'date_from_user' => !empty($model_update->date_from_user) ? DateTimeHelper::getDateTime($model_update->date_from_user, 'd/m/Y H:i') : '', //thời gian đón cán bộ
							'date_to_user' => !empty($model_update->date_to_user) ? DateTimeHelper::getDateTime($model_update->date_to_user, 'd/m/Y H:i') : '', // thời gian trả cán bộ
							'place_user' => $model_update->place_user,  // Địa điểm đón cán bộ
							'note_document' => $model_update->note_document, // Ghi chú
							'created_date' => !empty($model_update->created_date) ? DateTimeHelper::getDateTime($model_update->created_date, 'd/m/Y H:i') : '',
							'lenght' => $model_update->lenght,
							'user_in_doc' => $data_user,
							'data_location' => $data_location,
							'data_city' => $citys,
							//'data_district' => $districts
					];
				}
			}
			elseif($post['is_update'] == 1){
				/** @var  $document Document*/
				$model_update = new Document();
				$model_update = Document::findOne($post['id']);
				$model_update->name = $post['name']; // tên đơn
				$model_update->full_name = $post['full_name'];
				$model_update->department_id = $post['department_id'];
				$model_update->from = $post['from'];
				$model_update->from_name = $post['from_name'];
				$model_update->number_user = $post['number_user'];
				$model_update->date_from = DateTimeHelper::getDateTime($post['date_from'],'Y-m-d h:i');
				$model_update->date_to = DateTimeHelper::getDateTime($post['date_to'],'Y-m-d h:i');
				$model_update->date_from_car = DateTimeHelper::getDateTime($post['date_from_car'],'Y-m-d h:i');
				$model_update->date_to_car = DateTimeHelper::getDateTime($post['date_to_car'],'Y-m-d h:i');
				$model_update->purpose_car = $post['purpose_car'];
				$model_update->date_from_user = DateTimeHelper::getDateTime($post['date_from_user'],'Y-m-d h:i');
				$model_update->date_to_user = DateTimeHelper::getDateTime($post['date_to_user'],'Y-m-d h:i');
				$model_update->place_user = $post['place_user'];
				$model_update->note_document = $post['note_document'];
				$model_update->modify_date = DateTimeHelper::getDateTime('now','Y-m-d h:i');

				$model_update->update();
				$this->saveDocumentHistory($model_update->id,'Cập nhật thông tin hồ sơ', $user->id);
				if(!empty($model_update->userInDoc)){
					UserInDocument::deleteAll('document_id = '.$model_update->id.'',['document_id' => $model_update->id]);
				}
				$model_update->full_name_user = $post['full_name_user'];
				$model_update->department_user = $post['department_user'];
				$model_update->position_user = $post['position_user'];
				$model_update->phone_user = $post['phone_user'];
				$model_update->email_user = $post['email_user'];
				$model_update->insertUserDocument();
				Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Cập nhật hồ sơ thành công'));
				return $this->redirect(Url::toRoute(['/frontend/document']));
			}
		}
		echo json_encode($data);
	}

	public function actionsCreatedUserDocument($id)
	{
		$userDocument = new UserInDocument();

		if ($post = Yii::$app->request->post())
		{
			$userDocument = $post;
		}
		return $this->render('create',['userDocument'=> $userDocument]);

	}

	public function actionGetDocumentHistory(){
		$data = [];
		if($post = Yii::$app->request->post()){
			/** @var  $document Document*/
			$model = Document::findOne($post['id']);
			if($model){
				$data_document_history = $model->documentHistory;
				$data_history = [];
				foreach($data_document_history as $item){
					$data_history[] = [
							'id'=>$item->id,
							'creater'=> $item->user->user_name,
							'note'=>$item->note,
							'created_date'=>$item->created_date,
					];
				}

				$data = [
						'id' => $model->id,
						'code' => $model->code, //mã hồ sơ
						'data_history' => $data_history
				];
			}
		}

		echo json_encode($data);
	}

	public function actionSuggest($id){
		$model = Document::findOne($id);
		$list_car = CarInDocument::find()->where(['document_id' => $model->id])->all();
		$list_user = UserInDocument::find()->where(['document_id' => $model->id])->all();
		$count = count($list_car);
		$str_car_code = "";
		foreach($list_car as $key => $item){
			if($key < $count - 1){
				$str_car_code .= $item->car->code.', ';
			}else{
				$str_car_code .= $item->car->code.'.';
			}
		}

		$list_address = DocumentLocation::find()->where(['document_id' => $id])->orderBy(['index' => SORT_ASC])->all();
		$count_list = count($list_address);
		$str_address = "";
		foreach($list_address as $key => $item){
			if($key < $count_list - 1){
				$str_address .= $item->district->name . ", ";
			}else{
				$str_address .= $item->district->name;
			}
		}

		return $this->render('suggest',
				[
						'model'=>$model,
						'list_car_code' => $str_car_code,
						'list_car'=>$list_car,
						'list_user'=>$list_user,
						'list_address' => $str_address
				]
		);
	}



}
