<?php

namespace app\modules\frontend\controllers;

use app\components\Controller;
use app\helpers\DateTimeHelper;
use app\models\Banner;
use app\models\App;
use app\models\City;
use app\models\District;
use app\models\InfoFooter;
use app\models\News;
use app\models\NewsCat;
use app\modules\question\models\Question;
use app\modules\user\models\forms\LoginForm;
use app\modules\user\models\User;
use app\modules\user\models\UserExtra;
use app\modules\user\models\UserFeed;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;


class SiteController extends Controller{

	public function behaviors(){
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					//                    'delete' => ['post'],
				],
			],
			// http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
			'access' => [
				'class' => AccessControl::className(),
                'only' => ['login'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'], // chỉ dành cho user chưa đăng nhập
                    ],
                ],
			],
		];
	}

	public function init(){

		parent::init();

	}

	public function actionIndex(){
		return $this->render('index', [

			]);
	}

    /**
     * Trang đăng nhập
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {

        $this->layout = '@theme/layouts/login_admin';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if($post = Yii::$app->request->post()){
            $model->load($post);
            if($model->validate()){

                Yii::$app->user->login($model->getUser(), Yii::$app->params['user']['rememberLogin']);
                //$this->user = Yii::$app->user->identity;
                return $this->redirect(['/frontend/document/index']);
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Thoát
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        Yii::$app->session->setFlash('warning', Yii::t('user', 'Đăng xuất thành công'));

        return $this->redirect(Url::toRoute(['/frontend/site/login']));
    }
}
