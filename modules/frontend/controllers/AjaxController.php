<?php

namespace app\modules\frontend\controllers;

use app\helpers\DateTimeHelper;
use app\models\App;
use app\modules\frontend\components\Controller;
use app\modules\user\models\UserApp;
use app\modules\user\models\UserActivity;
use app\models\AppExtra;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\db\Query;


class AjaxController extends Controller{

	public function behaviors(){
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					//                    'delete' => ['post'],
				],
			],
			// http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['payment'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
				'rules' => [
					//                    [
					//                        'actions' => ['register'],
					//                        'allow' => true,
					//                        'roles' => ['?'], // chỉ dành cho user chưa đăng nhập
					//                    ],
					[
						'actions' => ['payment'],
						'allow' => true,
						'roles' => ['@'], // chỉ dành cho user đã đăng nhập. nếu chưa đăng nhập thì redirect sang trang login.
					],
					//                    [
					//                        'actions' => ['sync'],
					//                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
					//                        'matchCallback' => function ($rule, $action){
					//                            return true; //Yii::$app->user->can(AuthManager::ROLE_ADMIN);
					//                        }
					//                    ],
					//                    [
					//                        'actions' => ['create', 'index', 'update'],
					//                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
					//                        'matchCallback' => function ($rule, $action){
					//                            return Yii::$app->user->can(AuthManager::ROLE_AUTHOR);
					//                        }
					//                    ],
				],
			],
		];
	}

	public function init(){

		parent::init();

	}


	public function actionGetDistance(){
		if($post = Yii::$app->request->post()){
			$data = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$post['origins'].'&destinations='.$post['destinations'].'&mode=driving&language=vi-VN&key=AIzaSyB3kmEH37NEKuD6d7aBqGJkXOyN0LX2nag');
			$data = json_decode($data);
			$amount = 0;
			if(!empty($data->rows[0]->elements[0]->distance->value)){
				$amount = $data->rows[0]->elements[0]->distance->value;
			}
			echo json_encode(['amount' => number_format($amount/1000,1,'.','')]);
		}
	}

}
