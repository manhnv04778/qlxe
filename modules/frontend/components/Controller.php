<?php
/**
 * Created by PhpStorm.
 * User: ABC
 * Date: 10/17/14
 * Time: 20:36
 */

namespace app\modules\frontend\components;

use app\modules\user\models\UserRequest;
use Yii;
use yii\helpers\Url;
use yii\web\View;

class Controller extends \app\components\Controller{


    public function init(){
        parent::init();


        $this->layout = '@theme/layouts/front_end';
        Yii::$app->user->loginUrl = '/frontend/site/login';
        $this->user = Yii::$app->user->identity;
        if(!$this->user){
            $this->redirect(Url::toRoute(['/frontend/site/login']));
        }
    }


}

