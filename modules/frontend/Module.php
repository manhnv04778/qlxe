<?php

namespace app\modules\frontend;
use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\frontend\controllers';

	public function init()
	{
		parent::init();

        // load module config
        Yii::configure(\Yii::$app, require(__DIR__ . '/config/config.php'));

	}
}
