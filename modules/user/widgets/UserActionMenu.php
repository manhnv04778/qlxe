<?php
namespace app\modules\user\widgets;

use Yii;
use yii\data\ActiveDataProvider;

class UserActionMenu extends \yii\base\Widget
{

    public $user;
    public function init()
    {
        parent::init();

    }

    public function run()
    {
        return $this->render('user-action-menu', [
            'user' => $this->user
        ]);
    }
}