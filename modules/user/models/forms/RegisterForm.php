<?php
namespace app\modules\user\models\forms;

use app\modules\user\models\User;
use Curl\Curl;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class RegisterForm extends Model
{
    public $username;
    public $password;
    public $passwordReType;
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'passwordReType', 'username'], 'required'],
            [['password', 'passwordReType', 'username'], 'filter', 'filter' => 'trim'],
            ['username', 'string', 'max' => 20],
            ['username', 'string', 'min' => 6],
            ['phone', function ($attribute, $params) {
                $this->phone = str_replace(" ", "", $this->phone);
                if (!preg_match('/^0\d{9,10}$/', $this->phone)) {
                    $this->addError('phone', Yii::t('register', Yii::t('register', 'Số điện thoại phải bắt đầu bằng số 0 và từ 10-11 số')));
                }
            }],
            ['password', function ($attribute, $params) {
                if (preg_match("/\\s/", $this->password)) {
                    $this->addError('password', Yii::t('register', Yii::t('register', 'Mật khẩu phải viết liền không dấu và không có dấu cách')));
                }
            }],
            ['username', 'checkDuplicate', 'on' => 'create'],
            ['password', 'string', 'min' => 6],
            ['password', 'string', 'max' => 20],
            ['passwordReType', 'compare', 'compareAttribute' => 'password', 'message' => 'Nhập lại mật khẩu không khớp'],
            [['phone'],'safe']
        ];
    }


    public function attributeLabels()
    {
        return [
            'email' => Yii::t('user', 'Email'),
            'username' => Yii::t('user', 'Tên đăng nhập'),
            'password' => Yii::t('user', 'Mật khẩu'),
            'phone' => Yii::t('user', 'Số điện thoại'),
            'passwordReType' => Yii::t('user', 'Xác nhận mật khẩu'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $curl = new Curl();
            $curl->post('http://ids.cpvm.vn/services/api/users/dk.php', array(
                'p' => $this->username,
                't' => 1,
            ));

            $data = $curl->exec();
            $curl->close();

            if($data->s){
                $this->addError('username', Yii::t('user','Tên đăng nhập đã tồn tại'));
            }
        }
    }

    /**
     * Sends an email with a link, for verify password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail($user, $userEmail)
    {
        /* @var $user \app\modules\user\models\User  */
        /* @var $user \app\modules\user\models\UserEmail  */

        return Yii::$app->mailer->compose('verifiedRegistrationToken', ['user' => $user, 'userEmail' => $userEmail])
            ->setFrom([
                Yii::$app->params['support']['email'] => Yii::$app->name
            ])
            ->setTo($userEmail->email)
            ->setSubject('Verify register for ' . Yii::$app->name)
            ->send();

    }

    public function checkDuplicate()
    {
        $user = User::findOne(['username' => $this->username]);
        if($user){
            $this->addError('username', Yii::t('user','Tên đăng nhập đã tồn tại'));
        }else{
            if(preg_match('/\s|[^a-z0-9]/', $this->username)){
                $this->addError('username', Yii::t('user','Tên đăng nhập phải là chữ thường viết liền không dấu và không có ký tự đặc biệt'));
            }

        }
    }
}
