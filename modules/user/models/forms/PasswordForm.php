<?php
namespace app\modules\user\models\forms;

use app\modules\user\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class PasswordForm extends Model
{
    public $passwordOld;
    public $passwordNew;
    public $passwordNewReType;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['passwordOld', 'passwordNew', 'passwordNewReType'],  'filter', 'filter' => 'trim'],

            ['passwordOld', 'required', 'on' => 'update'],
            ['passwordOld', 'validatePasswordOld', 'on' => 'update'],

            [['passwordNew', 'passwordNewReType'], 'required'],
            ['passwordNew', 'string', 'min' => 6],
            ['passwordNewReType', 'compare', 'compareAttribute' => 'passwordNew'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'passwordOld' => Yii::t('user', 'Current password'),
            'passwordNew' => Yii::t('user', 'New password'),
            'passwordNewReType' => Yii::t('user', 'Re-type new password'),
        ];
    }


    /**
     * Validates the old password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePasswordOld($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = Yii::$app->user->identity;

            if (!$user->validatePassword($this->passwordOld)) {
                $this->addError($attribute, Yii::t('user','Incorrect old password.'));
            }
        }
    }
}
