<?php
namespace app\modules\user\models\forms;

use app\helpers\DateTimeHelper;
use app\modules\user\models\User;
use app\modules\user\models\UserEmail;
use yii\base\Model;
use Yii;

/**
 * Password reset request form
 */
class ForgotPasswordForm extends Model
{
    public $code;
    public $passwordNew;
    public $passwordNewReType;
    public $user_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['code', 'required'],
            ['user_name', 'required'],
            [['code','user_name'], 'string'],
            [['passwordNew', 'passwordNewReType'],  'filter', 'filter' => 'trim'],
            [['passwordNew', 'passwordNewReType'], 'required'],
            ['passwordNew', 'string', 'min' => 6],
            ['passwordNew', 'string', 'max' => 20],
            ['passwordNewReType', 'compare', 'compareAttribute' => 'passwordNew'],
            ['code', 'checkCode', 'on' => 'fogot'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_name' => Yii::t('user', 'Tên đăng nhập'),
            'code' => Yii::t('user', 'Mã xác nhận'),
            'passwordNew' => Yii::t('user', 'Mật khẩu mới'),
            'passwordNewReType' => Yii::t('user', 'Nhập lại mật khẩu'),
        ];
    }

    public function checkCode()
    {
        $user = $this->getUser();
        if(!$user){
            $this->addError('user_name', Yii::t('user','Tài khoản không tồn tại'));
        }else{
            $time = strtotime(DateTimeHelper::getDateTime()) - strtotime($user->verify_expire);
            if($time > 1800){
                $this->addError('code', Yii::t('user','Mã xác nhận đã hết hạn'));
            }elseif($user->code != $this->code){
                $this->addError('code', Yii::t('user','Mã xác nhận không đúng'));
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        $user = User::findByUsername($this->user_name);
        return $user;
    }

}
