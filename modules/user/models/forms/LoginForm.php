<?php
namespace app\modules\user\models\forms;

use app\libraries\simple_html_dom;
use app\modules\user\models\User;
use Curl\Curl;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    public $data;
    public $reCaptcha;
    //public $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('user', 'Email'),
            'password' => Yii::t('user', 'Password'),
            'rememberMe' => Yii::t('user', 'Remember me'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {

            $user = $this->getUser();
            if($user){
                if(!$user->validatePassword($this->password)) {
                    $this->addError($attribute, Yii::t('user','Tên đăng nhập hoặc mật khẩu không đúng'));
                }
            }else{
                $curl = new Curl();
                $curl->post('http://ids.egame.vn/services/api/users/signin.php', array(
                    'u' => $this->username,
                    'p' => $this->password,
                ));

                $this->data = $curl->exec();
                $curl->close();

                if(!$this->data->s){
                    $this->addError($attribute, Yii::t('user','Tên đăng nhập hoặc mật khẩu không đúng'));
                }
            }


        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {

            $this->_user = $this->getUser();
            if($this->_user){
                return Yii::$app->user->login($this->getUser(), $this->rememberMe ? Yii::$app->params['user']['rememberLogin'] : 0);
            }
        } else {
            return false;
        }
    }
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
