<?php
namespace app\modules\user\models\forms;

use app\modules\user\models\UserEmail;
use yii\base\Model;
use Yii;

/**
 * Password reset request form
 */
class PasswordResetForm extends Model
{
    public $user_name;
    public $phone;
    public $code;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_name', 'required'],
            ['code', 'string']
//            ['email', 'exist',
//                'targetClass' => '\common\models\User',
//                'filter' => ['status' => 'enable'],
//                'message' => Yii::t('user','There is no user with such email.')
//            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_name' => Yii::t('user', 'Tên đăng nhập'),
        ];
    }

}
