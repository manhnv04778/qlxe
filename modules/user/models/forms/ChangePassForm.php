<?php
namespace app\modules\user\models\forms;

use app\modules\user\models\User;
use Curl\Curl;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class ChangePassForm extends Model
{
    public $passOld;
    public $passNew;
    public $passNewReType;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['passOld', 'passNew', 'passNewReType'], 'required'],
            [['passOld', 'passNew', 'passNewReType'], 'filter', 'filter' => 'trim'],
            [['passOld', 'passNew', 'passNewReType'], 'string', 'min' => 6],
            ['passNewReType', 'compare', 'compareAttribute' => 'passNew', 'message' => 'Nhập lại mật khẩu không khớp với mật khẩu mới'],
            ['passOld', 'validatePassword'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'passOld' => Yii::t('user', 'Mật khẩu cũ'),
            'passNew' => Yii::t('user', 'Mật khẩu mới'),
            'passNewReType' => Yii::t('user', 'Nhập lại mật khẩu mới'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError($attribute, Yii::t('user','Mật khẩu cũ không đúng'));
            }elseif($user->password_hash){
                if(!$user->validatePassword($this->passOld)){
                    $this->addError($attribute, Yii::t('user','Mật khẩu cũ không đúng'));
                }
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findOne(Yii::$app->controller->user->id);
        }

        return $this->_user;
    }
}
