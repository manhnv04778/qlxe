<?php

namespace app\modules\user\models\elasticsearch;

use app\components\Elasticsearch;
use app\modules\user\models\behavior\UserBehavior;
use app\modules\question\models\QuestionUser;
use app\modules\user\models\User;
use Yii;
use app\modules\question\models\Question;
use app\modules\question\models\QuestionAnswer;
use app\modules\question\models\QuestionTag;
use yii\db\ActiveQuery;
use app\modules\UserActivity;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for document "question" of elasticsearch.
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $name
 * @property string $name_alias
 * @property string $birthday
 * @property string $gender
 * @property string $website
 * @property string $image
 * @property string $signature
 * @property string $address
 * @property integer $district_id
 * @property integer $city_id
 * @property integer $user_follow_count
 * @property integer $user_following_count
 * @property string $verified
 * @property string $status
 * @property string $role
 * @property string $created
 * @property string $updated
 *
 * @property integer $reputation
 * @property integer $reputation_week
 * @property integer $reputation_month
 * @property integer $reputation_quater
 * @property integer $reputation_year
 * @property integer $question_count
 * @property integer $question_count_week
 * @property integer $question_count_month
 * @property integer $question_count_quater
 * @property integer $question_count_year
 * @property integer $answer_count
 * @property integer $answer_count_week
 * @property integer $answer_count_month
 * @property integer $answer_count_quater
 * @property integer $answer_count_year
 * @property integer $answer_accepted_count
 * @property integer $answer_accepted_count_week
 * @property integer $answer_accepted_count_month
 * @property integer $answer_accepted_count_quater
 * @property integer $answer_accepted_count_year
 * @property integer $comment_count
 * @property integer $favourite_count
 * @property integer $voted_count
 * @property integer $voted_up_count
 * @property integer $voted_down_count
 * @property integer $vote_count
 * @property integer $vote_up_count
 * @property integer $vote_down_count
 * @property integer $tag_count
 * @property integer $tag_interested_count
 * @property integer $view
 *
 * @property bool $isSuper
 * @property bool $isEnable
 * @property bool $isDisable
 * @property QuestionUser $questionUser
 */
class UserElasticsearch extends Elasticsearch
{
    public function behaviors()
    {
        return [
            'userBehavior' => [
                'class' => UserBehavior::className(),
                // config
                //'attr1' => 'value1',
            ],
        ];
    }

    public function init(){
        parent::init();
    }

    /**
     * @return string the name of the type of this record.
     */
    public static function type(){
        return 'user';
    }

    public static function getModelReference(){
        return 'app\modules\user\models\User';
    }

    public static function typeFields(){

        return [
            'id' => ['type' => 'integer', 'store' => 'yes'],
            'username' => ['type' => 'string', 'store' => 'yes', 'index' => 'not_analyzed'],
            'email' => ['type' => 'string', 'store' => 'yes', 'index' => 'not_analyzed'],
            'name' => ['type' => 'string', 'store' => 'yes', 'index' => 'analyzed',
				//"index_analyzer" => "index_ngram",
                //"search_analyzer" => "search_ngram"
            ],
            'name_alias' => ['type' => 'string', 'store' => 'yes', 'index' => 'analyzed'],
            'birthday' => ['type' => 'date', 'store' => 'yes', 'format' => 'YYYY-MM-dd'],
            'gender' => ['type' => 'string', 'store' => 'yes', 'index' => 'not_analyzed'],

            'website' => ['type' => 'string', 'store' => 'yes', 'index' => 'no'],
            'image' => ['type' => 'string', 'store' => 'yes', 'index' => 'no'],
            'signature' => ['type' => 'string', 'store' => 'yes', 'index' => 'no'],

            'address' => ['type' => 'string', 'store' => 'yes', 'index' => 'no'],
            'district_id' => ['type' => 'integer', 'store' => 'yes'],
            'city_id' => ['type' => 'integer', 'store' => 'yes'],

            'user_follow_count' => ['type' => 'integer', 'store' => 'yes'],
            'user_following_count' => ['type' => 'integer', 'store' => 'yes'],
            'verified' => ['type' => 'integer', 'store' => 'yes'],

            'status' => ['type' => 'string', 'store' => 'yes', 'index' => 'not_analyzed'],
            'role' => ['type' => 'string', 'store' => 'yes', 'index' => 'not_analyzed'],

            'created' => ['type' => 'date', 'store' => 'yes', 'format' => 'YYYY-MM-dd HH:mm:ss'],
            'updated' => ['type' => 'date', 'store' => 'yes', 'format' => 'YYYY-MM-dd HH:mm:ss'],



            'reputation' => ['type' => 'integer', 'store' => 'yes'],
            'reputation_week' => ['type' => 'integer', 'store' => 'yes'],
            'reputation_month' => ['type' => 'integer', 'store' => 'yes'],
            'reputation_quater' => ['type' => 'integer', 'store' => 'yes'],
            'reputation_year' => ['type' => 'integer', 'store' => 'yes'],
            'question_count' => ['type' => 'integer', 'store' => 'yes'],
            'question_count_week' => ['type' => 'integer', 'store' => 'yes'],
            'question_count_month' => ['type' => 'integer', 'store' => 'yes'],
            'question_count_quater' => ['type' => 'integer', 'store' => 'yes'],
            'question_count_year' => ['type' => 'integer', 'store' => 'yes'],
            'answer_count' => ['type' => 'integer', 'store' => 'yes'],
            'answer_count_week' => ['type' => 'integer', 'store' => 'yes'],
            'answer_count_month' => ['type' => 'integer', 'store' => 'yes'],
            'answer_count_quater' => ['type' => 'integer', 'store' => 'yes'],
            'answer_count_year' => ['type' => 'integer', 'store' => 'yes'],
            'answer_accepted_count' => ['type' => 'integer', 'store' => 'yes'],
            'answer_accepted_count_week' => ['type' => 'integer', 'store' => 'yes'],
            'answer_accepted_count_month' => ['type' => 'integer', 'store' => 'yes'],
            'answer_accepted_count_quater' => ['type' => 'integer', 'store' => 'yes'],
            'answer_accepted_count_year' => ['type' => 'integer', 'store' => 'yes'],
            'comment_count' => ['type' => 'integer', 'store' => 'yes'],
            'favourite_count' => ['type' => 'integer', 'store' => 'yes'],
            'voted_count' => ['type' => 'integer', 'store' => 'yes'],
            'voted_up_count' => ['type' => 'integer', 'store' => 'yes'],
            'voted_down_count' => ['type' => 'integer', 'store' => 'yes'],
            'vote_count' => ['type' => 'integer', 'store' => 'yes'],
            'vote_up_count' => ['type' => 'integer', 'store' => 'yes'],
            'vote_down_count' => ['type' => 'integer', 'store' => 'yes'],
            'tag_count' => ['type' => 'integer', 'store' => 'yes'],
            'tag_interested_count' => ['type' => 'integer', 'store' => 'yes'],
            'view' => ['type' => 'integer', 'store' => 'yes'],

        ];
    }

    private $_questionUser;
    /**
     * @return QuestionUser
     */
    public function getQuestionUser(){
        if($this->_questionUser) return $this->_questionUser;
        return $this->_questionUser = QuestionUser::findOneCache(['user_id' => $this->id]);
    }

    /**
     * insert or update question
     * @param $ids question id, hoac 1 mang question id
     */
    public static function saveDocument($ids){

        $ids = is_array($ids) ? $ids : array($ids);

        $users = User::find()
            ->where(['id' => $ids])
            ->with([
                'questionUser' => function($query){
                    /* @var $query ActiveQuery */
                },
            ])
            ->asArray()
            ->all();
//        echo "<pre>"; print_r($users); echo "</pre>"; die;

        // TODO -o haidm: tìm hoặc viết thêm insert theo batch nếu có thời gian.
        foreach($users as $user){
            $qe = self::findOne($user['id']);
            if(!$qe) $qe = new self;

            $qe->setAttributes($user, false);
            $qe->setAttributes($user['questionUser'], false);
            $qe->save();

        }


    }


    // only fields in rules() are searchable
    public function rules()
    {
        return [
            [ array_keys(self::typeFields()), 'safe']
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return parent::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     *
     * Nếu muốn search theo điều kiện AND|ỎR thì truyền vào
     * $params['operator'] = 'AND'; Nếu ko truyền, mặc định là OR
     *
     */
    public function search($params)
    {
        $operator = (!empty($params['operator']) && in_array($params['operator'], ['AND', 'OR'])) ? $params['operator'] : 'OR';

//        echo "<pre>"; print_r($params); echo "</pre>"; die;
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => 20,
            ],
            'sort'=> ['defaultOrder' => ['created'=>SORT_DESC]]
        ]);

        if (!$params) return $dataProvider;


        if(!empty($params['UserElasticsearch'])){
            $this->load($params);
        }else{
            $this->load($params, '');
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'username' => $this->username,
            'email' => $this->email,
//            'name' => $this->name,
//
//            'name_alias' => $this->name_alias,
            'birthday' => $this->birthday,
            'gender' => $this->gender,

//            'website' => $this->website,
//            'image' => $this->image,
//            'signature' => $this->signature,

            'address' => $this->address,
            'district_id' => $this->district_id,
            'city_id' => $this->city_id,

            'user_follow_count' => $this->user_follow_count,
            'user_following_count' => $this->user_following_count,
            'verified' => $this->verified,

            'status' => $this->status,
            'role' => $this->role,

            'created' => $this->created,
            'updated' => $this->updated,

            'reputation' => $this->reputation,
            'reputation_week' => $this->reputation_week,
            'reputation_month' => $this->reputation_month,
            'reputation_quater' => $this->reputation_quater,
            'reputation_year' => $this->reputation_year,
            'question_count' => $this->question_count,
            'question_count_week' => $this->question_count_week,
            'question_count_month' => $this->question_count_month,
            'question_count_quater' => $this->question_count_quater,
            'question_count_year' => $this->question_count_year,
            'answer_count' => $this->answer_count,
            'answer_count_week' => $this->answer_count_week,
            'answer_count_month' => $this->answer_count_month,
            'answer_count_quater' => $this->answer_count_quater,
            'answer_count_year' => $this->answer_count_year,
            'answer_accepted_count' => $this->answer_accepted_count,
            'answer_accepted_count_week' => $this->answer_accepted_count_week,
            'answer_accepted_count_month' => $this->answer_accepted_count_month,
            'answer_accepted_count_quater' => $this->answer_accepted_count_quater,
            'answer_accepted_count_year' => $this->answer_accepted_count_year,
            'comment_count' => $this->comment_count,
            'favourite_count' => $this->favourite_count,
            'voted_count' => $this->voted_count,
            'voted_up_count' => $this->voted_up_count,
            'voted_down_count' => $this->voted_down_count,
            'vote_count' => $this->vote_count,
            'vote_up_count' => $this->vote_up_count,
            'vote_down_count' => $this->vote_down_count,
            'tag_count' => $this->tag_count,
            'tag_interested_count' => $this->tag_interested_count,
            'view' => $this->view,
        ]);




        // custom filter
        $mustedFilters = [];
        if($this->created){
            $mustedFilters[] = ['range' => ['created' => [
                'gte' => $this->created.' 00:00:00',
                'lte' => $this->created.' 23:59:59',
            ]]];
        }
        if($this->updated){
            $mustedFilters[] = ['range' => ['updated' => [
                'gte' => $this->updated.' 00:00:00',
                'lte' => $this->updated.' 23:59:59',
            ]]];
        }

        if($mustedFilters){
            $query->filter([
                "bool" => [
                    "must" => $mustedFilters
                ],
            ]);
        }



        // custom query
        // search nhiều trường với điều kiện AND
        $mustedMatches = [];

        if($this->name){
            $mustedMatches[] = ['multi_match' => [
                'query' => $this->name,
                'fields' => ['name^3', 'name_alias'],
                'operator' => $operator
            ]];
        }

//        if($this->title){
//            $mustedMatches[] = ['match' => ['title' => $this->title]];
//        }

        if($mustedMatches){
            $query->query([
                "bool" => [
                    "must" => $mustedMatches
                ]
            ]);
        }



        // custom sort
        if(isset($params['sort'])){
            $sort = $params['sort'];
            if(in_array($sort, ['name', '-name'])){
                $dataProvider->sort->attributes['clinic'] = [
                    'asc' => ['name' => SORT_ASC],
                    'desc' => ['name' => SORT_DESC],
                ];
            }

            if ($sort == 'name') {
                $query->orderBy(['name' => SORT_ASC]);
            }
        }


        return $dataProvider;
    }
}
