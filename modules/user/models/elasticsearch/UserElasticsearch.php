<?php

namespace app\modules\user\models\elasticsearch;

use app\components\Elasticsearch;
use app\modules\user\models\behavior\UserBehavior;
use app\modules\user\models\UserExtra;
use app\modules\user\models\User;
use Yii;
use app\modules\question\models\Question;
use app\modules\question\models\QuestionAnswer;
use app\modules\question\models\QuestionTag;
use yii\db\ActiveQuery;
use app\modules\UserActivity;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for document "question" of elasticsearch.
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $name
 * @property string $name_alias
 * @property string $birthday
 * @property string $gender
 * @property string $website
 * @property string $image
 * @property string $image_picasa
 * @property string $about
 * @property string $address
 * @property integer $district_id
 * @property integer $city_id
 * @property integer $user_follow_count
 * @property integer $user_following_count
 * @property string $verified
 * @property string $status
 * @property string $role
 * @property string $created
 * @property string $updated
 *
 * @property integer $reputation
 * @property integer $reputation_week
 * @property integer $reputation_month
 * @property integer $reputation_quater
 * @property integer $reputation_year
 * @property integer $question_count
 * @property integer $question_count_week
 * @property integer $question_count_month
 * @property integer $question_count_quater
 * @property integer $question_count_year
 * @property integer $answer_count
 * @property integer $answer_count_week
 * @property integer $answer_count_month
 * @property integer $answer_count_quater
 * @property integer $answer_count_year
 * @property integer $answer_accepted_count
 * @property integer $answer_accepted_count_week
 * @property integer $answer_accepted_count_month
 * @property integer $answer_accepted_count_quater
 * @property integer $answer_accepted_count_year
 * @property integer $comment_count
 * @property integer $favourite_count
 * @property integer $voted_count
 * @property integer $voted_up_count
 * @property integer $voted_down_count
 * @property integer $vote_count
 * @property integer $vote_up_count
 * @property integer $vote_down_count
 * @property integer $tag_count
 * @property integer $tag_interested_count
 * @property integer $view
 *
 * @property bool $isSuper
 * @property bool $isEnable
 * @property bool $isDisable
 * @property UserExtra $userExtra
 */
class UserElasticsearch extends Elasticsearch
{
    public function behaviors()
    {
        return [
            'userBehavior' => [
                'class' => UserBehavior::className(),
                // config
                //'attr1' => 'value1',
            ],
        ];
    }

    public function init(){
        parent::init();
    }

    /**
     * @return string the name of the type of this record.
     */
    public static function type(){
        return 'user';
    }

    public static function getModelReference(){
        return 'app\modules\user\models\User';
    }

    public static function typeFields(){

        return [
            'id' => ['type' => 'integer', 'store' => 'yes'],
            'username' => ['type' => 'string', 'store' => 'yes', 'index' => 'not_analyzed'],
            'email' => ['type' => 'string', 'store' => 'yes', 'index' => 'not_analyzed'],
            'name' => ['type' => 'string', 'store' => 'yes', 'index' => 'analyzed',
				//"index_analyzer" => "index_ngram",
                //"search_analyzer" => "search_ngram"
            ],
            'name_alias' => ['type' => 'string', 'store' => 'yes', 'index' => 'analyzed'],
            'birthday' => ['type' => 'date', 'store' => 'yes', 'format' => 'YYYY-MM-dd'],
            'gender' => ['type' => 'string', 'store' => 'yes', 'index' => 'not_analyzed'],

            'website' => ['type' => 'string', 'store' => 'yes', 'index' => 'no'],
            'image' => ['type' => 'string', 'store' => 'yes', 'index' => 'no'],
            'image_id' => ['type' => 'integer', 'store' => 'yes', 'index' => 'no'],
            'image_picasa' => ['type' => 'string', 'store' => 'yes', 'index' => 'no'],
            'about' => ['type' => 'string', 'store' => 'yes', 'index' => 'no'],

            'address' => ['type' => 'string', 'store' => 'yes', 'index' => 'no'],
            'district_id' => ['type' => 'integer', 'store' => 'yes'],
            'city_id' => ['type' => 'integer', 'store' => 'yes'],

            'user_follow_count' => ['type' => 'integer', 'store' => 'yes'],
            'user_following_count' => ['type' => 'integer', 'store' => 'yes'],
            'verified' => ['type' => 'integer', 'store' => 'yes'],

            'status' => ['type' => 'string', 'store' => 'yes', 'index' => 'not_analyzed'],
            'role' => ['type' => 'string', 'store' => 'yes', 'index' => 'not_analyzed'],

            'created' => ['type' => 'date', 'store' => 'yes', 'format' => 'YYYY-MM-dd HH:mm:ss'],
            'updated' => ['type' => 'date', 'store' => 'yes', 'format' => 'YYYY-MM-dd HH:mm:ss'],

            'package' => ['type' => 'string', 'store' => 'yes', 'index' => 'not_analyzed'],
            'package_expire' => ['type' => 'date', 'store' => 'yes', 'format' => 'YYYY-MM-dd HH:mm:ss'],
            'user_facebook_count' => ['type' => 'integer', 'store' => 'yes'],
            'task_count' => ['type' => 'integer', 'store' => 'yes'],

        ];
    }

    private $_userExtra;
    /**
     * @return UserExtra
     */
    public function getUserExtra(){
        if($this->_userExtra) return $this->_userExtra;
        return $this->_userExtra = UserExtra::findOneCache(['user_id' => $this->id]);
    }


    private static function _builDocument($item)
    {
        if(!empty($item['userExtra'])){
            $item = array_merge($item, $item['userExtra']);
        }


        unset($item['userExtra']);

        return $item;

    }

    public static function syncDocuments($limit = 100){

        $items =  User::find()
            ->where(['user.sync_status' => 'pending'])
            ->joinWith([
                'userExtra' => function($query){
                    /* @var $query ActiveQuery */
                },
            ])
            ->indexBy('id')
            ->limit($limit)
            ->asArray()
            ->all();

        if(!$items) return;

        $ids = array_keys($items);
        User::updateAll(['sync_status' => 'processing'], ['id' => $ids]);


        $datas = [];
        foreach ($items as $item) {
            $datas[] = self::_builDocument($item);
        }

        self::insertAll($datas);
        unset($datas);

        User::updateAll(['sync_status' => 'done'], ['id' => $ids]);

        return count($ids);
    }


    /**
     * insert or update
     * @param $ids id, hoac 1 mang id
     */
    public static function saveDocument($ids){

        $ids = is_array($ids) ? $ids : array($ids);

        $items = User::find()
            ->where(['id' => $ids])
            ->with([
                'userExtra' => function($query){
                    /* @var $query ActiveQuery */
                },
            ])
            ->asArray()
            ->all();
//        echo "<pre>"; print_r($users); echo "</pre>"; die;

        foreach($items as $item){

            $data = self::_builDocument($item);
            $e = self::findOne($data['id']);

            if (!$e) {
                $e = new self();
            }
            $e->setAttributes($data, false);
            $e->save();
            //echo "<pre>";print_r($e);echo "</pre>";die;
        }

    }


    // only fields in rules() are searchable
    public function rules()
    {
        return [
            [ array_keys(self::typeFields()), 'safe']
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return parent::scenarios();
    }

    /**
     * Search
     * @author haidm
     * @param array $params
     * có thể filter các trường sau:
    'id',
    'user_id',
    'user.id',
    'user.username',

    'view',
    'view_week',
    'view_month',

    'cat_id',
    'cat.alias',

    'tag_count',
    'status',

    'tags.id',
    'tags.alias',

     * có thể search các trường sau:
    'title',
    'user.name',

     * default value
    'operator' = 'OR' : search operator, dùng khi search. chỉ chấp nhận 2 giá trị AND|OR. nếu là AND thì tìm kiếm đúng khi có đầy đủ các từ. nếu là OR thì chỉ cần 1 trong những từ được tìm thấy.
    'distance' = 1000 : tìm trong khoảng cách bao nhiêu m.

     * Chú ý:
     * Những trường không được liệt kê ở trên thì ko được thực hiện. VD: $params['abc'] = 'def'; sẽ bị bỏ qua.
     * Những trường được liệt kê ở trên đều lọc theo điều kiện: AND
     * Những trường filter đều có thể filter với nhiều giá trị. VD tìm 2 đơn khám có ID = 1,2. $params['id'] = [1,2];
     * Những trường filter theo GEO(latlng) thì tự động sắp xếp theo gần nhất. Set trường 'distance' để xác định bán kính tìm kiếm.
     * Những trường search thì tự động sắp xếp theo (gần) đúng nhất
     * Muốn order by thì dùng tham số sort. Chỉ sort được những thuộc tính trực tiếp, muốn sắp xếp các tham số nested thì phải viết thêm. VD sắp xếp theo mới nhất: $params['sort'] = '-created';
     *
     * @param array $fields chỉ trả về những thuộc tính trực tiếp được chỉ định. Nếu không chỉ định thì sẽ trả về tất cả.
     * @return ActiveDataProvider
     *
     */
    public function search($params, $fields = [], $aggregations = null, $returnQuery = false)
    {
        // default params
        $operator = (!empty($params['operator']) && in_array($params['operator'], ['AND', 'OR'])) ? $params['operator'] : 'OR';
        $distance = (!empty($params['distance']) && is_numeric($params['distance'])) ? $params['distance'] : 1000; //m

//        echo "<pre>"; print_r($params); echo "</pre>"; die;
        $query = self::find();
        if($aggregations){
            $query->aggregations = $aggregations;
        }
        $pageSize = (isset($params['limit'])) ? $params['limit'] : 20;
        $page = (isset($params['page'])) ? $params['page'] - 1 : 0;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $pageSize,
                'page' => $page
            ],
            'sort'=> ['defaultOrder' => ['created'=>SORT_DESC]]
        ]);

        if (!$params) return $dataProvider;

        if(!empty($params[$this->shortClassName])){
            $params = $params[$this->shortClassName];
        }

        $this->load($params, '');



        // AND filters
        $mustFilters = [];

        // AND matches
        $mustMatches = [];


        $this->setMustFilters($params, $mustFilters, [
            'id',
            'username',
            'email',
            'gender',
            'district_id',
            'city_id',
            'verified',
            'status',
            'role',
        ]);

//        $this->setMustFiltersNested($params, $mustFilters, [
//
//        ]);

        $this->setMustFiltersDate($params, $mustFilters, [
            'created',
            'updated',
            'birthday',
        ]);

        $this->setMustMatches($params, $mustMatches, [
            [
                'query' => 'name',
                'fields' => [
                    'name^2',
                    'name_alias',
                ],
                'operator' => $operator
            ],


        ]);

//        $this->setMustMatchesNested($params, $mustMatches, [
//        ]);

//        $this->setMustFiltersGeo($params, $mustFilters, 'latlng', $distance);

        // add filters
        $filters = [];
        if($mustFilters){
            $filters['bool']['must'] = $mustFilters;
            /*$filters = [
                "bool" => [
                    "must" => $mustedFilters
                ],
            ];*/
        }
        if ($filters){
            $query->filter($filters);
        }


        // add matches
        $matches = [];
        if($mustMatches){
            $matches['bool']['must'] = $mustMatches;
        }
        if ($matches){
            $query->query($matches);
        }


        // custom sort
        if(!empty($params['sort'])){
            $sort = $params['sort'];

//            if(in_array($sort, ['user', '-user'])){
//                $dataProvider->sort->attributes['user'] = [
//                    'asc' => ['user.name' => SORT_ASC],
//                    'desc' => ['user.name' => SORT_DESC],
//                ];
//            }


//            if(in_array($sort, ['name', '-name'])){
//                $dataProvider->sort->attributes['name'] = [
//                    'asc' => ['name' => SORT_ASC],
//                    'desc' => ['name' => SORT_DESC],
//                ];
//                $query->orderBy['name'] = $sort == 'name' ? SORT_ASC : SORT_DESC;
//            }

        }

        // sort ASC by GEO TODO: not test :(
        // doc: http://www.elasticsearch.org/blog/geo-location-and-search/
//        if(!empty($params['latlng'])){
//            $query->orderBy['_geo_distance'] = [
//                'latlng' => $params['latlng'],
//                'order' => 'asc',
//                'unit' => 'm',
//            ];
//        }


        if($fields){
            $query->fields = $fields;
        }

        if($returnQuery) return $query;

//        echo "<pre>"; print_r($query); echo "</pre>"; die;

        return $dataProvider;
    }
}
