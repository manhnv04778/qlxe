<?php

namespace app\modules\user\models;

use app\models\Department;
use app\models\Position;
use app\modules\user\components\Model;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $password
 * @property integer $role_id
 * @property integer $position_id
 * @property string $full_name
 * @property integer $department_id
 * @property string $image
 * @property string $birthday
 * @property integer $status
 * @property integer $gender
 * @property integer $is_lock
 * @property integer $is_driver
 * @property string $created
 * @property integer $creat_id
 * @property string $modify
 * @property integer $modify_id
 * @property string $email
 * @property string $access_token
 * @property string $lockout_end
 * @property integer $email_confirmed
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface // limit APIs
{
    /**
     * @inheritdoc
     */

    public $rePassword;
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_id', 'gender', 'creat_id', 'user_name','password', 'full_name','rePassword'], 'required'],
            [['role_id', 'position_id', 'department_id', 'status', 'gender', 'is_lock', 'creat_id', 'modify_id', 'email_confirmed','is_driver'], 'integer'],
            [['birthday', 'created', 'modify', 'lockout_end'], 'safe'],
            [['user_name', 'full_name', 'image', 'email', 'access_token'], 'string', 'max' => 255],
            [['phone',], 'string', 'max' => 11],
            [['email'], 'email'],
            ['password', 'string', 'min' => 6],
            [['user_name'], 'unique'],
            //['rePassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Nhập lại mật khẩu không khớp'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'user_name' => Yii::t('user', 'Tên đăng nhập'),
            'password' => Yii::t('user', 'Mật khẩu'),
            'rePassword' => Yii::t('user', 'Nhập lại mật khẩu'),
            'role_id' => Yii::t('user', 'Quyền'),
            'position_id' => Yii::t('user', 'Chức danh'),
            'full_name' => Yii::t('user', 'Họ tên'),
            'department_id' => Yii::t('user', 'Đơn vị'),
            'image' => Yii::t('user', 'Image'),
            'birthday' => Yii::t('user', 'Ngày sinh'),
            'status' => Yii::t('user', 'Tình trạng'),
            'gender' => Yii::t('user', 'Giới tính'),
            'is_lock' => Yii::t('user', 'Khóa tài khoản'),
            'created' => Yii::t('user', 'Ngày tạo'),
            'creat_id' => Yii::t('user', 'Người tạo'),
            'modify' => Yii::t('user', 'Ngày sửa'),
            'modify_id' => Yii::t('user', 'Người sửa'),
            'email' => Yii::t('user', 'Email'),
            'access_token' => Yii::t('user', 'Token Key'),
            'lockout_end' => Yii::t('user', 'Lockout End'),
            'email_confirmed' => Yii::t('user', 'Email nhận thông báo'),
        ];
    }

    public static function getStatus(){
        $data = [
            '0' => 'Không hoạt động',
            '1' => 'Hoạt động'
        ];
        return $data;
    }

    public function search($params){
        $query = User::find()->where(['is_driver' => 0]);
        if(!empty($params['User'])){
            if(!empty($params['User']['user_name'])) {
                $query->andWhere(['user_name' => $params['User']['user_name']]);
            }
            if(!empty($params['User']['department_id'])) {
                $query->andWhere(['department_id' => $params['User']['department_id']]);
            }
            if(!empty($params['User']['position_id'])) {
                $query->andWhere(['position_id' => $params['User']['position_id']]);
            }
            if(!empty($params['User']['role_id'])) {
                $query->andWhere(['role_id' => $params['User']['role_id']]);
            }
        }
        $query->orderBy(['created' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public function searchDriver($params){
        $query = User::find()->where(['is_driver' => 1]);
        if(!empty($params['User'])){
            if(!empty($params['User']['user_name'])) {
                $query->andWhere(['user_name' => $params['User']['user_name']]);
            }
            if(!empty($params['User']['department_id'])) {
                $query->andWhere(['department_id' => $params['User']['department_id']]);
            }
            if(!empty($params['User']['position_id'])) {
                $query->andWhere(['position_id' => $params['User']['position_id']]);
            }
            if(!empty($params['User']['role_id'])) {
                $query->andWhere(['role_id' => $params['User']['role_id']]);
            }
        }
        $query->orderBy(['created' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!($this->load($params) && $this->validate())){
            return $dataProvider;
        }
        return $dataProvider;
    }

    public static function getUser(){
        $query = User::find()->asArray()->all();

        $data = ArrayHelper::map($query, 'id', 'full_name');
        return $data;
    }

    public function getDepartment(){
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    public function getPosition(){
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }

    /**
     * Finds user by username|email
     *
     * @param string $username username or email
     * @return User|null
     */
    public static function findByUsername($username)
    {
        $user = null;
        $user = User::findOne(['user_name' => $username]);
        return $user;
    }

    /*
	* Validates password
	*
	* @param string $password password to validate
	* @return boolean if password provided is valid for current user
	*/
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }
}
