<?php
/**
 * Những thuộc tính và phương thức dùng chung cho Question và QuestionElasticsearch
 */

namespace app\modules\user\models\behavior;

use app\components\rbac\AuthManager;
use app\helpers\AppHelper;
use app\modules\user\models\User;
use yii\base\Behavior;
use yii\helpers\Url;
use Yii;

class UserBehavior extends Behavior
{
//    public $prop1;
//
//    private $_prop2;
//
//    public function getProp2()
//    {
//        return $this->_prop2;
//    }
//
//    public function setProp2($value)
//    {
//        $this->_prop2 = $value;
//    }
    /* ATTRIBUTES
    -------------------------------------------------- */
    public function getIsSuper()
    {
        return $this->owner->role == 'super';
    }
    public function getIsAdmin()
    {
        return Yii::$app->user->can(AuthManager::ROLE_ADMIN);
    }
    public function getIsMod()
    {
        return Yii::$app->user->can(AuthManager::ROLE_MOD);
    }
    public function getIsAuthor()
    {
        return Yii::$app->user->can(AuthManager::ROLE_AUTHOR);
    }
    public function getIsEnable()
    {
        return $this->owner->status == 'enable';
    }
    public function getIsDisable()
    {
        return $this->owner->status == 'disable';
    }

    /* DATA
    -------------------------------------------------- */

    public function getGenderValue(){
        $genderData = User::getGenderData();
        return $genderData[$this->owner->gender];
    }

    public function getStatusValue(){
        $statusData = User::getStatusData();
        return $statusData[$this->owner->status];
    }


    public function getRoleValue(){
        $roleData = User::getRoleData();
        return $this->owner->role ? $roleData[$this->owner->role] : null;
    }


    public function getFolderNum($id = NULL)
    {
        $id = $id ? $id : $this->owner->id;
        for ($i = 100; $i < 100000000; $i += 100) {
            if ($id <= $i) return $i;
        }
    }

    /**
     * path: post/{folder}/{id}
     * @param int $id
     * @return string
     */
    public function getImagePath($id = null)
    {
        $id = $id ? $id : $this->owner->id;
        $folder = $this->getFolderNum($id);

        $pathFormat = Yii::$app->params['user']['path'];


        return sprintf($pathFormat, $folder, $id);
    }

    /**
     * path: upload/user/$folder/$id/$image_$size.jpg
     * @var $imageUrl
     * @param string $size 100x100, 300x300
     * @param null $id
     * @param null $image
     * @param bool $getPathOnly
     * @return string
     */
//    public function getImageUrl($size = '100x100', $id = null, $image = null, $getPathOnly = false)
//    {
//        $id = $id ? $id : $this->owner->id;
//        $image = $image ? $image : $this->owner->image;
//
//        $imagePath = $this->getImagePath($id) . "/{$image}_{$size}.jpg";
//        if ($getPathOnly) return $imagePath;
//
//        return Url::base(true) . "/{$imagePath}";
//    }

    /**
     * Avatar picasa thumb
     * @param string $size
     * @return mixed|string
     */
    public function getImageUrl6($size = 'w200-h200-c'){
        if($this->owner->image && substr($this->owner->image, 0, 4) == 'http'){
            return $this->owner->image;
        }
        // lấy ảnh default
        else{
            return Url::base(true).'/files/image/user/avatar-default_50x50.jpg';
        }
    }

    public function getImageUrl($size = '300x300', $id = null, $image = null)
    {
        $id = $id ? $id : $this->owner->id;
        $image = $image ? $image : $this->owner->image;

        if($image){
            if(substr($image, 0, 4) == 'http')
                return $image;
            else
                return STATIC_FILE."/{$this->getImagePath($id)}/{$this->owner->name_alias}_{$size}.jpg";
        }else{
            return STATIC_FILE.'/files/image/user/avatar-default_50x50.jpg';
        }
    }


    public function getUserRouter($path = '/user/user/profile', $params = []){
        $router = [$path];
       /* if($this->owner->username){
            $router['username'] = $this->owner->username;
        }else{
            $router['id'] = $this->owner->id;
            $router['alias'] = $this->owner->name_alias;
        }*/
        $router['id'] = $this->owner->id;
        if($params){
            $router = array_merge($router, $params);
        }

        return Url::toRoute($router);
    }


    /**
     * get user url
     */
    public function getUrl($params = []){

        return $this->getUserRouter('/user/user/profile', $params);
    }
}