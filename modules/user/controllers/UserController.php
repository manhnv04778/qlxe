<?php
/**
 * Controller thực hiện các hành động: đăng nhập, đăng ký, quên mật khẩu, ...
 */

namespace app\modules\user\controllers;

use app\components\AuthAction;
use app\components\rbac\AuthManager;
use app\helpers\AppHelper;
use app\helpers\DateTimeHelper;
use app\helpers\FacebookHelper;
use app\libraries\PicasaClient;
use app\libraries\SendSms;
use app\models\App;
use app\models\Comment;
use app\models\Image;
use app\modules\exam\models\Round;
use app\modules\exam\models\UserRegister;
use app\modules\user\models\forms\ChangePassForm;
use app\modules\user\models\forms\ForgotPasswordForm;
use app\modules\user\models\forms\PasswordResetForm;
use app\modules\user\models\forms\RegisterForm;
use app\modules\user\models\forms\UpdateInfoForm;
use app\modules\user\models\forms\UpdateInfoFormCpvm;
use app\modules\user\models\forms\VerifyForm;
use app\modules\user\models\UserExtra;
use app\modules\user\models\UserSocialFriend;
use Curl\Curl;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\FileHelper;
use app\modules\user\models\User;
use app\modules\user\models\UserEmail;
use app\modules\user\models\UserRequest;
use app\modules\user\models\UserFollowUser;
use WideImage\WideImage;
use Yii;
use app\modules\user\models\forms\LoginForm;
use app\modules\user\models\forms\PasswordResetRequestForm;
use app\modules\user\models\forms\ResetPasswordForm;
use app\modules\user\models\forms\SignupForm;
use yii\base\InvalidParamException;
use yii\helpers\StringHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\user\components\Controller;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use app\modules\user\models\elasticsearch\UserElasticsearch;
use yii\web\Session;

class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'logout' => ['post'],
                    'login2' => ['post']
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'admin',
                    'reset-password', 'reset-password-token', 'register', 'login',
                    'logout', 'update', 'manage-login','password','update-profile', 'step', 'verify','check-forgot-pass','forgot-pass'
                ], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
                    [
                        'actions' => ['reset-password', 'reset-password-token', 'register', 'login','check-forgot-pass','forgot-pass'],
                        'allow' => true,
                        'roles' => ['?'], // chỉ dành cho user chưa đăng nhập
                    ],
                    [
                        'actions' => ['logout', 'update', 'manage-login','password','update-profile', 'step', 'verify'],
                        'allow' => true,
                        'roles' => ['@'], // chỉ dành cho user đã đăng nhập, nếu chưa đăng nhập thì redirect sang trang login.
                    ],
                    [
                        'actions' => ['admin'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action){
                            return Yii::$app->user->can(AuthManager::ROLE_ADMIN);
                        }
                    ],
                ],


            ],
        ];

    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'auth' => [
                'class' => AuthAction::className(),
                'successCallback' => [$this, 'socialSuccessCallback'],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function init(){
        parent::init();

        if(in_array($_SERVER['HTTP_HOST'], ['cp.dev','chinhphucvumon.vn'])){
            $this->layout = '@theme/layouts/cpvm';
        }

    }


    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionAdmin()
    {

        $searchModel = new UserElasticsearch();

        // Check if there is an Editable ajax request
        if (Yii::$app->request->post('hasEditable')) {
            $out = Json::encode(['output'=>'', 'message'=>'']);

            $post = [];
            $posted = current($_POST['UserElasticsearch']);
            $post['UserElasticsearch'] = $posted;

            if ($searchModel->load($post)) {
                $id = Yii::$app->request->post('editableKey');

                if ($id && isset($posted['name'])) {
                    User::updateAll([
                        'name' => $searchModel->name,
                        'name_alias' => StringHelper::toSEOString($searchModel->name)
                    ], ['id' => $id]);
                    UserElasticsearch::updateAll([
                        'name' => $searchModel->name,
                        'name_alias' => StringHelper::toSEOString($searchModel->name)
                    ], ['id' => $id]);
                }
            }
            echo $out;
            return;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Đăng nhập bằng google|facebook. Hoặc add thêm account google|facebook để login
     * @param \yii\authclient\clients\Facebook $client
     * @throws \yii\base\Exception
     */
    public function socialSuccessCallback($client)
    {

        $returnUrl = Yii::$app->user->returnUrl != '/' ? Yii::$app->user->returnUrl : Yii::$app->request->referrer;
//        $returnUrl = Url::to('/user/site/well-come');

        Yii::$app->user->setReturnUrl($returnUrl);

        if(!$this->user){ // Login

            $this->_socialLogin($client);

        }else{ // Add more social account to login
//            Yii::$app->user->setReturnUrl(Yii::$app->request->referrer);

            $this->_socialAdd($client);
        }
    }

    private function _socialLogin($client){

        $avatar = null;
        $avatarSave = false;

        if ($client->id == 'facebook') {
            $attributes = $client->api('me?fields=id,name,email,birthday,gender');

            $avatar = "http://graph.facebook.com/{$attributes['id']}/picture?width=300&height=300";
            $userEmail = UserEmail::find()->where([
                'social_type' => 'facebook',
                'social_id' => $attributes['id'],
            ])->with('user')->one();

            if ($userEmail) {
                $user = $userEmail->user;
            }
            else {

                $email = !empty($attributes['email']) ? $attributes['email'] : null;

                // if email is exists then merge email with social
                if($email) {
                    $userEmail = UserEmail::find()->where([
                        'email' => $email,
                    ])->with('user')->one();
                }

//                echo "\n\n<pre>";print_r($email);echo "</pre>\n\n";
//                echo "\n\n<pre>";print_r($userEmail);echo "</pre>\n\n";
//                die;


                if($userEmail){
                    $user = $userEmail->user;

                    // email dang ky binh thuong
                    if(!$userEmail->social_type){
                        $userEmail->social_type = 'facebook';
                        $userEmail->social_id = $attributes['id'];
                        $userEmail->social_token = $client->accessToken->token;
                        $userEmail->update();

                    // email chua link to facebook, hoac app user id invalid
                    }else{
                        if($userEmail->social_type == 'facebook'){
                            $userEmail->social_id = $attributes['id'];
                            $userEmail->social_token = $client->accessToken->token;
                            $userEmail->update();
                        }else{
                            $userEmail = new UserEmail();
                            $userEmail->user_id = $user->id;
                            $userEmail->email = $email;
                            $userEmail->social_type = 'facebook';
                            $userEmail->social_id = $attributes['id'];
                            $userEmail->social_token = $client->accessToken->token;
                            $userEmail->insert();
                        }
                    }
                }
                else {

                    $user = User::find()->where(['email' => $email])->one();
                    if(!$user){
                        $user = new User();
                        $user->name = !empty($attributes['name']) ? $attributes['name'] : 'user_'.substr(time(), -6);
                        $user->gender = $attributes['gender'];
                        $user->email = !empty($attributes['email']) ? $attributes['email'] : null;
                        $user->birthday = !empty($attributes['birthday']) ? DateTimeHelper::getDateTime($attributes['birthday'], 'Y-m-d') : null;                    $user->username = $user->email ? preg_replace('/^(.+)@.+$/', '$1', $user->email) : null;
                        if ($user->username && $user->existsUsername()) $user->username = null;
                        $user->name_alias = StringHelper::toSEOString($user->name);
                        $user->image = $avatar;
                        $user->insert();
                    }

                    $userEmail = new UserEmail();
                    $userEmail->user_id = $user->id;
                    $userEmail->email = $email;
                    $userEmail->social_type = 'facebook';
                    $userEmail->social_id = $attributes['id'];

                    $userEmail->social_token = $client->accessToken->token;
                    $userEmail->is_main = 1;
                    $userEmail->insert();

                    $avatarSave = true;

                    //$user->sendEmailRegisterSuccessful();
                }

            }
// neu viet kieu nay thi ko kick hoat duoc after insert
//            $sqlValues = implode(',', $sqlValueArray);
//            $sql = "INSERT IGNORE INTO user_social_friend (social_type, social_id, social_friend_id, social_friend_name, user_id) VALUES {$sqlValues}";
//            Yii::$app->db->createCommand($sql)->execute();

        }
        elseif ($client->id == 'google') {

            $attributes = $client->getUserAttributes();

//            $data = $client->api('/people/me/people/visible');
//            echo "<pre>"; print_r($data); echo "</pre>"; die;
            $avatar = !empty($attributes['image']['url']) ? preg_replace('{\?sz=\d+}', '', $attributes['image']['url']) : null;

            $userEmail = UserEmail::find()->where([
                'social_type' => 'google',
                'social_id' => $attributes['id'],
            ])->with('user')->one();

            if ($userEmail) {
                $user = $userEmail->user;
            }
            else {

                $email = !empty($attributes['emails'][0]['value']) ? $attributes['emails'][0]['value'] : null;

                // if email is exists then merge email with social
                if($email) {
                    $userEmail = UserEmail::find()->where([
                        'email' => $email,
                    ])->with('user')->one();
                }

                if($userEmail){
                    $user = $userEmail->user;

                    // email dang ky binh thuong
                    if(!$userEmail->social_type){
                        $userEmail->social_type = 'google';
                        $userEmail->social_id = $attributes['id'];
                        $userEmail->social_token = $client->accessToken->token;
                        $userEmail->update();

                    // email chua link to facebook, hoac app user id invalid
                    }else{
                        if($userEmail->social_type == 'google'){
                            $userEmail->social_id = $attributes['id'];
                            $userEmail->social_token = $client->accessToken->token;
                            $userEmail->update();
                        }else{
                            $userEmail = new UserEmail();
                            $userEmail->user_id = $user->id;
                            $userEmail->email = $email;
                            $userEmail->social_type = 'google';
                            $userEmail->social_id = $attributes['id'];
                            $userEmail->social_token = $client->accessToken->token;
                            $userEmail->insert();
                        }
                    }
                }

                else {

                    $user = new User();
                    $user->gender = !empty($attributes['gender']) ? $attributes['gender'] : null;
                    $user->email = $email;
                    $user->username = $user->email ? preg_replace('/^(.+)@.+$/', '$1', $user->email) : null;
                    if ($user->username && $user->existsUsername()) $user->username = null;
                    $user->name = !empty($attributes['displayName']) ? $attributes['displayName'] : 'user_'.substr(time(), -6);
                    $user->name_alias = StringHelper::toSEOString($user->name);
                    $user->image = $avatar;
                    $user->insert();

                    $userEmail = new UserEmail();
                    $userEmail->user_id = $user->id;
                    $userEmail->email = $user->email;
                    $userEmail->social_type = 'google';
                    $userEmail->social_id = $attributes['id'];
                    $userEmail->social_token = $client->accessToken->token;
                    $userEmail->is_main = 1;
                    $userEmail->insert();

                }

            }


        }


        $user->access_token = Yii::$app->security->generateRandomString();
        $user->update();
        Yii::$app->user->login($user, Yii::$app->params['user']['rememberLogin']);

        $redirectUrl = Yii::$app->user->returnUrl;

        Yii::$app->user->setReturnUrl($redirectUrl);

    }

    private function _socialAdd($client){
        $attributes = $client->getUserAttributes();

        $user = &$this->user;

        $avatar = null;
        if ($client->id == 'facebook') {
            $avatar = "http://graph.facebook.com/{$attributes['id']}/picture?type=large";

            $userEmail = UserEmail::find()->where([
                'social_type' => 'facebook',
                'social_id' => $attributes['id'],
            ])->with('user')->one();

            if ($userEmail) {
                if($userEmail->user_id == $user->id) Yii::$app->session->setFlash('danger', Yii::t('user', "Tài khoản {social}: {email} đã được thêm rồi", ['social' => 'Facebook', 'email' => $userEmail->email]));
                else Yii::$app->session->setFlash('danger', Yii::t('user', "Tài khoản {social}: {email} đã được sử dụng bởi tài khoản khác", ['social' => 'Facebook', 'email' => $userEmail->email]));
            }
            else {

                $email = !empty($attributes['email']) ? $attributes['email'] : null;

                // if email is exists then merge email with social
                if($email) {
                    $userEmail = UserEmail::find()->where([
                        'email' => $email,
                    ])->with('user')->one();
                }

                if($userEmail){
                    // email dang ky binh thuong
                    if(!$userEmail->social_type){
                        $userEmail->social_type = 'facebook';
                        $userEmail->social_id = $attributes['id'];
                        $userEmail->social_token = $client->accessToken->token;
                        $userEmail->update();

                    // email chua link to facebook
                    }else{
                        $userEmail = new UserEmail();
                        $userEmail->user_id = $user->id;
                        $userEmail->email = $email;
                        $userEmail->social_type = 'facebook';
                        $userEmail->social_id = $attributes['id'];
                        $userEmail->social_token = $client->accessToken->token;
                        $userEmail->insert();
                    }
                }else{
                    $userEmail = new UserEmail();
                    $userEmail->user_id = $user->id;
                    $userEmail->email = $email;
                    $userEmail->social_type = 'facebook';
                    $userEmail->social_id = $attributes['id'];
                    $userEmail->social_token = $client->accessToken->token;
                    $userEmail->insert();
                }


                $socialFriendIds = UserSocialFriend::find()->select('social_friend_id')
                    ->where(['social_id' => $attributes['id']])
                    ->column();

                // lấy danh sách friends cùng join app
                $data = $client->api('/me/friends?limit=5000');
                foreach ($data['data'] as $fbUser) {

                    if(in_array($fbUser['id'], $socialFriendIds)) continue;

                    $usf = new UserSocialFriend();
                    $usf->social_type = 'facebook';
                    $usf->social_id = $attributes['id'];
                    $usf->social_friend_id = $fbUser['id'];
                    $usf->social_friend_name = $fbUser['name'];
                    $usf->user_id = $user->id;
                    $usf->insert();

                    $socialFriendIds[] = $fbUser['id'];
                }

                Yii::$app->session->setFlash('success', Yii::t('user', 'Thêm tài khoản {social}: {email} thành công', ['social' => 'Facebook', 'email' => $userEmail->email]));

                $userEmail->sendEmailAddSuccessful();
            }



        }
        elseif ($client->id == 'google') {
//            $data = $client->api('/people/me/people/visible');
//            echo "<pre>"; print_r($data); echo "</pre>"; die;
            $avatar = !empty($attributes['image']['url']) ? preg_replace('{\?sz=\d+}', '', $attributes['image']['url']) : null;

            $userEmail = UserEmail::find()->where([
                'social_type' => 'google',
                'social_id' => $attributes['id'],
            ])->with('user')->one();


            if ($userEmail) {

                if($userEmail->user_id == $user->id) Yii::$app->session->setFlash('danger', Yii::t('user', "Tài khoản {social}: {email} đã được thêm rồi", ['social' => 'Google', 'email' => $userEmail->email]));
                else Yii::$app->session->setFlash('danger', Yii::t('user', "Tài khoản {social}: {email} đã được sử dụng bởi tài khoản khác", ['social' => 'Google', 'email' => $userEmail->email]));

            }
            else {

                $email = !empty($attributes['emails'][0]['value']) ? $attributes['emails'][0]['value'] : null;

                // if email is exists then merge email with social
                if($email) {
                    $userEmail = UserEmail::find()->where([
                        'email' => $email,
                    ])->with('user')->one();
                }

                if($userEmail){
                    // email dang ky binh thuong
                    if(!$userEmail->social_type){
                        $userEmail->social_type = 'google';
                        $userEmail->social_id = $attributes['id'];
                        $userEmail->social_token = $client->accessToken->token;
                        $userEmail->update();

                        // email chua link to facebook
                    }else{
                        $userEmail = new UserEmail();
                        $userEmail->user_id = $user->id;
                        $userEmail->email = $email;
                        $userEmail->social_type = 'google';
                        $userEmail->social_id = $attributes['id'];
                        $userEmail->social_token = $client->accessToken->token;
                        $userEmail->insert();
                    }
                }else{
                    $userEmail = new UserEmail();
                    $userEmail->user_id = $user->id;
                    $userEmail->email = $email;
                    $userEmail->social_type = 'google';
                    $userEmail->social_id = $attributes['id'];
                    $userEmail->social_token = $client->accessToken->token;
                    $userEmail->insert();
                }

                Yii::$app->session->setFlash('success', Yii::t('user', 'Thêm tài khoản {social}: {email} thành công', ['social' => 'Google', 'email' => $userEmail->email]));

                $userEmail->sendEmailAddSuccessful();


            }

        }

    }




    /**
     * Trang đăng ký
     * @return string|\yii\web\Response
     */
    public function actionRegister()
    {
        $registerForm = new RegisterForm();
        $registerForm->scenario = 'create';


        if ($post = Yii::$app->request->post()) {
            $registerForm->load($post);

            if ($registerForm->validate()) {

                $mobile = $registerForm->phone ? $registerForm->phone : null;
                $curl = new Curl();
                $data = $curl->get('http://ids.egame.vn/services/api/users/register.php?u='.$registerForm->username.'&p='.$registerForm->password.'&m='.$mobile);
                $curl->close();



                //$session = Yii::$app->session;
                //$session->set('idstk',$data->v->t);


                $user = new User();
                $user->username = $registerForm->username;
                $user->phone = $registerForm->phone ? $registerForm->phone : null;
                $user->verified = 0;
                $user->name = 'user_'.substr(time(), -6);

                //echo '<pre>'; print_r($data); echo '</pre>';

                if($data->s == true){
                    $user->ids_id = $data->v->u->id;
                    $user->ids_token = $data->v->t;
                }

                //echo '<pre>'; print_r($user); echo '</pre>';die;
                //$user->ids_id = $data->v->u->id;
                //$user->ids_token = $data->v->t;
                $user->setPassword($registerForm->password);

                if($user->validate()){
                    $user->insert();
                    Yii::$app->getUser()->login($user, Yii::$app->params['user']['rememberLogin']);
                    // send mail
                    //$registerForm->sendEmail($user, $userEmail);
                    return $this->redirect(['/user/user/update-profile']);
                }


            }
        }

        return $this->render('register', [
            'registerForm' => $registerForm,
        ]);
    }

    /**
     * Trang đăng nhập
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        // set return url to redirect after login successfully~


        Yii::$app->user->setReturnUrl(Yii::$app->user->returnUrl != '/' ? Yii::$app->user->returnUrl : Yii::$app->request->referrer);
        $model = new LoginForm();

        if($post = Yii::$app->request->post()){

            $model->load($post);
            if ($model->login()) {
                Yii::$app->session->setFlash('success', Yii::t('user', 'Login successful'));
                return $this->goBack();
            }
        }


        return $this->render('login', [
            'model' => $model,
        ]);

    }
    /**
     * Thoát
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        Yii::$app->session->setFlash('warning', Yii::t('user', 'Logout successful'));

        return $this->goHome();
    }

    /**
     * Cập nhật thông tin thành viên
     * @param null $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id = null, $username = null)
    {
        $this->layout = '@theme/modules/user/site/layout_manage';

        $this->sidebarChildSelected = 'user-profile';

        /* @var $model User */
        if($id && $this->user->id != $id){
            $model = User::findOne(['id' => $id]);
            if(!$model) throw new NotFoundHttpException();
        }
        elseif($username && $this->user->username != $username){
            $model = User::findOne(['username' => $username]);
            if(!$model) throw new NotFoundHttpException();
        }else{
            $model = Yii::$app->user->identity;
        }

        // if update other user, must check permission
        if($this->user->id != $model->id){
            $b = Yii::$app->user->can(AuthManager::PERMISSION_UPDATE_USER);
            if(!$b) throw new ForbiddenHttpException(Yii::t('user', "You can't update other user"));
        }


        // image for view: saved image url or post data
        $imageView = $model->image_picasa ? AppHelper::getImagePicasa($model->image_picasa) : null;

        if ($post = Yii::$app->request->post()) {
            $model->load($post);

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = 'json';
                return \yii\widgets\ActiveForm::validate($model);
            }

            $model->name_alias = StringHelper::toSEOString($model->name);
            $model->birthday = DateTimeHelper::getDateTime($model->birthday, 'Y-m-d');

            if ($model->image_upload) $imageView = $model->image_upload; // imageView = post data

            if ($model->validate()) {

                // save cover image
                if($model->image_upload) {
                    $imageUpload = trim($model->image_upload);

                    if (substr($imageUpload, 0, 4) == 'http') {
                        $imageSource = $imageUpload;
                    }else{
                        $fileData = FileHelper::getDataFileInfo($imageUpload);
                        $imageSource = 'md5:'.md5($fileData['data']);
                    }

                    // kiểm tra cover đã được upload trước đó chưa, nếu rồi thì ko up nữa
                    $images = Image::find()->where(['user_photo_id' => $model->id])->all();
                    $image = null;
                    if($images){
                        foreach($images as $_image){
                            if($_image->source == $imageSource){
                                $image = $_image;
                                break;
                            }

                            $_imageUploadPathInfo = AppHelper::getPicasaPathInfo($imageUpload);
                            if(substr($imageUpload, 0, 4) == 'http' && $_imageUploadPathInfo && $_image->image_picasa){
                                $_imagePicasaPathInfo = AppHelper::getPicasaPathInfo($_image->image_picasa);

                                if($_imagePicasaPathInfo['key'] == $_imageUploadPathInfo['key']){
                                    $image = $_image;
                                }
                            }
                        }
                    }
                    // chưa có thì up
                    if(!$image){

                        $imageAttributes = [
                            'user_id' => $this->user->id,
                            'user_photo_id' => $model->id,
                            'source' => $imageSource,
                        ];
                        $image = Image::uploadPicasa($imageAttributes, $imageUpload, $model->name_alias);
                    }

                    $model->image_id = $image->id;
                    $model->image_picasa = AppHelper::getImagePicasa($image->image_picasa);
                }

                $model->update();


                Yii::$app->session->setFlash('success', Yii::t('user', 'Update Profile successful'));


                return $this->refresh();

            }

        }

        // parse birth day to vietnamese date
        $model->birthday = DateTimeHelper::getDateTime($model->birthday, 'd-m-Y');

        return $this->render('//modules/user/site/update', [
            'model' => $model,
            'imageView' => $imageView,
        ]);

    }

    /**
     * Quản lý email và các tài khoản social để đăng nhập
     * @param null $action remove|main|request
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionManageLogin($action = null, $id = null)
    {

        $this->layout = '@theme/modules/user/site/layout_manage';
        $this->sidebarChildSelected = 'user-login';

        // remove|change main email|request to verify email
        if($action)  {

            if(!Yii::$app->request->isPost || !$id){
                throw new BadRequestHttpException('Bad request');
            }


            /* @var $userEmail \app\modules\user\models\UserEmail */
            $userEmail = UserEmail::findOne($id);

            if(!$userEmail || $userEmail->user_id != $this->user->id){
                throw new BadRequestHttpException('Bad request');
            }


            if($action == 'remove'){
                $userEmail->delete();
                Yii::$app->session->setFlash('success', Yii::t('user', 'Remove email successful'));

            }
            elseif($action == 'main'){
                UserEmail::updateAll(['is_main' => 0], ['user_id' => $userEmail->user_id]);
                $userEmail->updateAttributes(['is_main' => 1]);
                Yii::$app->session->setFlash('success', Yii::t('user', 'Change main email successful'));

            }
            elseif($action == 'request' && !$userEmail->verified){

                $userEmail->generateVerifiedToken();
                $userEmail->updateAttributes(['verified_token' => $userEmail->verified_token]);

                Yii::$app->mailer->compose('verifiedEmailToken', ['user' => $this->user, 'userEmail' => $userEmail])
                    ->setFrom([Yii::$app->params['support']['email'] => Yii::$app->name . ' robot'])
                    ->setTo($userEmail->email)
                    ->setSubject('Verify email for ' . Yii::$app->name)
                    ->send();


                Yii::$app->session->setFlash('success', Yii::t('user', 'Gửi yêu cầu xác nhận email thành công. Hãy kiểm tra email và xác nhận để hoàn thành'));

            }

            return $this->redirect('/'.Yii::$app->request->pathInfo);
        }




        $addEmailForm = new \app\modules\user\models\forms\AddEmailForm();

        $user = &$this->user;
        $userEmails = $user->userEmails;


        if ($post = Yii::$app->request->post()) {

            $addEmailForm->load($post);

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = 'json';
                return \yii\widgets\ActiveForm::validate($addEmailForm);
            }

            if ($addEmailForm->validate()) {

                $userEmail = new UserEmail();
                $userEmail->user_id = $this->user->id;
                $userEmail->email = $addEmailForm->email;
                $userEmail->is_main = 0;
                $userEmail->verified = 0;
                $userEmail->generateVerifiedToken();
                $userEmail->insert();

                $addEmailForm->sendEmail($this->user, $userEmail);

                Yii::$app->session->setFlash('success', Yii::t('user', 'Thêm email thành công. Hãy kiểm tra email và xác nhận để hoàn thành'));


                return $this->refresh();
            }

        }



        return $this->render('//modules/user/site/manageLogin', [
            'addEmailForm' => $addEmailForm,
            'user' => $user,
            'userEmails' => $userEmails,
        ]);

    }

    /**
     * Create new password or Change password
     */
    public function actionPassword()
    {
        $this->layout = '@theme/modules/user/site/layout_manage';
        $this->sidebarChildSelected = 'user-password';

        /* @var $user \app\modules\user\models\User */
        $user = Yii::$app->user->identity;

        $passwordForm = new \app\modules\user\models\forms\PasswordForm();
        if ($user->password_hash) $passwordForm->scenario = 'update';


        if ($post = Yii::$app->request->post()) {

            $passwordForm->load($post);


            if ($passwordForm->validate()) {


                $user->setPassword($passwordForm->passwordNew);


                $user->update();

                if ($passwordForm->scenario == 'update')
                    Yii::$app->session->setFlash('success', Yii::t('user', 'Mật khẩu của bạn đã được cập nhật thành công'));
                else
                    Yii::$app->session->setFlash('success', Yii::t('user', 'Mật khẩu của bạn đã được tạo thành công'));


                return $this->refresh();
            }


        }

        return $this->render('//modules/user/site/password', [
            'passwordForm' => $passwordForm,
        ]);

    }

    /**
     * Form nhập email để nhận mail có link reset mật khẩu
     */
    public function actionResetPassword()
    {
        $this->sidebarChildSelected = 'user-password';

        $resetForm = new \app\modules\user\models\forms\PasswordResetForm();

        if ($post = Yii::$app->request->post()) {

            $resetForm->load($post);

            if ($resetForm->validate()) {

                if ($resetForm->sendEmail()) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Kiểm tra hòm thư của bạn và tiến hành làm mới mật khẩu'));
                } else {
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Lỗi! Hệ thống không gửi email tới bạn được. Bạn hãy kiểm tra lại email hoặc liên hệ lại với chúng tôi'));
                }

                return $this->refresh();
            }

        }

        return $this->render('passwordReset', [
            'resetForm' => $resetForm,
        ]);

    }

    /**
     * Khi người dùng click vào link xác nhận reset được nhân bằng email thì đăng nhập và redirect sang trang tạo mật khẩu.
     * @param $token
     * @throws \Exception
     */
    public function actionResetPasswordToken($token)
    {

        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Token cannot be blank.');
        }

        if (!User::isPasswordResetTokenValid($token)) {
            throw new InvalidParamException('Invalid token.');
        }

        /* @var $user \app\modules\user\models\User */
        $user = User::findByPasswordResetToken($token);
        if (!$user) {
            throw new InvalidParamException('Wrong token.');
        }

        $user->password_reset_token = null;
        $user->password_hash = null;
        $user->update();

        Yii::$app->user->login($user, \Yii::$app->params['user']['rememberLogin']);

        Yii::$app->getSession()->setFlash('success', 'Hãy đặt lại mật khẩu mới cho tài khoản của bạn');

        return $this->redirect('/user/user/password');

    }

    /**
     * Khi người dùng click vào link verified email được nhân bằng email thì đăng nhập và redirect sang trang manage-login.
     * @param $token
     * @throws \Exception
     */
    public function actionEmailVerifiedToken($token)
    {

        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Token sai');
        }

        if (!UserEmail::isVerifiedTokenValid($token)) {
            throw new InvalidParamException('Token này đã hết hạn');
        }


        /** @var  $user User*/
        $user = User::find()->where(['verify_token' => $token])->one();

        if(!$user){
            throw new InvalidParamException('Token sai');
        }
        // after execution: mysql trigger auto update user.email
        $user->verify_token = null;
        $user->verified = 1;
        $user->update();


        if(!$this->user){
            Yii::$app->user->login($user, \Yii::$app->params['user']['rememberLogin']);
        }

        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Xác nhận email thành công'));

        return $this->redirect(Url::base(true));

    }

    public function actionVerifyPhone($type = 'pupil'){
        $this->layout = '@theme/layouts/topbar_landing';

        if(!$this->user){

            return $this->redirect('/');
        }
        return $this->render('verify-phone', [
            'type' => $type
        ]);
    }



    /**
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionProfile($username = null, $id = null, $type = 'feed')
    {
        $this->layout = '@theme/layouts/front_end';

        $userCondition = [];
        if($username) {
            $userCondition = ['username' => $username];
        } elseif($id){
            $userCondition = ['id' => $id];
        } else {
            throw new BadRequestHttpException();
        }

        $user = User::find()
            ->where($userCondition)
            ->one();

        $dataProvider_friend = null;
        $dataProvider_waiting = null;
        $dataProvider_request = null;

        $query_friend = UserFollowUser::find()
            ->where([
                'is_friend' => '1',
                'follow_user_id' => $id
            ])
            ->orWhere([
                'is_friend' => '1',
                'user_id' => $id
            ])
            ->orderBy('created desc');

        $is_search = '';
        if($post = Yii::$app->request->post()) {
            $user_login = Yii::$app->controller->user;
            if (!empty($_POST['name_friend']) && empty($user_login)) {
                if($id) {
                    $query_friend = User::find()
                        ->select('*')
                        ->innerJoin('user_follow_user', 'user_follow_user.follow_user_id = user.id')
                        ->where(['user_follow_user.user_id' => $id, 'user_follow_user.is_friend' => 1])
                        ->andWhere(['like', 'full_name', $_POST['name_friend']])
                        ->with('userFollow');
                }else{
                    $query_friend = User::find()
                        ->select('*')
                        ->innerJoin('user_follow_user', 'user_follow_user.user_id = user.id')
                        ->where(['user_follow_user.follow_user_id' => $user->id, 'user_follow_user.is_friend' => 1])
                        ->andWhere(['like', 'full_name', $_POST['name_friend']])
                        ->with('userFollow');
                }

                $is_search = 'search';
            }elseif(!empty($_POST['name_friend']) && !empty($user_login)){
                if($id == Yii::$app->controller->user->id){
                    $query_friend = User::find()
                        ->select('*')
                        ->innerJoin('user_follow_user', 'user_follow_user.user_id = user.id')
                        ->where(['user_follow_user.follow_user_id' => $id, 'user_follow_user.is_friend' => 1])
                        ->andWhere(['like', 'full_name', $_POST['name_friend']])
                        ->with('userFollow');
                }
                $is_search = 'search';
            }
        }

        $dataProvider_friend = new ActiveDataProvider([
            'query' => $query_friend,
            'pagination' => [
                'defaultPageSize' => 8,
            ]
        ]);

        //echo '<pre>'; print_r($dataProvider_friend->models); echo '</pre>';die;




        $friend_check['process_user'] = '';
        $friend_check['process_user_follow'] = '';
        $friend_check['done_user'] = '';
        $friend_check['done_user_follow'] = '';

        if($this->user && $this->user->id == $id){



            $query_waiting = UserRequest::find()
                ->where([
                    'user_id' => $id,
                    'activity' => 'friend'
                ])
                ->orderBy('created desc');

            $dataProvider_waiting = new ActiveDataProvider([
                'query' => $query_waiting,
                'pagination' => [
                    'defaultPageSize' => 8,
                ]
            ]);

            $query_request = UserRequest::find()
                ->where([
                    'follow_user_id' => $id,
                    'activity' => 'friend'
                ])
                ->orderBy('created desc');

            $dataProvider_request = new ActiveDataProvider([
                'query' => $query_request,
                'pagination' => [
                    'defaultPageSize' => 8,
                ]
            ]);
        }else{
            $friend_check['process'] = '';
            $friend_check['done'] = ''; 
        }

        if($this->user){
            $friend_check['process_user'] = UserRequest::find()->where([
                'user_id' => $this->user->id,
                'follow_user_id' => $user->id,
                'activity' => 'friend',
            ])->one();



            $friend_check['process_user_follow'] = UserRequest::find()->where([
                'user_id' => $user->id,
                'follow_user_id' => $this->user->id,
                'activity' => 'friend',
            ])->one();

            $friend_check['done_user'] = UserFollowUser::find()->where([
                'user_id' => $this->user->id,
                'follow_user_id' => $user->id,
                'is_friend' => 1
            ])->one();

            $friend_check['done_user_follow'] = UserFollowUser::find()->where([
                'user_id' => $user->id,
                'follow_user_id' => $this->user->id,
                'is_friend' => 1,
            ])->one();
        }

        $apps = App::find()
            ->where([
                    'state'=>'live'
                ])
            ->orderBy(new Expression('rand()'))
            ->limit(8)->all();


        return $this->render('profile', [
            'user' => $user,
            'apps' => $apps,
            'friend_check' => $friend_check,
            'dataProvider_friend' => $dataProvider_friend,
            'dataProvider_waiting' => $dataProvider_waiting,
            'dataProvider_request' => $dataProvider_request,
            'id' => $id,
            'is_search' => $is_search
        ]);

    }

    public function actionUpdateProfile(){



        /** @var  $model User*/
        $model = $this->user;
        if(!$model) throw new ForbiddenHttpException();

        if($model->is_lock == 1){
            return $this->render('_require_lock');
        }
        if(in_array($_SERVER['HTTP_HOST'], ['cp.dev', 'chinhphucvumon.vn'])){
            $model_comment = new Comment();
            $userExtra = $model->userExtra;
            $updateFormCpvm = new UpdateInfoFormCpvm();
            $updateFormCpvm->type = 'pupil';
            $updateFormCpvm->attributes = $userExtra->attributes;
            $updateFormCpvm->full_name = $model->full_name;
            $updateFormCpvm->phone = $model->phone;
            $updateFormCpvm->birthday = $model->birthday;
            $round = Round::getRound();

            if($post = Yii::$app->request->post()) {
                $updateFormCpvm->load($post);

                if($updateFormCpvm->validate()){
                    $model->attributes = $updateFormCpvm->attributes;

                    $userRC = UserRegister::find()->where([
                        'birthday' => $updateFormCpvm->birthday,
                        'school_id' => $updateFormCpvm->school_id,
                        'class_id' => $updateFormCpvm->class_id
                    ])->all();
                    $flag = true;
                    if($userRC){
                        foreach($userRC as $uRC){
                            if($uRC->full_name === $model->full_name){
                                $flag = false;
                                break;
                            }
                        }
                    }
                    if(!$flag){
                        return $this->render('_require_register');
                    }

                    $model->update();


                    $userExtra->attributes = $updateFormCpvm->attributes;
                    $userExtra->type = 'pupil';
                    $userExtra->update();



                    /** @var  $userRegister UserRegister*/
                    $userRegister = UserRegister::find()->where(['user_id' => $model->id])->one();
                    if(!$userRegister){
                        $userRegister = new UserRegister();
                        $userRegister->attributes = $userExtra->attributes;
                        $userRegister->user_name = $model->username;
                        $userRegister->full_name = $model->full_name;
                        $userRegister->ids_id = $model->ids_id;
                        $userRegister->address = $model->address;
                        $userRegister->region = $userExtra->city->region;
                        $userRegister->class_id = $userExtra->class_id;
                        $userRegister->school_id = $userExtra->school_id;
                        $userRegister->level = $userExtra->class->level;
                        $userRegister->round = $round->id;
                        $userRegister->birthday = $updateFormCpvm->birthday;
                        $dateCode = $userRegister->birthday ? DateTimeHelper::getDateTime($userRegister->birthday,'y') : '00';
                        $userRegister->code = $userExtra->city->code.$userExtra->district->code.$userExtra->school->code.$dateCode.str_pad($userExtra->school->user_reg_exam + 1, 3, '0', STR_PAD_LEFT);
                        if($userRegister->validate()){
                            $userRegister->insert();
                        }
                    }else{
                        $userRegister->attributes = $userExtra->attributes;
                        $userRegister->user_name = $model->username;
                        $userRegister->full_name = $model->full_name;
                        $userRegister->ids_id = $model->ids_id;
                        $userRegister->address = $model->address;
                        $userRegister->region = $userExtra->city->region;
                        $userRegister->class_id = $userExtra->class_id;
                        $userRegister->school_id = $userExtra->school_id;
                        $userRegister->level = $userExtra->class->level;
                        $userRegister->round = $round->id;
                        $dateCode = $userRegister->birthday ? DateTimeHelper::getDateTime($userRegister->birthday,'y') : '00';
                        $userRegister->code = $userExtra->city->code.$userExtra->district->code.$userExtra->school->code.$dateCode.str_pad($userExtra->school->user_reg_exam + 1, 3, '0', STR_PAD_LEFT);
                        if($userRegister->validate()){
                            $userRegister->update();
                        }

                    }
                    return $this->render('accept_info');
                }


            }

            return $this->render('update_profile_cpvm', [
                'updateFormCpvm' => $updateFormCpvm,
                'model_comment' => $model_comment
            ]);

        }else{
            $model->scenario = 'update';

            $imageView = $model->image ? $model->imageUrl : '';
            $model->birthday = $model->birthday ? DateTimeHelper::getDateTime($model->birthday,'d-m-Y') : null;

            if($post = Yii::$app->request->post()) {

                $model->load($post);

                $model->name_alias = $model->name ? StringHelper::toSEOString($model->name) : 'user';
                $model->birthday = DateTimeHelper::getDateTime($model->birthday, 'Y-m-d');

                if ($model->validate()) {
                    if ($model->image_upload) {
                        $imageUpload = trim($model->image_upload);
                        $savedPath = $model->getImagePath();
                        FileHelper::createDirectory($savedPath);
                        $wideImageLoad = WideImage::load($imageUpload);

                        foreach(Yii::$app->params['user']['thumbs'] as $size => $body){
                            $wideImageLoad = $wideImageLoad->crop('center','center',$body['w'],$body['h']);
                            $imagePath = $savedPath . "/{$model->name_alias}_{$size}.jpg";
                            $model->image = "{$model->name_alias}_{$size}";
                            $wideImageLoad->saveToFile($imagePath);
                        }

                    }
                    //$model->complete = 'step';
                    $model->update();
                    return $this->redirect(Url::toRoute(['/user/user/step']));
                }
            }

            return $this->render('update_profile', [
                'model' => $model,
                'imageView' => $imageView
            ]);
        }

    }


    public function actionStep(){


        /** @var  $user User*/
        $user = $this->user;
        $session = new Session;
        $session->open();
        if(!$user) throw new ForbiddenHttpException();

        if($user->is_lock == 1){
            return $this->render('_require_lock');
        }

        /** @var  $userExtra UserExtra*/
        $userExtra = $user->userExtra;
        $model_comment = new Comment();
        $model = new UpdateInfoForm();
        $data = $userExtra->attributes;

        $model->attributes = $data;

        if(in_array($_SERVER['HTTP_HOST'], ['cp.dev', 'chinhphucvumon.vn'])){
            $model->type = 'pupil';
            $model->scenario = 'step';
        }

        if($post = Yii::$app->request->post()) {
            $params = Yii::$app->request->queryParams;
            $model->load($post);
            if($model->type == 'pupil'){
                $model->scenario = 'step';
            }
            if($model->validate()){

                $userExtra->attributes = $model->attributes;

                $userExtra->school_id = !empty($model->school_id) ? $model->school_id : NULL;


                $userRC = UserRegister::find()->where([
                    'birthday' => $user->birthday,
                    'school_id' => $model->school_id,
                    'class_id' => $model->class_id
                ])->all();
                $flag = true;
                if($userRC){
                    foreach($userRC as $uRC){
                        if($uRC->full_name === $user->full_name){
                            $flag = false;
                            break;
                        }
                    }
                }
                if(!$flag){
                    return $this->render('_require_register');
                }

                $userExtra->created = DateTimeHelper::getDateTime($model->created, 'Y-m-d');

                $userExtra->update();

                $user->complete = 'complete';

                if($session['accept_info']){
                    $user->complete = 'success';
                    $user->is_lock = 1;
                    $user->time_lock = '2017-08-01 00:00:00';
                    $session->remove('accept_info');
                }

                $user->update();

                if($model->type == 'pupil'){
                    /** @var  $userRegister UserRegister*/
                    $userRegister = UserRegister::find()->where(['user_id' => $user->id])->one();
                    if(!$userRegister){

                        $round = Round::getRound();

                        $userRegister = new UserRegister();
                        $userRegister->user_id = $user->id;
                        $userRegister->user_name = $user->username;
                        $userRegister->full_name = $user->full_name;
                        $userRegister->ids_id = $user->ids_id;
                        $userRegister->address = $user->address;
                        $userRegister->city_id = $model->city_id;
                        $userRegister->district_id = $model->district_id;
                        $userRegister->region = $userExtra->city->region;
                        $userRegister->class_id = $userExtra->class_id;
                        $userRegister->school_id = $userExtra->school_id;
                        $userRegister->level = $userExtra->class->level;
                        $userRegister->phone = $user->phone ? $user->phone : NULL;
                        $userRegister->email = $user->email ? $user->email : NULL;
                        $userRegister->birthday = $user->birthday ? $user->birthday : NULL;
                        $userRegister->round = $round->id;

                        $dateCode = $userRegister->birthday ? DateTimeHelper::getDateTime($userRegister->birthday,'y') : '00';
                        $userRegister->code = $userExtra->city->code.$userExtra->district->code.$userExtra->school->code.$dateCode.str_pad($userExtra->school->user_reg_exam + 1, 3, '0', STR_PAD_LEFT);
                        if($userRegister->validate()){
                            $userRegister->insert();
                        }
                    }else{
                        $userRegister->user_id = $user->id;
                        $userRegister->user_name = $user->username;
                        $userRegister->full_name = $user->full_name;
                        $userRegister->ids_id = $user->ids_id;
                        $userRegister->address = $user->address;
                        $userRegister->city_id = $model->city_id;
                        $userRegister->district_id = $model->district_id;
                        $userRegister->region = $userExtra->city->region;
                        $userRegister->class_id = $userExtra->class_id;
                        $userRegister->school_id = $userExtra->school_id;
                        $userRegister->level = $userExtra->class->level;
                        $userRegister->phone = $user->phone ? $user->phone : NULL;
                        $userRegister->email = $user->email ? $user->email : NULL;
                        $userRegister->birthday = $user->birthday ? $user->birthday : NULL;
                        $userRegister->update();
                    }
                }
                echo "<script>var show_accept = true;var show_modal = false</script>";
                return $this->render('//modules/exam/exam/require_exam',['state' => 'require-accept','url' => Url::base(true)]);
            }
        }

        $params['Comment'] = !empty($params['Comment']) ? $params['Comment'] : [];
        return $this->render('step', [
            'model' => $model,
            'model_comment' => $model_comment,
            'params' => $params['Comment']
        ]);
    }

    public function actionSendComment(){
        Yii::$app->user->setReturnUrl(Yii::$app->user->returnUrl != '/' ? Yii::$app->user->returnUrl : Yii::$app->request->referrer);
        $user_name = Yii::$app->controller->user->username;
        $model = new Comment();
        $model->status = 'new';
        $model->created = DateTimeHelper::getDateTime();
        $model->user_name = $user_name;
        if($post = Yii::$app->request->post()){
            $model->load($post);
            if($model->validate()){
                $model->insert();

                return $this->goBack();
            }
        }
        return $this->goBack();
    }

    public function actionSendError(){
        //Yii::$app->user->setReturnUrl(Yii::$app->user->returnUrl != '/' ? Yii::$app->user->returnUrl : Yii::$app->request->referrer);
        $user_name = Yii::$app->controller->user->username;
        $model = new Comment();
        $model->status = 'new';
        $model->created = DateTimeHelper::getDateTime();
        $model->user_name = $user_name;
        $model->type = 'error';
        if($post = Yii::$app->request->post()){
//            echo '<pre>'; print_r($post); echo '</pre>';die;
            $model->load($post);
            if($model->validate()){
                $model->insert();
                return $this->render('success_sendError');
            }
        }
        return $this->render('success_sendError');
    }

    public function actionFriend($id){

        $query_friend = UserFollowUser::find()
            ->where([
                'is_friend' => '1',
                'follow_user_id' => $id
            ])
            ->orWhere([
                'is_friend' => '1',
                'user_id' => $id
            ])
            ->orderBy('created desc');

        $dataProvider_friend = new ActiveDataProvider([
            'query' => $query_friend,
            'pagination' => [
                'defaultPageSize' => 20,
            ]
        ]);

        //echo '<pre>'; print_r($dataProvider_friend->query); echo '</pre>';die;

        $query_waiting = UserRequest::find()
            ->where([
                'follow_user_id' => $id,
                'activity' => 'friend'
            ])
            ->orderBy('created desc');

        $dataProvider_waiting = new ActiveDataProvider([
            'query' => $query_waiting,
            'pagination' => [
                'defaultPageSize' => 20,
            ]
        ]);

        $query_search = UserFollowUser::find()
            ->where([
                'is_friend' => '1',
                'follow_user_id' => $id
            ])
            ->orWhere([
                'is_friend' => '1',
                'user_id' => $id
            ])
            ->orderBy('created desc');

        $is_search = '';
        if($post = Yii::$app->request->post()) {
            if (!empty($_POST['name_friend'])) {
                $query_search = User::find()
                    ->with(['userFollow' => function ($query) use ($id) {
                        $query->where([
                            'user_id' => $id,
                            'is_friend' => 1
                        ]);
                    }])
                    ->where(['like', 'full_name', $_POST['name_friend']])
                    ->orderBy('created desc');
                $is_search = 'search';
            }
        }

        $dataProvider_search = new ActiveDataProvider([
            'query' => $query_search,
            'pagination' => [
                'defaultPageSize' => 20,
            ]
        ]);


        return $this->render('friend',[
            'dataProvider_friend' => $dataProvider_friend,
            'dataProvider_waiting' => $dataProvider_waiting,
            'dataProvider_search' => $dataProvider_search,
            'is_search' => $is_search,
            'id' => $id
        ]);
    }

    public function actionDeleteFriend(){
        $id_friend = $_POST['id_friend'];
        $id_user = $_POST['id_user'];
        $model = UserFollowUser::find()
            ->where([
                'user_id' => $id_user,
                'follow_user_id' => $id_friend
            ])
            ->one();
        if($model){
            UserFollowUser::deleteAll(['user_id' => $id_user,'follow_user_id' => $id_friend]);
            $user_extra = UserExtra::find()
                ->where([
                    'user_id' => $id_user
                ])
                ->one();

            if($user_extra->friends > 0){
                $user_extra->friends--;
                $user_extra->update();
            }

            echo json_encode(['stt' => true]);
        }
    }

    public function actionVerify()
    {

        $user = $this->user;
        $verifyForm = new VerifyForm();

        $changePassForm = new ChangePassForm();

        if($post = Yii::$app->request->post()){
            $changePassForm->load($post);
            if($changePassForm->validate()){
                $user = $changePassForm->getUser();

                if(!empty($user->ids_id)){

                    //http://ids.egame.vn/services/api/users/changepass.php?p=duymanh&id=4435890
                    $curl = new Curl();
                    $data = $curl->get('http://ids.egame.vn/services/api/users/changepass.php?p='.$changePassForm->passNew.'&id='.$user->ids_id);
                    //echo '<pre>'; print_r("dsdsjhdsjkds"); echo '</pre>';die;
                    $curl->close();
                    //echo '<pre>'; print_r($data); echo '</pre>';die;
                }else{
                    $curl = new Curl();
                    $data = $curl->get('http://ids.egame.vn/services/api/users/register.php?u='.$user->username.'&p='.$changePassForm->passNew);
                    $curl->close();
                    //echo '<pre>'; print_r($data); echo '</pre>';die;
                    if($data->s == true){
                        $user->ids_id = $data->v->u->id;
                        $user->ids_token = $data->v->t;
                    }
                }

                $user->setPassword($changePassForm->passNew);
                $user->update();

                Yii::$app->user->login($user,Yii::$app->params['user']['rememberLogin']);
                Yii::$app->session->setFlash('success', Yii::t('user', "Thay đổi mật khẩu thành công"));

                return $this->refresh();


            }else{
                echo '<pre>'; print_r($changePassForm->errors); echo '</pre>';die;
            }
        }

//        if($post = Yii::$app->request->post()){
//            $changePassForm->load($post);
//            if($changePassForm->validate()){
//                $user = $changePassForm->getUser();
//
//                $user->setPassword($changePassForm->passNew);
//                $user->update();
//
//                Yii::$app->user->login($user,Yii::$app->params['user']['rememberLogin']);
//                Yii::$app->session->setFlash('success', Yii::t('user', "Thay đổi mật khẩu thành công"));
//
//                return $this->refresh();
//
//
//            }else{
//                echo '<pre>'; print_r($changePassForm->errors); echo '</pre>';die;
//            }
//        }



        return $this->render('verify',[
            'verifyForm' => $verifyForm,
            'user' => $user,
            'changePassForm' => $changePassForm
        ]);
    }

    public function actionChangePass()
    {
        $changePassForm = new ChangePassForm();

        if($post = Yii::$app->request->post()){
            $changePassForm->load($post);
            if($changePassForm->validate()){
                $user = $changePassForm->getUser();

                if(!empty($user->ids_id)){

                    //http://ids.egame.vn/services/api/users/changepass.php?p=duymanh&id=4435890
                    $curl = new Curl();
                    $data = $curl->get('http://ids.egame.vn/services/api/users/changepass.php?p='.$changePassForm->passNew.'&id='.$user->ids_id);
                    $curl->close();
                }else{
                    $curl = new Curl();
                    $data = $curl->get('http://ids.egame.vn/services/api/users/register.php?u='.$user->username.'&p='.$changePassForm->passNew);
                    $curl->close();
                    if($data->s == true){
                        $user->ids_id = $data->v->u->id;
                        $user->ids_token = $data->v->t;
                    }
                }

                $user->setPassword($changePassForm->passNew);
                $user->update();

                Yii::$app->user->login($user,Yii::$app->params['user']['rememberLogin']);
                Yii::$app->session->setFlash('success', Yii::t('user', "Thay đổi mật khẩu thành công"));

                return $this->refresh();


            }else{
                echo '<pre>'; print_r($changePassForm->errors); echo '</pre>';die;
            }
        }

        return $this->render('change_pass', [
            'changePassForm' => $changePassForm
        ]);
    }

    public function actionSuccess(){
        return $this->render('success');
    }


    public function actionCheckForgotPass()
    {

        $passwordReset = new PasswordResetForm();


        if($post = Yii::$app->request->post()){
            $username = strip_tags($post['user_name']);

            /** @var  $user User*/
            $user = User::findOne(['username' => $username]);
            if(!$user){
                $data['stt'] = false;
                $data['message'] = 'Tên đăng nhập không tồn tại';
            }else{
                if($user->verified == 0 || empty($user->phone)){
                    $data['stt'] = false;
                    $data['message'] = 'Tài khoản chưa được xác nhận số điện thoại';
                }else{
                    $time = strtotime(DateTimeHelper::getDateTime()) - strtotime($user->verify_expire);
                    if(!empty($user->code) && $time < 1800){
                        $data['stt'] = true;
                        $data['message'] = 'Mã xác nhận đã được gửi tới số điện thoại '.$user->phone.' trước đó.<br>Để nhận mã mới bạn phải đợi trong vòng 30 phút!';
                    }else{
                        $user->code = substr( md5(rand()), 0, 5);
                        $user->verify_expire = DateTimeHelper::modifyDateTime('now', '+30 minute');
                        $user->update();
                        $data['stt'] = true;
                        $data['message'] = 'Mã xác nhận đã được gửi tới số điện thoại '.$user->phone.'.<br>Nhập mã xác nhận bạn vừa nhận được để lấy lại mật khẩu!';
                        $message = 'Ma xac nhan tai khoan cua ban tai egame.vn la: '.$user->code;
                        $sendSms = new SendSms();
                        $sendSms->sendSms($message, $user->phone);
                    }
                }
            }

            echo json_encode($data);
            return;
        }

        return $this->render('check_forgot',[
            'passwordReset' => $passwordReset
        ]);
    }


    public function actionForgotPass($u)
    {
        if($this->user){
            return $this->redirect(Url::base(true));
        }

        $model = User::findByUsername($u);


        $forgotPassForm = new ForgotPasswordForm();
        $forgotPassForm->scenario = 'fogot';
        $forgotPassForm->user_name = $u;
        if($post = Yii::$app->request->post()){
            $forgotPassForm->load($post);

            if($forgotPassForm->validate()){
                $user = $forgotPassForm->getUser();
                $user->code = NULL;
                $user->verify_expire = NULL;
                $user->setPassword($forgotPassForm->passwordNew);
                $user->update();

                if(!empty($user->ids_id)){
                    $curl = new Curl();
                    $curl->get('http://ids.egame.vn/services/api/users/changepass.php?p='.$forgotPassForm->passwordNew.'&id='.$user->ids_id);
                    $curl->close();
                }

                Yii::$app->session->setFlash('success', Yii::t('user', 'Đổi mật khẩu thành công'));
                return $this->redirect(Url::toRoute(['/user/user/login']));

            }
        }


        return $this->render('forgot_pass',[
            'forgotPassForm' => $forgotPassForm,
            'model' => $model
        ]);
    }
}
