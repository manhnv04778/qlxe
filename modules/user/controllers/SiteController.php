<?php
/**
 * Controller thực hiện các hành động: đăng nhập, đăng ký, quên mật khẩu, ...
 */

namespace app\modules\user\controllers;

use app\helpers\BaoKimHelper;
use app\helpers\DateTimeHelper;
use app\helpers\ElephantHelper;
use app\models\Comment;
use app\models\geo\City;
use app\modules\question\models\Question;
use app\modules\user\models\UserExtra;
use app\modules\user\models\Conversation;
use app\modules\user\models\ConversationHasUser;
use app\modules\user\models\ConversationNotice;
use app\modules\user\models\forms\RegisterForm;
use app\modules\user\models\UserActivity;
use app\modules\user\models\UserFeed;
use app\modules\user\models\UserFollowUser;
use app\modules\user\models\UserSocialFriend;
use app\modules\user\models\UserTransaction;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use app\modules\user\models\User;
use app\modules\user\models\UserEmail;
use WideImage\WideImage;
use Yii;
use app\modules\user\models\forms\LoginForm;
use yii\base\InvalidParamException;
use yii\helpers\StringHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\user\components\Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use app\modules\question\models\QuestionAnswer;
use yii\db\ActiveQuery;
use yii\web\UnauthorizedHttpException;
use app\modules\question\models\QuestionUserTag;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//				'only' => ['logout', 'login', 'update'],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'], // Allow guest only
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'], // Allow authenticated only
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }



    /**
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionIndex($username = null, $id = null, $type = 'feed')
    {
//        $this->layout = '@theme/layouts/topbar';

        $userCondition = [];
        if($username) {
            $userCondition = ['username' => $username];
        } elseif($id){
            $userCondition = ['id' => $id];
        } else {
            throw new BadRequestHttpException();
        }

        $user = User::find()
            ->where($userCondition)
            ->with(['userExtra'])
            ->one();

        if($type == 'feed'){

            $query = UserFeed::find()->where([
                'user_id' => $user->id,
            ])
            ->orderBy(['created' => SORT_DESC]);
        }

        if($type == 'question'){
            $query = Question::find()
                ->where([
                    'user_id' => $user->id
                ])
                ->orderBy(['created' => SORT_DESC]);
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => Yii::$app->params['user']['pageSize'],
            ],
        ]);

        return $this->render('index', [
            'user' => $user,
            'type' => $type,
            'dataProvider' => $dataProvider
        ]);

    }


    public function actionTransaction(){
        if(!$this->user) throw new UnauthorizedHttpException('Bạn chưa đăng nhập');

//        $query = UserTransaction::find()
//            ->where([
//                'user_id' => $this->user->id
//            ])
//            ->orderBy([
//                'created' => SORT_ASC
//            ]);
//
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//            'pagination' => [
//                'defaultPageSize' => Yii::$app->params['user']['pageSize'],
//            ],
//        ]);

        $model = new UserTransaction();

        $queryParams = Yii::$app->request->queryParams;

        $dataProvider = $model->search($queryParams);


        return $this->render('transaction',[
            'searchModel' => $model,
            'dataProvider' => $dataProvider
        ]);
    }


    /**
     * @author: thoaivan
     */
    public function actionWellCome(){

        if($this->user->phone_verified == 1){
            if($this->user->type == null || $this->user->type == 'pupil'){

                return $this->redirect('/');

            } elseif($this->user->type == 'teacher'){

                // TODO: sau khi làm xong trang dashboard thì về trang dashboard của chuyên gia.
                return $this->redirect('/');

            }
        }
        return $this->render('well-come', [

        ]);
    }

    public function actionTestSms(){

//
//        $userFeed = UserFeed::find()
//            ->where([
//                'user_id' => 28773
//            ])
//            ->one();
//
//        $receiverIds = [
//            28773 => 1552
//        ];
//
//
//        ElephantHelper::notification($userFeed, $receiverIds);
//
//        die;

//        $user = $this->user;
//        $userExtra = $user->userExtra;

//
//        $bamKimSms = new BaoKimHelper();
//
//
//        $content = 'Mã số kích hoạt tài khoản tại egroup.com là: 123456';
//        $bamKimSms->sendSMS('0947704720', $content);
//
//        echo "<pre>";print_r('test send sms');echo "</pre>"; die;
    }




}
