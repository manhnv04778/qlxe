<?php
/**
 * Controller thực hiện các hành động: đăng nhập, đăng ký, quên mật khẩu, ...
 */

namespace app\modules\user\controllers;

use app\helpers\BaoKimHelper;
use app\helpers\DateTimeHelper;
use app\helpers\ElephantHelper;
use app\libraries\SendSms;
use app\models\geo\City;
use app\modules\exam\models\School;
use app\modules\question\models\Question;
use app\modules\user\models\UserExtra;
use app\modules\user\models\Conversation;
use app\modules\user\models\ConversationHasUser;
use app\modules\user\models\ConversationNotice;
use app\modules\user\models\forms\RegisterForm;
use app\modules\user\models\UserActivity;
use app\modules\user\models\UserFeed;
use app\modules\user\models\UserFollowUser;
use app\modules\user\models\UserSocialFriend;
use app\modules\user\models\UserTransaction;
use app\modules\user\models\UserRequest;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use app\modules\user\models\User;
use app\modules\user\models\UserEmail;
use WideImage\WideImage;
use Yii;
use app\modules\user\models\forms\LoginForm;
use yii\base\InvalidParamException;
use yii\helpers\StringHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\user\components\Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use app\modules\question\models\QuestionAnswer;
use yii\db\ActiveQuery;
use yii\web\UnauthorizedHttpException;
use app\modules\question\models\QuestionUserTag;

/**
 * Site controller
 */
class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//				'only' => ['logout', 'login', 'update'],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'], // Allow guest only
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'], // Allow authenticated only
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actionAutocomplete()
    {
        $q = Yii::$app->request->getQueryParam('q');
        $user = User::find()
            ->where(['like', 'user_name', $q])
            ->all();
        $data = [];
        if($user){

            foreach ($user as $item) {
                $data[] = [
                    "id" => $item->id,
                    "key"  => $item->user_name,
                    "text" => $item->user_name,
                ];
            }
        }

        echo json_encode($data);
    }


}
