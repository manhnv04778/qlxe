<?php
return [
    'user' => [
        'rememberLogin' => 3600*24*365*10,
        'path' => "upload/user/%d/%d", // upload/post/$folder/$id/image/file-name_WxH.jpg
        'thumbs' => [
            '500x500'   => ['scale' => 'any',  'quality' => 90, 'w' => '500', 'h' => 500],
            '300x300'   => ['scale' => 'any',  'quality' => 90, 'w' => '300', 'h' => '300'],
        ],
        'emailVerifiedExpire' => 3600*24,
    ],
];