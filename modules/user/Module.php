<?php

namespace app\modules\user;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\user\controllers';

	public function init()
	{
		parent::init();

        // load module config
        \Yii::configure(\Yii::$app, require(__DIR__ . '/config/config.php'));

	}
}
