<?php
use yii\helpers\Url;

/* @var $this app\components\View */
/* @var $user app\modules\user\models\User */
$user = &Yii::$app->controller->user;
?>
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; background: #0082d0; color:white;">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand text-white" href="index.html">Hệ thống quản lý sử dụng xe công</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <?php if($user):?>
        <li class="dropdown">
            <a class="dropdown-toggle text-white" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <?=$user->user_name?> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="<?=Url::toRoute(['/admin/admin/logout'])?>" data-method="post"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <?php endif;?>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <!--Trang chủ-->
                <li><a href="<?=Url::toRoute(['/frontend/document'])?>"><i class="fa fa-bar-chart-o fa-fw"></i> Trang chủ</a></li>
                <!--Vai trò-->
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý vai trò<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=Url::toRoute(['/admin/role/create'])?>">Thêm vai trò</a>
                        </li>
                        <li>
                            <a href="<?=Url::toRoute(['/admin/role/add-user'])?>">Cấp vai trò cho user</a>
                        </li>
                        <li>
                            <a href="<?=Url::toRoute(['/admin/role/index'])?>">Danh sách vai trò</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>

                <!--Người dùng-->
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý user<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=Url::toRoute(['/admin/user/create']) ?>">Thêm người dùng</a>
                        </li>
                        <li>
                            <a href="<?=Url::toRoute(['/admin/user/index']) ?>">Danh sách người dùng</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>

                <!--Tài xế-->
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý lái xe<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=Url::toRoute(['/admin/driver/create']) ?>">Thêm lái xe</a>
                        </li>
                        <li>
                            <a href="<?=Url::toRoute(['/admin/driver/index']) ?>">Danh sách lái xe</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>

                <!--Đơn vị-->
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý Đơn vị<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=Url::toRoute(['/admin/department/create'])?>">Thêm đơn vị</a>
                        </li>
                        <li>
                            <a href="<?=Url::toRoute(['/admin/department/index'])?>">Danh sách đơn vị</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>

                <!--Chức danh-->
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý Chức Danh<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=Url::toRoute(['/admin/position/create'])?>">Thêm chức danh</a>
                        </li>
                        <li>
                            <a href="<?=Url::toRoute(['/admin/position/index'])?>">Danh sách chức danh</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>

                <!--Xe-->
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý Xe<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=Url::toRoute(['/admin/car/create']) ?>">Thêm mới xe</a>
                        </li>
                        <li>
                            <a href="<?=Url::toRoute(['/admin/car/index']) ?>">Danh sách xe</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>

                <!--Đăng kiểm-->
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý đăng kiểm xe<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=Url::toRoute(['/admin/register/create']) ?>">Thêm mới</a>
                        </li>
                        <li>
                            <a href="<?=Url::toRoute(['/admin/register/index']) ?>">Danh sách</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>

                <!--Quá trình bảo dưỡng-->
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý quá trình bảo dưỡng<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=Url::toRoute(['/admin/car-log/create']) ?>">Thêm quá trình bảo dưỡng</a>
                        </li>
                        <li>
                            <a href="<?=Url::toRoute(['/admin/car-log/index']) ?>">Danh sách bảo dưỡng</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>

                <!--Đăng ký sử dụng xe-->
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý đăng ký sử dụng xe<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=Url::toRoute(['/admin/document/index']) ?>">Danh sách  đơn</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>

                <!--Sử dụng nhiên liệu-->
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý sử dụng nhiên liệu<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=Url::toRoute(['/admin/combustible/create']) ?>">Thêm mới</a>
                        </li>
                        <li>
                            <a href="<?=Url::toRoute(['/admin/combustible/index']) ?>">Quá trình sử dụng nhiên liệu</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
