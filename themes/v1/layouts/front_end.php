<?php
/* @var $this \yii\web\View */

use app\components\assets\AppAsset;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

\app\components\assets\AppAsset::register($this);
$this->registerJsFile('https://www.google.com/recaptcha/api.js', ['depends' => [AppAsset::className()]]);

?>

<?php $this->beginContent('@theme/layouts/root.php'); ?>
    <body class="hold-transition skin-fb layout-top-nav">
<!--    <?/*= $this->render("//modules/user/user/_loginModal")*/?>
    --><?/*= $this->render("//modules/user/user/_registerModal")*/?>

    <div id="modal-future" class="modal fade modal-confirm" role="dialog" data-toggle="modal">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content radius0">
                <div class="modal-header padding0">
                    <h4 class="modal-title bg-blue text-upper text-white padding10">Thông báo</h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-20 margintop20 marginbottom20">Tính năng sắp ra mắt</h3>
                </div>
            </div>
        </div>
    </div>

    <?php $this->beginBody() ?>

    <div class="loader"></div>

    <div class="wrapper">
        <?=$this->render('front_end/header');?>
        <div class="content-wrapper paddingbottom200">
            <div class="padding0">
                <section class="content-header">
                    <?=\app\widgets\Alert::widget(['options' => ['class' => 'marginbottom0']]) ?>
                    <?=\app\widgets\FlashAction::widget()?>
                </section>
                <?= $content ?>
            </div>

        </div>
        <!--/contact-map-->
        <!--modal contact-->

        <?php echo $this->render('_footer');?>
    </div>
    <?php $this->endBody() ?>
    </body>
<?php $this->endContent(); ?>
