<?php
/* @var $this \yii\web\View */

\app\modules\admin\components\AppAsset::register($this);

?>
<?php $this->beginContent('@theme/layouts/root.php'); ?>
<body class="hold-transition skin-fb sidebar-mini">

<?php //$this->render("//modules/user/user/_loginModal")?>

<?php $this->beginBody() ?>

<div id="loading">
    <img src="/files/image/loading1.gif">
</div>
<div class="loader"></div>
<div class="wrapper">
    <?=$this->render('sidebar_admin/header');?>
    <div class="content">
        <section class="content-header">
            <?=\app\widgets\Alert::widget(['options' => ['class' => 'marginbottom0']]) ?>
            <?=\app\widgets\FlashAction::widget()?>
        </section>

        <div id="page-wrapper">
            <?= $content ?>
        </div>

    </div>
</div>
<?php $this->endBody() ?>
</body>
<?php $this->endContent(); ?>
