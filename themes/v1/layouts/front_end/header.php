<?php
    use yii\helpers\Url;
    use app\helpers\DateTimeHelper;

    /* @var $this app\components\View */
    /* @var $user app\modules\user\models\User */


    //echo "<pre>";print_r($user);echo "</pre>"; die;

    $user = Yii::$app->controller->user;
    $controller = Yii::$app->controller->id;
    $filter = "";
    $filter = Yii::$app->getRequest()->getQueryParam('filter');
    $user_add_friend = Yii::$app->controller->user_add_friend;
    $countNoty = Yii::$app->controller->countNoty;
    $notyIds = Yii::$app->controller->notyIds;

    $keyword = !empty(Yii::$app->controller->keywordSearch) ? Yii::$app->controller->keywordSearch : '';
?>
<header class="main-header header header--fixed hide-from-print headroom headroom--not-bottom headroom--not-top headroom--pinned">
        <nav class="navbar navbar-static-top marginbottom0">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="navbar-header">

                        </div>
                        <ul class="nav navbar-nav navbar-primary">
                            <li><a href="<?=Url::base(true)?>" class=""><i class="<?=$controller == 'site' ? 'icon-home-active' : 'icon-home'?>"></i></a></li>
                            <li></i><a href="<?=Url::toRoute(['/frontend/document/create'])?>"> Tạo đăng ký xe</a></li>
                            <li><!--<i class="icon-app">--></i><a href="<?=Url::toRoute(['/frontend/document'])?>"> Danh sách hồ sơ</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?php if($user):?>
                                <?php if(Yii::$app->authManager->checkAccess($user->id,'quan-tri')):?>
                                <li class="dropdown paddingtop15">
                                    <span><a class="dropdown-toggle text-white" href="<?=Url::toRoute(['/admin/site/'])?>" data-method="post">Quản trị</a></span>
                                </li>
                                <?php endif;?>
                                <li class="dropdown paddingtop15">
                                    <span class="text-white"><i class="fa fa-user fa-fw"></i>Chào <?=$user->full_name?>, </span>
                                    <span><a class="dropdown-toggle text-white" href="<?=Url::toRoute(['/frontend/site/logout'])?>" data-method="post">Thoát</a></span>
                                </li>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </header>


