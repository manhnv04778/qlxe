<?php
/* @var $this \yii\web\View */

use app\components\assets\AppAsset;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

\app\components\assets\AppAsset::register($this);
$this->registerJsFile('https://www.google.com/recaptcha/api.js', ['depends' => [AppAsset::className()]]);

?>

<?php $this->beginContent('@theme/layouts/root.php'); ?>
<body class="hold-transition skin-fb layout-top-nav" style="background: #08295f;">
<?php $this->beginBody() ?>
<div class="wrapper">
    <div class="container paddingbottom200">
        <div class="padding0">
            <section class="content-header">
                <?=\app\widgets\Alert::widget(['options' => ['class' => 'marginbottom0']]) ?>
                <?=\app\widgets\FlashAction::widget()?>
            </section>
            <?= $content ?>
        </div>

    </div>
</div>
<?php $this->endBody() ?>
</body>
<?php $this->endContent(); ?>
