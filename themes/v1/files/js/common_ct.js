

var csrfToken = $('meta[name="csrf-token"]').attr("content");

$(function () {

    $("[id^=pjax-]").on("pjax:start", function() {
        $("#ajax-loading").fadeIn();
    });
    $("[id^=pjax-]").on("pjax:end", function() {
        $("#ajax-loading").fadeOut();
    });


    //$('.logout').click(function(e){
    //    e.preventDefault();
    //    socket.emit('logout', user);
    //
    //})

    // ajax update viewed of conversation, user-feed

    $('body').on('click', '#dropdown-friend', function(e){

        var element = $(this);
        var ids = $(this).attr('data-ids');
        if (ids == '') return;
        $.post('/user/ajax/update-add-friend', {ids: ids}, function () {
            element.attr('data-ids', '');
            $('#dropdown-friend .noti-box').html('');
        });
    });



});

$(window).scroll(function(){
    if($(window).scrollTop() >= 400) {
        $('.button_scroll2top').show();
    } else {
        $('.button_scroll2top').hide();
    }
});

function page_scroll2top(){
    $('html,body').animate({
        scrollTop: 0
    }, 500);
}


$("#btn-up-down" ).click(function() {
    var action = $( "#btn-up-down" ).attr( "class" );
    if(action =='closed'){
        $(".address-footer").slideDown();
        $("#icon-up-down").attr("class",'glyphicon glyphicon-menu-down text-blue text-18');
        $(".footer-menu").attr("class", 'footer-menu col-md-8  margintop20')
        $("#btn-up-down" ).attr( "class", 'open');
        $('html,body').animate({
            scrollTop: 4000
        }, 2000);
    }
    else{
        $(".address-footer").slideUp('slow');
        $("#icon-up-down").attr("class",'glyphicon glyphicon-menu-up text-blue text-18');
        $(".footer-menu").attr("class", 'footer-menu col-md-8 marginleft-100 margintop-6')
        $("#btn-up-down" ).attr( "class", 'closed');
    }
});

$(".submit-comment" ).click(function(e) {

    var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    var txtPhone = $('#comment-phone').val();
    if(txtPhone.length > 0) {
        if (txtPhone.match(phoneno)) {
            e.preventDefault();
            var dataArray = $("#form-comment").serializeArray(),
                dataObj = {};
            console.dir(dataArray); //require firebug

            $(dataArray).each(function (i, field) {
                dataObj[field.name] = field.value;
            });

            var recaptcha = (dataObj['g-recaptcha-response']);
            if (recaptcha == "") {
                $('.captcha-error').html('Bạn chưa chọn captcha!');
                return false;
            } else {
                $('.captcha-error').html('');
                $("#form-comment").submit();
            }
            return true;
        }
        else {
            $('.phone-error').html('Số điện thoại không đúng định dạng');
            return false;
        }
    }else{
        e.preventDefault();
        var dataArray = $("#form-comment").serializeArray(),
            dataObj = {};
        console.dir(dataArray); //require firebug

        $(dataArray).each(function (i, field) {
            dataObj[field.name] = field.value;
        });

        var recaptcha = (dataObj['g-recaptcha-response']);
        if (recaptcha == "") {
            $('.captcha-error').html('Bạn chưa chọn captcha!');
            return false;
        } else {
            $('.captcha-error').html('');
            $("#form-comment").submit();
        }
        return true;
    }
});

$('input[name="RegisterForm[username]"]').on("change paste keyup", function() {
    var username = $(this).val();

    if(username == ''){
        $('.field-registerform-username').removeClass('has-error');
        $('input[name="RegisterForm[username]"]').val(username);
        $('.field-registerform-username .help-block').html('');
    }
});

$('input[name="RegisterForm[username]"]').focusout(function(){
    var username = $(this).val();

    $.post('/user/ajax/check-user-name',{username : username},function(data){
        if(data.status == true){
            $('.field-registerform-username').addClass('has-error');
            $('input[name="RegisterForm[username]"]').val(username);
            $('.field-registerform-username .help-block').html('Tên đăng nhập đã tồn tại');
        }else{
            $('.field-registerform-username').removeClass('has-error');
            $('input[name="RegisterForm[username]"]').val(username);
            $('.field-registerform-username .help-block').html('');
        }
    }, 'json')
});

$('body').on('click','#btn-accept', function(e){
    e.preventDefault();
    var uid = $(this).attr('data-id');
    var url = $(this).attr('href');
    $.post('/user/ajax/accept-info', {
        id : uid
    }, function(data){

        if(data.stt == true){
            window.location.href = url;
        }
    }, 'json')

});

$(document).ready(function(){
    $('[data-tooltip="tooltip"]').tooltip();
    $('.tt_large1').tooltip({
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large"></div></div>'
    });
});







