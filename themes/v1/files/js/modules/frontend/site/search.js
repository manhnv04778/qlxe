/**
 * Created by DELL on 9/25/2016.
 */


$(function(){

    $('body').on('click','.sort',function(e){
        e.preventDefault();
        var val = $(this).attr('data-value');
        var text = $(this).text();
        $('input[name=sort]').val(val);
        $('.sort-text').text(text);


        $('#filter').submit();
    });

});