/**
 * Created by DELL on 11/14/2016.
 */

$(function(){
    var count_user_in_doc = 0;
    $('body').on('click', '.view', function(e){
        e.preventDefault();
        $(".loader").show();
        var id = $(this).attr('data-id');
        $.post('/frontend/document/get-document',{id:id}, function(data){
            $('.full_name').html(data.full_name);
            $('.name').html(data.name);
            $('.code').html(data.code);
            $('.department').html(data.department);
            $('.from_name').html(data.from_name);
            $('.number_user').html(data.number_user);
            $('.date_from').html(data.date_from);
            $('.date_to').html(data.date_to);
            $('.date_from_car').html(data.date_from_car);
            $('.date_to_car').html(data.date_to_car);
            $('.purpose_car').html(data.purpose_car);
            $('.number_car').html(data.number_car);
            $('.date_from_user').html(data.date_from_user);
            $('.date_to_user').html(data.date_to_user);
            $('.place_user').html(data.place_user);
            $('.note_document').html(data.note_document);
            $('.created_date').html(data.created_date);
            $('.from').html(data.from);
            $('.hdf-id').val(data.id);
            $("#btn-send").attr("data-id-send",data.id);
            if(data.is_modify){
                $("#btn-send").attr("class","display-none");
            }else{
                $("#btn-send").attr("class","btn bg-blue text-white");
            }

            var str = '<table class="table table-strip"><tr class="bg-blue text-white text-center"><td>Họ và Tên</td><td>Phòng ban</td><td>Chức danh</td><td>Email</td><td>Điện thoại</td></tr>';
            console.log(data);
            $.each(data.data_user, function(id, value){
                str += '<tr class="text-normal text-left">';
                str += '<td>'+value.user_name+'</td><td>'+value.department+'</td><td>'+value.position+'</td><td>'+value.email+'</td><td>'+value.phone+'</td>';
                str += '</tr>';
            });
            str += '</table>';
            $('.list_can_bo').html(str);

            $('#modal-view').modal(function(){
            });
            $(".loader").hide();
        },'json');

    });

    $('body').on('click', '.view-history', function(e){
        e.preventDefault();
        $(".loader").show();
        var id = $(this).attr('data-id');
        $.post('/frontend/document/get-document-history',{id:id}, function(data){
            var str = '<table class="table table-strip"><tr class="bg-blue text-white text-center"><td>STT</td><td>Cán bộ</td><td>Chi tiết</td><td>Thời gian cập nhật</td></tr>';
            var count_up = 0;
            $.each(data.data_history, function(id, value){
                count_up++;
                str += '<tr class="text-normal text-left">';
                str += '<td class="text-center">'+count_up+'</td><td class="text-center">'+value.creater+'</td><td class="text-center">'+value.note+'</td><td class="text-center">'+value.created_date+'</td>';
                str += '</tr>';
            });
            str += '</table>';
            $('.history').html(str);

            $('#modal-view-history').modal(function(){
            });
            $(".loader").hide();
        },'json');

    });

    var count_click_user = 1;

    $('body').on('click', '.update', function(e){
        e.preventDefault();
        $(".loader").show();
        count_click_user = 0;
        var id = $(this).attr('data-id');
        $.post('/frontend/document/get-document-update',{id:id, is_update:0}, function(data){
            $('.full_name').val(data.full_name);
            $('.name').val(data.name);
            $('.code').html(data.code);
            $('.department').val(data.department);
            $('.from_name').val(data.from_name);
            $('#total-round').val(data.lenght);
            $('#document-date_from').val(data.date_from);
            $('#document-date_to').val(data.date_to);
            $('#document-date_from_car').val(data.date_from_car);
            $('#document-date_to_car').val(data.date_to_car);
            $('.purpose_car').val(data.purpose_car);
            $('.number_car').val(data.number_car);
            $('#document-date_from_user').val(data.date_from_user);
            $('#document-date_to_user').val(data.date_to_user);
            $('.place_user').val(data.place_user);
            $('.note_document').val(data.note_document);
            $('#document-lenght').val(data.lenght);
            $('.from').val(data.from);
            $('.hdf-id-update').val(data.id);
            $(".update-model").attr("data-id-update",data.id);
            var str ='';
            $.each(data.user_in_doc, function(key, value){
                str += '<div class="row list-user margintop20">'
                    +'<div class="col-sm-3 paddingleft30 paddingright10"><input type="text" name ="full_name_user[]" class="full_name_update form-control" placeholder="Họ và tên" value="'+value.user_name+'"/></div>'
                    +'<div class="col-sm-2 paddingleft5 paddingright10"><input type="text" name ="department_user[]" class="department_update form-control" placeholder="Đơn vị" value="'+value.department+'"/></div>'
                    +'<div class="col-sm-2 paddingleft5 paddingright5"><input type="text" name ="position_user[]" class="position_update form-control" placeholder="Chức danh" value="'+value.position+'"/></div>'
                    +'<div class="col-sm-2 paddingleft10 paddingright20"><input type="text" name ="phone_user[]" class="phone_update form-control" placeholder="Điện thoại" value="'+value.phone+'"/></div>'
                    +'<div class="col-sm-2 paddingleft0 paddingright20 marginleft-5"><input type="text" name ="email_user[]" class="email_update form-control" placeholder="Email" value="'+value.email+'"/></div>'
                    +'<a id="delete-user" class="text-error form-controlsss text-red text-18" data-id="" href="javascript();"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>'
                    +'</div>';
                count_click_user++;
            });
            str += '<div class="row list-user margintop20">'
                +'<div class="col-sm-3 paddingleft30 paddingright10"><input type="text" name ="full_name_user[]" class="full_name_update form-control" placeholder="Họ và tên" value=""/></div>'
                +'<div class="col-sm-2 paddingleft5 paddingright10"><input type="text" name ="department_user[]" class="department_update form-control" placeholder="Đơn vị" value=""/></div>'
                +'<div class="col-sm-2 paddingleft5 paddingright5"><input type="text" name ="position_user[]" class="position_update form-control" placeholder="Chức danh" value=""/></div>'
                +'<div class="col-sm-2 paddingleft10 paddingright20"><input type="text" name ="phone_user[]" class="phone_update form-control" placeholder="Điện thoại" value=""/></div>'
                +'<div class="col-sm-2 paddingleft0 paddingright20 marginleft-5"><input type="text" name ="email_user[]" class="email_update form-control" placeholder="Email" value=""/></div>'
                +'<a id="delete-user" class="text-error form-controlsss text-red text-18" data-id="" href="javascript();"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>'
                +'</div>';
            $('#document-number_user').val(count_click_user++);
            $("#record-update-user").html('');
            $("#record-update-user").append(str);
            $('.panel-content-location').html('');
            //console.log(data.data_location.districts);
            $.each(data.data_location, function(key, value){
                key++;
                var str_location = '<div class="row marginbottom20 locations" id="location-'+ key +'">'
                    +'<h4 class="text-14 paddingleft15">Điểm xuất phát</h4>'
                    +'<div class="col-sm-3">'
                    +'<select id="city-'+key+'" class="form-control citys" name="citys['+key+']">'
                    +'<option value="">Chọn tỉnh/thành</option>';
                var str_city = "";
                $.each(data.data_city, function(key_city, value_city){
                    if(key_city == value.city_id){
                        str_city+= '<option value="'+key_city+'" selected>'+value_city+'</option>';
                    }else{
                        str_city+= '<option value="'+key_city+'">'+value_city+'</option>';
                    }

                });
                var str_district = "";
                $.each(value.districts, function (key_district, value_district){
                    if(key_district == value.district_id){
                        str_district+= '<option value="'+key_district+'" selected>'+value_district+'</option>';
                    }else{
                        str_district+= '<option value="'+key_district+'">'+value_district+'</option>';
                    }
                });

                str_location += str_city + '</select></div>';
                str_location += '<div class="col-sm-3"><select id="district-'+key+'" class="form-control districts" name="districts['+key+']"><option value="">Chọn quận/huyện</option>'+str_district+'</select></div><div class="col-sm-4"><input type="text" id="address-'+key+'" class="form-control address" name="address['+key+']" value="'+value.address+'"></div></div>';
                $('.panel-content-location').append(str_location);
            });


            $('#modal-update').modal(function(){
            });
            $(".loader").hide();
        },'json');

    });

    $('body').on('click', '#btn-ok', function(e){
        e.preventDefault();
        if(!checkValidate()) {
            return false;
        }else {
            $('#modal-add-userdocument').modal('hide');
            return true;
        }
    });

    $('body').on('click','.update-model', function(e){
        e.preventDefault();
        var id = $(this).attr('data-id-update');
        var full_name = $('#document-full_name').val();
        var name = $('#document-name').val();
        var department = $('#document-department_id').val();
        var from_name = $('#document-from_name').val();
        var number_user = $('#document-number_user').val();
        var date_from = $('#document-date_from').val();
        var date_to = $('#document-date_to').val();
        var date_from_car = $('#document-date_from_car').val();
        var date_to_car = $('#document-date_to_car').val();
        var purpose_car = $('#document-purpose_car').val();
        var number_car = $('#document-number_car').val();
        var date_from_user = $('#document-date_from_user').val();
        var date_to_user = $('#document-date_to_user').val();
        var place_user = $('#document-place_user').val();
        var note_document = $('#document-note_document').val();
        var created = $('#document-created_date').val();
        var from = $('#document-from').val();

        if(!checkValidate_update()) {
            return false;
        }
        $(".loader").show();
        var list_user = [];
        var full_name_user = [];
        var department_user = [];
        var position_user = [];
        var phone_user = [];
        var email_user = [];

        $.each($('#form-document .full_name_update'), function (key, value) {
            var parrent = $(value).parents()[1];
            full_name_user.push($(value).val());
        });
        list_user['full_name'] = full_name_user;

        $.each($('#form-document .department_update'), function (key, value) {
            var parrent = $(value).parents()[1];
            department_user.push($(value).val());
        });
        list_user['department'] = department_user;

        $.each($('#form-document .position_update'), function (key, value) {
            var parrent = $(value).parents()[1];
            position_user.push($(value).val());
        });
        list_user['position'] = position_user;

        $.each($('#form-document .phone_update'), function (key, value) {
            var parrent = $(value).parents()[1];
            phone_user.push($(value).val());
        });
        list_user['phone'] = phone_user;

        $.each($('#form-document .email_update'), function (key, value) {
            var parrent = $(value).parents()[1];
            email_user.push($(value).val());
        });
        list_user['email'] = email_user;

        $.post('/frontend/document/get-document-update', {
            id: id,
            is_update: 1,
            full_name: full_name,
            name: name,
            department_id: department,
            from_name: from_name,
            number_user: number_user,
            date_from: date_from,
            date_to: date_to,
            date_from_car: date_from_car,
            date_to_car: date_to_car,
            purpose_car: purpose_car,
            date_from_user: date_from_user,
            date_to_user: date_to_user,
            place_user: place_user,
            note_document: note_document,
            from: from,
            full_name_user:full_name_user,
            department_user :department_user,
            position_user :position_user,
            phone_user :phone_user,
            email_user :email_user,
        }, function(data){
            $(".loader").hide();
        },'json');

    });

    $('body').on('click','#btn-send', function(e){
        e.preventDefault();
        $(".loader").show();
        var id = $(this).attr('data-id-send');
        var flag = true;
        flag = confirm('Bạn có chắc chắn muốn gửi hồ sơ ?');
        if(flag){
            $.post('/frontend/document/send-document',{id:id}, function(data){
                $(".loader").hide();
            },'json');
        }
    });

    $('body').on('click', '#add-userdocument', function(e){
        e.preventDefault();
        $('#modal-add-userdocument').modal(function(){
        });

    });

    $('body').on('click', '#btn-create-doc', function(e){
        ////alert($('#txt_date_from').val() + ' - ' + $('#txt_date_to').val());
        //var date_from = new Date($('#txt_date_from').val());
        //var date_to = new Date($('#txt_date_to').val());
        //console.log(date_from);
        //return false;
        //if(date_from > date_to){
        //    alert('not ok');
        //    return false;
        //}else{
        //    alert('ok');
        //}

    });

    $('body').on('click', '#close-userdocument', function(e){
        e.preventDefault();

    });

    $('body').on('click', '#btn-ok', function(e){
        e.preventDefault();
        if(!checkValidate()) {
            return false;
        }else {
            $('#modal-add-userdocument').modal('hide');
            $('#document-number_user').val(count_click_user);
            $('#number_user_show').val(count_click_user);
            return true;
        }
    });

    $("#delete-document").confirm();

    //$('body').on('click', '#delete-document', function(e){
    //
    //});



    function checkValidate() {
        var full_name = true;
        var department = true;
        var position = true;
        var phone = true;
        var email = true;

        $.each($('#form-document .full_name'), function (key, value) {
            if ($(value).val() == '' || $(value).val().length < 0) {
                full_name = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
            else{
                $(this).css({
                    border: "1px solid #cccccc"
                });
            }
        });
        $.each($('#form-document .department'), function (key, value) {
            if ($(value).val() == '' || $(value).val().length < 0) {
                department = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
            else{
                $(this).css({
                    border: "1px solid #cccccc"
                });
            }
        });
        $.each($('#form-document .position'), function (key, value) {
            if ($(value).val() == '' || $(value).val().length < 0) {
                position = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
            else{
                $(this).css({
                    border: "1px solid #cccccc"
                });
            }
        });
        $.each($('#form-document .phone'), function (key, value) {
            var flag = validatePhone($(value).val());
            if ($(value).val() == '' || $(value).val().length < 0 || flag == false) {
                phone = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
            else{
                $(this).css({
                    border: "1px solid #cccccc"
                });
            }
        });
        $.each($('#form-document .email'), function (key, value) {
            var flag = validateEmail($(value).val());
            if ($(value).val() == '' || $(value).val().length < 0 || flag == false) {
                email = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
            else{
                $(this).css({
                    border: "1px solid #cccccc"
                });
            }
        });

        return (full_name && department && position && phone && email);
    }

    //function validate field Phone
    function validatePhone(txtPhone){
        var pattern10 = /^\d{10}$/;
        var pattern11 = /^\d{11}$/;
        if(txtPhone.match(pattern10) || txtPhone.match(pattern11)){
            return true;
        }else{
            return false;
        }
    }

    // function validate field Email
    function validateEmail(txtEmail){
        var pattern_Email = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        if(pattern_Email.test(txtEmail)){
            return true;
        }else{
            return false;
        }
    }


    function checkValidate_update() {
        var full_name = true;
        var department = true;
        var position = true;
        var phone = true;
        var email = true;
        // duyệt tất cả các field để check validate
        $.each($('#form-document .full_name_update'), function (key, value) {
            if ($(value).val() == '' || $(value).val().length < 0) {
                full_name = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
            else{
                $(this).css({
                    border: "1px solid #cccccc"
                });
            }
        });
        $.each($('#form-document .department_update'), function (key, value) {
            if ($(value).val() == '' || $(value).val().length < 0) {
                department = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
            else{
                $(this).css({
                    border: "1px solid #cccccc"
                });
            }
        });
        $.each($('#form-document .position_update'), function (key, value) {
            if ($(value).val() == '' || $(value).val().length < 0) {
                posistion = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
            else{
                $(this).css({
                    border: "1px solid #cccccc"
                });
            }
        });
        $.each($('#form-document .phone_update'), function (key, value) {
            var flag = validatePhone($(value).val());
            if ($(value).val() == '' || $(value).val().length < 0 || flag == false) {
                phone = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
            else{
                $(this).css({
                    border: "1px solid #cccccc"
                });
            }
        });
        var pattern_email = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        $.each($('#form-document .email_update'), function (key, value) {
            var flag = validateEmail($(value).val());
            if ($(value).val() == '' || $(value).val().length < 0 || flag == false) {
                email = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
            else{
                $(this).css({
                    border: "1px solid #cccccc"
                });
            }
        });

        return (full_name && department && position && phone && email);
    }

    $('body').on('click', '#add-user', function(e){
        e.preventDefault();
        var str = '<div class="row list-user margintop20">'
            +'<div class="col-sm-3 paddingleft30 paddingright10"><input type="text" name ="full_name_user[]" class="full_name form-control" placeholder="Họ và tên"/></div>'
            +'<div class="col-sm-2 paddingleft5 paddingright10"><input type="text" name ="department_user[]" class="department form-control" placeholder="Đơn vị"/></div>'
            +'<div class="col-sm-2 paddingleft5 paddingright5"><input type="text" name ="position_user[]" class="position form-control" placeholder="Chức danh"/></div>'
            +'<div class="col-sm-2 paddingleft10 paddingright20"><input type="text" name ="phone_user[]" class="phone form-control" placeholder="Điện thoại"/></div>'
            +'<div class="col-sm-2 paddingleft0 paddingright20 marginleft-5"><input type="text" name ="email_user[]" class="email form-control" placeholder="Email"/></div>'
            +'<a id="delete-user" class="text-error form-controlsss text-red text-18" data-id="" href="javascript();"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>'
            +'</div>';
        $("#record-add-user").append(str);
        count_click_user++;
    });

    $('body').on('click', '#update-user', function(e){
        e.preventDefault();
        var str = '<div class="row list-user margintop20">'
            +'<div class="col-sm-3 paddingleft30 paddingright10"><input type="text" name ="full_name_user[]" class="full_name_update form-control" placeholder="Họ và tên"/></div>'
            +'<div class="col-sm-2 paddingleft5 paddingright10"><input type="text" name ="department_user[]" class="department_update form-control" placeholder="Đơn vị"/></div>'
            +'<div class="col-sm-2 paddingleft5 paddingright5"><input type="text" name ="position_user[]" class="position_update form-control" placeholder="Chức danh"/></div>'
            +'<div class="col-sm-2 paddingleft10 paddingright20"><input type="text" name ="phone_user[]" class="phone_update form-control" placeholder="Điện thoại"/></div>'
            +'<div class="col-sm-2 paddingleft0 paddingright20 marginleft-5"><input type="text" name ="email_user[]" class="email_update form-control" placeholder="Email"/></div>'
            +'<a id="delete-user" class="text-error form-controlsss text-red text-18" data-id="" href="javascript();"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>'
            +'</div>';
        $("#record-update-user").append(str);
        count_click_user++;
        $('#document-number_user').val(count_click_user);
    });

    $('body').on('click', '#delete-user', function(e){
        e.preventDefault();
        $(this).parent().remove();
        count_click_user--;
        $('#document-number_user').val(count_click_user);
    });

    $('body').on('click', '#btn-cancel-userdoc', function(e){
        e.preventDefault();

        $('.list-user').remove();
        $('#modal-add-userdocument').modal('hide');
    });

    $("body").on('change','.citys', function (e){
        var city_id = $(this).val();
        var parent = $(this).parent().parent();
        parent.find('.districts').html('sdfdsfd');
        if(city_id){
            $.each(districts[city_id], function (key, value) {
                parent.find('.districts').append('<option value="' + key + '">' + value + '</option>');
            });
        }
    });

    $('body').on('click','#add-location',function(e){
        e.preventDefault();
        var curent_index_location = $('#index-location').val();
        var options = '<option value="">Chọn tỉnh/thành</option>';

        var cityCurent = $('#city-'+curent_index_location).val();
        var districtCurent = $('#district-'+curent_index_location).val();
        var addressCurent = $('#address-'+curent_index_location).val();

        if(cityCurent == '' || districtCurent == '' || addressCurent == ''){
            alert('Bạn chưa chọn đầy đủ thông tin cho điểm đến trước!');
            return false;
        }

        $.each(citys, function(key, value){
            options += '<option value="' + key + '">' + value + '</option>';
        });
        var new_index_location = parseInt(curent_index_location) + 1;

        var html = '<div class="row margintop20 locations" id="location-'+new_index_location+'">'+
                        '<h4 class="text-14 paddingleft15">Điểm đến tiếp theo</h4>'+
                        '<div class="col-sm-3">'+
                            '<select class="form-control citys" name="citys['+new_index_location+']" id="city-'+new_index_location+'">'+ options + '</select>   '+
                        '</div>'+
                        '<div class="col-sm-3">'+
                            '<select class="form-control districts" data-index="'+new_index_location+'" name="districts['+new_index_location+']" id="district-'+new_index_location+'">'+
                                '<option value="">Chọn quận/huyện</option>'+
                            '</select>' +
                        '</div>'+
                        '<div class="col-sm-4">'+
                            '<input type="text" class="form-control address" name="address['+new_index_location+']" placeholder="Địa chỉ" id="address-'+new_index_location+'">'+
                            '<input type="hidden" class="long long-'+ new_index_location +'" name="long['+new_index_location+']">'+
                        '</div>'+
                        '<div class="col-xs-1"><i class="fa fa-close text-error text-20 pointer remove-location"></i></div>'+
                        '</div>';
        $('#index-location').val(new_index_location);
        $('.panel-content-location').append(html);
    });

    $('body').on('click','.remove-location',function(e){
        e.preventDefault();
        var curent_index_location = $('#index-location').val();
        $('#location-'+curent_index_location).remove();
        $('#index-location').val(parseInt(curent_index_location) - 1);
        var total = 0;
        $('.long').each(function(index) {
            total = total + parseFloat($(this).val());
        });
        console.log(total)
        $('#total-round').val(total);

    });

    $('body').on('change','.districts',function(e){
        var curent_index_location = $(this).attr('data-index');
        var index_pre = parseInt(curent_index_location) - 1;
        if($(this).attr('data-index') > 1){
            var district_pre = $('#district-'+ index_pre).val();
            var district_cur = $(this).val();

            $.post('/frontend/ajax/get-distance',{
                origins : district_pre,
                destinations : district_cur
            },function(data){
                $('.long-'+curent_index_location).val(data.amount);
                var total = 0;
                $('.long').each(function(index) {
                    total = total + parseFloat($(this).val());
                });
                $('#total-round').val(total);
            },'json');
        }
    });

    $('body').on('click', '.cancel', function(e){
        e.preventDefault();
        $('#modal-view').modal('hide');
    });

    $('body').on('click', '#btn-close', function(e){
        e.preventDefault();
        $('#modal-update').modal('hide');
    });

    $('body').on('click', '.cancel', function(e){
        e.preventDefault();
        $('#modal-view-history').modal('hide');
    });

});

