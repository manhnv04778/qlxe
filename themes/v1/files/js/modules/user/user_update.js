
$(function(){

		/* Upload image
		-------------------------------------------------- */
		// browse file
		$("#image_browse").change(function (e) {


				var files = event.target.files;
				var file = files[0];
				if (!file.type.match('image.*')) {
					growl('You must browse image', 'danger');
					return false;
				}

				var inputFile = $(this),
				numFiles = inputFile.get(0).files ? inputFile.get(0).files.length : 1,
				label = inputFile.val().replace(/\\/g, '/').replace(/.*\//, '');

				var inputText = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;

				if( inputText.length ) {
					inputText.val(log);
				}

				var reader = new FileReader();
				//reader.readAsText(file);
				reader.readAsDataURL(file);

				reader.onloadend = function(e) {
					// If we use onloadend, we need to check the readyState.
					//if (evt.target.readyState == FileReader.DONE) { // DONE == 2
					//    console.log(e.target.result);
					$('#image-preview img').attr('src', e.target.result);
					$('#user-image_upload').val(e.target.result);
					//}
				};

			});


		$("#image_url").on('change keyup blur', function (evt) {
				var ext = $(this).val().split('.').pop().toLowerCase();
				console.log(ext);
				//if ($.inArray(ext, ['jpeg', ''jpg', 'gif', 'png']) >= 0) {
				$('#image-preview img').attr('src', $(this).val());
				$('#user-image_upload').val($(this).val());
				//}
			});

	});
