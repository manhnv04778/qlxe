$(function(){
    var reg = /^0\d{9,10}$/;
    var regMail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;


   $('body').on('click','#send-code', function(e){

       e.preventDefault();
       var phone = $('#verifyform-phone').val();
//01232901783
       if(reg.test(phone)){
           //growl('Mã xác nhận đã được gửi vào số điện thoại ' +  phone + '<br>Nhập mã xác nhận bạn vừa nhận được để xác thực tài khoản','success');

           $('#loading').show();
           $.post('/user/ajax/send-code', {
               user_id : user.id,
               phone : phone
           }, function(data){
               $('#loading').hide();
               if(data.stt == false){
                   growl(data.message,'danger');
               }else{
                   growl(data.message,'success');
                   $('.verify-code').show();
                   $('.box-send').hide();
               }
           }, 'json');
       }else{
           growl('Số điện thoại bao gồm các số từ 0-9 bắt đầu từ 0 dài từ 10 đến 11 ký tự!','danger');
       }
   });


    $('body').on('click','.btn-verify-code', function(e){
        e.preventDefault();
        var code = $('#verifyform-code').val();

        if(code == ''){
            growl('Bạn chưa nhập mã xác nhận','danger')
        }else{
            $('#loading').show();
            $.post('/user/ajax/verify-code', {
                user_id : user.id,
                code : code
            }, function(data){
                $('#loading').hide();
                if(data.stt == false){
                    growl(data.message,'danger');
                }else{
                    $('#modal-verify').modal({
                        show: true,
                        backdrop: 'static',
                        keyboard: true
                    });
                }
            }, 'json');
        }
    });

    $('body').on('click','.btn-send-mail', function(e){
        e.preventDefault();
        var email = $('#verifyform-email').val();

        if(regMail.test(email)){
            $('#loading').show();
            $.post('/user/ajax/send-mail', {
                user_id : user.id,
                email : email
            }, function(data){
                $('#loading').hide();
                growl(data.message,'success');
            }, 'json');
        }else{
            growl('Email không đúng định dạng!','danger');
        }
    });
    $('#lblChangePass').click(function () {
        if ($('#form-changePass').hasClass("display-none")) {
            $('#form-changePass').removeClass('display-none');
        } else {
            $('#form-changePass').addClass('display-none');
        }
    });

});

