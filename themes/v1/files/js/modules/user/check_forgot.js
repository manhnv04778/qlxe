$(function(){

   $('body').on('click','#check-forgot', function(e){
       e.preventDefault();
       var user_name = $('#passwordresetform-user_name').val();
       $('#loading').show();
       $.post('/user/user/check-forgot-pass', {
           user_name : user_name,
       }, function(data){
           console.log(data)
           $('#loading').hide();
           if(data.stt == false){
               growl(data.message,'danger');
           }else{
               growl(data.message,'success');
               $('.box-send').hide();
               window.location.href = '/user/user/forgot-pass?u='+user_name;
           }
       }, 'json');
   });
});

