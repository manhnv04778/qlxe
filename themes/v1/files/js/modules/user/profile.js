

$(function(){
    $('.h-tooltip').tooltip({
        placement : 'bottom'
    });

});

$(document).on('click', '.unfriend', function(){
    var list_id = $(this).attr('data_id');
    var arr_id = list_id.split("_");
    var id_user = arr_id[1];
    var id_friend = arr_id[0];
    var id_parent = '#friend_' + list_id;
    var r = confirm('Bạn có chắc chắn muốn xóa?');
    var href = '/user/user/delete-friend';
    var unfriend = $(this);

    if(r == true) {
        $.post(href, {id_user: id_user, id_friend: id_friend}, function (data) {
            if (data.stt == true) {
                unfriend.parent().parent().remove();
                var count_before = $('.count-friend').text();
                if(count_before > 0){
                    count_before = count_before - 1;
                    $('.count-friend').text(count_before);
                }
            }
        }, 'json');
    }
});