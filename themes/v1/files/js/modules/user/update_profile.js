$(function(){
		//$("#user-city_id").change(function () {

		$('.h-tooltip').tooltip({
				placement : 'bottom'
			});

	$('#user-birthday').combodate({
		minYear: 2000,
		maxYear: 2016,
		minuteStep: 10,
		format : 'DD-MM-YYYY'
	});

	var city = $("#user-city_id").val();
		if(!city){
			$("#user-district_id").attr('disabled','disabled');
		}
		$("#user-city_id").on('change keyup blur', function (e){
				var city_id = $(this).val();
				$("#user-district_id").html('');
				if(city_id){
					$("#user-district_id").removeAttr('disabled');
					$.each(districtDataGroupByCityId[city_id], function (key, value) {
							$("#user-district_id").append('<option value="' + key + '">' + value + '</option>');
						});
				}else{
					$("#user-district_id").attr('disabled','disabled');
				}

			});

		/* Upload image
		-------------------------------------------------- */
		// browse file
		$("#image_browse").change(function (e) {
				var files = event.target.files;
				var file = files[0];
				if (!file.type.match('image.*')) {
					growl('You must browse image', 'danger');
					return false;
				}

				var inputFile = $(this),
				numFiles = inputFile.get(0).files ? inputFile.get(0).files.length : 1,
				label = inputFile.val().replace(/\\/g, '/').replace(/.*\//, '');

				var inputText = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;

				if( inputText.length ) {
					inputText.val(log);
				}

				var reader = new FileReader();
				//reader.readAsText(file);
				reader.readAsDataURL(file);

				reader.onload = function(e) {
					// If we use onloadend, we need to check the readyState.
					//if (evt.target.readyState == FileReader.DONE) { // DONE == 2
					//    console.log(e.target.result);
					$('#image-preview img').attr('src', e.target.result);
					$('#user-image_upload').val(e.target.result);
					//}
				};

			});


		$("#image_url").on('change keyup blur', function (evt) {
				var ext = $(this).val().split('.').pop().toLowerCase();
				console.log(ext);
				//if ($.inArray(ext, ['jpeg', ''jpg', 'gif', 'png']) >= 0) {
				$('#image-preview img').attr('src', $(this).val());
				$('#user-image_upload').val($(this).val());
				//}
			});

	});
$(window).load(function() {
		var options ={
			thumbBox: '.thumbBox',
			spinner: '.spinner'
		}
		var cropper = $('.imageBox').cropbox(options);
		$('#file').on('change', function(){
				var reader = new FileReader();
				reader.onload = function(e) {
					options.imgSrc = e.target.result;
					cropper = $('.imageBox').cropbox(options);
				}
				reader.readAsDataURL(this.files[0]);
			})
		$('#btnCrop').on('click', function(){
				var img = cropper.getDataURL();
				$('.preview').html('<img src="'+img+'">');
				$('#user-image_upload').val(img);
			})
		$('#btnZoomIn').on('click', function(){
				cropper.zoomIn();
			})
		$('#btnZoomOut').on('click', function(){
				cropper.zoomOut();
			})
	});

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
});