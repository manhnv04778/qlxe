$(function(){
    //$("#updateinfoform-city_id").change(function () {
    var city = $("#updateinfoform-city_id").val();
    var type = $("#updateinfoform-type").val();
    if(type === 'pupil'){
        $('.pupil,.general').show();
    }else if(type === 'student'){
        $('.student,.general,.general-18').show();
    }else if(type === 'worker'){
        $('.worker,.general-18,.general').show();
    }
    if(!city){
        $("#updateinfoform-district_id").attr('disabled','disabled');
    }



    $("#updateinfoform-city_id").on('change keyup blur', function (e){
        var city_id = $(this).val();
        $("#updateinfoform-district_id").html('<option value="">Chọn quận/huyện</option>');
        if(city_id){
            $("#updateinfoform-district_id").removeAttr('disabled');
            $.each(districtDataGroupByCityId[city_id], function (key, value) {
                $("#updateinfoform-district_id").append('<option value="' + key + '">' + value + '</option>');
            });
        }else{
            $("#updateinfoform-district_id").attr('disabled','disabled');
        }
    });

    $("#updateinfoform-district_id").on('change keyup blur', function (e){
        var district_id = $(this).val();
        if(district_id){
            $("#updateinfoform-school").removeAttr('disabled');
            $.post('/user/ajax/get-school',{
                district_id : district_id
            }, function(data){
                $("#updateinfoform-school_id").html('<option value="">Chọn trường</option>');
                $.each(data, function (key, value) {
                    $("#updateinfoform-school_id").append('<option value="' + key + '">' + value + '</option>');
                });
            },'json');

        }else{
            $("#updateinfoform-district_id").attr('disabled','disabled');
        }
    });

    $("#updateinfoform-type").on('change keyup blur', function (e){
        var type = $(this).val();
        $('#form-step')[0].reset();
        $("#updateinfoform-type").val(type);

        if(type === 'pupil'){
            $('.pupil,.general').show('slow');
            $('.student,.worker,.general-18').hide();
        }else if(type === 'student'){
            $('.student,.general,.general-18').show('slow');
            $('.pupil,.worker').hide();
        }else if(type === 'worker'){
            $('.worker,.general-18,.general').show('slow');
            $('.pupil,.student').hide();
        }else{
            $('.pupil,.student,.worker, .general-18,.general').hide('slow');
        }
    });


    $("#comment_city_id").on('change keyup blur', function (e){
        var city_id = $(this).val();
        $("#comment_district_id").html('<option value="">Chọn quận/huyện</option>');
        if(city_id){
            $.each(districtDataGroupByCityId[city_id], function (key, value) {
                $("#comment_district_id").append('<option value="' + key + '">' + value + '</option>');
            });
        }

    });

    $(".submit-error" ).click(function(e) {
        //e.preventDefault();

        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        var phoneno2 = /^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        var txtPhone = $('#comment-phone').val();
        if(txtPhone.length > 0) {
            if (txtPhone.match(phoneno) || txtPhone.match(phoneno2)) {

                var dataArray = $("#form-comment").serializeArray(),
                    dataObj = {};
                console.dir(dataArray); //require firebug

                $(dataArray).each(function (i, field) {
                    dataObj[field.name] = field.value;
                });

                var recaptcha = (dataObj['g-recaptcha-response']);
                if (recaptcha == "") {
                    $('.captcha-error').html('Bạn chưa chọn captcha!');
                    return false;
                } else {
                    $('.captcha-error').html('');
                    //$("#form-comment").submit();
                    //return true;
                }

            }
            else {
                $('.phone-error').html('Số điện thoại không đúng định dạng');
                return false;
            }
        }else{
            var dataArray = $("#form-comment").serializeArray(),
                dataObj = {};
            console.dir(dataArray); //require firebug

            $(dataArray).each(function (i, field) {
                dataObj[field.name] = field.value;
            });

            var recaptcha = (dataObj['g-recaptcha-response']);
            if (recaptcha == "") {
                $('.captcha-error').html('Bạn chưa chọn captcha!');
                return false;
            } else {
                $('.captcha-error').html('');
               // $("#form-comment").submit();
                ///return true;
            }

        }
    });

});

