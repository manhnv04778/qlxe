function setResult(reportId, result){

    $.post('/admin/question-has-report/set-result', {
        reportId: reportId,
        result: result

    }, function(json){

        location.reload();

    });
};

$(function(){
    $('.reportFail').click(function(){
        var reportId = $(this).attr('report-id');
        setResult(reportId, 0);
    });
    $('.reportTrue').click(function(){
        var reportId = $(this).attr('report-id');
        setResult(reportId, 1);
    });
});