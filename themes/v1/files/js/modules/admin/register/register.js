/**
 * Created by Duy Dat on 11/30/2016.
 */

$(function(){
    $('#register-created').combodate({
        minYear: 2010,
        maxYear: 2030,
        minuteStep: 10,
        format : 'DD-MM-YYYY'
    });

    $('#register-expire').combodate({
        minYear: 2010,
        maxYear: 2030,
        minuteStep: 10,
        format : 'DD-MM-YYYY'
    });
    $('#register-expire_cost').combodate({
        minYear: 2010,
        maxYear: 2030,
        minuteStep: 10,
        format : 'DD-MM-YYYY'
    });
    $('#register-created_cost').combodate({
        minYear: 2010,
        maxYear: 2030,
        minuteStep: 10,
        format : 'DD-MM-YYYY'
    });
});
