
$(function(){
    $('.submit-result').click(function(){

       var feedback_id = $(this).attr('feedback-id');
       var feedback_result = $(this).attr('result');

        $.post('/admin/answer-feedback/set-result', {
            feedback_id : feedback_id,
            result: feedback_result
        }, function(json){

            var result = JSON.parse(json);

            if(result.success == true){
                location.reload();
            } else {
                alert(result.msg);
                location.reload();
            }
        });
    });
});