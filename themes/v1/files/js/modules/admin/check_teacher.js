// kiểm tra câu trả lời được chấm chưa
function answer_validate(seletor, key) {

    if($(seletor + ':checked').length > 0){
        $('.exercise' + key).removeClass('invalid-check');
        return $(seletor + ':checked').val();

    } else {
        $('.exercise' + key).addClass('invalid-check');
        return null;
    }
};


$(function(){

    console.log(exercise);
    $('#check-teacher').submit(function(e){



        var valid = true;
        for( key in exercise){
            console.log(key);
            var selector = 'input[name="' + key +'"]';

            var exerciseCheck = answer_validate(selector, key);

            if(exerciseCheck == null){
                valid = false;
                $('#check_complete-alert').modal();
            } else {
                exercise[key] = exerciseCheck;
            }
        }

        if(valid){
            // ajax lưu kết quả chấm.
            $.ajax({
                url: '/admin/exercise-test/check-exercise-test/' + testId,
                data: exercise, type: 'POST', dataType: 'json', cache: false, async: true,

                success: function(json){
                    if(json.success){
                        $('#completed-alert').modal();
                    }
                }

            });


        } else {

        }

        e.preventDefault();
        return false;
    });
});
