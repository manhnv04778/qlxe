/**
 * Created by DELL on 11/14/2016.
 */

$(function(){
    $('body').on('click','.view', function(e){
        e.preventDefault();

        var id = $(this).attr('data-id');
        $.post('/admin/combustible/get-combustible',{id:id}, function(data){
            $('.name').html(data.name);

            $('#modal-view').modal(function(){

            });
        },'json');

    });
});
