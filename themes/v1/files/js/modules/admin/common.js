var pagedownSettings = {
	'sanatize': false
	,'help': function () {
        $('#modal-markdown-help').modal();
    }
	,'hooks': [ // https://code.google.com/p/pagedown/wiki/PageDown
		//{
		//    'event': 'preConversion',
		//    'callback': function (text) {
		//
		//        text2 =  text.replace(/\b(a\w*)/gi, "*$1*");
		//
		//
		//        console.log('preConversion');
		//        //console.log(text);
		//        //console.log(text2);
		//
		//        return text2;
		//    }
		//},
		//{
		//    'event': 'postConversion',
		//    'callback': function (text) {
		//        text2 =  text + "<br>\n**This is not bold, because it was added after the conversion**";
		//
		//        console.log('postConversion');
		//        //console.log(text);
		//        //console.log(text2);
		//        //alert('postConversion');
		//
		//        return text2;
		//    }
		//},
		//{
		//    'event': 'plainLinkText',
		//    'callback': function (url) {
		//        console.log('plainLinkText');
		//
		//        return "This is a link to " + url.replace(/^https?:\/\//, "");
		//    }
		//}
	]
};


//// typeahead
//var bloodhoundTag = new Bloodhound({
//	name: 'tags',
//	local: questionTags,
//	limit : 8,
//	datumTokenizer: function (d) {
//		return Bloodhound.tokenizers.whitespace(d.alias.replaceAll('-', ' '));
//	},
//	queryTokenizer: function (q) {
//		return Bloodhound.tokenizers.whitespace(q.toAlias().replaceAll('-', ' '));
//	}
//});
//
//var bloodhoundQuestion = new Bloodhound({
//	name: 'questions',
//	remote: '/question/ajax/autocomplete-question?q=%QUERY',
//	limit:8,
//	datumTokenizer: function (d) {
//		return Bloodhound.tokenizers.whitespace(d.alias.replaceAll('-', ' '));
//	},
//	queryTokenizer: function (q) {
//		return Bloodhound.tokenizers.whitespace(q.toAlias().replaceAll('-', ' '));
//	}
//});
//
//bloodhoundTag.initialize();
//bloodhoundQuestion.initialize();
//
//function typehead(selector){
//	//console.log(selector);
//	$(selector).typeahead(
//		{
//			hint: true,
//			highlight: true,
//			minLength: 1
//		},
//		{
//			name: 'questions',
//			displayKey: 'title',
//			source: bloodhoundQuestion.ttAdapter(),
//			templates: {
//				header: '<h4 class="title sr-only">Câu hỏi</h4>',
//				suggestion: Handlebars.compile('<div><a href="{{url}}" title="{{title}}"><i class="ion-ios-help-outline"></i> {{title}}</a> <i title="{{title}}" class="push-text pointer ion-android-arrow-up pull-right text-muted"></i></div>')
//			}
//		},
//		{
//			name: 'tags',
//			displayKey: 'name',
//			source: bloodhoundTag.ttAdapter(),
//			templates: {
//				header: '<h4 class="title sr-only">Tags</h4>',
//				suggestion: Handlebars.compile('<div><a href="{{url}}" title="{{name}}"><i class="ion-ios-pricetag-outline"></i> {{name}}</a> <i title="{{name}}" class="push-text pointer ion-android-arrow-up pull-right text-muted"></i></div>')
//			}
//		}
//	);
//	/*.on('typeahead:opened', onOpened)
//	 .on('typeahead:selected', onSelected)
//	 .on('typeahead:autocompleted', onAutocompleted)
//
//	 function onOpened($e) {
//	 console.log('opened');
//	 }
//
//	 function onAutocompleted($e, datum) {
//	 console.log('autocompleted');
//	 console.log(datum);
//	 }
//
//	 function onSelected($e, datum) {
//	 console.log('selected');
//	 console.log(datum);
//	 }*/
//}


// ready
$(function () {

	//typehead('.autocomplete');

    // pjax loading
    $("[id^=pjax-]").on("pjax:start", function() {
        $("#ajax-loading").fadeIn();
    });
    $("[id^=pjax-]").on("pjax:end", function() {
        $("#ajax-loading").fadeOut();
    });


	$('#search-top, #search-navbar').submit(function(e){
		e.preventDefault();
		var keyword = $('input', this).val().replace(/\s+/g, '+').trim();
		var url = keyword != '' ? '/directory/search:'+keyword : '/directory';
		window.location = url;
	});


	//function detectWidth() {
	//	if ($(window).width() < 862) {
	//		// tooltip
	//		$("[data-toggle=tooltip]").tooltip('destroy');
    //
	//		$('[data-toggle=popover]').popover('destroy');
	//	}else{
	//		// tooltip
	//		$("[data-toggle=tooltip]").tooltip();
    //
	//		// popover
	//		$("[data-toggle=popover]").popover();
    //
	//	}
	//}
    //
	//detectWidth();
	//$(window).resize(detectWidth);


	// ajax update viewed of conversation, user-feed
	$('#dropdown-conversation').click(function () {
		var element = $(this);
		var ids = $(this).attr('data-ids');
		if (ids == '') return;

		element.addClass('pointer-disable');
		$('.count', element).addClass('hidden');
		$.post('/user/ajax/conversation-update-viewed', {ids: ids}, function () {
			element.attr('data-ids', '');
			element.removeClass('pointer-disable');
		});
	});
	$('#dropdown-user-feed').click(function () {
		var element = $(this);
		var ids = $(this).attr('data-ids');
		if (ids == '') return;

		element.addClass('pointer-disable');
		$('.count', element).addClass('hidden');
		$.post('/user/ajax/user-feed-update-viewed', {ids: ids}, function () {
			element.attr('data-ids', '');
			element.removeClass('pointer-disable');
		});
	});


	$('.user-follow').on('click', function (e) {
		e.preventDefault();
		if ($(this).hasClass('require-login')) {
			return false;
		}
		var parent = $(this).parent();
		var thisObject = $(this);

		var follow_id = $(this).attr('data-follow_id');
		var follow_name = $(this).attr('data-follow_name');
		var action = $(this).attr('data-action');
		var followed_count = parseInt($('.user-follow-count-' + follow_id, parent).text());
		if (action == 'follow') {
			$('.user-follow-event-' + follow_id).text('Unfollow');
			$('.user-follow-event-' + follow_id).attr('data-action', 'unfollow');
			$('.user-follow-count-' + follow_id).text(followed_count + 1);
		} else {
			$('.user-follow-event-' + follow_id).text('Follow');
			$('.user-follow-event-' + follow_id).attr('data-action', 'follow');
			$('.user-follow-count-' + follow_id).text(followed_count - 1);
		}

		$(this).removeClass('pointer').addClass('pointer-disable');

		$.post('/user/page/ajaxFollow', {action: action, follow_id: follow_id}, function () {
			$.post('/hangve/page/ajaxFollow', {action: action, follow_id: follow_id}, function () {
				thisObject.removeClass('pointer-disable').addClass('pointer');
			});
		});

		myGrowl(action == 'follow' ? 'Bạn đã follow ' + follow_name + ' thành công.' : 'Bạn đã Unfollow ' + follow_name + ' thành công.', 'info');

	});

	$('.user-like').on('click', function (e) {
		e.preventDefault();
		if ($(this).hasClass('require-login')) {
			return false;
		}
		var thisObject = $(this);

		var object = $(this).attr('data-object');
		var object_id = $(this).attr('data-object_id');
		var object_user_id = $(this).attr('data-object_user_id');
		var action = $(this).attr('data-action');
		if (action == 'like') {
			$(this).html('<i class="fa fa-times"></i> Bỏ thích');
			$(this).attr('action', 'unlike');
		} else {
			$(this).html('<i class="fa fa-thumbs-up"></i> Thích');
			$(this).attr('action', 'like');
		}

		$(this).addClass('pointer-disable');

		$.post('/hangve/page/ajaxLike', {action: action, object: object, object_id: object_id, object_user_id: object_user_id}, function () {
			thisObject.removeClass('pointer-disable');
		});
		var actionName = action == 'like' ? 'thích' : 'bỏ thích';
		var objectName = object == 'post' ? 'bài viết' : 'bài trả lời';
		myGrowl('Bạn đã ' + actionName + ' ' + objectName + ' thành công.', 'info');

	});


	var objectReport;
	var object;
	var object_id;
	$('.user-report').on('click', function (e) {
		e.preventDefault();
		if ($(this).hasClass('require-login')) {
			return false;
		}
		objectReport = $(this);

		object = $(this).attr('data-object');
		object_id = $(this).attr('data-object_id');
		status = $(this).attr('data-status');

		var objectName = object == 'Post' ? 'bài viết' : 'bài trả lời';

		if (status == 'reported') {
			myGrowl('Bạn đã gửi báo xấu ' + objectName + ' này rồi.', 'warning');
			return false;
		}

		var dialogHtml =
			'<div id="dialog-report" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
			'<div class="modal-dialog">' +
			'<div class="modal-content">' +
			'<div class="modal-header">' +
			'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
			'<h4 class="modal-title">Thông báo xấu</h4>' +
			'</div>' +
			'<div class="modal-body">' +
			'<form onsubmit="return false" class="form-horizontal">' +
			'<div class="form-group">' +
			'<label class="col-sm-4 control-label">Lý do</label>' +
			'<div class="col-sm-20">' +
			'<input placeholder="Tối đa 255 ký tự" maxlength="255" id="report-reason" type="text" autocomplete="off" class="form-control">' +
			'</div>' +
			'</div>' +
			'<div class="form-group">' +
			'<label class="col-sm-4 control-label">Thông báo</label>' +
			'<div class="col-sm-8">' +
			'<select id="report-for" class="form-control">' +
			'<option value="SOCIAL">cho cộng đồng</option>' +
			'<option value="ADMIN">chỉ cho Admin</option>' +
			'</select>' +
			'</div>' +
			'</div>' +
			'</form>' +
			'</div>' +
			'<div class="modal-footer">' +
			'<button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>' +
			'<button class="btn btn-primary" type="button" id="report-send">Gửi báo sai</button>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>';

		if($('#dialog-report').length == 0)
			$('body').append(dialogHtml);


		$("#dialog-report").modal({backdrop: 'static'});

	});

// send report from dialog
	$('#report-send').on('click', function(){
		var reason = $('#report-reason').val().replace(/(<([^>]+)>)/ig,"");
		var report = $('#report-for').val();

		if($.trim(reason) == ''){
			myGrowl('Bạn cần điền lý do báo xấu', 'danger');
			return false;
		}
		objectReport.attr('status', 'reported');
		objectReport.html('<i class="fa fa-warning"></i> Đã báo xấu');

		myGrowl('Bạn đã gửi thông báo xấu thành công.', 'success');

		$.post('/hangve/page/ajaxReport', {object:object, object_id: object_id, reason: reason, report: report});

		$('#dialog-report').modal('hide');
		$('#report-reason').val('');
		$('#report-for').val('SOCIAL');

	});



    //thay đổi tỉnh thành
    //duynv
    $('.dropdown-location .dropdown-menu a').click(function(){
        $('.dropdown-selected-text').text($(this).text());
        $.cookie('city_alias', $(this).attr('data-alias'), { expires: 365, path: '/' });
        $('input[name=city_alias]').val($(this).attr('data-alias'));
        $('#latlng').val($(this).attr('data-latlng'));
        $('#form-search').submit();
    });

    $('.select-city-mobile').change(function(){
        var val = $(this).val();
        var latlng = $('.select-city-mobile option:selected' ).attr('data-latlng');
        $.cookie('city_alias', val, { expires: 365, path: '/' });
        $('.dropdown-selected-text').text($('.select-city-mobile option:selected').text());
        $('input[name=city_alias]').val(val);
        $('#latlng').val(latlng);
        $('#form-search').submit();
    });

    if($(window).width() <= 768){
        $('.box-search-mobile').html($('.form-search-desktop').html());
    }
    $(window).resize(function() {
        if ($(window).width() <= 768) {
            $('.box-search-mobile').html($('.form-search-desktop').html());
        }
    });



    $('.activity-favourite').click(function(){
        var sender = $(this);
        var locationId = sender.attr('locationId');

        var post = {
            locationId: locationId
        };

        $.post('/location/ajax/favourite', post, function(data, extStatus, jqXHR){
            if(data.success){
                if(data.added == 0){
                    growl(data.message, 'danger');
                    sender.removeClass('location-favourited').removeClass('text-bold');
                    sender.find('.text-favourite').html('Lưu địa điểm');
                } else{
                    growl(data.message, 'success');
                    sender.addClass('location-favourited').addClass('text-bold');
                    sender.find('.text-favourite').html('Đã lưu');
                }
            }
        }, 'json').fail(function(jqXHR) {
            growl(jqXHR.statusText, 'danger');
        });
    })

});

// like end useful review.
// @author: thoaivan
$(document).on('click', '.review-activity', function(){

    var sender = $(this);
    var activity = sender.attr('activity');
    var action = sender.attr('activity-action');

    var post = {
        action: action,
        locationId: $(this).attr('locationId'),
        reviewId: $(this).attr('reviewId'),
        activity: activity
    };

    $.post('/location/ajax/activity', post, function(data, extStatus, jqXHR){
        if(data.success){
            //sender.find('.activity-text').html(data.text);

            if(parseInt(data.count) > 0){
                sender.find('.activity-count').html(data.count);
            } else {
                sender.find('.activity-count').html('');
            }
            sender.attr('activity-action', data.action);
            sender.addClass(data.addClass);
            sender.removeClass(data.removeClass);
        }
    }, 'json').fail(function(jqXHR) {
        growl(jqXHR.statusText, 'danger');
    });
});





