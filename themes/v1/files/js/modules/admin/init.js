$(function(){
    //console.log(device);
    $.material.init();

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();


    scrollToAnchorByQuery();

    // data prompt
    $('body').on('click', '[data-prompt]', function (e) {
        e.preventDefault();
        var element = $(this);
        bootbox.prompt(element.attr('data-prompt'), function (text) {
            if (text !== null) {
                submitPostUrl(element.attr('href'), {
                    text : text
                });
            }
        });
    });


    //override yii confirm
    yii.allowAction = function ($e) {
        var message = $e.data('confirm');
        return message === undefined || yii.confirm(message, $e);
    };
    yii.confirm = function (message, ok, cancel) {
        bootbox.confirm(message, function (confirmed) {
            if (confirmed) {
                !ok || ok();
            } else {
                !cancel || cancel();
            }
        });
    }



	$('.authchoice').authchoice();

    /////////////////////// REQUIRE LOGIN /////////////////
    //$('.require-login').bind('click blur focus keypress keyup paste change', function (e) {
    $('.require-login').bind('click', function (e) {
        e.preventDefault();
        $('#modal-login').modal();
        if(!$(this).hasClass('no-alert')){
            $.growl(loginMessage, {type: 'danger'});
        }

        //if ($(this).attr('redirect') != undefined) {
        //    $.cookie('redirect', $(this).attr('redirect'), { path: '/' });
        //}
    });
    $('#modal-login').on('hide.bs.modal', function () {
        // remove cookie
        //$.cookie('redirect', null, { path: '/' });
    });

//	$(".navbar-fixed-top").headroom({
//		// vertical offset in px before element is first unpinned
//		offset : 50,
//		// scroll tolerance in px before state changes
//		tolerance : 0,
//		// or scroll tolerance per direction
//		tolerance : {
//			down : 0,
//			up : 5
//		},
//		// css classes to apply
//		classes : {
//			// when element is initialised
//			initial : "headroom",
//			// when scrolling up
//			pinned : "headroom--pinned",
//			// when scrolling down
//			unpinned : "headroom--unpinned",
//			// when above offset
//			top : "headroom--top",
//			// when below offset
//			notTop : "headroom--not-top"
//		},
//		// callback when pinned, `this` is headroom object
//		onPin : function() {},
//		// callback when unpinned, `this` is headroom object
//		onUnpin : function() {},
//		// callback when above offset, `this` is headroom object
//		onTop : function() {},
//		// callback when below offset, `this` is headroom object
//		onNotTop : function() {}
//	});


    $("header.header").addClass("animated").headroom({
        offset : 50,
        tolerance : {
            down : 0,
            up : 5
        },
        classes: {
            initial : "headroom",
            //pinned : "fadeInDown",
            //unpinned : "fadeOutUp",
            pinned : "slideInDown",
            unpinned : "slideOutUp",
            top : "",
            notTop : ""
        }
        //classes: {
        //    initial : "headroom",
        //    pinned : "fadeInDown",
        //    unpinned : "fadeOutUp",
        //    top : "",
        //    notTop : ""
        //}
    });


$.scrollUp({
    scrollName: 'scrollUp', // Element ID
    scrollDistance: 500, // Distance from top/bottom before showing element (px)
    scrollFrom: 'top', // 'top' or 'bottom'
    scrollSpeed: 300, // Speed back to top (ms)
    easingType: 'linear', // Scroll to top easing (see http://easings.net/)
    animation: 'fade', // Fade, slide, none
    animationInSpeed: 200, // Animation in speed (ms)
    animationOutSpeed: 200, // Animation out speed (ms)
    scrollText: '<i class="ion-android-arrow-up"></i>', // Text for element, can contain HTML
    scrollTitle: 'Lên Top', // Set a custom <a> title if required. Defaults to scrollText
    scrollImg: false, // Set true to use image
    activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    zIndex: 2147483647 // Z-Index for the overlay
});

});