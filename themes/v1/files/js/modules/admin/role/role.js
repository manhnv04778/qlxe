/**
 * Created by DELL on 11/20/2016.
 */
$(function(){
    $('body').on('change','input[name=check-all]',function(e){
        if($(this).is(":checked")) {
            $('body').find('input[type=checkbox]').prop("checked", true);
        }else{
            $('body').find('input[type=checkbox]').prop("checked", false);
        }
    });
});