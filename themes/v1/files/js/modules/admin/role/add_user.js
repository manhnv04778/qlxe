/**
 * Created by Duy Dat on 11/14/2016.
 */
var apps = new Bloodhound({

    datumTokenizer: function (d) {
        return Bloodhound.tokenizers.whitespace(d.key);
    },
    //d là từ khóa ghi nhập vào input
    queryTokenizer: function (d) {
        return Bloodhound.tokenizers.whitespace(d);
    },
    remote: {
        url: '/user/ajax/autocomplete?q=%QUERY',
    }
});

// Initialize the Bloodhound suggestion engine
apps.initialize();

// Instantiate the Typeahead UI


$('#adduserform-user_id').typeahead({
        hint: true,
        highlight: true
    }, {
        displayKey: 'text',
        source: apps.ttAdapter(),
        templates: {
            suggestion: Handlebars.compile('<div><strong>{{text}}</strong></div>'),
            footer: Handlebars.compile(''),
        }
    }
)

//.on('typeahead:opened', onOpened)
    .on('typeahead:autocompleted', onAutocompleted)
    .on('typeahead:selected', onSelected)

//function onOpened($e) {
//    console.log('opened');
//}
//
function onAutocompleted($e, datum) {

}

function onSelected($e, datum) {
    console.log(datum)
}

/*$('input:radio[name=ecoin]').change(function() {
 //$('input:radio[name=ecoin]').change(function() {
 var href = '/user/wallet/check-ecoin';
 var exu =  $('input:radio[name=ecoin]:checked').val();
 alert(exu);
 $.post(href, { exu: exu, user_id: user_id}, function (data){
 if(data.stt == false){
 $('#error').html('Bạn không đủ Exu để thực hiện chuyển đổi với mức này!');
 }
 }, 'json');
 });*/

$(function(){
    $('body').on('change','input[type=radio][name=ecoin]',function(event){
        event.preventDefault();
        var ecoin = this.value;
        var user_ecoin = $('input[name=hdf-wallet]').val();
        $('input[name=pacId]').val($(this).attr('data-id'));
        if(parseFloat(ecoin) > parseFloat(user_ecoin)) {
            $('#btn-send').attr('disabled','disabled');
            $('#hdf-exuLevel').val(null);
            $('#error').html('Số xu của bạn không đủ để thực hiện giao dịch!');
            return false;
        }else{
            $('#btn-send').removeAttr('disabled');
            $('#hdf-exuLevel').val(ecoin);
            $('#error').html(null);
            return true;
        }
    });
});

