$(function(){
    $('.summernote').summernote({
        width: null,                  // set editor width
        height: 300,                 // set editor height, ex) 300

        minHeight: null,              // set minimum height of editor
        maxHeight: null,              // set maximum height of editor

        focus: false,                 // set focus to editable area after initializing summernote

        tabsize: 4,                   // size of tab ex) 2 or 4
        styleWithSpan: true,          // style with span (Chrome and FF only)

        disableLinkTarget: false,     // hide link Target Checkbox
        disableDragAndDrop: false,    // disable drag and drop event
        disableResizeEditor: false,   // disable resizing editor

        codemirror: {                 // codemirror options
            mode: 'text/html',
            htmlMode: true,
            lineNumbers: true
        },

        // language
        lang: 'en-US',                // language 'en-US', 'ko-KR', ...
        direction: null,              // text direction, ex) 'rtl'

        // toolbar
        toolbar: [
            ['style', ['style']],
            ['font', ['blockquote', 'pre', 'clear', 'bold', 'italic', 'underline', 'strikethrough']],
            ['fontname', ['fontname']],
            //['fontsize', ['fontsize']], // Still buggy
            //['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            //['height', ['height']],
            ['table', ['table']],
            ['fontsize', ['fontsize']],
            ['insert', ['link', 'picture', 'video', 'hr']],
            ['view', ['undo', 'redo', 'fullscreen', 'codeview']],
            //['help', ['help']]
        ],

        // style tag
        styleTags: ['h3', 'h4', 'p'],

        oninit: function () {

            var lineCount = 10;
            var lines = '';
            for (i = 0; i < lineCount; i++) {
                lines += '<p><br></p>'
            }

            $('.note-editable').html($('.note-editable').html() + lines);
            console.log('Summernote is launched');
        },
        onpaste: function(e) {
            console.log('Called event paste');
            console.log(e);
        }
        //onenter: function(e) {
        //    console.log('Enter/Return key pressed');
        //    //console.log(e);
        //},

    });


    /* Upload image
     -------------------------------------------------- */
    // browse file
    $("#image_browse").change(function (evt) {
        var files = evt.target.files;
        var f = files[0];

        if (!f.type.match('image.*')) {
            alert('File không hợp lệ! Vui lòng chọn ảnh khác');

            return false;
        }

        var i = document.createElement('input');
        if ('multiple' in i) {
            var reader = new FileReader();
            reader.readAsDataURL(f);
            reader.onload = (function () {
                return function (e) {
                    $('#image-preview img').attr('src', e.target.result);
                    $('#footer-image_upload').val(e.target.result);
                };
            })(f);
        }
    });


    $("#image_url").on('change keyup blur', function (evt) {
        var ext = $(this).val().split('.').pop().toLowerCase();
        console.log(ext);
        //if ($.inArray(ext, ['jpeg', ''jpg', 'gif', 'png']) >= 0) {
        $('#image-preview img').attr('src', $(this).val());
        $('#footer-image_upload').val($(this).val());
        //}
    });
});