/**
 * Created by Duy Dat on 12/1/2016.
 */

$(function(){
    $('.summernote').summernote({
        width: null,                  // set editor width
        height: 300,                 // set editor height, ex) 300

        minHeight: null,              // set minimum height of editor
        maxHeight: null,              // set maximum height of editor

        focus: false,                 // set focus to editable area after initializing summernote

        tabsize: 4,                   // size of tab ex) 2 or 4
        styleWithSpan: true,          // style with span (Chrome and FF only)

        disableLinkTarget: false,     // hide link Target Checkbox
        disableDragAndDrop: false,    // disable drag and drop event
        disableResizeEditor: false,   // disable resizing editor

        codemirror: {                 // codemirror options
            mode: 'text/html',
            htmlMode: true,
            lineNumbers: true
        },

        // language
        lang: 'en-US',                // language 'en-US', 'ko-KR', ...
        direction: null,              // text direction, ex) 'rtl'

        // toolbar
        toolbar: [
            ['style', ['style']],
            ['font', ['blockquote', 'pre', 'clear', 'bold', 'italic', 'underline', 'strikethrough']],
            //['fontname', ['fontname']],
            //['fontsize', ['fontsize']], // Still buggy
            //['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            //['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video', 'hr']],
            ['view', ['undo', 'redo', 'fullscreen', 'codeview']],
            //['help', ['help']]
        ],

        // style tag
        styleTags: ['h3', 'h4', 'p'],

        oninit: function () {

            var lineCount = 10;
            var lines = '';
            for (i = 0; i < lineCount; i++) {
                lines += '<p><br></p>'
            }

            $('.note-editable').html($('.note-editable').html() + lines);
            console.log('Summernote is launched');
        },
        onpaste: function(e) {
            console.log('Called event paste');
            console.log(e);
        }
        //onenter: function(e) {
        //    console.log('Enter/Return key pressed');
        //    //console.log(e);
        //},

    });

    $('#carlog-date_care').combodate({
        minYear: 2010,
        maxYear: 2030,
        minuteStep: 10,
        format : 'DD-MM-YYYY'
    });
});
