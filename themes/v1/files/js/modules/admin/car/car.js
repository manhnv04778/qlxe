/**
 * Created by DELL on 11/14/2016.
 */

$(function(){
    $('body').on('click','.view', function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        $.post('/admin/car/get-car',{id:id}, function(data){
            $('.name').html(data.name);
            $('.code').html(data.code);
            $('.color').html(data.color);
            $('.upholsterer').html(data.upholsterer+' chỗ');
            $('.speed').html(data.speed+'L');
            $('.type').html(data.type);
            $('.country').html(data.country);
            $('.manufacture').html(data.manufacture);
            $('.time_start').html(data.time_start);
            $('.time_end').html(data.time_end);

            $('#modal-view').modal(function(){

            });
        },'json');

    });
});
