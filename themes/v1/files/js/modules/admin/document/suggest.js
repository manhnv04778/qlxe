/**
 * Created by DELL on 11/14/2016.
 */

$(function(){
    $('body').on('click', '#print-document', function(e){
        e.preventDefault();
        $('#print').printThis({
            debug: false,               // show the iframe for debugging
            importCSS: true,            // import page CSS
            importStyle: false,         // import style tags
            printContainer: true,       // grab outer container as well as the contents of the selector
            loadCSS: "path/to/my.css",  // path to additional css file - us an array [] for multiple
            pageTitle: "",              // add title to print page
            removeInline: false,        // remove all inline styles from print elements
            printDelay: 333,            // variable print delay; depending on complexity a higher value may be necessary
            header: null,               // prefix to html
            base: false,                 // preserve the BASE tag, or accept a string for the URL
            formValues: true            // preserve input/form values
        });
    });
});
