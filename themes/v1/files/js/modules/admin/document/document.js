/**
 * Created by DELL on 11/14/2016.
 */

$(function(){

    $('body').on('click', '.view', function(e){
        e.preventDefault();
        $(".loader").show();
        var id = $(this).attr('data-id');
        $.post('/admin/document/get-document',{id:id}, function(data){
            $('.full_name').html(data.full_name);
            $('.name').html(data.name);
            $('.code').html(data.code);
            $('.department').html(data.department);
            $('.from_name').html(data.from_name);
            $('.number_user').html(data.number_user);
            $('.date_from').html(data.date_from);
            $('.date_to').html(data.date_to);
            $('.date_from_car').html(data.date_from_car);
            $('.date_to_car').html(data.date_to_car);
            $('.purpose_car').html(data.purpose_car);
            $('.number_car').html(data.number_car);
            $('.date_from_user').html(data.date_from_user);
            $('.date_to_user').html(data.date_to_user);
            $('.place_user').html(data.place_user);
            $('.note_document').html(data.note_document);
            $('.created_date').html(data.created_date);
            $('.from').html(data.from);
            $('.hdf-id').val(data.id);
            $("#btn-send").attr("data-id-send",data.id);

            var str = '<table class="table table-strip"><tr class="bg-blue text-white text-center"><td>Họ và Tên</td><td>Phòng ban</td><td>Chức danh</td><td>Email</td><td>Điện thoại</td></tr>';
            $.each(data.data_user, function(id, value){
                str += '<tr class="text-normal text-left">';
                str += '<td>'+value.user_name+'</td><td>'+value.department+'</td><td>'+value.position+'</td><td>'+value.email+'</td><td>'+value.phone+'</td>';
                str += '</tr>';
            });
            str += '</table>';
            $('.list_can_bo_approve').html(str);

            $('#modal-view').modal(function(){
            });
            $(".loader").hide();

        },'json');

    });

    $('body').on('click', '.view-history', function(e){
        e.preventDefault();
        $(".loader").show();
        var id = $(this).attr('data-id');
        $.post('/frontend/document/get-document-history',{id:id}, function(data){
            var str = '<table class="table table-strip"><tr class="bg-blue text-white text-center"><td>STT</td><td>Cán bộ</td><td>Chi tiết</td><td>Thời gian cập nhật</td></tr>';
            var count_up = 0;
            $.each(data.data_history, function(id, value){
                count_up++;
                str += '<tr class="text-normal text-left">';
                str += '<td class="text-center">'+count_up+'</td><td class="text-center">'+value.creater+'</td><td class="text-center">'+value.note+'</td><td class="text-center">'+value.created_date+'</td>';
                str += '</tr>';
            });
            str += '</table>';
            $('.history').html(str);

            $('#modal-view-history').modal(function(){
            });
            $(".loader").hide();
        },'json');

    });

    var data_document1 = [];
    $('body').on('click', '.approve', function(e){
        e.preventDefault();
        $(".loader").show();
        var id = $(this).attr('data-id');

        $.post('/admin/document/get-document-approve',{id:id}, function(data_document){
            $('.full_name_approve').html(data_document.full_name);
            $('.name_approve').html(data_document.name);
            $('.code_approve').html(data_document.code);
            $('.department_approve').html(data_document.department);
            $('.from_name_approve').html(data_document.from_name);
            $('.number_user_approve').html(data_document.number_user);
            $('.date_from_approve').html(data_document.date_from);
            $('.date_to_approve').html(data_document.date_to);
            $('.date_from_car_approve').html(data_document.date_from_car);
            $('.date_to_car_approve').html(data_document.date_to_car);
            $('.purpose_car_approve').html(data_document.purpose_car);
            $('.number_car_approve').html(data_document.number_car);
            $('.date_from_user_approve').html(data_document.date_from_user);
            $('.date_to_user_approve').html(data_document.date_to_user);
            $('.place_user_approve').html(data_document.place_user);
            $('.note_document_approve').html(data_document.note_document);
            $('.created_date_approve').html(data_document.created_date);
            $('.from_approve').html(data_document.from);
            $('.hdf-id_approve').val(data_document.id);
            $(".update-approve").attr("data-id-approve",data_document.id);
            $('.length_approve').val(data_document.lenght);
            $('#list-car').html('');
            $('#select_driver').html('');
            var str_list_car = '<option value="0">Chọn xe</option>';
            $.each(data_document.data_car, function(id, value) {
                str_list_car+= '<option value="'+value.id+'">'+value.name+'</option>';
            });
            $('#list-car').append(str_list_car);
            var str_list_driver = '<option value="0">Chọn lái xe</option>';
            $.each(data_document.data_driver, function(driver_id, full_name) {
                str_list_driver+= '<option value='+driver_id+'>'+ full_name +'</option>';
            });
            $('#select_driver').append(str_list_driver);
            var str = '<table class="table table-strip"><tr class="bg-blue text-white text-center"><td>Họ và Tên</td><td>Phòng ban</td><td>Chức danh</td><td>Email</td><td>Điện thoại</td></tr>';
            $.each(data_document.data_user, function(id, value){
                str += '<tr class="text-normal text-left">';
                str += '<td>'+value.user_name+'</td><td>'+value.department+'</td><td>'+value.position+'</td><td>'+value.email+'</td><td>'+value.phone+'</td>';
                str += '</tr>';
            });
                str += '</table>';
            $('.list_can_bo_approve').html(str);

            $('#modal-approve').modal(function(){
            });

            $(".loader").hide();
            data_document1 = data_document;

        },'json');

    });



    $('body').on('click','.update-approve', function(e){
        e.preventDefault();
        var href = '/admin/document/approve-document';
        var id = $(this).attr('data-id-approve');
        var is_approve = $('input[name="is_approve"]:checked').val();
        var car_id = '';
        var driver_id = '';
        var note_approve = $('#note_approve').val();

        if(is_approve == 'approved'){
            if(!checkValidate()) {
                return false;
            }else {
                $(".loader").show();
                var list_car = [];
                var list_driver = [];

                $.each($('#form-doctor .list_car'), function (key, value) {
                    var parrent = $(value).parents()[1];
                    list_car.push($(value).val());
                });

                $.each($('#form-doctor .list_driver'), function (key, value) {
                    var parrent = $(value).parents()[1];
                    list_driver.push($(value).val());
                });
                $('#modal-approve').modal('hide');
                $.post(href, {
                    id:id,
                    is_approve:is_approve,
                    list_car:list_car,
                    list_driver:list_driver,
                    note_approve:note_approve
                }, function (data) {

                }, 'json');
                return true;
            }
        }
    });

    $('body').on('click','#btn-send', function(e){
        e.preventDefault();
        var id = $(this).attr('data-id-send');
        var is_approve = $('input[type=radio][name=is_approve]').val();
        alert(is_approve);
        if(is_approve == 'approved'){

        }else if(is_approve == 'not_approved'){

        }
        //$.post('/frontend/document/send-document',{id:id}, function(data){
        //},'json');

    });

    $('body').on('change','input[type=radio][name=is_approve]',function(event){
        event.preventDefault();
        var is_approve = this.value;
        if(is_approve == 'approved'){
            $('#approves').attr('class', 'display-block');
            $('#btn-add-cars').attr('class', 'display-block');
        }else if(is_approve == 'not_approved') {
            $('#approves').attr('class', 'display-none');
            $('#btn-add-cars').attr('class', 'display-none');
        }
    });

    var id_car_current1 = $('#list-car').val();
    var id_car_current2 = $('#list-car-add').val();

    $('body').on('change', '#list-car', function(){
        var id_car_select = $('#list-car').val();
        id_car_current2 =  $('#list-car-add').val();
        var str = '<option value="0">Chọn xe</option>';
        if(id_car_select != 0){
            data_document1.data_car[id_car_select].status = 0;
            $.each(data_document1.data_car, function(id, value){
                if(value.status==1) {
                    if(value.id == id_car_current2){
                        str += '<option value="' + value.id + '" selected>' + value.name + '</option>';
                    }else{
                        str += '<option value="' + value.id + '">' + value.name + '</option>';
                    }
                }
            });
            data_document1.data_car[id_car_select].status = 1;
            $('#list-car-add').html(str);
        }else{
            $.each(data_document1.data_car, function(id, value){
                if(value.status==1) {
                    if(value.id == id_car_current2){
                        str += '<option value="' + value.id + '" selected>' + value.name + '</option>';
                    }else{
                        str += '<option value="' + value.id + '">' + value.name + '</option>';
                    }
                }
            });
            $('#list-car-add').html(str);
        }
    });

    $('body').on('change', '#list-car-add', function(){
        var id_car_select = $('#list-car-add').val();
        id_car_current1 = $('#list-car').val();
        var str = '<option value="0">Chọn xe</option>';
        if(id_car_select != 0){
            $.each(data_document1.data_car, function(id, value){
                data_document1.data_car[id_car_select].status = 0;
                if(value.status == 1){
                    if(value.id == id_car_current1){
                        str += '<option value="' + value.id + '" selected>' + value.name + '</option>';
                    }else{
                        str += '<option value="' + value.id + '">' + value.name + '</option>';
                    }
                }
            });
            data_document1.data_car[id_car_select].status = 1;
            $('#list-car').html(str);
        }else{
            $.each(data_document1.data_car, function(id, value){
                if(value.status==1) {
                     str += '<option value="' + value.id + '">' + value.name + '</option>';
                }
            });
            $('#list-car').html(str);
        }
    });

    var count_add = 1;

    $('body').on('click', '#add-car-driver', function(e){
        e.preventDefault();
        if($('#list-car').val() != 0) {
            count_add++;
            if (count_add < 3) {
                var car_selected = $('#list-car').val();
                var id = $('.update-approve').attr('data-id-approve');

                $('#list-car-add').html();
                $('#select-driver-add').html();
                var str_car = '';
                var str_driver = '';

                $.each(data_document1.data_car, function (id, value) {
                    str_car += '<option value="' + value.id + '">' + value.name + '</option>';
                });
                $.each(data_document1.data_driver, function (driver_id, full_name) {
                    str_driver += '<option value=' + driver_id + '>' + full_name + '</option>';
                });

                var str = '<div class="row paddingleft15 approves col-xs-12 marginbottom10 border-bottom-ddd paddingleft0 paddingbottom10"><div class="col-xs-3">Chọn xe:</div><div class="col-xs-3">'
                    + '<select name = "select_car[]" id="list-car-add" class="list_car form-control"><option value="0">Chọn xe</option>' + str_car + '</select></div>'
                    + '<div class="col-xs-2 paddingleft0 text-right">Chọn tài xế:</div>'
                    + '<div class="col-xs-3"><select name = "select_driver[]" id="select-driver-add" class="list_driver form-control"><option value="0">Chọn tài xế</option>' + str_driver + '</select></div><div class="col-xs-1 paddingtop5"><i class="delete-car text-error fa fa-times" aria-hidden="true"></i></div></div>';
                $("#approves").append(str);
                $("#list-car-add option[value='"+$('#list-car').val()+"']").remove();

            } else {
                alert('Bạn chỉ được chọn tối đa 2 xe');
            }
        }else{
            alert('Vui lòng chọn xe');
        }

    });

    $('body').on('click', '.delete-car', function(e){
        e.preventDefault();
        $(this).parent().parent().remove();
        id_car_current1 = $('#list-car').val();
        var str = '<option value="0">Chọn xe</option>';
        if(id_car_current1 != 0){
            $.each(data_document1.data_car, function(id, value){
                data_document1.data_car[id_car_current1].status = 1;
                    if(value.id == id_car_current1){
                        str += '<option value="' + value.id + '" selected>' + value.name + '</option>';
                    }else{
                        str += '<option value="' + value.id + '">' + value.name + '</option>';
                    }
            });
            $('#list-car').html(str);
        }else{
            $.each(data_document1.data_car, function(id, value){
                if(value.status==1) {
                    str += '<option value="' + value.id + '">' + value.name + '</option>';
                }
            });
            $('#list-car').html(str);
        }
        count_add--;
    });

    function checkValidate() {
        var car = true;
        var driver = true;
        $.each($('#form-doctor .list_car'), function (key, value) {
            var parrent = $(value).parents()[1];
            if ($(value).val() == '' || $(value).val().length < 0) {
                car = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
        });
        $.each($('#form-doctor .list_driver'), function (key, value) {
            var parrent = $(value).parents()[1];
            if ($(value).val() == '' || $(value).val().length < 0) {
                driver = false;
                $(this).css({
                    border: "1px solid red"
                });
            }
        });

        return (car && driver);
    }

    $('body').on('click', '.cancel', function(e){
        e.preventDefault();
        $('#modal-approve').modal('hide');
    });

    $('body').on('click', '.cancel-view', function(e){
        e.preventDefault();
        $('#modal-view').modal('hide');
    });

    $('body').on('click', '.cancel', function(e){
        e.preventDefault();
        $('#modal-view-history').modal('hide');
    });

    $("#delete-document").confirm();

});
