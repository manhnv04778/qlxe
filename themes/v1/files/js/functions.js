/**
 * Chuyển chữ tiếng việt thành alias
 */
String.prototype.toAlias = function () {
	var str = this;

	// str = str.toLowerCase();
	str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
	str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
	str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
	str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
	str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
	str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
	str = str.replace(/đ/g, "d");

	str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
	str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
	str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
	str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
	str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
	str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
	str = str.replace(/Đ/g, "D");

	// remove domain extends
	str = str.replace(/\.+([\w-]{2,4})?/g, "-");
	str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.| |\:|\;|\"|\&|\#|\[|\]|~|$|_/g, " ");
	/* tìm và thay thế các kí tự đặc biệt va khoang trang trong chuỗi sang kí tự khoang trang */
	str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1-
	str = str.replace(/^\-+|\-+$/g, "");
	str = str.replace(/^\s+|\s+$/g, "");
	//cắt bỏ ký tự - ở đầu và cuối chuỗi
	return str;
};

/**
 * string replace all
 * @param strTarget The substring you want to replace
 * @param strSubString The string you want to replace in.
 * @returns {String}
 */
String.prototype.replaceAll = function (strTarget, strSubString) {
	var strText = this;
	var intIndexOfMatch = strText.indexOf(strTarget);

	// Keep looping while an instance of the target string
	// still exists in the string.
	while (intIndexOfMatch != -1) {
		// Relace out the current instance.
		strText = strText.replace(strTarget, strSubString)

		// Get the index of any next matching substring.
		intIndexOfMatch = strText.indexOf(strTarget);
	}

	// Return the updated string with ALL the target strings
	// replaced out with the new substring.
	return ( strText );
};

// String: highlight text
String.prototype.highlight = function (replaceText) {
	var str = this;

	//var str = text;
	var strASCII = str.toAlias().toLowerCase();
	var replaceASCII = replaceText.toAlias().toLowerCase().replaceAll('\\', '');
	var strStart = strASCII.indexOf(replaceASCII);

	//console.log(str);
	//console.log(replaceASCII);

	var replaceText = str.substr(strStart, replaceASCII.length);
	return str.replace(replaceText, '<u>' + replaceText + '</u>');
};

/**
 * Format number
 */
function numberFormat(number, decimals, dec_point, thousands_sep) {
	/*
	 *     example 1: number_format(1234.56);
	 *     returns 1: '1,235'
	 *     example 2: number_format(1234.56, 2, ',', ' ');
	 *     returns 2: '1 234,56'
	 *     example 3: number_format(1234.5678, 2, '.', '');
	 *     returns 3: '1234.57'
	 *     example 4: number_format(67, 2, ',', '.');
	 *     returns 4: '67,00'
	 *     example 5: number_format(1000);
	 *     returns 5: '1,000'
	 *     example 6: number_format(67.311, 2);
	 *     returns 6: '67.31'
	 *     example 7: number_format(1000.55, 1);
	 *     returns 7: '1,000.6'
	 *     example 8: number_format(67000, 5, ',', '.');
	 *     returns 8: '67.000,00000'
	 *     example 9: number_format(0.9, 0);
	 *     returns 9: '1'
	 *    example 10: number_format('1.20', 2);
	 *    returns 10: '1.20'
	 *    example 11: number_format('1.20', 4);
	 *    returns 11: '1.2000'
	 *    example 12: number_format('1.2000', 3);
	 *    returns 12: '1.200'
	 *    example 13: number_format('1 000,50', 2, '.', ' ');
	 *    returns 13: '100 050.00'
	 */
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function (n, prec) {
			var k = Math.pow(10, prec);
			return '' + Math.round(n * k) / k;
		};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}

/**
 * Làm tròn số float
 * roundNumber(123.4567, 2) => 123.45
 */
function roundNumber(rnum, rlength) {
	return Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);
}

/**
 * get distance from 2 latlng in km|m
 * unit: K|M
 * var a = getDistanceFromLatLng('20.997684, 105.802034', '20.998586, 105.810370')
 * var b = getDistanceFromLatLng('20.997684, 105.802034', '20.998586, 105.810370', 'M')
 */
function getDistanceFromLatLng(latlng1, latlng2, unit) {

    function deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    unit = typeof unit !== 'undefined' ? unit : 'K';

    var latlng1s = latlng1.split(',');
    var lat1 = parseFloat(latlng1s[0]);
    var lng1 = parseFloat(latlng1s[1]);

    var latlng2s = latlng2.split(',');
    var lat2 = parseFloat(latlng2s[0]);
    var lng2 = parseFloat(latlng2s[1]);

    var R = 6371; // Radius of the earth in km
	var dLat = deg2rad(lat2 - lat1);
	var dLng = deg2rad(lng2 - lng1);
	var a =
			Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
			Math.sin(dLng / 2) * Math.sin(dLng / 2)
		;
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var k = R * c; // Distance in km
    var m = k*1000;

    k = k.toFixed(3);
    m = parseInt(m);

    if(unit == 'M') return m;
	return k;
}





/**
 * upper first char of string
 */
function mb_ucfirst_utf8($str) {
	$fc = $str.substr(0, 1);
	$fcUpper = $fc.toUpperCase();
	$strWithoutFc = $str.substr(1);
	$strWithoutFcLower = $strWithoutFc.toLowerCase();
	return $fcUpper.$strWithoutFcLower;
}

/**
 * Cuộn scroll tới selector
 * @param selector
 * @param top
 * @param duration
 * @param callback
 */
function scrollToAnchor(selector, top, duration, callback) {
	top = typeof top !== 'undefined' ? top : 100;
	duration = typeof duration !== 'undefined' ? duration : 500;

	$('html, body').animate({scrollTop: $(selector).offset().top - top}, duration);
	if(typeof callback == 'function') setTimeout(function(){callback();}, duration+100);
}



/**
 * Cuộn scroll tới selector
 * @param selector
 * @param to
 * @param top
 * @param duration
 * @param callback
 */
function scrollToBottom(selector,height, duration) {
	duration = typeof duration !== 'undefined' ? duration : 500;
	$(selector).animate({ scrollTop: height}, duration);
}




/**
 * Cuộn scroll tới selector từ query#hash
 */
function scrollToAnchorByQuery(){
	var anchor = $(location).attr('hash').replace('#', '');
	if($('#'+anchor).length > 0){
		scrollToAnchor('#'+anchor, 60, 600, function(){
			$('#'+anchor).fadeOut().fadeIn();
		});
	}
}

function submitPostUrl(url, attributes) {
	attributes = typeof attributes !== 'undefined' ? attributes : {};

	var form = $('<form action="'+url+'" method="post"></form>');

	var csrfParam = $('meta[name=csrf-param]').prop('content');
	var csrfToken = $('meta[name=csrf-token]').prop('content');

	attributes['_method'] = 'post';
	if(csrfParam && csrfToken){
		attributes[csrfParam] = csrfToken;
	}

	if(Object.keys(attributes).length > 0){
		$.each(attributes, function(key, value){
			form.append('<input type="hidden" name="'+key+'" value="'+value+'" />')
		});
	}

	form.appendTo($('body'));
    form.submit();
}

function updateBrowserUrl(url, title){
    title = typeof title !== 'undefined' ? title : 'Title';

    if(typeof(window.history.pushState) == 'function'){
        window.history.pushState({"pageTitle":title}, title, url);
    }
}


function growl(message, type, delay, from, align){
    type = typeof type !== 'undefined' ? type : 'info';
	delay = typeof delay !== 'undefined' ? delay : 10000;
	from = typeof from !== 'undefined' ? from : 'top';
	align = typeof align !== 'undefined' ? align : 'right';

    $.growl(message, {
        type: type,
        mouse_over: 'pause',
        spacing: 10,
        delay: delay,
        placement : {
            from: from,
            align: align
        },
        offset: {
            x: 10,
            y: 100
        },
        animate: {
            enter: 'animated rollIn',
            exit: 'animated rollOut'
        }
    });
}

// pretty code
function prettyCode(){
	var hasCode = false;
	$("pre code").parent().each(function() {

		if(!$(this).hasClass('prettyprint')){
			$(this).addClass('prettyprint linenums');
			hasCode = true;
		}

	});
	if (hasCode) prettyPrint();
}

function prettyCodePre(){
	var hasCode = false;
	$("pre").each(function() {
		$(this).addClass('prettyprint linenums');
		$(this).html('<code>'+$(this).html().replace(/<br\s*\/?>/gi, "\n")+'</code>');
		hasCode = true;
	});
	if (hasCode) prettyPrint();
}


// countdown
function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}
