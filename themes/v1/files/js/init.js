
$(function(){

    // check extension instelled
    //chrome.runtime.sendMessage(extensionId, { action: "getInfo" }, function (data) {
    //    if(data === undefined){
    //        console.log('not installed')
    //    }else{
    //        console.log(data);
    //    }
    //});




    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover();

    // data prompt
    $('body').on('click', '[data-prompt]', function (e) {
        e.preventDefault();
        var element = $(this);
        bootbox.prompt(element.attr('data-prompt'), function (text) {
            if (text !== null) {
                submitPostUrl(element.attr('href'), {
                    text : text
                });
            }
        });
    });


    //override yii confirm
    yii.allowAction = function ($e) {
        var message = $e.data('confirm');
        return message === undefined || yii.confirm(message, $e);
    };
    yii.confirm = function (message, ok, cancel) {
        bootbox.confirm(message, function (confirmed) {
            if (confirmed) {
                !ok || ok();
            } else {
                !cancel || cancel();
            }
        });
    }


	$('.authchoice').authchoice();

    /////////////////////// REQUIRE LOGIN /////////////////
    //$('.require-login').bind('click blur focus keypress keyup paste change', function (e) {
    $('.require-login').bind('click', function (e) {
        e.preventDefault();
        $('#modal-login').modal();
        return false;

        //if(!$(this).hasClass('no-alert')){
        //    $.growl(loginMessage, {type: 'danger'});
        //}

        //if ($(this).attr('redirect') != undefined) {
        //    $.cookie('redirect', $(this).attr('redirect'), { path: '/' });
        //}
    });

    $('.accept-info').bind('click', function (e) {
        e.preventDefault();
        $('#modal-accept-info').modal({
            backdrop: 'static',
            keyboard: true
        });
        return false;
    });

    $('.require-future').bind('click', function (e) {
        e.preventDefault();
        $('#modal-future').modal();
        return false;
    });

    $('.require-register').bind('click', function (e) {
        e.preventDefault();
        $('#modal-register').modal();
        return false;
    });

    $('.btn-register-modal').bind('click',function(e){
        e.preventDefault();

        $('#modal-login').modal('hide');
        $('#modal-register').modal();
        return false;
    });

    if($('header').length > 0){
        $("header").headroom({
            "offset": 10,
            "tolerance": 5,
            "classes": {
                "initial": "animated",
                "pinned": "slideDown",
                "unpinned": "slideUp"
            }
        });
    }

    $('#modal-login').on('hide.bs.modal', function () {
        // remove cookie
        //$.cookie('redirect', null, { path: '/' });
    });

    /*
    $(document).on('click', '.user-follow', function(){

        var href = '/user/ajax/user-follow';

        $.post( href, { user_id: user_id, follow_user_id: follow_user_id, _csrf:csrfToken }, function( data ) {
           console.log(data);
        });
    });*/



});