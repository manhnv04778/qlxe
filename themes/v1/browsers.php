<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=10.000">
        <meta charset="utf-8">
        <title>Nâng cấp trình duyệt</title>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta content="nofollow,noindex" name="robots">
        <style type="text/css">
            html {
                border: 0 none;
                font-family: inherit;
                font-size: 100%;
                font-style: inherit;
                font-weight: inherit;
                margin: 0;
                outline: 0 none;
                padding: 0;
                vertical-align: baseline;
            }
            body {
                color: #444444;
                direction: ltr;
                font: 400 13px/22px "Helvetica Neue","HelveticaNeue",Helvetica,sans-serif;
            }

            section {
                box-shadow: 0 0 30px -4px #999999;
                margin: auto;
                max-width: 1280px;
                display: block;
                border: 0 none;
                font-family: inherit;
                font-size: 100%;
                font-style: inherit;
                font-weight: inherit;

                outline: 0 none;
                padding: 0;
                vertical-align: baseline;
            }
            .page-width-container {
                margin: 0 auto;
                max-width: 978px;
                padding: 30px 0;
            }
            div {
                border: 0 none;
                font-family: inherit;
                font-size: 100%;
                font-style: inherit;
                font-weight: inherit;
                margin: 0;
                outline: 0 none;
                padding: 0;
                vertical-align: baseline;
            }

            h1 {
                font-size: 36px;
                font-weight: 300;
                line-height: 44px;
                margin-bottom: 30px;

                border: 0 none;
                font-family: inherit;

                font-style: inherit;
                font-weight: inherit;
                margin: 0;
                outline: 0 none;
                padding: 0;
                vertical-align: baseline;
            }

            .content-container p {
                margin: 1em 0;
            }
            p {
                margin: 13px 0;

                border: 0 none;
                font-family: inherit;
                font-size: 100%;
                font-style: inherit;
                font-weight: inherit;
                margin: 0;
                outline: 0 none;
                padding: 0;
                vertical-align: baseline;
            }

            table {
                border-collapse: collapse;
                border-spacing: 0;

                border: 0 none;
                font-family: inherit;
                font-size: 100%;
                font-style: inherit;
                font-weight: inherit;
                margin: 0;
                outline: 0 none;
                padding: 0;
                vertical-align: baseline;
            }
            .content-container .note {
                background-image: none;
                background-position: 2% 7px;
                background-repeat: no-repeat;
                background-size: auto 32px;
                border-bottom: 1px solid #EEEEEE;
                border-top: 1px solid #EEEEEE;
                min-height: 0;
                padding: 8px 0;
            }
            .content-container div {
                margin: 1em 0;
            }

            a {
                color: #245DC1;
                text-decoration: none;
            }
            a:hover {
                text-decoration: underline;
            }
            img{
                border: none;    
            }
            a span{
                display: block;
            }
        </style>
    </head>

    <body>
        <section class="primary-container">
            <div class="page-width-container">
                <div data-tracking-cat="article-container" class="article-container">

                    <h1>Bạn đang sử dụng trình duyệt IE cũ</h1>
                    <div class="main-section--answer main-section content-container">

                        <p>Để duyệt web tốt nhất, bạn hãy nâng cấp trình duyệt của mình</p>
                        <table>
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <a href="https://www.google.com/intl/en/chrome/browser/">
                                            <img src="/files/image/browsers/chrome.png">
                                            <span>Tải Chrome</span>
                                        </a>
                                    </td>
                                    <td align="center">
                                        <a href="http://www.mozilla.com/firefox/">
                                            <img src="/files/image/browsers/firefox.png">
                                            <span>Tải Firefox</span>
                                        </a>
                                    </td>
                                    <td align="center">
                                        <a href="http://www.opera.com/computer/windows">
                                            <img src="/files/image/browsers/opera.png">
                                            <span>Tải Opera</span>
                                        </a>
                                    </td>
                                    <td align="center">
                                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                            <img src="/files/image/browsers/ie.png">
                                            <span>Tải IE mới</span>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>

                        <div class="note">
                            <strong>Mách bạn:</strong> nên sử dụng trình duyệt <a href="https://www.google.com/intl/en/chrome/browser/">Chrome</a> phiên bản mới nhất khi duyệt web để có trải nghiệm tốt nhất.
                        </div>

                    </div>
                </div>

            </div>
        </section>

    </body>
</html>