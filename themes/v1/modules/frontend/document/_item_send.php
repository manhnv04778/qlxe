<?php
/**
 * Created by PhpStorm.
 * User: Duy Dat
 * Date: 8/31/2016
 * Time: 10:11 AM
 */
use app\helpers\DateTimeHelper;
use yii\helpers\Url;

?>
<?php $i=0;?>
<tr>

    <th class="text-center"><?=$i++;?></th>
    <th class="text-center"><a class="text-success view" data-id="<?=$model->id;?>"<?=$model->code?></a></th>
    <th class="text-center"><?=!empty($model->created_date) ? DateTimeHelper::getDateTime($model->created_date,'H:i d/m/Y') :  ''?></th>
    <th class="text-center"><?=$model->full_name?></th>
    <th class="text-center"><?=!empty($model->date_from) ? DateTimeHelper::getDateTime($model->date_from,'H:i d/m/Y') :  ''?></th>
    <th class="text-center"><?=!empty($model->date_to) ? DateTimeHelper::getDateTime($model->date_to,'H:i d/m/Y') :  ''?></th>
    <th class="text-center">
        <?php
            if($model->status==0){
                echo 'Mới tạo ';
            }elseif($model->status==1){
                echo 'Đã gửi';
            }elseif($model->status==2){
                echo 'Đã duyệt';
            }elseif($model->status==3){
                echo 'Từ chối';
            }elseif($model->status==4){
                echo 'Hủy';
            }
        ?>
    </th>
    <td class="text-center">
        <a class="btn text-success" href="<?=Url::to(['/frontend/document/update', 'id' => $model->id])?>"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
        <a class="btn text-error" data-pjax="0" data-method="post" data-confirm="Bạn có chắc chắn hủy không?" href="<?=Url::to(['/frontend/document/cancel', 'id' => $model->id])?>"><i class="fa fa-times" aria-hidden="true"></i> Hủy</a>
    </td>
<!--    <td>-->
<!--        <a class="btn text-error" data-pjax="0" data-method="post" data-confirm="Bạn có chắc chắn xoá không?" href="--><?//=Url::to(['/admin/document/delete', 'id' => $model->id])?><!--"><i class="fa fa-times" aria-hidden="true"></i> Gửi</a>-->
<!--    </td>-->
</tr>