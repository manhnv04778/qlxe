<?php
use app\components\assets\AppAsset;

Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [];


?>
<div class="action-content margintop60">
    <div class="row padding0">
        <div class="col-xs-8 col-sm-offset-2 bg-white">
            <div class="row padding0">
                <div class="col-xs-12 padding0">
                    <h4 class="bg-blue text-center padding10 text-white text-upper text-bold">Thêm mới</h4>
                </div>
            </div>
            <?= $this->render('_form', [
                'model' => $model,
                'form_user_doc' => $form_user_doc
            ]) ?>
        </div>
    </div>
</div>