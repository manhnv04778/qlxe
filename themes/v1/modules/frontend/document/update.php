<div class="action-content margintop60">
    <div class="row">
        <div class="col-xs-6 col-sm-offset-3">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="bg-blue text-center padding10 text-white text-upper text-bold">Cập nhật</h4>
                </div>
            </div>
            <?= $this->render('_form', [
                'model' => $model,
                'documentLocation' => $documentLocation
            ]) ?>
        </div>
    </div>
</div>
