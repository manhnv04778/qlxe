<?php
use app\components\assets\AppAsset;
use app\helpers\DateTimeHelper;
use app\models\Car;
use app\models\City;
use app\models\Country;
use app\models\Department;
use app\models\District;
use app\models\RegisterCar;
use app\modules\user\models\User;
use kartik\date\DatePicker;
use kartik\widgets\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$citys = City::getData();

$districts = District::getDataGroupByCity('id','latlng');
$this->registerJs("var districts = ".json_encode($districts).";", $this::POS_BEGIN);
$this->registerJs("var citys = ".json_encode($citys).";", $this::POS_BEGIN);
$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/frontend/document/document.js', ['depends' => [AppAsset::className()]]);
$total = 0;
?>

<?php
$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'id' => 'form-document',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-2',
            'offset' => 'col-md-offset-2',
            'wrapper' => 'col-md-12',
            'error' => '',
            'hint' => '',
        ],
    ],
    'options' => [
        'data-action' => Yii::$app->controller->action->id,
        'class' => 'paddingtop20'
    ]
]);
?>
<div class="row">
    <div class="col-sm-6">
        <div class="row margin0">
          <?php echo '<label>Mã hồ sơ: ' . $model->code .'</label>'; ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="row margin0">
            <?= $form->field($model, 'name')->begin(); ?>
            <?= Html::activeLabel($model, 'name', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeTextarea($model,'name',['class' => 'form-control'])?>
                <?= Html::error($model, 'name', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'name')->end(); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row margin0 marginbottom10">
            <?= $form->field($model, 'full_name')->begin(); ?>
            <?= Html::activeLabel($model, 'full_name', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeTextInput($model,'full_name',['class' => 'form-control'])?>
                <?= Html::error($model, 'full_name', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'full_name')->end(); ?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row margin0 marginbottom10">
            <?= $form->field($model, 'department_id')->begin(); ?>
            <?= Html::activeLabel($model, 'department_id', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeDropDownList($model,'department_id',Department::getDepartment(),['class' => 'form-control','prompt' => 'Chọn đơn vị'])?>
                <?= Html::error($model, 'department_id', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'department_id')->end(); ?>
        </div>
    </div>
</div>

    <div class="panel panel-info">
        <div class="panel-heading text-17">Địa điểm công tác</div>
        <div class="panel-body">
            <div class="panel-content-location">
                <?php if(empty($documentLocation)):?>
                <div class="row marginbottom20 locations" id="location-1">
                    <h4 class="text-14 paddingleft15">Điểm xuất phát</h4>
                    <div class="col-sm-3">
                        <?=Html::dropDownList('citys[1]',null,$citys,['class' => 'form-control citys','prompt' => 'Chọn tỉnh/thành','id' => 'city-1'])?>
                    </div>
                    <div class="col-sm-3">
                        <?=Html::dropDownList('districts[1]',null,[],['class' => 'form-control districts','prompt' => 'Chọn quận/huyện','id' => 'district-1'])?>
                    </div>
                    <div class="col-sm-4">
                        <?=Html::textInput('address[1]',null,['class' => 'form-control address','placeholder' => 'Địa chỉ','id' => 'address-1'])?>
                    </div>
                </div>

                <div class="row marginbottom20 locations" id="location-2">
                    <h4 class="text-14 paddingleft15">Điểm đến tiếp theo</h4>
                    <div class="col-sm-3">
                        <?=Html::dropDownList('citys[2]',null,$citys,['class' => 'form-control citys','prompt' => 'Chọn tỉnh/thành','id' => 'city-2'])?>
                    </div>
                    <div class="col-sm-3">
                        <?=Html::dropDownList('districts[2]',null,[],['class' => 'form-control districts','prompt' => 'Chọn quận/huyện','id' => 'district-2', 'data-index' => 2])?>
                    </div>
                    <div class="col-sm-4">
                        <?=Html::textInput('address[2]',null,['class' => 'form-control address','placeholder' => 'Địa chỉ','id' => 'address-2'])?>
                        <?=Html::hiddenInput('long[2]',null,['class' => 'long long-2'])?>
                    </div>
                </div>
                <?php else:?>
                    <?php foreach($documentLocation as $item): $total += $item->long;?>
                        <div class="row marginbottom20 locations" id="location-<?=$item->index?>">
                            <h4 class="text-14 paddingleft15">Điểm đến tiếp theo</h4>
                            <div class="col-sm-3">
                                <?=Html::dropDownList('citys['.$item->index.']',$item->city_id,$citys,['class' => 'form-control citys','prompt' => 'Chọn tỉnh/thành','id' => 'city-'.$item->index])?>
                            </div>
                            <div class="col-sm-3">
                                <?=Html::dropDownList('districts['.$item->index.']',$item->district_id,$districts[$item->city_id],['class' => 'form-control districts','prompt' => 'Chọn quận/huyện','id' => 'district-'.$item->index, 'data-index' => $item->index])?>
                            </div>
                            <div class="col-sm-4">
                                <?=Html::textInput('address['.$item->index.']',$item->address,['class' => 'form-control address','placeholder' => 'Địa chỉ','id' => 'address-'.$item->index])?>
                                <?=Html::hiddenInput('long['.$item->index.']',$item->long,['class' => 'long long-'.$item->index])?>
                            </div>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
            </div>

            <div class="row margintop20">
                <div class="col-sm-12 text-center">
                    <?=Html::button('Thêm điểm đến',['class' => 'btn btn-success','id' => 'add-location'])?>
                    <input type="hidden" value="2" name="index-location" id="index-location">
                </div>
                <div class="col-sm-12 text-center margintop20">
                    <label>Tổng đường đi là: </label><input name="Document[lenght]" type="text" value="<?=$total?>" id="total-round"><label> km</label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom10">
                <?= $form->field($model, 'attachment')->begin(); ?>
                <?= Html::activeLabel($model, 'attachment', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeFileInput($model,'attachment',['class' => 'form-control'])?>
                    <?= Html::error($model, 'attachment', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'attachment')->end(); ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row margin0 marginbottom10">
                <?= $form->field($model, 'number_user')->begin(); ?>
                <?= Html::activeLabel($model, 'number_user', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-6">
                    <?=Html::textInput('number_user_show','',['id' => 'number_user_show', 'class'=>'form-control', 'disabled' => 'disbaled'])?>
                    <?=Html::activeHiddenInput($model, 'number_user', ['class' => 'form-control'])?>
                    <?=Html::error($model, 'number_user', ['class' => 'help-block help-block-error']) ?>
                </div>
                <div class="col-sm-6">
                    <div class="row margin0 marginbottom10">
                        <a id="add-userdocument" class="text-error no-hover" data-id="" href="javascript();">Thêm cán bộ đi công tác</a>
                    </div>
                </div>
                <?= $form->field($model, 'number_user')->end(); ?>
            </div>
        </div>
    </div>



<div class="row">
    <div class="col-sm-6">
        <div class="row margin0 marginbottom20">
            <?php
            echo '<label>Ngày bắt đầu đi công tác</label>';
            echo DateTimePicker::widget([
                'id' => 'txt_date_from',
                'name' => 'Document[date_from]',
                'type' => DateTimePicker::TYPE_INPUT,
                'value' => !empty($model->date_from) ? DateTimeHelper::getDateTime($model->date_from,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy hh:ii'
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="row margin0 marginbottom20">
            <?php
            echo '<label>Ngày kết thúc công tác</label>';
            echo DateTimePicker::widget([
                'id' => 'txt_date_to',
                'name' => 'Document[date_to]',
                'type' => DateTimePicker::TYPE_INPUT,
                'value' => !empty($model->date_to) ? DateTimeHelper::getDateTime($model->date_to,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy hh:ii'
                ]
            ]);
            ?>
        </div>
    </div>
</div>

    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?php
                echo '<label>Thời gian sử dụng xe từ</label>';
                echo DateTimePicker::widget([
                    'name' => 'Document[date_from_car]',
                    'type' => DateTimePicker::TYPE_INPUT,
                    'value' => !empty($model->date_from_car) ? DateTimeHelper::getDateTime($model->date_from_car,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy hh:ii'
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?php
                echo '<label>Thời gian kết thúc sử dụng xe</label>';
                echo DateTimePicker::widget([
                    'name' => 'Document[date_to_car]',
                    'type' => DateTimePicker::TYPE_INPUT,
                    'value' => !empty($model->date_to_car) ? DateTimeHelper::getDateTime($model->date_to_car,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy hh:ii'
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row margin0 marginbottom10">
                <?= $form->field($model, 'purpose_car')->begin(); ?>
                <?= Html::activeLabel($model, 'purpose_car', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextarea($model,'purpose_car',['class' => 'form-control'])?>
                    <?= Html::error($model, 'purpose_car', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'purpose_car')->end(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row margin0 marginbottom10">
                <?= $form->field($model, 'place_user')->begin(); ?>
                <?= Html::activeLabel($model, 'place_user', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'place_user',['class' => 'form-control'])?>
                    <?= Html::error($model, 'place_user', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'place_user')->end(); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?php
                echo '<label>Thời gian đón cán bộ</label>';
                echo DateTimePicker::widget([
                    'name' => 'Document[date_from_user]',
                    'type' => DateTimePicker::TYPE_INPUT,
                    'value' => !empty($model->date_from_user) ? DateTimeHelper::getDateTime($model->date_from_user,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy hh:ii'
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?php
                echo '<label>Thời gian trả cán bộ</label>';
                echo DateTimePicker::widget([
                    'name' => 'Document[date_to_user]',
                    'type' => DateTimePicker::TYPE_INPUT,
                    'value' => !empty($model->date_to_user) ? DateTimeHelper::getDateTime($model->date_to_user,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy hh:ii'
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row margin0 marginbottom10">
                <?= $form->field($model, 'note_document')->begin(); ?>
                <?= Html::activeLabel($model, 'note_document', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextarea($model,'note_document',['class' => 'form-control'])?>
                    <?= Html::error($model, 'note_document', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'note_document')->end(); ?>
            </div>
        </div>
    </div>

    <div class="row margin0 marginbottom30">
        <div class="col-xs-12 text-center">
            <?= Html::submitButton(Yii::$app->controller->action->id == 'create' ? '<i class="fa fa-plus"></i> Thêm mới' : '<i class="fa fa-edit"></i> Cập nhật', ['class' => 'btn btn-success' , 'id' => 'btn-create-doc']) ?>
            <?= Html::resetButton('Hủy', ['class' => 'btn btn-success'])?>
        </div>
    </div>


    <div id="modal-add-userdocument" class="modal fade modal-confirm in" role="dialog" data-toggle="modal">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content radius0">
                <div class="modal-header padding0">
                    <h4 class="modal-title bg-blue text-upper text-white padding10 text-center">Thêm cán bộ đi công tác</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-3"><input type="text" name ="full_name_user[]" class="full_name form-control" placeholder="Họ và tên"/></div>
                        <div class="col-sm-2 paddingleft0"><input type="text" name ="department_user[]" class="department form-control" placeholder="Đơn vị"/></div>
                        <div class="col-sm-2 padding0"><input type="text" name ="position_user[]" class="position form-control" placeholder="Chức danh"/></div>
                        <div class="col-sm-2"><input type="text" name ="phone_user[]" class="phone form-control" placeholder="Điện thoại"/></div>
                        <div class="col-sm-2 paddingleft0"><input type="text" name ="email_user[]" class="email form-control" placeholder="Email"/></div>
                        <div class="col-sm-1 paddingleft5">
                            <a id="add-user" class="text-success form-control text-blue text-18" data-id="" href="javascript();"><i class="fa fa-plus-circle" aria-hidden="true"></i></i></a>
                        </div>
                    </div>
                    <div id="record-add-user" class="row">

                    </div>
                    <div class="row margintop30">
                        <div class="col-sm-7 col-sm-offset-4">
                            <input type = "button" class="btn btn-success" id="btn-ok" value="Thêm cán bộ vào hồ sơ"> </button>
                            <input type = "button" class="btn bg-red  text-white" id="btn-cancel-userdoc" value="Hủy thêm cán bộ"> </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>