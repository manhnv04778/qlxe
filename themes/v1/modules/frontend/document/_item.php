<?php
/**
 * Created by PhpStorm.
 * User: Duy Dat
 * Date: 8/31/2016
 * Time: 10:11 AM
 */
use app\helpers\DateTimeHelper;
use app\models\Document;
use yii\helpers\Url;

?>
<tr>
    <td class="text-center"><?=$index+1;?></td>
    <td class="text-center"><a class="text-success view-history" data-id="<?=$model->id;?>" href="javascript();"><?=$model->code?></a></td>
    <td class="text-center">
        <?php
        if($model->status==0){
            echo 'Mới tạo ';
        }elseif($model->status==1){
            echo 'Đã gửi';
        }elseif($model->status==2){
            ?>
            <a href ="<?=Url::to(['/frontend/document/suggest/', 'id' => $model->id])?>">Đã duyệt</a>
            <?php
        }elseif($model->status==3){
            echo 'Từ chối';
        }elseif($model->status==4){
            echo 'Hủy';
        }
        ?>
    </td>
    <td class="text-left"><?=!empty($model->name) ? \yii\helpers\StringHelper::wordLimit($model->name, 20) : ''?></td>
    <td class="text-left"><?=Document::getDocumentLocationByDocId($model->id);?></td>
    <td class="text-center"><?=!empty($model->date_from) ? DateTimeHelper::getDateTime($model->date_from,'H:i d/m/Y') :  ''?></td>
    <td class="text-center"><?=!empty($model->date_to) ? DateTimeHelper::getDateTime($model->date_to,'H:i d/m/Y') :  ''?></td>
    <td class="text-center"><?=!empty($model->created_date) ? DateTimeHelper::getDateTime($model->created_date,'H:i d/m/Y') :  ''?></td>
    <td class="text-center">
        <?php if($model->status < 1):?>
            <a class="text-blue view" data-id="<?=$model->id;?>" href="javascript();"><i class="fa fa-eye" aria-hidden="true"></i></a>
            <a class="text-success update" data-id="<?=$model->id;?>" href="javascript();"><i class="fa fa-pencil" aria-hidden="true"></i></a>
            <a class="text-error" id="delete-document"  href="<?=Url::to(['/frontend/document/delete', 'id' => $model->id])?>"><i class="fa fa-times" aria-hidden="true"></i></a>
        <?php else: ?>
            <a class="text-blue view" data-id="<?=$model->id;?>" href="javascript();"><i class="fa fa-eye" aria-hidden="true"></i></a>
        <?php endif?>
    </td>
</tr>