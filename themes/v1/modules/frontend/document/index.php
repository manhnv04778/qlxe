<!-- list hồ sơ mới tạo-->
<?php
use app\components\assets\AppAsset;
use app\helpers\DateTimeHelper;
use app\models\City;
use app\models\Department;
use app\models\District;
use app\models\Document;
use kartik\date\DatePicker;
use kartik\widgets\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

$citys = City::getData();

$districts = District::getDataGroupByCity('id','latlng');
$this->registerJs("var districts = ".json_encode($districts).";", $this::POS_BEGIN);
$this->registerJs("console.log(districts);", $this::POS_BEGIN);
$this->registerJs("var citys = ".json_encode($citys).";", $this::POS_BEGIN);
//Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [];
$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/frontend/document/document.js', ['depends' => [AppAsset::className()]]);

?>

    <div class="action-content">
        <div class="container">
        <div class="row paddingtop20 marginbottom20">
            <div class="col-xs-10 col-xs-offset-1 padding0 bg-white">
                <h4 class="modal-title bg-blue text-upper text-white padding10">Tìm kiếm hồ sơ</h4>
                <form class="form-search" id="form-search" action="<?=Url::to(['/frontend/document/index'])?>" method="get">
                    <div class="form-content">
                        <div class="row padding15">
                            <div class="col-xs-3">
                                <?=Html::label('Mã hồ sơ',null,['class'=> 'text-normal'])?>
                                <?=Html::textInput('Document[code]',!empty($params['Document']['code']) ? $params['Document']['code'] : '',['class' => 'form-control','placeholder' => 'Mã hồ sơ'])?>
                            </div>
                            <div class="col-xs-4 col-xs-offset-1 padding0">
                                <?=Html::label('Tên hồ sơ', '' ,['class'=> 'text-normal'])?>
                                <?=Html::textInput('Document[name]', !empty($params['Document']['name']) ? $params['Document']['name'] : '', ['class' => 'form-control','placeholder' => 'Tên hồ sơ'])?>
                            </div>
                            <div class="col-xs-3 col-xs-offset-1  padding0 paddingright15">
                                <?=Html::label('Tỉnh/TP công tác',null,['class'=> 'text-normal'])?>
                                <?=Html::dropDownList('Document[from]', !empty($params['Document']['from']) ? $params['Document']['from'] : '' , City::getCity(),['class' => 'form-control' ,'prompt' => 'Chọn tỉnh/thành'])?>
                            </div>
                        </div>
                        <div class="row padding15 paddingtop0">
                            <div class="col-xs-3">
                                <?=Html::label('Từ ngày',null,['class'=> 'text-normal'])?>
                                <?=DatePicker::widget([
                                    'name' => 'Document[created_from]',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'value'=> !empty($params['Document']['created_from']) ? $params['Document']['created_from'] : '',
                                    'class' => 'form-control',
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'dd/mm/yyyy'
                                    ]
                                ]);
                                ?>
                            </div>
                            <div class="col-xs-3 col-xs-offset-1 padding0">
                               <?=Html::label('Đến ngày',null,['class'=> 'text-normal'])?>
                                <?=DatePicker::widget([
                                    'name' => 'Document[created_to]',
                                    'type' => DatePicker::TYPE_INPUT,
                                   'value'=> !empty($params['Document']['created_to']) ? $params['Document']['created_to'] : '',
                                    'class' => 'form-control',
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'dd/mm/yyyy'
                                    ]
                                ]);
                                ?>
                            </div>
                            <div class="col-xs-3 col-xs-offset-2 padding0 paddingright15">
                                <?=Html::label('Tình trạng hồ sơ',null,['class'=> 'text-normal'])?>
                                <?=Html::dropDownList('Document[status]', !empty($params['Document']['status']) ? $params['Document']['status'] : '', Document::getStatus(),['class' => 'form-control' ,'prompt' => 'Tình trạng hồ sơ'])?>
                             </div>
                        </div>
                        <div class="row margintop20 paddingbottom30">
                            <div class="col-xs-12 text-center">
                                <button class="btn btn-success"><i class="fa fa-search" aria-hidden="true"></i> Tìm kiếm</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 bg-white padding0">
                <?php
                    \yii\widgets\Pjax::begin([
                    'id' => 'pjax-questions',
                    'formSelector' => '#form-search',
                    'enableReplaceState' => true,
                    ]);
                ?>
                <div class="list">
                    <div class="col-xs-4 text-blue text-bold float-left paddingtop15">Tổng số: <span class="text-error text-bold text-16"><?= StringHelper::numberFormat($dataProvider->totalCount)?></span></div>
                    <div class="col-xs-2 float-right textright paddingtop15">
                        <a class="btn btn-success" href="<?=Url::toRoute(['/frontend/document/create'])?>"><i class="fa fa-plus" aria-hidden="true"></i> Thêm Đăng ký xe</a>
                    </div>
                    <div class="col-xs-12 padding0">
                        <table class="table table-bordered table-hover margintop10">
                            <thead class="bg-blue text-white">
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Mã HS</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="text-center">Tên hồ sơ</th>
                                <th class="text-center">Tỉnh/TP công tác</th>
                                <th class="text-center">Công tác từ</th>
                                <th class="text-center">Công tác về</th>
                                <th class="text-center">Ngày tạo</th>
                                <th width="100" class="text-center">Hành động</th>
                            </tr>
                            </thead>
                            <?= ListView::widget([
                                'dataProvider' => $dataProvider,
                                'options' => [
                                    'tag' => 'tbody'
                                ],
                                'summary' => '',
                                'emptyText' => '',
                                'itemView' => '_item',
                                /* 'pager' => [
                                     'class' => \kop\y2sp\ScrollPager::className(),
                                     'triggerTemplate' => '<div class="app-more margintop20">
                                                             <a class="btn padding20 text-center" href="#">{text}</a>
                                                         </div>',
                                     'triggerText' => 'Xem thêm',
                                     'noneLeftText' => '',
                                 ],*/
                            ]);
                            ?>

                        </table>
                    </div>
                </div>

                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

<div id="modal-view" class="modal fade modal-confirm in" role="dialog" data-toggle="modal">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content radius0">
            <div class="modal-header padding0">
                <h4 class="modal-title bg-blue text-upper text-white padding10 text-center">Chi tiết hồ sơ</h4>
            </div>

            <div class="modal-body">
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Mã hồ sơ:</div>
                    <div class="col-xs-3">
                        <span class="code text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tên hồ sơ:</div>
                    <div class="col-xs-9">
                        <span class="name text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tên lãnh đạo:</div>
                    <div class="col-xs-3">
                        <span class="full_name text-bold"></span>
                    </div>
                    <div class="col-xs-3">Đơn vị:</div>
                    <div class="col-xs-3">
                        <span class="department text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tỉnh thành đến công tác :</div>
                    <div class="col-xs-3">
                        <span class="from text-bold"></span>
                    </div>
                    <div class="col-xs-3">Địa điểm đến công tác:</div>
                    <div class="col-xs-3">
                        <span class="from_name text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Số cán bộ đi công tác:</div>
                    <div class="col-xs-3">
                        <span class="number_user text-bold"></span>
                    </div>
                    <div class="col-xs-3">Ngày gửi:</div>
                    <div class="col-xs-3">
                        <span class="created_date text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian công tác từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian công tác đến:</div>
                    <div class="col-xs-3">
                        <span class="date_to text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian nhận xe từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from_car text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian trả xe:</div>
                    <div class="col-xs-3">
                        <span class="date_to_car text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Mục đích sử dụng xe:</div>
                    <div class="col-xs-9">
                        <span class="purpose_car text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian đón cán bộ từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from_user text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian trả cán bộ:</div>
                    <div class="col-xs-3">
                        <span class="date_to_user text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Địa điểm trả cán bộ:</div>
                    <div class="col-xs-9">
                        <span class="place_user text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Ghi chú:</div>
                    <div class="col-xs-9">
                        <span class="note_document text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Danh sách cán bộ:</div>
                    <div class="col-xs-9">
                        <span class="list_can_bo text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-7 col-xs-offset-5">
                        <button class="btn bg-blue text-white" type="button" data-id-send="" name ="btn-send" id="btn-send" value = "Gửi hồ sơ" > <i class="fa fa-paper-plane-o text-white"></i>Gửi hồ sơ</button>
                        <button class="btn btn-danger cancel" type="button" value="Thoát"> <i class="fa fa-times"></i>Thoát</button>
                        <input type ="hidden" class="hdf-id" name = "hdf-id" id = "hdf-id" value="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---Modal edit document -->
<div id="modal-update" class="modal fade modal-confirm in" role="dialog" data-toggle="modal">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content radius0">
            <div class="modal-header padding0">
                <h4 class="modal-title bg-blue text-upper text-white padding10 text-center">Cập nhật thông tin hồ sơ</h4>
            </div>
            <div class="modal-body">
                <?php
                $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'id' => 'form-document',
                    'action'=>'/frontend/document/get-document-update/',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-md-2',
                            'offset' => 'col-md-offset-2',
                            'wrapper' => 'col-md-12',
                            'error' => '',
                            'hint' => '',
                        ],
                    ],
                    'options' => [
                        'data-action' => Yii::$app->controller->action->id,
                        'class' => 'paddingtop20'
                    ]
                ]);
                ?>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3 text-bold">Mã hồ sơ:</div>
                    <div class="col-xs-3">
                        <span class="code text-bold text-red"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="row margin0">
                            <?= $form->field($model_update, 'name')->begin(); ?>
                            <?= Html::activeLabel($model_update, 'name', ['class' => 'col-sm-12']) ?>
                            <div class="col-sm-12">
                                <?= Html::activeTextarea($model_update,'name',['class' => 'name form-control'])?>
                                <?= Html::error($model_update, 'name', ['class' => 'help-block help-block-error']) ?>
                            </div>
                            <?= $form->field($model_update, 'name')->end(); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom10">
                            <?= $form->field($model_update, 'full_name')->begin(); ?>
                            <?= Html::activeLabel($model_update, 'full_name', ['class' => 'col-sm-12']) ?>
                            <div class="col-sm-12">
                                <?= Html::activeTextInput($model_update,'full_name',['class' => 'full_name form-control'])?>
                                <?= Html::error($model_update, 'full_name', ['class' => 'help-block help-block-error']) ?>
                            </div>
                            <?= $form->field($model_update, 'full_name')->end(); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom10">
                            <?= $form->field($model_update, 'department_id')->begin(); ?>
                            <?= Html::activeLabel($model_update, 'department_id', ['class' => 'col-sm-12']) ?>
                            <div class="col-sm-12">
                                <?= Html::activeDropDownList($model_update,'department_id',Department::getDepartment(),['class' => 'department form-control','prompt' => 'Chọn đơn vị'])?>
                                <?= Html::error($model_update, 'department_id', ['class' => 'help-block help-block-error']) ?>
                            </div>
                            <?= $form->field($model_update, 'department_id')->end(); ?>
                        </div>
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading text-17">Địa điểm công tác</div>
                    <div class="panel-body">
                        <div class="panel-content-location">

                        </div>

                        <div class="row margintop20">
                            <div class="col-sm-12 text-center">
                                <?=Html::button('Thêm điểm đến',['class' => 'btn btn-success','id' => 'add-location'])?>
                                <input type="hidden" value="2" name="index-location" id="index-location">
                            </div>
                            <div class="col-sm-12 text-center margintop20">
                                <label>Tổng đường đi là: </label><input name="Document[lenght]" type="text" value="" id="total-round"><label> km</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom10">
                            <?= $form->field($model_update, 'from')->begin(); ?>
                            <?= Html::activeLabel($model_update, 'from', ['class' => 'col-sm-12']) ?>
                            <div class="col-sm-12">
                                <?= Html::activeDropDownList($model_update,'from',City::getCity(),['class' => 'from form-control','prompt' => 'Chọn đơn vị'])?>
                                <?= Html::error($model_update, 'from', ['class' => 'help-block help-block-error']) ?>
                            </div>
                            <?= $form->field($model_update, 'from')->end(); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom10">
                            <?= $form->field($model_update, 'from_name')->begin(); ?>
                            <?= Html::activeLabel($model_update, 'from_name', ['class' => 'col-sm-12']) ?>
                            <div class="col-sm-12">
                                <?= Html::activeTextInput($model_update,'from_name',['class' => 'from_name form-control'])?>
                                <?= Html::error($model_update, 'from_name', ['class' => 'help-block help-block-error']) ?>
                            </div>
                            <?= $form->field($model_update, 'from_name')->end(); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom10">
                            <?= $form->field($model_update, 'attachment')->begin(); ?>
                            <?= Html::activeLabel($model_update, 'attachment', ['class' => 'col-sm-12']) ?>
                            <div class="col-sm-12">
                                <?= Html::activeFileInput($model_update,'attachment',['class' => 'attachment form-control'])?>
                                <?= Html::error($model_update, 'attachment', ['class' => 'help-block help-block-error']) ?>
                            </div>
                            <?= $form->field($model_update, 'attachment')->end(); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom10">
                            <?= $form->field($model_update, 'number_user')->begin(); ?>
                            <?= Html::activeLabel($model_update, 'number_user', ['class' => 'col-sm-12']) ?>
                            <div class="col-sm-6">
                                <?= Html::activeTextInput($model_update,'number_user',['class' => 'number_user form-control', 'disabled'=> 'disabled'])?>
                                <?= Html::error($model_update, 'number_user', ['class' => 'help-block help-block-error']) ?>
                            </div>
                            <?= $form->field($model_update, 'number_user')->end(); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom20">
                            <?php
                            echo '<label>Ngày bắt đầu đi công tác</label>';
                            echo DateTimePicker::widget([
                                'id' => 'document-date_from',
                                'name' => 'Document[date_from]',
                                'type' => DateTimePicker::TYPE_INPUT,
                                'value' => !empty($model_update->date_from) ? DateTimeHelper::getDateTime($model_update->date_from,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd/mm/yyyy hh:ii'
                                ]
                            ]);
                            ?>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom20">
                            <?php
                            echo '<label>Ngày kết thúc công tác</label>';
                            echo DateTimePicker::widget([
                                'id' => 'document-date_to',
                                'name' => 'Document[date_to]',
                                'type' => DateTimePicker::TYPE_INPUT,
                                'value' => !empty($model_update->date_to) ? DateTimeHelper::getDateTime($model_update->date_to,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd/mm/yyyy hh:ii'
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom20">
                            <?php
                            echo '<label>Thời gian sử dụng xe từ</label>';
                            echo DateTimePicker::widget([
                                'id' => 'document-date_from_car',
                                'name' => 'Document[date_from_car]',
                                'type' => DateTimePicker::TYPE_INPUT,
                                'value' => !empty($model_update->date_from_car) ? DateTimeHelper::getDateTime($model_update->date_from_car,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd/mm/yyyy hh:ii'
                                ]
                            ]);
                            ?>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom20">
                            <?php
                            echo '<label>Thời gian kết thúc sử dụng xe</label>';
                            echo DateTimePicker::widget([
                                'id' => 'document-date_to_car',
                                'name' => 'Document[date_to_car]',
                                'type' => DateTimePicker::TYPE_INPUT,
                                'value' => !empty($model_update->date_to_car) ? DateTimeHelper::getDateTime($model_update->date_to_car,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd/mm/yyyy hh:ii'
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row margin0 marginbottom10">
                            <?= $form->field($model_update, 'purpose_car')->begin(); ?>
                            <?= Html::activeLabel($model_update, 'purpose_car', ['class' => 'col-sm-12']) ?>
                            <div class="col-sm-12">
                                <?= Html::activeTextarea($model_update,'purpose_car',['class' => 'purpose_car form-control'])?>
                                <?= Html::error($model_update, 'purpose_car', ['class' => 'help-block help-block-error']) ?>
                            </div>
                            <?= $form->field($model_update, 'purpose_car')->end(); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row margin0 marginbottom10">
                            <?= $form->field($model_update, 'place_user')->begin(); ?>
                            <?= Html::activeLabel($model_update, 'place_user', ['class' => 'col-sm-12']) ?>
                            <div class="col-sm-12">
                                <?= Html::activeTextInput($model_update,'place_user',['class' => 'place_user form-control'])?>
                                <?= Html::error($model_update, 'place_user', ['class' => 'help-block help-block-error']) ?>
                            </div>
                            <?= $form->field($model_update, 'place_user')->end(); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom20">
                            <?php
                            echo '<label>Thời gian đón cán bộ</label>';
                            echo DateTimePicker::widget([
                                'id' => 'document-date_from_user',
                                'name' => 'Document[date_from_user]',
                                'type' => DateTimePicker::TYPE_INPUT,
                                'value' => !empty($model_update->date_from_user) ? DateTimeHelper::getDateTime($model_update->date_from_user,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd/mm/yyyy hh:ii'
                                ]
                            ]);
                            ?>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="row margin0 marginbottom20">
                            <?php
                            echo '<label>Thời gian trả cán bộ</label>';
                            echo DateTimePicker::widget([
                                'id' => 'document-date_to_user',
                                'name' => 'Document[date_to_user]',
                                'type' => DateTimePicker::TYPE_INPUT,
                                'value' => !empty($model_update->date_to_user) ? DateTimeHelper::getDateTime($model_update->date_to_user,'d/m/Y h:i') :  DateTimeHelper::getDateTime('now','d/m/Y h:i'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd/mm/yyyy hh:ii'
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="row margin0 marginbottom10">
                            <?= $form->field($model_update, 'lenght')->begin(); ?>
                            <?= Html::activeLabel($model_update, 'lenght', ['class' => 'col-sm-12']) ?>
                            <div class="col-sm-12">
                                <?= Html::activeTextInput($model_update,'lenght',['class' => 'lenght form-control'])?>
                                <?= Html::error($model_update, 'lenght', ['class' => 'help-block help-block-error']) ?>
                            </div>
                            <?= $form->field($model_update, 'lenght')->end(); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row margin0 marginbottom10">
                            <?= $form->field($model_update, 'note_document')->begin(); ?>
                            <?= Html::activeLabel($model_update, 'note_document', ['class' => 'col-sm-12']) ?>
                            <div class="col-sm-12">
                                <?= Html::activeTextarea($model_update,'note_document',['class' => 'note_document form-control'])?>
                                <?= Html::error($model_update, 'note_document', ['class' => 'help-block help-block-error']) ?>
                            </div>
                            <?= $form->field($model_update, 'note_document')->end(); ?>
                        </div>
                    </div>
                </div>

                <div id="record-update-user" class="row marginbottom20">

                </div>
                <div class="row paddingleft15">
                    <div class="col-xs-3 paddingleft0 paddingright30">
                        <a id="update-user" class="btn bg-blue text-white form-control" data-id="" href="javascript();"><i class="fa fa-plus-circle text-18" aria-hidden="true"></i>Thêm cán bộ đi công tác</a>
                    </div>
                </div>

                <div class="row margin0 marginbottom30">
                    <div class="col-xs-12 text-center">
                        <?= Html::submitButton(Yii::$app->controller->action->id == 'create' ? '<i class="fa fa-plus"></i> Thêm mới' : '<i class="fa fa-edit"></i> Cập nhật', ['class' => 'btn btn-success update-model']) ?>
                        <?= Html::Button('Hủy', ['class' => 'btn btn-success', 'id' => 'btn-close'])?>
                        <input type ="hidden" class="hdf-id-update" name = "hdf-id-update" id = "hdf-id-update" value="" />
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<!--end modal-->

<div id="modal-view-history" class="modal fade modal-confirm in" role="dialog" data-toggle="modal">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content radius0">
            <div class="modal-header padding0">
                <h4 class="modal-title bg-blue text-upper text-white padding10 text-center">Chi tiết hồ sơ</h4>
            </div>

            <div class="modal-body">
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="history"></div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-7 col-xs-offset-5">
                        <button class="btn btn-danger cancel" type="button" value="Thoát"> <i class="fa fa-times"></i>Thoát</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>