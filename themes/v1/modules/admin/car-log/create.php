<div class="action-content">
    <div class="row">
        <div class="col-xs-6 col-sm-offset-3">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="bg-blue text-center padding10 text-white text-upper text-bold">Thêm mới Quá trình bảo dưỡng</h4>
                </div>
            </div>
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>