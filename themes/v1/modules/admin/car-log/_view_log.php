<?php
/**
 * Created by PhpStorm.
 * User: Duy Dat
 * Date: 8/31/2016
 * Time: 10:11 AM
 */
use app\helpers\DateTimeHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;

?>
<h4 class="text-center text-upper margintop10 marginbottom30">Thông tin bảo dưỡng</h4>
<div class="row marginleft0 marginright0 marginbottom10 paddingbottom5 border-bottom-ddd">
    <div class="col-xs-2 text-bold">Tên xe:</div><div class="col-xs-2"><?=$data->car->code?></div>
    <div class="col-xs-2 text-bold">Số chỗ ngồi:</div><div class="col-xs-2"><?=$data->car->upholsterer?></div>
    <div class="col-xs-2 text-bold">Dung tích:</div><div class="col-xs-2"><?=$data->car->speed?></div>
</div>
<div class="row marginleft0 marginright0 marginbottom10 paddingbottom5 border-bottom-ddd">
    <div class="col-xs-2 text-bold">Số khung:</div><div class="col-xs-2"><?=$data->car->seria?></div>
    <div class="col-xs-2 text-bold">Số máy:</div><div class="col-xs-2"><?=$data->car->seria_machine?></div>
</div>
<div class="row marginleft0 marginright0 marginbottom10 paddingbottom5 border-bottom-ddd">
    <div class="col-xs-2 text-bold">Người bảo dưỡng:</div><div class="col-xs-2"><?=$data->userCare->full_name?></div>
    <div class="col-xs-2 text-bold">Lãnh đạo duyệt:</div><div class="col-xs-2"><?=$data->userApprove->full_name?></div>
    <div class="col-xs-2 text-bold">Ngày bảo dưỡng:</div><div class="col-xs-2"><?=DateTimeHelper::getDateTime($data->date_care,Yii::$app->params['dateFormat']);?></div>
</div>
<div class="row marginleft0 marginright0 marginbottom10 paddingbottom5 border-bottom-ddd">
    <div class="col-xs-2 text-bold">Chi phí bảo dưỡng:</div><div class="col-xs-2"><?=StringHelper::numberFormat($data->money).'đ';?></div>
</div>
<div class="row marginleft0 marginright0 marginbottom10 paddingbottom5">
    <div class="col-xs-12 text-bold">Nội dung bảo dưỡng:</div>
    <div class="col-xs-12 margintop15"><?=$data->desc?></div>
</div>

