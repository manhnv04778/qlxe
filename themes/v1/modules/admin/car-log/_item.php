<?php
/**
 * Created by PhpStorm.
 * User: Duy Dat
 * Date: 8/31/2016
 * Time: 10:11 AM
 */
use app\helpers\DateTimeHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
?>

<tr>
    <td><?=$model->id?></td>
    <td><?=$model->car->code?></td>
    <td><?=$model->userCare ? $model->userCare->full_name : ''?></td>
    <td><?=$model->userApprove ? $model->userApprove->full_name : ''?></td>
    <td><?=DateTimeHelper::getDateTime($model->date_care,Yii::$app->params['dateFormat'])?></td>
    <td><?=StringHelper::numberFormat($model->money)?>đ</td>
    <td>
        <a class="btn text-success" href="<?=Url::to(['/admin/car-log/update', 'id' => $model->id])?>"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
        <a class="btn text-error" data-pjax="0" data-method="post" data-confirm="Bạn có chắc chắn xoá không?" href="<?=Url::to(['/admin/car-log/delete', 'id' => $model->id])?>"><i class="fa fa-times" aria-hidden="true"></i> Xoá</a>
    </td>
</tr>