<?php
use app\helpers\DateTimeHelper;
use app\models\Car;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;
Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [];
?>
<div class="action-content">
    <div class="row">
        <div class="col-xs-6"></div>
        <div class="col-xs-6 text-right export-group">
            <div class="button-group float-right marginleft15">
                <a class="btn btn-success" href="<?=Url::toRoute(['/admin/car-log/create'])?>"><i class="fa fa-plus" aria-hidden="true"></i> Thêm quá trình bảo dưỡng</a>
            </div>
            <?php
            //upholsterer
            $gridColumns = [
                [
                    'attribute'=>'car_id',
                    'label'=>'Xe',
                    'vAlign'=>'middle',
                ],
                [
                    'attribute'=>'approve_id',
                    'vAlign'=>'middle',
                    'value' => function($model){
                        return $model->userApprove ? $model->userApprove->full_name : '';
                    }
                ],
                [
                    'attribute'=>'user_id',
                    'vAlign'=>'middle',
                    'value' => function($model){
                        return $model->userCare ? $model->userCare->full_name : '';
                    }
                ],
                [
                    'attribute'=>'date_care',
                    'vAlign'=>'middle',
                    'value' => function($model){
                        return DateTimeHelper::getDateTime($model->date_care,Yii::$app->params['dateFormat']);
                    }
                ],
                [
                    'attribute'=>'money',
                    'vAlign'=>'middle',
                    'value' => function($model){
                        return StringHelper::numberFormat($model->money).'đ';
                    }
                ],
            ];


            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Xuất file',
                    'class' => 'btn btn-default'
                ]
            ]);
            ?>

        </div>
    </div>
    <div class="row">
        <form class="" id="form-search" action="<?=Url::to(['/admin/car-log/index'])?>" method="get">
            <div class="row">
                <div class="col-xs-2">
                    <?=Html::dropDownList('CarLog[car_id]',!empty($params['CarLog']['car_id']) ? $params['CarLog']['car_id'] : null,Car::getCar('id','code'),['class' => 'form-control','prompt' => 'Chọn xe'])?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <?=Html::submitButton('Tìm kiếm',['class' => 'btn btn-success'])?>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="list margintop30">
                <?php

                $actionColumn = [
                    'class' => '\kartik\grid\ActionColumn',
                    'width' => '130px',
                    'template' => '{update}{delete}',
                    'buttons' => [
                        'update' => function($url2, $model){
                            return '<a class="btn text-success paddingleft5 paddingright5" href="'.Url::toRoute(['/admin/car-log/update','id' => $model->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>';
                        },
                        'delete' => function($url3, $model){
                            return '<a class="btn text-error paddingleft5 paddingright5" data-pjax="0" data-method="post" data-confirm="Bạn có chắc chắn xoá không?" href="'.Url::toRoute(['/admin/car-log/delete','id' => $model->id]).'"><i class="fa fa-times" aria-hidden="true"></i> Xoá</a>';
                        }
                    ]
                ];
                $gridColumns[] = [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'width' => '80px',
                    'expandIcon' => '<i class="fa fa-eye" aria-hidden="true"></i> Chi tiết',
                    'collapseIcon' => '<i class="fa fa-eye" aria-hidden="true"></i> Chi tiết',
                    'expandAllTitle' => 'Chi tiết',
                    'value' => function ($model) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'headerOptions' => ['class' => 'kartik-sheet-style'],
                    'detailAnimationDuration' => 200,
                    'detail' => function($data) {
                        return $this->render('_view_log', ['data' => $data]);
                    }
                ];
                $gridColumns[] = $actionColumn;
                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                    'bordered' => false,
                ]);
                ?>

            </div>

        </div>
    </div>
</div>