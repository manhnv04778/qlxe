<?php
use app\components\assets\AppAsset;
use app\models\Car;
use app\models\CarLog;
use app\models\Country;
use app\modules\user\models\User;
use kartik\widgets\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$this->registerCssFile('/files/js/summernote-master/dist/summernote.css', ['depends' => [AppAsset::className()]]);
$this->registerJsFile('/files/js/summernote-master/dist/summernote.js', ['depends' => [AppAsset::className()]]);
$this->registerJsFile(STATIC_FILE.'/files/js/moment.js', ['depends' => [AppAsset::className()]]);
$this->registerJsFile(STATIC_FILE.'/files/js/combodate/src/combodate.js', ['depends' => [AppAsset::className()]]);

$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/admin/car/car_log.js', ['depends' => [AppAsset::className()]]);
?>

<?php
$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'id' => 'form-doctor',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-2',
            'offset' => 'col-md-offset-2',
            'wrapper' => 'col-md-8',
            'error' => '',
            'hint' => '',
        ],
    ],
    'options' => [
        'data-action' => Yii::$app->controller->action->id,
        'class' => 'paddingtop20'
    ]
]);
?>
    <div class="row margin0 marginbottom10">
        <?= $form->field($model, 'car_id')->begin(); ?>
        <?= Html::activeLabel($model, 'car_id', ['class' => 'col-sm-12']) ?>
        <div class="col-sm-6">
            <?= Html::activeDropDownList($model,'car_id',Car::getCar('id','code'),['class' => 'form-control','prompt' => 'Chọn xe'])?>
            <?= Html::error($model, 'car_id', ['class' => 'help-block help-block-error']) ?>
        </div>
        <?= $form->field($model, 'car_id')->end(); ?>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'user_id')->begin(); ?>
                <?= Html::activeLabel($model, 'user_id', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeDropDownList($model,'user_id',User::getUser(),['class' => 'form-control','prompt' => 'Chọn người đi bảo dưỡng'])?>
                    <?= Html::error($model, 'user_id', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'user_id')->end(); ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'approve_id')->begin(); ?>
                <?= Html::activeLabel($model, 'approve_id', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeDropDownList($model,'approve_id',User::getUser(),['class' => 'form-control','prompt' => 'Chọn lãnh đạo duyệt'])?>
                    <?= Html::error($model, 'approve_id', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'approve_id')->end(); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'date_care')->begin(); ?>
                <?= Html::activeLabel($model, 'date_care', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'date_care',['class' => 'form-control', 'data-format' => 'YYYY-MM-DD', 'data-template' => 'D / MM / YYYY','value' => $model->date_care])?>
                    <?= Html::error($model, 'date_care', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'date_care')->end(); ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'money')->begin(); ?>
                <?= Html::activeLabel($model, 'money', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'money',['class' => 'form-control'])?>
                    <?= Html::error($model, 'money', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'money')->end(); ?>
            </div>
        </div>
    </div>

    <div class="row margin0 marginbottom20">
        <?= $form->field($model, 'desc')->begin(); ?>
        <?= Html::activeLabel($model, 'desc', ['class' => 'col-sm-12']) ?>
        <div class="col-sm-12">
            <?= Html::activeTextarea($model,'desc',['class' => 'form-control summernote'])?>
            <?= Html::error($model, 'desc', ['class' => 'help-block help-block-error']) ?>
        </div>
        <?= $form->field($model, 'desc')->end(); ?>
    </div>
    <div class="row margin0 marginbottom10">
        <?= $form->field($model, 'is_complete')->begin(); ?>
        <?= Html::activeLabel($model, 'is_complete', ['class' => 'col-sm-12']) ?>
        <div class="col-sm-6">
            <?= Html::activeDropDownList($model,'is_complete',CarLog::getStatusComplete(),['class' => 'form-control','prompt' => 'Trạng thái bảo dưỡng'])?>
            <?= Html::error($model, 'is_complete', ['class' => 'help-block help-block-error']) ?>
        </div>
        <?= $form->field($model, 'is_complete')->end(); ?>
    </div>


    <div class="row margin0 marginbottom30">
        <div class="col-xs-12 text-center">
            <?= Html::submitButton(Yii::$app->controller->action->id == 'create' ? '<i class="fa fa-plus"></i> Thêm mới' : '<i class="fa fa-edit"></i> Cập nhật', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>