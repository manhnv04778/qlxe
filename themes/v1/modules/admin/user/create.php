<div class="action-content">
    <div class="row">
        <div class="col-xs-8 col-sm-offset-2">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="bg-blue text-center padding10 text-white text-upper text-bold">Thêm người dùng mới</h4>
                </div>
            </div>
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>