<?php
use app\helpers\DateTimeHelper;
use app\models\Department;
use app\models\Position;
use app\models\Role;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;


?>

<div id="review-index">
    <div class="row paddingtop20 marginbottom20">
        <div class="col-xs-12">
            <form class="form-search" id="form-search" action="<?=Url::to(['/admin/user/index'])?>" method="get">
                <div class="form-content">
                    <div class="row margintop20">
                        <div class="col-xs-3">
                            <input placeholder="Tên đăng nhập" name="User[user_name]" type="text" value="<?=!empty($params['User']['user_name']) ? $params['User']['user_name'] : null?>">
                        </div>
                        <div class="col-xs-3 col-xs-offset-1">
                            <?=Html::dropDownList('User[department_id]', null,Department::getDepartment(),['class' => 'form-control','prompt' => 'Chọn đơn vị', 'id' => 'department_id'])?>
                        </div>
                    </div>
                    <div class="row margintop20">
                        <div class="col-xs-3">
                            <?=Html::dropDownList('User[role_id]', null,Role::getRole(),['class' => 'form-control','prompt' => 'Chọn quyền', 'id' => 'role_id'])?>
                        </div>
                        <div class="col-xs-3 col-xs-offset-1">
                            <?=Html::dropDownList('User[position_id]', null,Position::getPosition(),['class' => 'form-control','prompt' => 'Chọn chức danh', 'id' => 'position_id'])?>
                        </div>
                    </div>
                    <div class="row margintop20">
                        <div class="col-xs-6 text-right">
                            <button class="btn btn-success"><i class="fa fa-search" aria-hidden="true"></i> Tìm kiếm</button>
                        </div>
                        <div class="col-xs-6 text-right">
                            <div class="button-group">
                                <a class="btn btn-success" href="<?=Url::toRoute(['/admin/user/create'])?>"><i class="fa fa-plus" aria-hidden="true"></i> Thêm người dùng</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="wrapper">
        <div class="row margin0">
            <?php
            $columns = [
                [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'id',
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'width' => '30px',
                    'headerOptions' => ['class' => 'text-center'],
                ],
                [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'user_name',
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class' => 'text-center'],
                ],
                [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'full_name',
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'headerOptions' => ['class' => 'text-center'],
                ],
                [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'birthday',
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'value' => function($model){
                        return DateTimeHelper::getDateTime($model->birthday,'d-m-Y');
                    },
                    'headerOptions' => ['class' => 'text-center'],
                ],
                [
                    'attribute'=>'type',
                    'label'=>'Đơn vị',
                    'vAlign'=>'middle',
                    'value'=>function($model){ return Department::getDepartment($model->department_id);},
                ],


                [
                    'attribute'=>'is_lock',
                    'label'=>'Khóa',
                    'vAlign'=>'middle',
                    'value'=>function($model){
                                if($model->is_lock==1){
                                    return "Bị khóa";
                                }else{
                                    return  "Đang hoạt động";
                                }
                    },
                ],

                [
                    'class' => 'kartik\grid\ActionColumn',
                    'order'=>DynaGrid::ORDER_FIX_RIGHT,
                    'headerOptions' => [
                        'label' => Yii::t('app', 'Actions'),
                    ],
                    'template' => '{update} {delete} {lock}',
                    'buttons' => [
                        'update' =>  function ($url, $model, $key){
                            return Html::a('<span class="paddingright10 glyphicon glyphicon-pencil text-success"></span>',
                               Url::toRoute(['/admin/driver/update/', 'id' => $model->id]), [
                                    'target' => '_blank',
                                    'title' => Yii::t('yii', 'Update'),
                                    'data-pjax' => 0,
                                ]);
                        },
                        'delete' =>  function ($url, $model, $key){
                            return Html::a('<span class="glyphicon glyphicon-remove text-error"></span>',
                                Url::toRoute(['/admin/driver/delete/', 'id' => $model->id]), [
                                    'title' => Yii::t('yii', 'Xoá'),
                                    'data-pjax' => 0,
                                    'data-confirm' => Yii::t('yii', 'Bạn có muốn xóa người dừng này không?'),
                                    'data-method' => 'post',
                                ]);
                        },
                        'lock' =>  function ($url, $model, $key){
                            return Html::a('<span class="fa fa-lock"></span>',
                                Url::toRoute(['/admin/driver/lock/', 'id' => $model->id]), [
                                    'title' => Yii::t('yii', 'Khóa tài khoản'),
                                    'data-pjax' => 0,
                                    'data-confirm' => Yii::t('yii', 'Bạn có muốn khóa người dùng này không?'),
                                    'data-method' => 'post',
                                ]);
                        },
                    ],
                ],

            ];


            // http://demos.krajee.com/dynagrid#dynagrid
            echo DynaGrid::widget([
                'columns'=>$columns,
                'storage'=>DynaGrid::TYPE_COOKIE,
                'enableMultiSort' => false,
//                'theme'=>'panel-default',
                'options'=>['id'=>'dynagrid-product-admin'], // a unique identifier is important
                'gridOptions'=>[
                    'dataProvider'=>$dataProvider,
                    'filterModel'=>$searchModel,
                    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
                    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
                    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
                    'resizableColumns'=> true,
                    'resizeStorageKey'=> 'reviews-'. Yii::$app->user->id.'-'. date("m"),
                    'pjax' => true, // pjax is set to always true for this demo
                    'toolbar' =>  [
                        ['content'=>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/'.Yii::$app->request->pathInfo], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>Yii::t('app', 'Bỏ lọc')])
                        ],
                        '{export}',
                    ],
                    // export: http://demos.krajee.com/grid#grid-export
                    'export' => false,
                    'responsiveWrap'=>false,
                    'responsive'=>true,
                    'hover'=>true,
                    'bordered' => true,
                    'striped' => true,
                    'condensed' => true,
                    'panel' => [
                        'type'=>GridView::TYPE_PRIMARY,
                        'heading' => '<h3 class="panel-title"><i class="fa fa-list"></i> '.Yii::t('review', 'Danh sách đánh giá').'</h3>',
                    ],
                    'emptyText' => Yii::t('review', 'Không có tin nào.'),
                    'summary' => Yii::t('review', 'Đang hiển từ {begin} -> {end} trong tổng số '.$dataProvider->totalCount)
                ],

            ]); ?>


        </div>
    </div>
</div>