<?php
use app\helpers\DateTimeHelper;
use app\models\Department;
use app\models\Position;
use app\models\Role;
use app\modules\user\models\User;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class = "row margintop30">
    <div class = "col-xs-12">
    <?php
    $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'form-doctor',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-md-2',
                'error' => '',
                'hint' => '',
            ],
        ],
        'options' => [
            'data-action' => Yii::$app->controller->action->id
        ]
    ]);
    ?>

        <div class="row">
            <div class="col-sm-6">
                <div class="row margin0 marginbottom10">
                    <?= $form->field($model, 'user_name')->begin(); ?>
                    <?= Html::activeLabel($model, 'user_name', ['class' => 'col-sm-12']) ?>
                    <div class="col-sm-12">
                        <?php if(Yii::$app->controller->action->id == 'create'):?>
                            <?=Html::activeTextInput($model,'user_name',['class' => 'form-control'])?>
                        <?php else:?>
                            <?= Html::activeTextInput($model,'user_name',['class' => 'form-control', 'disabled' => 'disbaled'])?>
                        <?php endif;?>
                        <?= Html::error($model, 'user_name', ['class' => 'help-block help-block-error']) ?>
                    </div>
                    <?= $form->field($model, 'user_name')->end(); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row margin0 marginbottom10">
                    <?= $form->field($model, 'full_name')->begin(); ?>
                    <?= Html::activeLabel($model, 'full_name', ['class' => 'col-sm-12']) ?>
                    <div class="col-sm-12">
                        <?= Html::activeTextInput($model,'full_name',['class' => 'form-control'])?>
                        <?= Html::error($model, 'full_name', ['class' => 'help-block help-block-error']) ?>
                    </div>
                    <?= $form->field($model, 'full_name')->end(); ?>
                </div>
            </div>
        </div>
        <?php if(Yii::$app->controller->action->id == 'create'):?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="row margin0 marginbottom10">
                        <?= $form->field($model, 'password')->begin(); ?>
                        <?= Html::activeLabel($model, 'password', ['class' => 'col-sm-12']) ?>
                        <div class="col-sm-12">
                            <?= Html::activePasswordInput($model,'password',['class' => 'form-control'])?>
                            <?= Html::error($model, 'password', ['class' => 'help-block help-block-error']) ?>
                        </div>
                        <?= $form->field($model, 'password')->end(); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row margin0 marginbottom10">
                        <?= $form->field($model, 'rePassword')->begin(); ?>
                        <?= Html::activeLabel($model, 'rePassword', ['class' => 'col-sm-12']) ?>
                        <div class="col-sm-12">
                            <?= Html::activePasswordInput($model,'rePassword',['class' => 'form-control'])?>
                            <?= Html::error($model, 'rePassword', ['class' => 'help-block help-block-error']) ?>
                        </div>
                        <?= $form->field($model, 'rePassword')->end(); ?>
                    </div>
                </div>
            </div>

        <?php endif; ?>


        <div class="row">
            <div class="col-sm-6">
                <div class="row margin0 marginbottom10">
                    <div class="col-sm-12 paddingleft0">
                        <?php
                        echo '<b>Ngày sinh</b>';
                        echo DatePicker::widget([
                            'name' => 'User[birthday]',
                            'type' => DatePicker::TYPE_INPUT,
                            'value' => (!empty($model->birthday)) ? DateTimeHelper::getDateTime($model->birthday,'d-m-Y') : DateTimeHelper::getDateTime('now', 'd-m-Y'),
                            'class' => 'form-control',
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'dd-mm-yyyy'
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row margin0 marginbottom10">
                    <?= $form->field($model, 'email')->begin(); ?>
                    <?= Html::activeLabel($model, 'email', ['class' => 'col-sm-12']) ?>
                    <div class="col-sm-12">
                        <?= Html::activeTextInput($model,'email',['class' => 'form-control'])?>
                        <?= Html::error($model, 'email', ['class' => 'help-block help-block-error']) ?>
                    </div>
                    <?= $form->field($model, 'email')->end(); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="row margin0 marginbottom10">
                    <?= $form->field($model, 'department_id')->begin(); ?>
                    <?= Html::activeLabel($model, 'department_id', ['class' => 'col-sm-12']) ?>
                    <div class="col-sm-12">
                        <?= Html::activeDropDownList($model,'department_id',Department::getDepartment(),['class' => 'form-control'])?>
                        <?= Html::error($model, 'department_id', ['class' => 'help-block help-block-error']) ?>
                    </div>
                    <?= $form->field($model, 'department_id')->end(); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row margin0">
                    <?= $form->field($model, 'gender')->begin(); ?>
                    <?= Html::activeLabel($model, 'gender', ['class' => 'col-sm-12']) ?>
                    <div class="col-sm-6">
                        <?=Html::activeRadioList($model,'gender',['0' => 'Nam' , '1' => 'Nữ'], [])?>
                        <?= Html::error($model, 'gender', ['class' => 'help-block help-block-error']) ?>
                    </div>
                    <?= $form->field($model, 'gender')->end(); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="row margin0 marginbottom10">
                    <?= $form->field($model, 'status')->begin(); ?>
                    <?= Html::activeLabel($model, 'status', ['class' => 'col-sm-12']) ?>
                    <div class="col-sm-6">
                        <?= Html::activeDropDownList($model,'status',User::getStatus(),['class' => 'form-control'])?>
                        <?= Html::error($model, 'status', ['class' => 'help-block help-block-error']) ?>
                    </div>
                    <?= $form->field($model, 'status')->end(); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row margin0 marginbottom10">
                    <?= $form->field($model, 'email_confirmed')->begin(); ?>
                    <div class="col-sm-6">
                        <?= Html::activeCheckbox($model,'email_confirmed', [])?>
                        <?= Html::error($model, 'email_confirmed', ['class' => 'help-block help-block-error']) ?>
                    </div>
                    <?= $form->field($model, 'email_confirmed')->end(); ?>
                </div>
            </div>
        </div>




        <div class="row margin0 marginbottom10">
            <div class="col-xs-12 text-center">
                <?= Html::submitButton(Yii::$app->controller->action->id == 'create' ? '<i class="fa fa-plus"></i> Thêm user' : '<i class="fa fa-edit"></i> Cập nhật thông tin lái xe', ['class' => 'btn']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>
