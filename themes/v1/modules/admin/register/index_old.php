<?php
use app\helpers\DateTimeHelper;
use app\models\geo\City;
use app\models\News;
use kartik\dynagrid\DynaGrid;
use kartik\editable\Editable;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\popover\PopoverX;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="action-content">
    <div class="wrapper">
        <div class="row margin0">
            <?php

            $gridColumns = [
                ['class' => 'kartik\grid\SerialColumn'],
                'id',
                'name',
                [
                    'attribute'=>'author_id',
                    'label'=>'Author',
                    'vAlign'=>'middle',
                    'width'=>'190px',
                    'value'=>function ($model, $key, $index, $widget) {
                        return Html::a($model->author->name, '#', []);
                    },
                    'format'=>'raw'
                ],
                ['class' => 'kartik\grid\ActionColumn', 'urlCreator'=>function(){return '#';}]
            ];


            echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'fontAwesome' => true,
                    'dropdownOptions' => [
                        'label' => 'Export All',
                        'class' => 'btn btn-default'
                    ]
                ]) . "<hr>\n".
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                ]);

            ?>

        </div>
    </div>
</div>