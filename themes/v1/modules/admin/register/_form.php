<?php
use app\components\assets\AppAsset;
use app\helpers\DateTimeHelper;
use app\models\Car;
use app\models\Color;
use app\models\Country;
use app\models\Manufacture;
use app\models\Speed;
use app\models\Type;
use app\models\Upholsterer;
use app\modules\user\models\User;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->registerJsFile(STATIC_FILE.'/files/js/moment.js', ['depends' => [AppAsset::className()]]);
$this->registerJsFile(STATIC_FILE.'/files/js/combodate/src/combodate.js', ['depends' => [AppAsset::className()]]);
$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/admin/register/register.js', ['depends' => [AppAsset::className()]]);

?>

<?php
$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'id' => 'form-doctor',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-2',
            'offset' => 'col-md-offset-2',
            'wrapper' => 'col-md-8',
            'error' => '',
            'hint' => '',
        ],
    ],
    'options' => [
        'data-action' => Yii::$app->controller->action->id,
        'class' => 'paddingtop20'
    ]
]);
?>

    <div class="row">
        <div class="col-sm-4">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'car_id')->begin(); ?>
                <?= Html::activeLabel($model, 'car_id', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeDropDownList($model,'car_id',Car::getCar('id','code'),['class' => 'form-control','prompt' => 'Chọn xe'])?>
                    <?= Html::error($model, 'car_id', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'car_id')->end(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'department')->begin(); ?>
                <?= Html::activeLabel($model, 'department', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'department',['class' => 'form-control'])?>
                    <?= Html::error($model, 'department', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'department')->end(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'created')->begin(); ?>
                <?= Html::activeLabel($model, 'created', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'created',['class' => 'form-control', 'data-format' => 'YYYY-MM-DD', 'data-template' => 'D / MM / YYYY','value' => $model->created])?>
                    <?= Html::error($model, 'created', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'created')->end(); ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'expire')->begin(); ?>
                <?= Html::activeLabel($model, 'expire', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'expire',['class' => 'form-control', 'data-format' => 'YYYY-MM-DD', 'data-template' => 'D / MM / YYYY','value' => $model->expire])?>

                    <?= Html::error($model, 'expire', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'expire')->end(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12"><h4>Nộp phí đường bộ(nếu có)</h4></div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'department_cost')->begin(); ?>
                <?= Html::activeLabel($model, 'department_cost', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'department_cost',['class' => 'form-control'])?>
                    <?= Html::error($model, 'department_cost', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'department_cost')->end(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'money_cost')->begin(); ?>
                <?= Html::activeLabel($model, 'money_cost', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'money_cost',['class' => 'form-control'])?>
                    <?= Html::error($model, 'money_cost', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'money_cost')->end(); ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'number_cost')->begin(); ?>
                <?= Html::activeLabel($model, 'number_cost', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'number_cost',['class' => 'form-control'])?>
                    <?= Html::error($model, 'number_cost', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'number_cost')->end(); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'created_cost')->begin(); ?>
                <?= Html::activeLabel($model, 'created_cost', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'created_cost',['class' => 'form-control', 'data-format' => 'YYYY-MM-DD', 'data-template' => 'D / MM / YYYY', 'value' => $model->created_cost])?>
                    <?= Html::error($model, 'created_cost', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'created_cost')->end(); ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'expire_cost')->begin(); ?>
                <?= Html::activeLabel($model, 'expire_cost', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'expire_cost',['class' => 'form-control', 'data-format' => 'YYYY-MM-DD', 'data-template' => 'D / MM / YYYY','value' => $model->expire_cost])?>

                    <?= Html::error($model, 'expire_cost', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'expire_cost')->end(); ?>
            </div>
        </div>
    </div>


    <div class="row margin0 marginbottom30">
        <div class="col-xs-12 text-center">
            <?= Html::submitButton(Yii::$app->controller->action->id == 'create' ? '<i class="fa fa-plus"></i> Thêm mới' : '<i class="fa fa-edit"></i> Cập nhật', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>