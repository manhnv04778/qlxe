<?php
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;
?>
<div class="action-content">
    <div class="row">
        <div class="col-xs-6">
            <form class="form-search" id="form-search" action="<?=Url::to(['/admin/manufacture/index'])?>" method="get">
                <div class="form-content">
                    <input placeholder="Tìm kiếm..." name="Manufacture[name]" type="text" value="<?=!empty($params['Manufacture']['name']) ? $params['Speed']['name'] : null?>">
                    <button><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </form>
        </div>
        <div class="col-xs-6 text-right">
            <div class="button-group">
                <a class="btn btn-success" href="<?=Url::toRoute(['/admin/manufacture/create'])?>"><i class="fa fa-plus" aria-hidden="true"></i> Thêm dung tích mới</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?php
                \yii\widgets\Pjax::begin([
                'id' => 'pjax-questions',
                'formSelector' => '#form-search',
                'enableReplaceState' => true,
                ]);
            ?>
            <div class="list margintop30">
                <div class="text-blue text-bold">Tổng số: <span class="text-error text-bold text-16"><?= StringHelper::numberFormat($dataProvider->totalCount)?></span></div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Hãng sản xuất</th>
                        <th>Quốc gia</th>
                        <th width="180" class="text-center">Sửa/Xóa</th>
                    </tr>
                    </thead>
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'options' => [
                            'tag' => 'tbody'
                        ],
                        'summary' => '',
                        'emptyText' => '',
                        'itemView' => '_item',
                        /* 'pager' => [
                             'class' => \kop\y2sp\ScrollPager::className(),
                             'triggerTemplate' => '<div class="app-more margintop20">
                                                     <a class="btn padding20 text-center" href="#">{text}</a>
                                                 </div>',
                             'triggerText' => 'Xem thêm',
                             'noneLeftText' => '',
                         ],*/
                    ]);
                    ?>


                </table>
            </div>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>