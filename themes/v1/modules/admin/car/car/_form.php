<?php
use app\helpers\DateTimeHelper;
use app\models\Color;
use app\models\Country;
use app\models\Manufacture;
use app\models\Speed;
use app\models\Type;
use app\models\Upholsterer;
use app\modules\user\models\User;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<?php
$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'id' => 'form-doctor',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-2',
            'offset' => 'col-md-offset-2',
            'wrapper' => 'col-md-8',
            'error' => '',
            'hint' => '',
        ],
    ],
    'options' => [
        'data-action' => Yii::$app->controller->action->id,
        'class' => 'paddingtop20'
    ]
]);
?>

    <div class="row">
    <div class="col-sm-6">
        <div class="row margin0 marginbottom20">
            <?= $form->field($model, 'name')->begin(); ?>
            <?= Html::activeLabel($model, 'name', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeTextInput($model,'name',['class' => 'form-control'])?>
                <?= Html::error($model, 'name', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'name')->end(); ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="row margin0 marginbottom20">
            <?= $form->field($model, 'code')->begin(); ?>
            <?= Html::activeLabel($model, 'code', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeTextInput($model,'code',['class' => 'form-control'])?>
                <?= Html::error($model, 'code', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'code')->end(); ?>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'seria')->begin(); ?>
                <?= Html::activeLabel($model, 'seria', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'seria',['class' => 'form-control'])?>
                    <?= Html::error($model, 'seria', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'seria')->end(); ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'seria_machine')->begin(); ?>
                <?= Html::activeLabel($model, 'seria_machine', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'seria_machine',['class' => 'form-control'])?>
                    <?= Html::error($model, 'seria_machine', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'seria_machine')->end(); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'manufacture')->begin(); ?>
                <?= Html::activeLabel($model, 'manufacture', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'manufacture',['class' => 'form-control'])?>
                    <?= Html::error($model, 'manufacture', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'manufacture')->end(); ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'country')->begin(); ?>
                <?= Html::activeLabel($model, 'country', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'country',['class' => 'form-control'])?>
                    <?= Html::error($model, 'country', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'country')->end(); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'color')->begin(); ?>
                <?= Html::activeLabel($model, 'color', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'color',['class' => 'form-control'])?>
                    <?= Html::error($model, 'color', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'color')->end(); ?>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'upholsterer')->begin(); ?>
                <?= Html::activeLabel($model, 'upholsterer', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'upholsterer',['class' => 'form-control'])?>
                    <?= Html::error($model, 'upholsterer', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'upholsterer')->end(); ?>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="row margin0 marginbottom20">
                <?= $form->field($model, 'speed')->begin(); ?>
                <?= Html::activeLabel($model, 'speed', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'speed',['class' => 'form-control'])?>
                    <?= Html::error($model, 'speed', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'speed')->end(); ?>
            </div>
        </div>
    </div>
    <div class="row margin0 marginbottom20">
        <?= $form->field($model, 'type')->begin(); ?>
        <?= Html::activeLabel($model, 'type', ['class' => 'col-sm-12']) ?>
        <div class="col-sm-12">
            <?= Html::activeTextarea($model,'type',['class' => 'form-control','rows' => 5])?>
            <?= Html::error($model, 'type', ['class' => 'help-block help-block-error']) ?>
        </div>
        <?= $form->field($model, 'type')->end(); ?>
    </div>

    <div class="row margin0 marginbottom20">
        <?= $form->field($model, 'type_use')->begin(); ?>
        <?= Html::activeLabel($model, 'type_use', ['class' => 'col-sm-12']) ?>
        <div class="col-sm-12">
            <?= Html::activeTextarea($model,'type_use',['class' => 'form-control','rows' => 5])?>
            <?= Html::error($model, 'type_use', ['class' => 'help-block help-block-error']) ?>
        </div>
        <?= $form->field($model, 'type_use')->end(); ?>
    </div>



    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?php
                echo '<label>Ngày đăng kiểm</label>';
                echo DatePicker::widget([
                    'name' => 'Car[time_start]',
                    'type' => DatePicker::TYPE_INPUT,
                    'value' => DateTimeHelper::getDateTime('now','d-m-Y'),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-m-yyyy'
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?php
                echo '<label>Ngày hết hạn</label>';
                echo DatePicker::widget([
                    'name' => 'Car[time_end]',
                    'type' => DatePicker::TYPE_INPUT,
                    'value' => DateTimeHelper::getDateTime('now','d-m-Y'),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-m-yyyy'
                    ]
                ]);
                ?>
            </div>
        </div>


    </div>







    <div class="row margin0 marginbottom30">
        <div class="col-xs-12 text-center">
            <?= Html::submitButton(Yii::$app->controller->action->id == 'create' ? '<i class="fa fa-plus"></i> Thêm mới' : '<i class="fa fa-edit"></i> Cập nhật', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>