<?php
use app\modules\admin\components\AppAsset;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;
Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [];
$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/admin/car/car.js', ['depends' => [AppAsset::className()]]);
?>
<div class="action-content">
    <div class="row">
        <div class="col-xs-6">
            <form class="form-search" id="form-search" action="<?=Url::to(['/admin/car/index'])?>" method="get">
                <div class="form-content">
                    <input placeholder="Tìm kiếm..." name="Car[name]" type="text" value="<?=!empty($params['Car']['name']) ? $params['Car']['name'] : null?>">
                    <button><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </form>
        </div>
        <div class="col-xs-6 text-right export-group">
            <div class="button-group float-right marginleft15">
                <a class="btn btn-success" href="<?=Url::toRoute(['/admin/car/create'])?>"><i class="fa fa-plus" aria-hidden="true"></i> Thêm xe mới</a>
            </div>
            <?php
//upholsterer
            $gridColumns = [
                [
                    'attribute'=>'code',
                    'label'=>'Biển số',
                    'vAlign'=>'middle',
                ],
                [
                    'attribute'=>'name',
                    'label'=>'Tên xe',
                    'vAlign'=>'middle',
                ],
                [
                    'attribute'=>'speed',
                    'label'=>'Dung tích',
                    'vAlign'=>'middle',
                ],
                [
                    'attribute'=>'upholsterer',
                    'vAlign'=>'middle',
                ],
                [
                    'attribute'=>'color',
                    'vAlign'=>'middle',
                ],
            ];


            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Xuất file',
                    'class' => 'btn btn-default'
                ]
            ]);
            ?>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="list margintop30">
                <?php

                $actionColumn = [
                    'class' => '\kartik\grid\ActionColumn',
                    'width' => '130px',
                    'template' => '{update}{delete}',
                    'buttons' => [
                        'update' => function($url2, $model){
                            return '<a class="btn text-success paddingleft5 paddingright5" href="'.Url::toRoute(['/admin/car/update','id' => $model->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>';
                        },
                        'delete' => function($url3, $model){
                            return '<a class="btn text-error paddingleft5 paddingright5" data-pjax="0" data-method="post" data-confirm="Bạn có chắc chắn xoá không?" href="'.Url::toRoute(['/admin/car/delete','id' => $model->id]).'"><i class="fa fa-times" aria-hidden="true"></i> Xoá</a>';
                        }
                    ]
                ];
                $gridColumns[] = [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'width' => '80px',
                    'expandIcon' => '<i class="fa fa-eye" aria-hidden="true"></i> Chi tiết',
                    'collapseIcon' => '<i class="fa fa-eye" aria-hidden="true"></i> Chi tiết',
                    'expandAllTitle' => 'Chi tiết',
                    'value' => function ($model) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'headerOptions' => ['class' => 'kartik-sheet-style'],
                    'detailAnimationDuration' => 200,
                    'detail' => function($data) {
                        return $this->render('_view_car', ['data' => $data]);
                    }
                ];
                $gridColumns[] = $actionColumn;
                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                    'bordered' => false,
                ]);
                ?>

            </div>

        </div>
    </div>
</div>


