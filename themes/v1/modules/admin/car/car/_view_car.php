<?php
/**
 * Created by PhpStorm.
 * User: Duy Dat
 * Date: 8/31/2016
 * Time: 10:11 AM
 */
use app\helpers\DateTimeHelper;
use yii\helpers\Url;
$sumOil = $data->oilSum > 0 ? $data->oilSum : 0;
$countLog = $data->carLogCount > 0 ? $data->carLogCount : 0;
?>
<h4 class="text-center text-upper margintop10 marginbottom30">Thông tin chi tiết xe</h4>
<div class="row marginleft0 marginright0 marginbottom10 paddingbottom5 border-bottom-ddd">
    <div class="col-xs-2 text-bold">Tên xe:</div><div class="col-xs-2"><?=$data->name?></div>
    <div class="col-xs-2 text-bold">Số chỗ ngồi:</div><div class="col-xs-2"><?=$data->upholsterer?></div>
    <div class="col-xs-2 text-bold">Dung tích:</div><div class="col-xs-2"><?=$data->speed?></div>
</div>
<div class="row marginleft0 marginright0 marginbottom10 paddingbottom5 border-bottom-ddd">
    <div class="col-xs-2 text-bold">Biển số:</div><div class="col-xs-2"><?=$data->code?></div>
    <div class="col-xs-2 text-bold">Số khung:</div><div class="col-xs-2"><?=$data->seria?></div>
    <div class="col-xs-2 text-bold">Số máy:</div><div class="col-xs-2"><?=$data->seria_machine?></div>
</div>
<div class="row marginleft0 marginright0 marginbottom10 paddingbottom5 border-bottom-ddd">
    <div class="col-xs-2 text-bold">Nước sản xuất:</div><div class="col-xs-2"><?=$data->country?></div>
    <div class="col-xs-2 text-bold">Hãng sản xuất:</div><div class="col-xs-2"><?=$data->manufacture?></div>
    <div class="col-xs-2 text-bold">Số km đã sử dụng:</div><div class="col-xs-2"><?=$data->total?></div>
</div>
<h5 class="text-17 text-bold margintop30 marginbottom20">Thông tin đăng kiểm</h5>

<?php if(!empty($data->registers)):?>
    <?php foreach($data->registers as $item):?>
        <div class="row marginleft0 marginright0 marginbottom10 paddingbottom5 border-bottom-ddd">
            <div class="col-xs-2 text-bold">Ngày đăng kiểm:</div><div class="col-xs-2"><?=DateTimeHelper::getDateTime($item->created,Yii::$app->params['dateFormat'])?></div>
            <div class="col-xs-2 text-bold">Ngày hết hạn:</div><div class="col-xs-2"><?=DateTimeHelper::getDateTime($item->expire,Yii::$app->params['dateFormat'])?></div>
        </div>

        <div class="row marginleft0 marginright0 marginbottom10 paddingbottom5 border-bottom-ddd">
            <div class="col-xs-2 text-bold">Đơn vị đăng kiểm:</div>
            <div class="col-xs-10"><?=$item->department?></div>
        </div>
    <?php endforeach;?>
<?php endif;?>

<div class="row marginleft0 marginright0 marginbottom10 paddingbottom5">
    <div class="col-xs-12 margintop15">
        <a href="#">Xem thông tin quá trình sử dụng của xe</a>
    </div>
    <div class="col-xs-12 margintop15">
        <a href="<?=Url::toRoute(['/admin/car-log/index','CarLog[car_id]' => $data->id ])?>">Đã bảo dưỡng <b><?=$countLog?></b> lần</a>
    </div>
    <div class="col-xs-12 margintop15">
        <a href="<?=Url::toRoute(['/admin/combustible/index','Combustible[car_id]' => $data->id ])?>">
                Đã sử dụng <b><?=$sumOil?></b> lit nhiên liệu
        </a>
    </div>
</div>
