<?php
use app\modules\user\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<?php
$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'id' => 'form-doctor',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-2',
            'offset' => 'col-md-offset-2',
            'wrapper' => 'col-md-8',
            'error' => '',
            'hint' => '',
        ],
    ],
    'options' => [
        'data-action' => Yii::$app->controller->action->id,
        'class' => 'paddingtop20'
    ]
]);
?>

    <div class="row margin0 marginbottom20">
        <?= $form->field($model, 'name')->begin(); ?>
        <?= Html::activeLabel($model, 'name', ['class' => 'col-sm-12']) ?>
        <div class="col-sm-8">
            <?= Html::activeTextInput($model,'name',['class' => 'form-control'])?>
            <?= Html::error($model, 'name', ['class' => 'help-block help-block-error']) ?>
        </div>
        <?= $form->field($model, 'name')->end(); ?>
    </div>

    <div class="row margin0 marginbottom20">
        <?= $form->field($model, 'desc')->begin(); ?>
        <?= Html::activeLabel($model, 'desc', ['class' => 'col-sm-12']) ?>
        <div class="col-sm-8">
            <?= Html::activeTextarea($model,'desc',['class' => 'form-control'])?>
            <?= Html::error($model, 'desc', ['class' => 'help-block help-block-error']) ?>
        </div>
        <?= $form->field($model, 'desc')->end(); ?>
    </div>


    <div class="row margin0 marginbottom30">
        <div class="col-xs-12 text-center">
            <?= Html::submitButton(Yii::$app->controller->action->id == 'create-speed' ? '<i class="fa fa-plus"></i> Thêm mới' : '<i class="fa fa-edit"></i> Cập nhật', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>