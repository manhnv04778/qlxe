<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this app\components\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\modules\user\models\forms\LoginForm; */

$this->title = Yii::t('user', 'Login');

?>
<div id="user-login" class="padding30">
    <div class="row" style="margin:0;">
        <div class="col-md-12 text-center" style="margin-top: 30px;">
            <h2 class="panel-title" style="font-size: 25px; color: white;text-transform: uppercase">Hệ thống quản lý sử dụng xe công</h2>
        </div>
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-primary" style="margin-top: 10%;">
                <div class="panel-heading">
                    <h3 class="panel-title">Vui lòng đăng nhập</h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'layout' => 'horizontal',
                        'id' => 'form-login',
                        'fieldConfig' => [
                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-3',
                                'offset' => 'col-sm-offset-3',
                                'wrapper' => 'col-sm-9',
                                'error' => '',
                                'hint' => '',
                            ],
                        ],
//                            'options' => [
//                                'data-action' => Yii::$app->controller->action->id
//                            ]
                    ]); ?>

                    <?=$form->field($model, 'username')->begin();?>
                    <div class="col-sm-12">
                        <?=Html::activeTextInput($model, 'username', ['class' => 'form-control', 'placeholder' => Yii::t('user','Tên đăng nhập')])?>
                        <?=Html::error($model, 'username', ['class' => 'help-block help-block-error'])?>
                    </div>
                    <?=$form->field($model, 'username')->end();?>


                    <?=$form->field($model, 'password')->begin();?>
                    <div class="col-sm-12">
                        <?=Html::activePasswordInput($model, 'password', ['class' => 'form-control', 'placeholder' => Yii::t('user','Mật khẩu')])?>
                        <?=Html::error($model, 'password', ['class' => 'help-block help-block-error'])?>
                    </div>
                    <?=$form->field($model, 'password')->end();?>

                    <div class="row">
                        <div class="col-xs-12 loginform-rememberme">
                            <?= Html::activeCheckbox($model, 'rememberMe',['label' => 'Ghi nhớ','for' => 'loginform-rememberme']) ?>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="row" style="margin:0;">
                            <div class="col-xs-12">
                                <button style="width: 100%; text-align: center;" class="btn btn-primary" type="submit"><?=Yii::t('user', 'Đăng nhập')?></button>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>