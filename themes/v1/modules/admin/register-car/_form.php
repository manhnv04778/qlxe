<?php
use app\helpers\DateTimeHelper;
use app\models\Car;
use app\models\Country;
use app\models\RegisterCar;
use app\modules\user\models\User;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<?php
$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'id' => 'form-doctor',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-2',
            'offset' => 'col-md-offset-2',
            'wrapper' => 'col-md-8',
            'error' => '',
            'hint' => '',
        ],
    ],
    'options' => [
        'data-action' => Yii::$app->controller->action->id,
        'class' => 'paddingtop20'
    ]
]);
?>

<div class="row">
    <div class="col-sm-6">
        <div class="row margin0 marginbottom10">
            <?= $form->field($model, 'user_id')->begin(); ?>
            <?= Html::activeLabel($model, 'user_id', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeDropDownList($model,'user_id',User::getUser(),['class' => 'form-control','prompt' => 'Chọn người đề xuât'])?>
                <?= Html::error($model, 'user_id', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'user_id')->end(); ?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row margin0 marginbottom10">
            <?= $form->field($model, 'driver_id')->begin(); ?>
            <?= Html::activeLabel($model, 'driver_id', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeDropDownList($model,'driver_id',User::getUser(),['class' => 'form-control','prompt' => 'Chọn tài xế'])?>
                <?= Html::error($model, 'driver_id', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'driver_id')->end(); ?>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-sm-6">
        <div class="row margin0 marginbottom20">
            <?= $form->field($model, 'content')->begin(); ?>
            <?= Html::activeLabel($model, 'content', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeTextarea($model,'content',['class' => 'form-control'])?>
                <?= Html::error($model, 'content', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'content')->end(); ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="row margin0 marginbottom10">
            <?= $form->field($model, 'car_id')->begin(); ?>
            <?= Html::activeLabel($model, 'car_id', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeDropDownList($model,'car_id',Car::getCar(),['class' => 'form-control','prompt' => 'Chọn danh mục'])?>
                <?= Html::error($model, 'car_id', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'car_id')->end(); ?>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-6">
        <div class="row margin0 marginbottom20">
            <?php
            echo '<label>Ngày bắt đầu</label>';
            echo DatePicker::widget([
                'name' => 'RegisterCar[start]',
                'type' => DatePicker::TYPE_INPUT,
                'value' => DateTimeHelper::getDateTime('now','d-m-Y'),
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-M-yyyy'
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="row margin0 marginbottom20">
            <?php
            echo '<label>Ngày kết thúc</label>';
            echo DatePicker::widget([
                'name' => 'RegisterCar[end]',
                'type' => DatePicker::TYPE_INPUT,
                'value' => DateTimeHelper::getDateTime('now','d-m-Y'),
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-M-yyyy'
                ]
            ]);
            ?>
        </div>
    </div>
</div>

    <div class="row margin0 marginbottom30">
        <div class="col-xs-12 text-center">
            <?= Html::submitButton(Yii::$app->controller->action->id == 'create' ? '<i class="fa fa-plus"></i> Thêm mới' : '<i class="fa fa-edit"></i> Cập nhật', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>