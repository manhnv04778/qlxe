<?php
/**
 * Created by PhpStorm.
 * User: Duy Dat
 * Date: 8/31/2016
 * Time: 10:11 AM
 */
use yii\helpers\Url;

?>

<tr>
    <th><?=$model->id?></th>
    <th><?=$model->user->full_name?></th>
    <th><?=$model->driver->full_name?></th>
    <th><?=$model->start?></th>
    <th><?=$model->end?></th>
    <th><?$model->date_suggest?></th>
    <th><?$model->license?></th>
    <th>
        <?php
            if($model->status==0){
                echo 'Chờ tiếp nhận';
            }elseif($model->status==1){
                echo 'Đã tiếp nhận';
            }elseif($model->status==2){
                echo 'Đã duyệt';
            }elseif($model->status==3){
                echo 'Không duyệt';
            }
        ?>
    </th>
    <td>
        <a class="btn text-success" href="<?=Url::to(['/admin/register-car/approve', 'id' => $model->id])?>"><i class="fa fa-check" aria-hidden="true"></i> Duyệt</a>
        <a class="btn text-success" href="<?=Url::to(['/admin/register-car/approve', 'id' => $model->id])?>"><i class="fa fa-times" aria-hidden="true"></i> Không chấp nhận</a>
        <a class="btn text-success" href="<?=Url::to(['/admin/register-car/update', 'id' => $model->id])?>"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
        <a class="btn text-error" data-pjax="0" data-method="post" data-confirm="Bạn có chắc chắn xoá không?" href="<?=Url::to(['/admin/register-car/delete', 'id' => $model->id])?>"><i class="fa fa-times" aria-hidden="true"></i> Xoá</a>
    </td>
</tr>