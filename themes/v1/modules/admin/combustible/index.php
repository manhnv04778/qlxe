<?php
use app\models\Car;
use app\modules\admin\components\AppAsset;
use kartik\dynagrid\DynaGrid;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

//$this->removeJsFile('/files/themes/admin/bower_components/bootstrap/dist/js/bootstrap.min.js', ['depends' => [AppAsset::className()]]);
Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [];
$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/admin/combustible/combustible.js', ['depends' => [AppAsset::className()]]);

$cars = Car::getCar();
?>
<div class="action-content">
    <div class="row">
        <div class="col-xs-6"></div>
        <div class="col-xs-6 text-right export-group">
            <div class="button-group float-right marginleft15">
                <a class="btn btn-success" href="<?=Url::toRoute(['/admin/combustible/create'])?>"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</a>
            </div>
            <?php

            $gridColumns = [
                [
                    'attribute'=>'id',
                    'label'=>'Mã',
                ],
                [
                    'attribute'=>'name',
                    'label'=>'Tên quá trình sử dụng',
                    'filter' => false
                ],
                [
                    'attribute'=>'desc',
                    'label'=>'Mô tả',
                    'filter' => false
                ],
                [
                    'attribute'=>'car_id',
                    'label'=>'Xe',
                    'filter' => false,
                    'value' => function($model){
                        return $model->car_id ? $model->car->code : '';
                    }
                ],
            ];


            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Xuất file',
                    'class' => 'btn btn-default'
                ]
            ]);
            ?>

        </div>
    </div>

    <div class="row">
        <form class="" id="form-search" action="<?=Url::to(['/admin/combustible/index'])?>" method="get">
            <div class="row">
                <div class="col-xs-2">
                    <?=Html::dropDownList('Combustible[car_id]',!empty($params['Combustible']['car_id']) ? $params['Combustible']['car_id'] : null,Car::getCar('id','code'),['class' => 'form-control','prompt' => 'Chọn xe'])?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <?=Html::submitButton('Tìm kiếm',['class' => 'btn btn-success'])?>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="list margintop30">
                <?php
                if($dataProvider->models){
                    $actionColumn = [
                        'class' => '\kartik\grid\ActionColumn',
                        'width' => '250px',
                        'buttons' => [
                            'view' => function($url1,$model){
                                $u = $model->id;
                                return '<a class="btn text-success view" data-id="'.$u.'" href="'.Url::toRoute(['/admin/combustible/view']).'"><i class="fa fa-eye" aria-hidden="true"></i> Chi tiết</a>';
                            },
                            'update' => function($url2, $model){
                                return '<a class="btn text-success" href="'.Url::toRoute(['/admin/combustible/update','id' => $model->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>';
                            },
                            'delete' => function($url3, $model){
                                return '<a class="btn text-error" data-pjax="0" data-method="post" data-confirm="Bạn có chắc chắn xoá không?" href="'.Url::toRoute(['/admin/combustible/delete','id' => $model->id]).'"><i class="fa fa-times" aria-hidden="true"></i> Xoá</a>';
                            }
                        ]
                    ];
                    $gridColumns[] = $actionColumn;
                }



                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                ]);
                ?>

            </div>
        </div>
    </div>
</div>

<div id="modal-view" class="modal fade modal-confirm in" role="dialog" data-toggle="modal">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content radius0">
            <div class="modal-header padding0">
                <h4 class="modal-title bg-blue text-upper text-white padding10 text-center">Chi sử dụng nhiên liệu</h4>
            </div>

            <div class="modal-body">
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-2">Tên xe:</div><div class="col-xs-10"><span class="name text-bold"></span></div>
                </div>

                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-2">Biển số:</div><div class="col-xs-4"><span class="code text-bold"></span></div>
                    <div class="col-xs-2">Màu:</div><div class="col-xs-2"><span class="color text-bold"></span></div>
                </div>

                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Số chỗ ngồi:</div><div class="col-xs-3"><span class="upholsterer text-bold"></span></div>
                    <div class="col-xs-2">Dung tích:</div><div class="col-xs-2"><span class="speed text-bold"></span></div>
                </div>

                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Nước sản xuất:</div><div class="col-xs-3"><span class="country text-bold"></span></div>
                    <div class="col-xs-3">Nhà sản xuất:</div><div class="col-xs-3"><span class="manufacture text-bold"></span></div>
                </div>

                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Ngày đăng kiểm:</div><div class="col-xs-3"><span class="time_start text-bold"></span></div>
                    <div class="col-xs-3">Ngày hết hạn:</div><div class="col-xs-3"><span class="time_end text-bold"></span></div>
                </div>

                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-12">Hình thức sử dụng: <span class="type text-bold"></span></div>
                </div>
            </div>
        </div>
    </div>
</div>