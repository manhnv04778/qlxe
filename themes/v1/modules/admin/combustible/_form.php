<?php
use app\helpers\DateTimeHelper;
use app\models\Car;
use app\models\Color;
use app\models\Combustible;
use app\models\Country;
use app\models\Manufacture;
use app\models\Speed;
use app\models\Type;
use app\models\Upholsterer;
use app\modules\user\models\User;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<?php
$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'id' => 'form-doctor',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-2',
            'offset' => 'col-md-offset-2',
            'wrapper' => 'col-md-8',
            'error' => '',
            'hint' => '',
        ],
    ],
    'options' => [
        'data-action' => Yii::$app->controller->action->id,
        'class' => 'paddingtop20'
    ]
]);
?>
    <div class="row margin0">
        <?= $form->field($model, 'name')->begin(); ?>
        <?= Html::activeLabel($model, 'name', ['class' => 'col-sm-12']) ?>
        <div class="col-sm-12">
            <?= Html::activeTextInput($model,'name',['class' => 'form-control'])?>
            <?= Html::error($model, 'name', ['class' => 'help-block help-block-error']) ?>
        </div>
        <?= $form->field($model, 'name')->end(); ?>
    </div>

    <div class="row margin0">
        <?= $form->field($model, 'desc')->begin(); ?>
        <?= Html::activeLabel($model, 'desc', ['class' => 'col-sm-12']) ?>
        <div class="col-sm-12">
            <?= Html::activeTextarea($model,'desc',['class' => 'form-control','rows' => 5])?>
            <?= Html::error($model, 'desc', ['class' => 'help-block help-block-error']) ?>
        </div>
        <?= $form->field($model, 'desc')->end(); ?>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <div class="row margin0">
                <?= $form->field($model, 'car_id')->begin(); ?>
                <?= Html::activeLabel($model, 'car_id', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeDropDownList($model,'car_id',Car::getCar('id','code'),['class' => 'form-control'])?>
                    <?= Html::error($model, 'car_id', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'car_id')->end(); ?>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="row margin0">
                <?= $form->field($model, 'capacity')->begin(); ?>
                <?= Html::activeLabel($model, 'capacity', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'capacity',['class' => 'form-control'])?>
                    <?= Html::error($model, 'capacity', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'capacity')->end(); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <div class="row margin0">
                <?= $form->field($model, 'lenght')->begin(); ?>
                <?= Html::activeLabel($model, 'lenght', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'lenght',['class' => 'form-control'])?>
                    <?= Html::error($model, 'lenght', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'lenght')->end(); ?>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="row margin0">
                <?= $form->field($model, 'type')->begin(); ?>
                <?= Html::activeLabel($model, 'type', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeDropDownList($model,'type',Combustible::getType(),['class' => 'form-control'])?>
                    <?= Html::error($model, 'type', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'type')->end(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="row margin0">
                <?= $form->field($model, 'amount')->begin(); ?>
                <?= Html::activeLabel($model, 'amount', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'amount',['class' => 'form-control'])?>
                    <?= Html::error($model, 'amount', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'amount')->end(); ?>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="row margin0">
                <?= $form->field($model, 'billing_number')->begin(); ?>
                <?= Html::activeLabel($model, 'billing_number', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'billing_number',['class' => 'form-control'])?>
                    <?= Html::error($model, 'billing_number', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'billing_number')->end(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="row margin0">
                <?= $form->field($model, 'status')->begin(); ?>
                <?= Html::activeLabel($model, 'status', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeDropDownList($model,'status',Combustible::getStatus(),['class' => 'form-control'])?>
                    <?= Html::error($model, 'status', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'status')->end(); ?>
            </div>
        </div>
    </div>

    <div class="row margin0 marginbottom30">
        <div class="col-xs-12 text-center">
            <?= Html::submitButton(Yii::$app->controller->action->id == 'create' ? '<i class="fa fa-plus"></i> Thêm mới' : '<i class="fa fa-edit"></i> Cập nhật', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>