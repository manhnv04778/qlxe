<?php
/**
 * Created by PhpStorm.
 * User: Duy Dat
 * Date: 8/31/2016
 * Time: 10:11 AM
 */
use yii\helpers\Url;

?>

<tr>
    <td><?=$index + 1?></td>
    <td><?=$model->description?></td>
    <td>
        <a class="btn text-success" href="<?=Url::to(['/admin/role/update', 'name' => $model->name])?>"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
        <a class="btn text-error" data-pjax="0" data-method="post" data-confirm="Bạn có chắc chắn xoá không?" href="<?=Url::to(['/admin/role/delete', 'name' => $model->name])?>"><i class="fa fa-times" aria-hidden="true"></i> Xoá</a>
    </td>
</tr>