<div class="action-content">
    <div class="row">
        <div class="col-xs-10 col-sm-offset-1">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="bg-blue text-center padding10 text-white text-upper text-bold">Thêm vai trò mới</h4>
                </div>
            </div>
        <?= $this->render('_form', [
            'roleForm' => $roleForm,
            'permissions' => $permissions,
            'permissinOfRole' => $permissinOfRole
        ]) ?>
        </div>
    </div>
</div>