<?php
use app\components\assets\AppAsset;
use app\modules\user\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/admin/role/role.js', ['depends' => [AppAsset::className()]]);
?>

<?php
$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'id' => 'form-doctor',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-2',
            'offset' => 'col-md-offset-2',
            'wrapper' => 'col-md-8',
            'error' => '',
            'hint' => '',
        ],
    ],
    'options' => [
        'data-action' => Yii::$app->controller->action->id
    ]
]);
?>
    <div class="row">
        <div class="col-xs-12">
            <div class="row margin0 marginbottom20">
                <?= $form->field($roleForm, 'description')->begin(); ?>
                <?= Html::activeLabel($roleForm, 'description', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($roleForm,'description',['class' => 'form-control'])?>
                    <?= Html::error($roleForm, 'description', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($roleForm, 'description')->end(); ?>
            </div>

            <div class="row">
                <div class="col-xs-12"><h5 class="text-bold text-15 marginbottom20 float-left">Quyền của vai trò</h5><label class="float-right display-block"><input type="checkbox" value="" name="check-all">Chọn tất cả</label></div>
                <?php if(!empty($permissions)):?>
                    <?php foreach($permissions as $k => $p):?>
                        <div class="row margin0 margintop20">
                            <h4 class="paddingleft15 marginbottom0 text-13 text-bold text-italic"><?=$p['group_name']?></h4>
                            <?php if(!empty($p['data'])):?>
                                <?php foreach($p['data'] as $per => $item):?>
                                    <div class="checkbox col-xs-3">
                                        <label><input type="checkbox" value="<?=$per?>" name="RoleForm[permission][]" <?=in_array($per,$permissinOfRole) ? 'checked' : ''?>><?=$item?></label>
                                    </div>
                                <?php endforeach;?>
                            <?php endif;?>
                            <hr class="divider">
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
            </div>

            <div class="row margin0 margintop30 marginbottom30">
                <div class="col-xs-12 text-center">
                    <?= Html::submitButton(Yii::$app->controller->action->id == 'create' ? '<i class="fa fa-plus"></i> Thêm Quyền' : '<i class="fa fa-edit"></i> Cập nhật Quyền', ['class' => 'btn']) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>