<?php
use app\components\assets\AppAsset;
use app\models\AuthItem;
use app\modules\user\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$this->registerJsFile(STATIC_FILE.$this->theme->baseUrl.'/files/js/modules/admin/role/add_user.js', ['depends' => [AppAsset::className()]]);
?>
<div class="action-content">
    <?php
    $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'form-doctor',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-md-2',
                'offset' => 'col-md-offset-2',
                'wrapper' => 'col-md-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        'options' => [
            'data-action' => Yii::$app->controller->action->id,
            'class' => 'margintop30'
        ]
    ]);
    ?>
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="bg-blue text-center padding10 text-white text-upper text-bold marginbottom30">Cấp vai trò cho người dùng</h4>
                </div>
            </div>
            <div class="row margin0 marginbottom20">
                <?= $form->field($addUserForm, 'user_id')->begin(); ?>
                <?= Html::activeLabel($addUserForm, 'user_id', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($addUserForm,'user_id',['class' => 'form-control'])?>
                    <?= Html::error($addUserForm, 'user_id', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($addUserForm, 'user_id')->end(); ?>
            </div>

            <div class="row margin0 marginbottom20">
                <?= $form->field($addUserForm, 'item_name')->begin(); ?>
                <?= Html::activeLabel($addUserForm, 'item_name', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeDropDownList($addUserForm,'item_name',AuthItem::getRole(),['class' => 'form-control','prompt' => 'Chọn vai trò'])?>
                    <?= Html::error($addUserForm, 'item_name', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($addUserForm, 'item_name')->end(); ?>
            </div>

            <div class="row margin0 margintop30 marginbottom30">
                <div class="col-xs-12 text-center">
                    <?= Html::submitButton(Yii::$app->controller->action->id == 'create' ? '<i class="fa fa-plus"></i> Thêm Quyền' : '<i class="fa fa-edit"></i> Cập nhật Quyền', ['class' => 'btn']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
