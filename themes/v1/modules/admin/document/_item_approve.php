<?php
/**
 * Created by PhpStorm.
 * User: Duy Dat
 * Date: 8/31/2016
 * Time: 10:11 AM
 */
use app\helpers\DateTimeHelper;
use yii\helpers\Url;

?>

<tr>
    <td><?=$index+1;?></td>
    <td><?=$model->code;?></td>
    <td><?=!empty($model->created_date) ? DateTimeHelper::getDateTime($model->created_date,'H:i d-m-Y') :  ''?></td>
    <td><?=$model->full_name?></td>
    <td><?=!empty($model->approve_date) ? DateTimeHelper::getDateTime($model->approve_date,'H:i d-m-Y') :  ''?></td>
    <td><?=!empty($model->date_from_car) ? DateTimeHelper::getDateTime($model->date_from,'d-m-Y') :  ''?></td>
    <td><?=!empty($model->date_to_car) ? DateTimeHelper::getDateTime($model->date_to,'d-m-Y') :  ''?></td>
    <td>
        <?php

            if($model->status==1){
                echo 'Chờ duyệt';
            }elseif($model->status==2){
                echo 'Đã duyệt';
            }elseif($model->status==3){
                echo 'Hủy';
            }
        ?>
    </td>
</tr>