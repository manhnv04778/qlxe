<?php
use app\components\assets\AppAsset;
use app\models\City;
use app\models\Document;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/admin/document/document.js', ['depends' => [AppAsset::className()]]);
?>
<div class="action-content">
    <div class="row paddingtop20">
        <div class="col-xs-12 padding0 bg-white">
            <h4 class="modal-title bg-blue text-upper text-white padding10">Tìm kiếm hồ sơ</h4>
            <form class="form-searchs" id="form-searchs" action="<?=Url::to(['/admin/document/index'])?>" method="get">
                <div class="form-content">
                    <div class="row padding15">
                        <div class="col-xs-3">
                            <?=Html::label('Mã hồ sơ',null,['class'=> 'text-normal'])?>
                            <?=Html::textInput('Document[code]',!empty($params['Document']['code']) ? $params['Document']['code'] : '',['class' => 'form-control','placeholder' => 'Mã hồ sơ'])?>
                        </div>
                        <div class="col-xs-4 col-xs-offset-1 padding0">
                            <?=Html::label('Tên hồ sơ', '' ,['class'=> 'text-normal'])?>
                            <?=Html::textInput('Document[name]', !empty($params['Document']['name']) ? $params['Document']['name'] : '', ['class' => 'form-control','placeholder' => 'Tên hồ sơ'])?>
                        </div>
                        <div class="col-xs-3 col-xs-offset-1  padding0 paddingright15">
                            <?=Html::label('Tỉnh/TP công tác',null,['class'=> 'text-normal'])?>
                            <?=Html::dropDownList('Document[from]', !empty($params['Document']['from']) ? $params['Document']['from'] : '' , City::getCity(),['class' => 'form-control' ,'prompt' => 'Chọn tỉnh/thành'])?>
                        </div>
                    </div>
                    <div class="row padding15 paddingtop0">
                        <div class="col-xs-3">
                            <?=Html::label('Từ ngày',null,['class'=> 'text-normal'])?>
                            <?=DatePicker::widget([
                                'name' => 'Document[created_from]',
                                'type' => DatePicker::TYPE_INPUT,
                                'value'=> !empty($params['Document']['created_from']) ? $params['Document']['created_from'] : '',
                                'class' => 'form-control',
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd/mm/yyyy'
                                ]
                            ]);
                            ?>
                        </div>
                        <div class="col-xs-3 col-xs-offset-1 padding0">
                            <?=Html::label('Đến ngày',null,['class'=> 'text-normal'])?>
                            <?=DatePicker::widget([
                                'name' => 'Document[created_to]',
                                'type' => DatePicker::TYPE_INPUT,
                                'value'=> !empty($params['Document']['created_to']) ? $params['Document']['created_to'] : '',
                                'class' => 'form-control',
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd/mm/yyyy'
                                ]
                            ]);
                            ?>
                        </div>
                        <div class="col-xs-3 col-xs-offset-2 padding0 paddingright15">
                            <?=Html::label('Tình trạng hồ sơ',null,['class'=> 'text-normal'])?>
                            <?=Html::dropDownList('Document[status]', !empty($params['Document']['status']) ? $params['Document']['status'] : '', Document::getStatus(),['class' => 'form-control' ,'prompt' => 'Tình trạng hồ sơ'])?>
                        </div>
                    </div>
                    <div class="row margintop20 paddingbottom30">
                        <div class="col-xs-12 text-center">
                            <button class="btn btn-success"><i class="fa fa-search" aria-hidden="true"></i> Tìm kiếm</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="list paddingbottom60">
                <div class="text-blue text-bold">
                    <div class="col-xs-3 float-left paddingtop20">
                        Tổng số: <span class="text-error text-bold text-16"><?= StringHelper::numberFormat($dataProvider->totalCount)?></span>
                    </div>
                    <div class="col-xs-2 float-right paddingtop15">
                        <a class="btn btn-success" href="<?=Url::toRoute(['/frontend/document/create'])?>"><i class="fa fa-plus" aria-hidden="true"></i> Thêm Đăng ký xe</a>
                    </div>
                </div>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">STT</th>
                        <th class="text-center">Mã HS</th>
                        <th class="text-center">Tên hồ sơ</th>
                        <th class="text-center">Ngày tạo</th>
                        <th class="text-center">Người gửi</th>
                        <th class="text-center">Công tác từ</th>
                        <th class="text-center">Công tác đến</th>
                        <th class="text-center">Trạng thái</th>
                        <th width="100" class="text-center">Duyệt/Từ chối</th>
                    </tr>
                    </thead>
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'options' => [
                            'tag' => 'tbody'
                        ],
                        'summary' => '',
                        'emptyText' => '',
                        'itemView' => '_item',
                        /* 'pager' => [
                             'class' => \kop\y2sp\ScrollPager::className(),
                             'triggerTemplate' => '<div class="app-more margintop20">
                                                     <a class="btn padding20 text-center" href="#">{text}</a>
                                                 </div>',
                             'triggerText' => 'Xem thêm',
                             'noneLeftText' => '',
                         ],*/
                    ]);
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="modal-view" class="modal fade modal-confirm in" role="dialog" data-toggle="modal">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content radius0">
            <div class="modal-header padding0">
                <h4 class="modal-title bg-blue text-upper text-white padding10 text-center">Thông tin hồ sơ</h4>
            </div>

            <div class="modal-body">
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Mã hồ sơ:</div>
                    <div class="col-xs-3">
                        <span class="code text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tên hồ sơ:</div>
                    <div class="col-xs-9">
                        <span class="name text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tên lãnh đạo:</div>
                    <div class="col-xs-3">
                        <span class="full_name text-bold"></span>
                    </div>
                    <div class="col-xs-3">Đơn vị:</div>
                    <div class="col-xs-3">
                        <span class="department text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tỉnh thành đến công tác :</div>
                    <div class="col-xs-3">
                        <span class="from text-bold"></span>
                    </div>
                    <div class="col-xs-3">Địa điểm đến công tác:</div>
                    <div class="col-xs-3">
                        <span class="from_name text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Số cán bộ đi công tác:</div>
                    <div class="col-xs-3">
                        <span class="number_user text-bold"></span>
                    </div>
                    <div class="col-xs-3">Ngày gửi:</div>
                    <div class="col-xs-3">
                        <span class="created_date text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian công tác từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian công tác đến:</div>
                    <div class="col-xs-3">
                        <span class="date_to text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian nhận xe từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from_car text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian trả xe:</div>
                    <div class="col-xs-3">
                        <span class="date_to_car text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Mục đích sử dụng xe:</div>
                    <div class="col-xs-9">
                        <span class="purpose_car text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian đón cán bộ từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from_user text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian trả cán bộ:</div>
                    <div class="col-xs-3">
                        <span class="date_to_user text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Địa điểm trả cán bộ:</div>
                    <div class="col-xs-9">
                        <span class="place_user text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Địa điểm trả cán bộ:</div>
                    <div class="col-xs-9">
                        <span class="note_document text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Danh sách cán bộ:</div>
                    <div class="col-xs-9">
                        <span class="list_can_bo_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10 text-center">
                    <button class="btn btn-danger cancel-view" type="button" value = "Hủy"> <i class='fa fa-times'></i>Thoát</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-approve" class="modal fade modal-confirm in" role="dialog" data-toggle="modal">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content radius0">
            <div class="modal-header padding0">
                <h4 class="modal-title bg-blue text-upper text-white padding10 text-center">Phê duyệt hồ sơ</h4>
            </div>

            <div class="modal-body">
                <?php
                $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'id' => 'form-doctor',
                    'action'=>'/admin/document/approve-document',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-md-2',
                            'offset' => 'col-md-offset-2',
                            'wrapper' => 'col-md-12',
                            'error' => '',
                            'hint' => '',
                        ],
                    ],
                    'options' => [
                        //'data-action' => Yii::$app->controller->action->id,
                        'class' => ''
                    ]
                ]);
                ?>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Mã hồ sơ:</div>
                    <div class="col-xs-3">
                        <span class="code_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tên hồ sơ:</div>
                    <div class="col-xs-9">
                        <span class="name_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tên lãnh đạo:</div>
                    <div class="col-xs-3">
                        <span class="full_name_approve text-bold"></span>
                    </div>
                    <div class="col-xs-3">Đơn vị:</div>
                    <div class="col-xs-3">
                        <span class="department_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tỉnh thành đến công tác :</div>
                    <div class="col-xs-3">
                        <span class="from_approve text-bold"></span>
                    </div>
                    <div class="col-xs-3">Địa điểm đến công tác:</div>
                    <div class="col-xs-3">
                        <span class="from_name_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Số cán bộ đi công tác:</div>
                    <div class="col-xs-3">
                        <span class="number_user_approve text-bold"></span>
                    </div>
                    <div class="col-xs-3">Ngày gửi:</div>
                    <div class="col-xs-3">
                        <span class="created_date_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian công tác từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from_approve text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian công tác đến:</div>
                    <div class="col-xs-3">
                        <span class="date_to_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian nhận xe từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from_car_approve text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian trả xe:</div>
                    <div class="col-xs-3">
                        <span class="date_to_car_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Mục đích sử dụng xe:</div>
                    <div class="col-xs-9">
                        <span class="purpose_car_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian đón cán bộ từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from_user_approve text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian trả cán bộ:</div>
                    <div class="col-xs-3">
                        <span class="date_to_user_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Địa điểm trả cán bộ:</div>
                    <div class="col-xs-9">
                        <span class="place_user_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Địa điểm trả cán bộ:</div>
                    <div class="col-xs-9">
                        <span class="note_document_approve text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Số km:</div>
                    <div class="col-xs-3">
                        <input type="text" name="length" class="length_approve form-control" />
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Danh sách cán bộ:</div>
                    <div class="col-xs-9">
                        <span class="list_can_bo_approve text-bold"></span>
                    </div>
                </div>

                <div class="col-xs-12 marginbottom10 border-bottom-ddd paddingleft0 paddingbottom10">
                    <div class="col-xs-3">Tình trạng duyệt:</div>
                    <div class="col-xs-9 paddingleft0 ">
                        <div class ="col-xs-2">
                            <input type ="radio" name = "is_approve" value ="approved" checked> Duyệt
                        </div>
                        <div class ="col-xs-3">
                            <input type ="radio" name = "is_approve" value ="not_approved"> Không duyệt
                        </div>
                    </div>
                </div>
                <!--Begin Duyệt-->
                <div id ="approves">
                    <div class="row paddingleft15 approves col-xs-12 marginbottom10 border-bottom-ddd paddingleft0 paddingbottom10">
                        <div class="col-xs-3">Chọn xe:</div>
                        <div class="col-xs-3">
                            <select name = "select_car[]" id="list-car" class="list_car form-control">

                            </select>
                        </div>
                        <div class="col-xs-2 paddingleft0 text-right">Chọn tài xế:</div>
                        <div class="col-xs-3">
                            <select name = "select_driver[]" id="select_driver" class="list_driver form-control">

                            </select>
                        </div>
                    </div>
                </div>

                <div id="btn-add-cars">
                    <div id ="approves" class="approves col-xs-12 marginbottom10 border-bottom-ddd paddingleft0 paddingbottom10">
                        <div class="col-xs-3 col-xs-offset-3">
                            <a id="add-car-driver" class="btn bg-blue text-white form-control" data-id="" href="javascript();"><i class="fa fa-plus-circle text-18" aria-hidden="true"></i>Thêm xe và lái xe</a>
                        </div>
                    </div>
                </div>
                <!-- End begin duyệt -->
                <div class="col-xs-12 marginbottom10 border-bottom-ddd paddingleft0 paddingbottom10">
                    <div class="col-xs-3">Ghi chú</div>
                    <div class="col-xs-9 paddingleft0 ">
                        <textarea id="note_approve" name = "note_approve" class="form-control" rows="4"></textarea>
                    </div>
                </div>
                <div class="row margin0 marginbottom30">
                    <div class="col-xs-12 text-center">
                        <button class="btn btn-success update-approve" type="button" value = "Gửi hồ sơ"> <i class='fa fa-plus'></i>Cập nhật</button>
                        <button class="btn btn-danger cancel" type="button" value = "Hủy"> <i class='fa fa-times'></i>Thoát</button>
                        <input type ="hidden" class="hdf-id-update" name = "hdf-id-update" id = "hdf-id-update" value="" />
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>

            </div>
        </div>
    </div>
</div>

<div id="modal-view-history" class="modal fade modal-confirm in" role="dialog" data-toggle="modal">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content radius0">
            <div class="modal-header padding0">
                <h4 class="modal-title bg-blue text-upper text-white padding10 text-center">Chi tiết hồ sơ</h4>
            </div>

            <div class="modal-body">
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="history"></div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-7 col-xs-offset-5">
                        <button class="btn btn-danger cancel" type="button" value="Thoát"> <i class="fa fa-times"></i>Thoát</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>



