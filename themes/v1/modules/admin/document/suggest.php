<?php
use app\components\assets\AppAsset;
use app\helpers\DateTimeHelper;
use yii\bootstrap\Html;

$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/admin/document/printThis-master/printThis.js', ['depends' => [AppAsset::className()]]);
$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/admin/document/suggest.js', ['depends' => [AppAsset::className()]]);
?>
<div class="col-xs-10 col-xs-offset-1 bg-white">
    <div id="print" style = "padding-top:100px">
        <div class="row">
            <div class="col-xs-6  text-center text-upper text-bold">
                <p>Văn phòng bộ nông nghiệp <br/>và phát triển nông thôn</p>
            </div>
            <div class="col-xs-6 text-upper text-bold ">
                <p class="text-center">
                Cộng hòa xã hội chủ nghĩa việt nam<br/>
                Độc lập - Tự do - Hạnh phúc<br/>
                --------------------
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center">
                <p class="text-upper text-center text-18 text-bold">Lệnh điều xe</p>
                <p class="text-center  text-18">Số: <?=$model->code;?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-xs-offset-1">
                <p>- Căn cứ quyết định của: <?=$model->userApprove->full_name;?></p>
                <p>- Biển số xe: <?=$list_car_code?></p>
                <p>- Họ tên người lái xe:
                    <?php foreach($list_car as $key => $item):?>
                        <p style="margin-left:140px">+ <?=$item->user->full_name;?></p>
                    <?php endforeach;?>
                </p>
                <p>- <span>Họ tên người dùng xe: <?=$model->full_name?></span><span style ="margin-left: 100px;">Chức vụ: <?=$model->user->position->name;?></span></p>
                <p>- Đơn vị: <?=$model->user->department->name;?></p>
                <p>- Nơi đến công tác: <?=$list_address?>.</p>
                <p>- Nội dung công việc: <?=$model->name;?></p>
                <p>- Danh sách cán bộ đi công tác:
                    <?php foreach($list_user as $key => $item):?>
                        <p style="margin-left:140px">+ <?=$item->user_name;?></p>
                    <?php endforeach;?>
                </p>
                <p>- Thời gian đón cán bộ: <?=DateTimeHelper::getDateTime($model->date_from_user, 'H:i d/m/Y')?></p>
                <p>- Đón cán bộ tại: <?=$model->place_user;?></p>
                <p>- Số Km dự kiến: <?=$model->lenght?> km</p>
                <p>- Đi từ ngày: <?=DateTimeHelper::getDateTime($model->date_from, 'H:i d/m/Y')?> </p>
                <p>- Đến ngày: <?=DateTimeHelper::getDateTime($model->date_to, 'H:i d/m/Y')?> </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-11 col-xs-offset-1">
                <div class="col-xs-6">
                </div>
                <div class="col-xs-6 text-center">
                    <p>Ngày ... tháng ... năm ......</p>
                    <p class="text-upper">Lãnh đạo ký duyệt</p>
                    <p class="text-center" style="margin-bottom: 100px;">(Ký, họ tên)</p>

                    <p class="text-bold text-upper"><?=$model->userApprove->full_name;?></p>
                </div>
            </div>
        </div>
        <div class="row" style ="margin-top:200px">
            <div class="col-xs-11 col-xs-offset-1">
                Bảng danh sách cán bộ đi công tác:
                <table class="table table-strip">
                    <tr>
                        <th class="text-center">STT</th>
                        <th class="text-center">Họ và tên</th>
                        <th class="text-center">Chức danh</th>
                        <th class="text-center">Đơn vị</th>
                        <th class="text-center">Điện thoại</th>
                        <th class="text-center">Email</th>
                    </tr>
                    <?php
                        foreach($list_user as $key => $item):
                    ?>
                        <tr>
                            <td class="text-center"><?=$key+1;?></td>
                            <td class="text-center"><?=$item->user_name?></td>
                            <td class="text-center"><?=$item->position?></td>
                            <td class="text-center"><?=$item->department?></td>
                            <td class="text-center"><?=$item->phone?></td>
                            <td class="text-center"><?=$item->email?></td>
                        </tr>
                    <?php
                        endforeach;
                    ?>
                </table>
            </div>
        </div>
    </div>
    <div class="row" style = "margin-top:30px; padding-bottom:100px">
        <div class="col-xs-10 col-xs-offset-1 text-center">
<!--            <a href="javascript:void(0)" onclick="In_Content('Content_ID')">IN</a>-->
            <?=Html::button('In lệnh điều động',['class' => 'btn btn-success','id' => 'print-document'])?>
        </div>
    </div>
</div>