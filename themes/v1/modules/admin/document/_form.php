<?php
use app\helpers\DateTimeHelper;
use app\models\Car;
use app\models\Country;
use app\models\Department;
use app\models\RegisterCar;
use app\modules\user\models\User;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<?php
$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'id' => 'form-doctor',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-md-2',
            'offset' => 'col-md-offset-2',
            'wrapper' => 'col-md-12',
            'error' => '',
            'hint' => '',
        ],
    ],
    'options' => [
        'data-action' => Yii::$app->controller->action->id,
        'class' => 'paddingtop20'
    ]
]);
?>
<div class="row">
    <div class="col-sm-6">
        <div class="row margin0 marginbottom10">
            <?= $form->field($model, 'code')->begin(); ?>
            <?= Html::activeLabel($model, 'code', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-6">
                <?= Html::activeTextInput($model,'code',['class' => 'form-control'])?>
                <?= Html::error($model, 'code', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'code')->end(); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="row margin0 marginbottom10">
            <?= $form->field($model, 'name')->begin(); ?>
            <?= Html::activeLabel($model, 'name', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeTextarea($model,'name',['class' => 'form-control'])?>
                <?= Html::error($model, 'name', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'name')->end(); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row margin0 marginbottom10">
            <?= $form->field($model, 'full_name')->begin(); ?>
            <?= Html::activeLabel($model, 'full_name', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeTextInput($model,'full_name',['class' => 'form-control'])?>
                <?= Html::error($model, 'full_name', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'full_name')->end(); ?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row margin0 marginbottom10">
            <?= $form->field($model, 'department_id')->begin(); ?>
            <?= Html::activeLabel($model, 'department_id', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeDropDownList($model,'department_id',Department::getDepartment(),['class' => 'form-control','prompt' => 'Chọn đơn vị'])?>
                <?= Html::error($model, 'department_id', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'department_id')->end(); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row margin0 marginbottom10">
            <?= $form->field($model, 'from')->begin(); ?>
            <?= Html::activeLabel($model, 'from', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeDropDownList($model,'from',Department::getDepartment(),['class' => 'form-control','prompt' => 'Chọn đơn vị'])?>
                <?= Html::error($model, 'from', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'from')->end(); ?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row margin0 marginbottom10">
            <?= $form->field($model, 'from_name')->begin(); ?>
            <?= Html::activeLabel($model, 'from_name', ['class' => 'col-sm-12']) ?>
            <div class="col-sm-12">
                <?= Html::activeTextInput($model,'from_name',['class' => 'form-control'])?>
                <?= Html::error($model, 'from_name', ['class' => 'help-block help-block-error']) ?>
            </div>
            <?= $form->field($model, 'from_name')->end(); ?>
        </div>
    </div>
</div>

    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom10">
                <?= $form->field($model, 'attachment')->begin(); ?>
                <?= Html::activeLabel($model, 'attachment', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeFileInput($model,'attachment',['class' => 'form-control'])?>
                    <?= Html::error($model, 'attachment', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'attachment')->end(); ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row margin0 marginbottom10">
                <?= $form->field($model, 'number_user')->begin(); ?>
                <?= Html::activeLabel($model, 'number_user', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-6">
                    <?= Html::activeTextInput($model,'number_user',['class' => 'form-control'])?>
                    <?= Html::error($model, 'number_user', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'number_user')->end(); ?>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-sm-6">
        <div class="row margin0 marginbottom20">
            <?php
            echo '<label>Ngày bắt đầu đi công tác</label>';
            echo DatePicker::widget([
                'name' => 'Document[date_from]',
                'type' => DatePicker::TYPE_INPUT,
                'value' => !empty($model->date_from) ? DateTimeHelper::getDateTime($model->date_from,'d-m-Y') :  DateTimeHelper::getDateTime('now','d-m-Y'),
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'd-m-yyyy'
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="row margin0 marginbottom20">
            <?php
            echo '<label>Ngày kết thúc công tác</label>';
            echo DatePicker::widget([
                'name' => 'Document[date_to]',
                'type' => DatePicker::TYPE_INPUT,
                'value' => !empty($model->date_to) ? DateTimeHelper::getDateTime($model->date_to,'d-m-Y') :  DateTimeHelper::getDateTime('now','d-m-Y'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'd-m-yyyy'
                ]
            ]);
            ?>
        </div>
    </div>
</div>

    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?php
                echo '<label>Thời gian sử dụng xe từ</label>';
                echo DatePicker::widget([
                    'name' => 'Document[date_from_car]',
                    'type' => DatePicker::TYPE_INPUT,
                    'value' => !empty($model->date_from_car) ? DateTimeHelper::getDateTime($model->date_from_car,'d-m-Y') :  DateTimeHelper::getDateTime('now','d-m-Y'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'd-m-yyyy'
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?php
                echo '<label>Thời gian kết thúc sử dụng xe</label>';
                echo DatePicker::widget([
                    'name' => 'Document[date_to_car]',
                    'type' => DatePicker::TYPE_INPUT,
                    'value' => !empty($model->date_to_car) ? DateTimeHelper::getDateTime($model->date_to_car,'d-m-Y') :  DateTimeHelper::getDateTime('now','d-m-Y'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'd-m-yyyy'
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row margin0 marginbottom10">
                <?= $form->field($model, 'purpose_car')->begin(); ?>
                <?= Html::activeLabel($model, 'purpose_car', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextarea($model,'purpose_car',['class' => 'form-control'])?>
                    <?= Html::error($model, 'purpose_car', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'purpose_car')->end(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row margin0 marginbottom10">
                <?= $form->field($model, 'place_user')->begin(); ?>
                <?= Html::activeLabel($model, 'place_user', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextInput($model,'place_user',['class' => 'form-control'])?>
                    <?= Html::error($model, 'place_user', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'place_user')->end(); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?php
                echo '<label>Thời gian đón cán bộ</label>';
                echo DatePicker::widget([
                    'name' => 'Document[date_from_user]',
                    'type' => DatePicker::TYPE_INPUT,
                    'value' => !empty($model->date_from_user) ? DateTimeHelper::getDateTime($model->date_from_user,'d-m-Y') :  DateTimeHelper::getDateTime('now','d-m-Y'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'd-m-yyyy'
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="row margin0 marginbottom20">
                <?php
                echo '<label>Thời gian trả cán bộ</label>';
                echo DatePicker::widget([
                    'name' => 'Document[date_to_user]',
                    'type' => DatePicker::TYPE_INPUT,
                    'value' => !empty($model->date_to_user) ? DateTimeHelper::getDateTime($model->date_to_user,'d-m-Y') :  DateTimeHelper::getDateTime('now','d-m-Y'),//DateTimeHelper::getDateTime('now','d-m-Y'),
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'd-m-yyyy'
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="row margin0 marginbottom10">
                <?= $form->field($model, 'note_document')->begin(); ?>
                <?= Html::activeLabel($model, 'note_document', ['class' => 'col-sm-12']) ?>
                <div class="col-sm-12">
                    <?= Html::activeTextarea($model,'note_document',['class' => 'form-control'])?>
                    <?= Html::error($model, 'note_document', ['class' => 'help-block help-block-error']) ?>
                </div>
                <?= $form->field($model, 'note_document')->end(); ?>
            </div>
        </div>
    </div>

    <div class="row margin0 marginbottom30">
        <div class="col-xs-12 text-center">
            <?= Html::submitButton(Yii::$app->controller->action->id == 'create' ? '<i class="fa fa-plus"></i> Thêm mới' : '<i class="fa fa-edit"></i> Cập nhật', ['class' => 'btn btn-success']) ?>
            <?= Html::resetButton('Hủy', ['class' => 'btn btn-success'])?>
        </div>
    </div>
<?php ActiveForm::end(); ?>