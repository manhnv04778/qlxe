<?php
use app\components\assets\AppAsset;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/admin/document/document.js', ['depends' => [AppAsset::className()]]);

?>
<div class="action-content">
    <div class="row">
        <div class="col-xs-6">
            <form class="form-search" id="form-search" action="<?=Url::to(['/admin/document/index'])?>" method="get">
                <div class="form-content">
                    <input placeholder="Tìm kiếm..." name="RegisterCar[content]" type="text" value="<?=!empty($params['Document']['content']) ? $params['Document']['desc'] : null?>">
                    <button><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </form>
        </div>
        <div class="col-xs-6 text-right">
            <div class="button-group">
                <a class="btn btn-success" href="<?=Url::toRoute(['/admin/document/create'])?>"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?php
                \yii\widgets\Pjax::begin([
                'id' => 'pjax-questions',
                'formSelector' => '#form-search',
                'enableReplaceState' => true,
                ]);
            ?>
            <div class="list margintop30">
                <div class="text-blue text-bold">Tổng số: <span class="text-error text-bold text-16"><?= StringHelper::numberFormat($dataProvider->totalCount)?></span></div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Mã hồ sơ</th>
                        <th>Ngày gửi y/c</th>
                        <th>Người đăng ký</th>
                        <th>Ngày duyệt</th>
                        <th>Ngày nhận xe</th>
                        <th>Ngày trả xe</th>
                        <th>Trạng thái</th>
<!--                        <th width="180" class="text-center">Sửa/Xóa</th>-->
                    </tr>
                    </thead>
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'options' => [
                            'tag' => 'tbody'
                        ],
                        'summary' => '',
                        'emptyText' => '',
                        'itemView' => '_item_approve',
                        /* 'pager' => [
                             'class' => \kop\y2sp\ScrollPager::className(),
                             'triggerTemplate' => '<div class="app-more margintop20">
                                                     <a class="btn padding20 text-center" href="#">{text}</a>
                                                 </div>',
                             'triggerText' => 'Xem thêm',
                             'noneLeftText' => '',
                         ],*/
                    ]);
                    ?>


                </table>
            </div>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>

<div id="modal-view" class="modal fade modal-confirm in" role="dialog" data-toggle="modal">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content radius0">
            <div class="modal-header padding0">
                <h4 class="modal-title bg-blue text-upper text-white padding10 text-center">Chi tiết hồ sơ</h4>
            </div>

            <div class="modal-body">
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Mã hồ sơ:</div>
                    <div class="col-xs-3">
                        <span class="code text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tên hồ sơ:</div>
                    <div class="col-xs-9">
                        <span class="name text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tên lãnh đạo:</div>
                    <div class="col-xs-3">
                        <span class="full_name text-bold"></span>
                    </div>
                    <div class="col-xs-3">Đơn vị:</div>
                    <div class="col-xs-3">
                        <span class="department text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Tỉnh thành đến công tác :</div>
                    <div class="col-xs-3">
                        <span class="from text-bold"></span>
                    </div>
                    <div class="col-xs-3">Địa điểm đến công tác:</div>
                    <div class="col-xs-3">
                        <span class="from_name text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Số cán bộ đi công tác:</div>
                    <div class="col-xs-3">
                        <span class="number_user text-bold"></span>
                    </div>
                    <div class="col-xs-3">Ngày gửi:</div>
                    <div class="col-xs-3">
                        <span class="created_date text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian công tác từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian công tác đến:</div>
                    <div class="col-xs-3">
                        <span class="date_to text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian nhận xe từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from_car text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian trả xe:</div>
                    <div class="col-xs-3">
                        <span class="date_to_car text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Mục đích sử dụng xe:</div>
                    <div class="col-xs-9">
                        <span class="purpose_car text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Thời gian đón cán bộ từ:</div>
                    <div class="col-xs-3">
                        <span class="date_from_user text-bold"></span>
                    </div>
                    <div class="col-xs-3">Thời gian trả cán bộ:</div>
                    <div class="col-xs-3">
                        <span class="date_to_user text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Địa điểm trả cán bộ:</div>
                    <div class="col-xs-9">
                        <span class="place_user text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-3">Địa điểm trả cán bộ:</div>
                    <div class="col-xs-9">
                        <span class="note_document text-bold"></span>
                    </div>
                </div>
                <div class="row marginbottom10 border-bottom-ddd paddingbottom10">
                    <div class="col-xs-7 col-xs-offset-5">
                        <button class="bg-blue text-white" type="button" data-id-send="" name ="btn-send" id="btn-send" value = "Gửi hồ sơ" >Gửi hồ sơ</button>
                        <input type ="hidden" class="hdf-id" name = "hdf-id" id = "hdf-id" value="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>