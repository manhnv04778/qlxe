<?php
/**
 * Created by PhpStorm.
 * User: Duy Dat
 * Date: 8/31/2016
 * Time: 10:11 AM
 */
use app\helpers\DateTimeHelper;
use yii\helpers\Url;

?>

<tr>
    <td class="text-center"><?=$index+1;?></td>
    <td class="text-center"><a class="text-success view-history" data-id="<?=$model->id;?>" href="javascript();"><?=$model->code?></a></td>
    <td class=""><?= \yii\helpers\StringHelper::wordLimit($model->name,20)?></td>
    <td class="text-center"><?=!empty($model->created_date) ? DateTimeHelper::getDateTime($model->created_date,'H:i d/m/Y') :  ''?></td>
    <td class="text-center"><?=$model->full_name?></td>
    <td class="text-center"><?=!empty($model->date_from) ? DateTimeHelper::getDateTime($model->date_from,'H:i d/m/Y') :  ''?></td>
    <td class="text-center"><?=!empty($model->date_to) ? DateTimeHelper::getDateTime($model->date_to,'H:i d/m/Y') :  ''?></td>
<!--    <th>Trạng thái</th>-->
    <td>
        <?php
            if($model->status==1){
                echo 'Chờ duyệt';
            }elseif($model->status==2){
            ?>
                <a href ="<?=Url::to(['/admin/document/suggest/', 'id' => $model->id, 'alias' => 'test'])?>">Đã duyệt</a>
            <?php
            }elseif($model->status==3){
                echo 'Không duyệt';
            }elseif($model->status==4){
                echo 'Hủy';
            }
        ?>
    </td>
    <td class="text-center">
        <?php if($model->status < 2):?>
                <a class="btn text-success approve" data-id="<?=$model->id;?>" href="javascript();"><i class="fa fa-check" aria-hidden="true"></i> Duyệt</a>
                <a class="btn text-error" data-pjax="0" data-method="post" data-confirm="Bạn có chắc chắn hủy không?" href="<?=Url::to(['/admin/document/cancel', 'id' => $model->id])?>"><i class="fa fa-times" aria-hidden="true"></i> Hủy</a>
        <?php else:?>
            <a class="text-success view" data-id="<?=$model->id;?>" href="javascript();"><i class="fa fa-eye" aria-hidden="true"></i> Xem </a>
        <?php endif;?>
    </td>
</tr>