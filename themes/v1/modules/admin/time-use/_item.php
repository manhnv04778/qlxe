<?php
/**
 * Created by PhpStorm.
 * User: Duy Dat
 * Date: 8/31/2016
 * Time: 10:11 AM
 */
use yii\helpers\Url;

?>

<tr>
    <td><?=$model->id?></td>
    <td><?=$model->desc?></td>
    <td><?=$model->leader->full_name?></td>
    <td><?=$model->driver->full_name?></td>
    <td>
        <a class="btn text-success" href="<?=Url::to(['/admin/time-use/update', 'id' => $model->id])?>"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
        <a class="btn text-error" data-pjax="0" data-method="post" data-confirm="Bạn có chắc chắn xoá không?" href="<?=Url::to(['/admin/time-use/delete', 'id' => $model->id])?>"><i class="fa fa-times" aria-hidden="true"></i> Xoá</a>
    </td>
</tr>