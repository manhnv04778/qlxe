<?php
/**
 * Created by PhpStorm.
 * User: van
 * Date: 2/22/2016
 * Time: 9:19 AM
 */
use yii\helpers\Url;

$user = Yii::$app->controller->user;

?>

<?php if($user):?>
<div class="user-action-menu">

    <?php if($user->type == 'user'):?>

    <div class="user-menu bg-white">
        <ul class="ul-menu account">
            <li><a class="menu-select text-black" href="<?=$user->getUrl(['type' => 'feed'])?>"><i class="fa fa-bell"></i> Thông báo</a></li>
            <li><a class="menu-select text-black" href="<?=$user->getUrl(['type' => 'question'])?>"><i class="fa fa-question"></i> Câu hỏi đã đăng</a></li>
            <li><a class="menu-select text-black" href="<?=Url::to(['/user/site/transaction'])?>"><i class="fa fa-usd"></i> Quản lý xu</a></li>
<!--            <li><a class="menu-select text-black" href=""><i class="fa fa-pencil"></i> Cập nhật tài khoản</a></li>-->
        </ul>
    </div>

    <?php elseif($user->type == 'expert' && $user->expert_status == 'done'): ?>
    <div class="user-menu bg-white">
        <ul class="ul-menu expert">
            <li><a class="menu-select text-black" href="<?=$user->getUrl(['type' => 'feed'])?>"><i class="fa fa-bell"></i> Thông báo</a></li>
            <li><a class="menu-select text-black" href="<?=Url::toRoute('/expert/question/history')?>" ><i class="fa fa-history"></i> Lịch sử giải bài tập</a></li>
            <li><a class="menu-select text-black" href="<?=Url::toRoute('/expert/site/statistic')?>"><i class="fa fa-bar-chart"></i> Thống kê</a></li>
            <li><a class="menu-select text-black" href="<?=Url::to(['/user/site/transaction'])?>"><i class="fa fa-usd"></i> Quản lý xu</a></li>
<!--            <li><a class="menu-select text-black "><i class="fa fa-wrench"></i> Cài đặt </a></li>-->
        </ul>
    </div>
    <?php endif; ?>
</div>

<?php endif; ?>

