<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\components\assets\AppAsset;

use app\models\geo\City;
use app\models\geo\District;
use app\components\rbac\AuthManager;


use app\helpers\DateTimeHelper;

$user = &Yii::$app->user->identity;

/* @var $this app\components\View */
/* @var $model app\modules\user\models\User */
/* @var $user app\modules\user\models\User */

$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/user/user_update.js', ['depends' => [AppAsset::className(), yii\jui\JuiAsset::className()]]);

$this->title = Yii::t('user', 'Update profile');
$sidebarSelected = &Yii::$app->controller->sidebarSelected;
$sidebarChildSelected = &Yii::$app->controller->sidebarChildSelected;
?>

<div class="row">

    <div class="col-lg-8 col-md-8 col-sm-7">


        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'id' => 'form-user',
            'enableAjaxValidation' => true,
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-md-2',
                    'offset' => 'col-md-offset-2',
                    'wrapper' => 'col-md-10',
                    'error' => '',
                    'hint' => '',
                ],
            ],
            'options' => [
                'data-action' => Yii::$app->controller->action->id
            ]
        ]); ?>

        <?php //echo $form->errorSummary($model, ['id' => 'error-summary-block', 'class' => 'callout callout-danger']) ?>

        <?=$form->field($model, 'username')->begin();?>
        <?=Html::activeLabel($model, 'username', ['class' => 'control-label col-md-2'])?>
        <div class="col-md-8">
            <?=Html::activeTextInput($model, 'username', ['class' => 'form-control']) ?>
            <?=Html::error($model, 'username', ['class' => 'help-block help-block-error'])?>
        </div>
        <?=$form->field($model, 'username')->end();?>

        <?=$form->field($model, 'name')->begin();?>
        <?=Html::activeLabel($model, 'name', ['class' => 'control-label col-md-2'])?>
        <div class="col-md-8">
            <?=Html::activeTextInput($model, 'name', ['class' => 'form-control']) ?>
            <?=Html::error($model, 'name', ['class' => 'help-block help-block-error'])?>
        </div>
        <?=$form->field($model, 'name')->end();?>


        <div class="form-group">
            <label class="control-label col-md-2" for="post-image"><?= $model->getAttributeLabel('image') ?></label>

            <div class="col-md-8">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#browse" role="tab" data-toggle="tab" data-image-method="browse"><?= Yii::t('app', 'Browse photo') ?></a>
                    </li>
                    <li role="presentation">
                        <a href="#url" role="tab" data-toggle="tab" data-image-method="url"><?= Yii::t('app', 'Photo url') ?></a></li>
                </ul>
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in active" id="browse">

                        <div class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-primary btn-flat btn-file">
                                    Browse… <?= Html::fileInput('image_browse', null, ['class' => 'form-control', 'id' => 'image_browse']) ?>
                                </span>
                            </span>
                            <input type="text" class="form-control text-file" readonly="">
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="url">
                        <?= Html::textInput('image_url', null, ['class' => 'form-control', 'id' => 'image_url', 'placeholder' => Yii::t('app', 'Photo Url')]) ?>
                    </div>
                    <?= Html::activeHiddenInput($model, 'image_upload')?>

                    <div id="image-preview">
                        <img style="max-height: 150px" src="<?=$imageView?>" />
                    </div>
                </div>

            </div>
        </div>


        <?php if(Yii::$app->user->can(AuthManager::ROLE_ADMIN) && $user->id != $model->id):?>

            <?=$form->field($model, 'role')->begin();?>
            <?=Html::activeLabel($model, 'role', ['class' => 'control-label col-md-2 hidden-xs'])?>
            <div class="col-md-8">
                <?=Html::activeDropDownList($model, 'role', $model->getRoleData(true), ['class' => 'form-control', 'prompt' => '']) ?>
                <?=Html::error($model, 'role', ['class' => 'help-block help-block-error'])?>
            </div>
            <?=$form->field($model, 'role')->end();?>

        <?php endif?>

        <?php if(Yii::$app->user->can(AuthManager::PERMISSION_UPDATE_USER) && $user->id != $model->id):?>
            <?=$form->field($model, 'status')->begin();?>
            <?=Html::activeLabel($model, 'status', ['class' => 'control-label col-md-2 hidden-xs'])?>
            <div class="col-md-8">
                <?=Html::activeDropDownList($model, 'status', $model->getStatusData(), ['class' => 'form-control']) ?>
                <?=Html::error($model, 'status', ['class' => 'help-block help-block-error'])?>
            </div>
            <?=$form->field($model, 'status')->end();?>
        <?php endif?>

        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-flat btn-lg btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <div class="col-lg-4 col-md-4 col-sm-5">
        <div id="help-block" class="alert bg-gray">
            <h3><i class="icon fa fa-lightbulb-o"></i> Tips!</h3>
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li>You can use username to login after password was created.</li>
                    </ul>
                </div>
            </div>
        </div>



    </div>


</div>
