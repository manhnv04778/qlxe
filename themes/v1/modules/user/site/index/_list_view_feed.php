<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\components\assets\AppAsset;

/* @var $this app\components\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="user-page notifications">
    <div class="page-header">
        <h4 class="margin0 font-weight-600">Thông báo của bạn</h4>
    </div>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'multiple-list feeds', 'tag' => 'ul'],
        'itemOptions' => ['tag' => false],
        'summary' => '',
        'itemView' => '_view_feed',
        'viewParams' => ['pageItemcount' => $dataProvider->count],
        'emptyTextOptions' => ['class' => ''],
        'pager' => [
            'maxButtonCount' => 5,
            'options' => [
                'class' => 'pagination pagination-lg pull-right',
            ]
        ]
    ]) ?>
</div>