<?php

use yii\helpers\Html;
use app\helpers\DateTimeHelper;
use app\modules\question\models\QuestionAnswer;
use app\modules\user\models\User;
use app\modules\question\models\QuestionUserVote;
use app\modules\question\models\QuestionUserFavourite;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\modules\user\models\UserActivity;
use app\modules\user\models\ConversationNotice;


/* @var $this app\components\View */

/* @var $conversation app\modules\user\models\Conversation */

/* @var $key integer, key of model */
/* @var $index integer, index of array */
/* @var $widget yii\widgets\ListView */
/* @var $pageItemcount integer, item count in page */

/* @var $user app\modules\user\models\User */
/* @var $currentUser app\modules\user\models\User */
/* @var $CNLastReply ConversationNotice */

$currentUser = & Yii::$app->controller->user;

$conversation = &$model;

$user = $conversation->user;

$CNLastReply = ConversationNotice::find()
    ->where(['conversation_id' => $conversation->id])
    ->orderBy(['created' => SORT_DESC])
    ->one();

?>
<div class="conversation padding10">
    <div class="row">
        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 col-user">
            <a href="<?=$user->getUrl()?>">
                <img class="img-thumbnail img-circle padding1 marginbottom0" src="<?=$user->getImageUrl()?>" />
            </a>
        </div>
        <div class="col-lg-22 col-md-21 col-sm-21 col-xs-20 col-feed">

            <div class="message">
                <?php if($CNLastReply && $CNLastReply->sender_id != $currentUser->id && $CNLastReply->message_id == $conversation->id && !$CNLastReply->viewed_detail): ?>
                    <i class="fa fa-circle text-green pull-right"></i>
                <?php endif ?>

                <div>
                    <i class="fa fa-comments-o"></i>
                    <a class="text-bold" href="<?=$user->getUrl()?>"><?=$user->name?></a> <span><?=Yii::t('user', 'gửi tin nhắn')?></span>
                    <span class="text-muted text-small"><i class="fa fa-clock-o"></i> <?=DatetimeHelper::relative($conversation->created)?></span>

                    <p><a href="<?php echo $conversation->url?>"><?=$conversation->getContentLimit(20)?></a></p>

                </div>


                <?php if($CNLastReply && $CNLastReply->message_id != $conversation->id):?>
                    <hr class="marginbottom10" />
                    <?php if($CNLastReply->sender_id != $currentUser->id && !$CNLastReply->viewed_detail): ?>
                        <i class="fa fa-circle text-green pull-right"></i>
                    <?php endif ?>
                    <div>
                        <i class="fa fa-reply text-muted"></i>
                        <a href="<?=$CNLastReply->sender->url?>"><?php echo $CNLastReply->sender->name?></a> <span><?=Yii::t('user', 'trả lời')?></span>
                        <a href="<?=$conversation->getUrl(['#' => $CNLastReply->message_id])?>"><?=$CNLastReply->message_short?></a>
                        <span class="text-muted text-small"><i class="fa fa-clock-o"></i> <?=DatetimeHelper::relative($CNLastReply->created)?></span>
                    </div>
                <?php endif?>

                <a class="btn btn-default btn-xs margintop10" href="<?=$conversation->getUrl(['#' => 'reply'])?>"><i class="fa fa-edit"></i> <?=Yii::t('app', 'Gửi trả lời')?></a>
            </div>
        </div>
    </div>
</div>