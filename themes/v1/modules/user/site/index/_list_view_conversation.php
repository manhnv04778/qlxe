<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\components\assets\AppAsset;

/* @var $this app\components\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'list-view conversations'],
    'itemOptions' => ['tag' => false],
    'summary' => '',
    'itemView' => '_view_conversation',
    'viewParams' => ['pageItemcount' => $dataProvider->count, 'user' => $user],
    'emptyTextOptions' => ['class' => ''],
    'pager' => [
        'maxButtonCount' => 5,
        'options' => [
            'class' => 'pagination pagination-lg pull-right',
        ]
    ]
]) ?>