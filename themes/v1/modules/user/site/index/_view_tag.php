<?php

use yii\helpers\Html;
use app\helpers\DateTimeHelper;
use app\modules\question\models\QuestionAnswer;
use app\modules\user\models\User;
use app\modules\question\models\QuestionUserVote;
use app\modules\question\models\QuestionUserFavourite;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\modules\user\models\UserActivity;
use app\modules\user\models\ConversationNotice;


/* @var $this app\components\View */

/* @var $key integer, key of model */
/* @var $index integer, index of array */
/* @var $widget yii\widgets\ListView */
/* @var $pageItemcount integer, item count in page */

/* @var $questionUserTag app\modules\question\models\QuestionUserTag */
/* @var $questionTag app\modules\question\models\QuestionTag */

$currentUser = & Yii::$app->controller->user;

$questionUserTag = &$model;
$questionTag = &$model->tag;
?>


<div class="tag well well-sm no-shadow">
    <div class="tag-label">
        <a class="label label-default" href="<?=$questionTag->url?>">
            <?=$questionTag->name?>
        </a>
        <span class="text-muted">&nbsp; × <?=$questionUserTag->tag_count?></span>
    </div>
    <div class="tag-desc text-muted">
        <?=$questionTag->desc?>
    </div>
</div>