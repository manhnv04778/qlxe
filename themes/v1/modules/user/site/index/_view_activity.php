<?php

use yii\helpers\Html;
use app\helpers\DateTimeHelper;
use app\modules\question\models\QuestionAnswer;
use app\modules\user\models\User;
use app\modules\question\models\QuestionUserVote;
use app\modules\question\models\QuestionUserFavourite;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\modules\user\models\UserActivity;

/* @var $this app\components\View */

/* @var $userFeed app\modules\user\models\UserFeed */
/* @var $activityUser app\modules\user\models\UserActivity */

/* @var $key integer, key of model */
/* @var $index integer, index of array */
/* @var $widget yii\widgets\ListView */
/* @var $pageItemcount integer, item count in page */

/* @var $user app\modules\user\models\User */
/* @var $currentUser app\modules\user\models\User */

$currentUser = & Yii::$app->controller->user;

$userFeed = &$model;

$printDate = false;
$date = DateTimeHelper::formatDatetimeByLang($userFeed->created);
if(empty($this->variables['feedDate'][$date])){
    $this->variables['feedDate'][$date] = $date;
    $printDate = true;
}

// feed of comment, answer or question
$isComment = preg_match('/^.+\.comment_.+$/', $userFeed->activity);
$isAnswer = preg_match('/^answer\..+/', $userFeed->activity);
$isQuestion = preg_match('/^question\..+/', $userFeed->activity);

$isVoteUp = preg_match('/^.+\.vote_up$/', $userFeed->activity);
$isAccept = preg_match('/^.+\.accept$/', $userFeed->activity);

//echo "<pre>"; print_r($userFeed->status); echo "</pre>";
?>

<?php if($printDate):?>
<li class="time-label text-11">
    <span>
        <?=$date?>
    </span>
</li>
<?php endif?>

<li class="timeline-group">

    <?php if($isVoteUp):?>
        <i class="fa fa-thumbs-o-up <?=$userFeed->status == 'enable' ? 'bg-olive' : ''?> hidden-xs"></i>

    <?php elseif($isAccept):?>
        <i class="fa fa-check <?=$userFeed->status == 'enable' ? 'bg-green' : ''?> hidden-xs"></i>

    <?php elseif($isComment):?>
        <i class="fa fa-comment-o <?=$userFeed->status == 'enable' ? 'bg-yellow' : ''?> hidden-xs"></i>

    <?php elseif($isAnswer):?>
        <i class="fa fa-pencil-square-o <?=$userFeed->status == 'enable' ? 'bg-red' : ''?> hidden-xs"></i>

    <?php elseif($isQuestion):?>
        <i class="fa fa-question <?=$userFeed->status == 'enable' ? 'bg-blue' : ''?> hidden-xs"></i>
    <?php endif?>


    <div class="timeline-item <?=$userFeed->status == 'disable' ? 'bg-deleted' : ''?>">

        <div class="media">
            <a class="media-left" href="#">
                <img class="img-circle img-responsive" src="<?=$user->getImageUrl()?>">
            </a>
            <div class="media-body">
                <span class="icon-inline">
                    <?php if($isVoteUp):?>
                        <i class="fa fa-fw fa-thumbs-o-up text-olive visible-xs-inline img-circle"></i>

                    <?php elseif($isAccept):?>
                        <i class="fa fa-check text-green visible-xs-inline img-circle"></i>

                    <?php elseif($isComment):?>
                        <i class="fa fa-comment-o text-yellow visible-xs-inline img-circle"></i>

                    <?php elseif($isAnswer):?>
                        <i class="fa fa-pencil-square-o text-red visible-xs-inline img-circle"></i>

                    <?php elseif($isQuestion):?>
                        <i class="fa fa-question text-blue visible-xs-inline img-circle"></i>
                    <?php endif?>
                </span>

                <a href="<?=$user->url?>"><?=$user->name?></a>
                <?=UserActivity::getActivityLabel($userFeed->activity)?>


                <?php if($isComment):?>
                    <a href="<?=$userFeed->comment->getUrl()?>"><?=$userFeed->question->title?></a>
                <?php elseif($isAnswer):?>
                    <a href="<?=$userFeed->answer->getUrl()?>"><?=$userFeed->question->title?></a>
                <?php elseif($isQuestion):?>
                    <a href="<?=$userFeed->question->url?>"><?=$userFeed->question->title?></a>
                <?php endif?>

                <?php if($userFeed->status == 'disable'):?>
                    <span class="text-small text-danger"><?=Yii::t('question', 'Deleted')?></span>
                <?php endif?>

                <div class="text-small text-muted"><i class="fa fa-clock-o"></i> <?=DateTimeHelper::relative($userFeed->created)?></div>

            </div>
        </div>


        <?php if(!preg_match('/.+\.(vote_up|accept)$/', $userFeed->activity)):?>
        <div class="timeline-content">
            <?php if($isComment):?>
                <p><?=StringHelper::parsedown($userFeed->comment->content)?></p>
            <?php elseif($isAnswer):?>
                <p><?=StringHelper::parsedown($userFeed->answer->content)?></p>
            <?php elseif($isQuestion):?>
                <p><?=StringHelper::parsedown($userFeed->question->content)?></p>
            <?php endif?>
        </div>
        <?php endif?>
    </div>
</li>
<!-- END timeline item -->

<?php if($index + 1 == $pageItemcount):?>
<li>
    <i class="fa fa-clock-o"></i>
</li>
<?php endif?>