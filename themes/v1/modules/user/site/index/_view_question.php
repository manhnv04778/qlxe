<?php

use yii\helpers\Html;
use app\helpers\DateTimeHelper;
use app\modules\question\models\Question;
use app\modules\question\models\QuestionAnswer;
use app\modules\user\models\User;
use app\modules\question\models\QuestionUserVote;
use app\modules\question\models\QuestionUserFavourite;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\modules\user\models\UserActivity;

/* @var $this app\components\View */

/* @var $question app\modules\question\models\Question */

/* @var $key integer, key of model */
/* @var $index integer, index of array */
/* @var $widget yii\widgets\ListView */
/* @var $pageItemcount integer, item count in page */

/* @var $user app\modules\user\models\User action user*/
/* @var $currentUser app\modules\user\models\User */


$question = &$model;

?>
<li class="">
    <div class="question-item">
        <a class="text-bold" href="<?=$question->url?>"><?=$question->title?></a>

        <div class="row">
            <div class="col-xs-6 text-small text-muted">
                <i class="fa fa-clock-o"></i> <?=DateTimeHelper::relative($question->created)?>
            </div>

            <div class="col-xs-6 text-small text-muted text-right">
                <?=Question::getStateLabel($question->state); ?>
            </div>

        </div>
    </div>
</li>

