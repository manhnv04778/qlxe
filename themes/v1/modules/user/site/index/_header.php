<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\components\assets\AppAsset;
use yii\widgets\Pjax;
use app\components\View;
use app\helpers\DateTimeHelper;

/* @var $this app\components\View */
/* @var $currentUser app\modules\user\models\User*/
/* @var $user app\modules\user\models\User*/
/* @var $userExtra app\modules\user\models\UserExtra */

$currentUser = & Yii::$app->controller->user;

$userExtra = &$user->userExtra;

/* @var $follow 0: chưa follow, 1: đã follow, 2: đã là bạn */
$follow = $currentUser ? $currentUser->checkFollow($user->id) : 0;

$action = Yii::$app->controller->id.'_'.Yii::$app->controller->action->id;
?>

<div class="box box-solid box-header">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-4 text-center">
                <div class="avatar">
                    <a href="<?=$user->url?>">
                        <img class="img-responsive thumbnail marginbottom0" src="<?=$user->getImageUrl()?>">
                    </a>
                </div>
            </div>
            <div class="col-sm-20 text-white text-right">
                <h3 class="marginbottom0"><?=$user->name?></h3>
                <div class="text-small">
                    <?=Yii::t('user', 'Tham gia từ')?>
                    <?=DateTimeHelper::relative($user->created, 'short')?>
                </div>

                <div class="row gutter">
                    <div class="col-xs-24">

                        <div class="row gutter margintop10">
                            <div class="col-xs-20 col-sm-20 col-md-21 col-lg-22"><?=Yii::t('question', 'Số câu hỏi')?></div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
                                <span class="text-primary help" title="<?=Yii::t('question', 'Số câu hỏi')?>"><i class="fa fa-circle"></i></span>
                                <?=$userExtra->user_facebook_count?>
                            </div>
                        </div>
                        <div class="row gutter">
                            <div class="col-xs-20 col-sm-20 col-md-21 col-lg-22"><?=Yii::t('question', 'Số câu trả lời đúng')?></div>
                            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
                                <span class="text-success help" title="<?=Yii::t('question', 'Số câu trả lời đúng')?>"><i class="fa fa-circle"></i></span>
                                <?=$userExtra->user_facebook_count?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer clearfix">


        <?php if($currentUser && $currentUser->id == $user->id):?>
        <div class="pull-right">

            <div class="btn-group btn-group-sm visible-sm visible-xs btn-group-right">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user text-primary"></i> <?=$user->name?> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li class="<?=$action == 'user_update' ? 'active' : ''?>"><a href="<?=Url::toRoute('/user/user/update')?>"><i class="fa fa-edit fa-lg fa-fw small"></i> <?=Yii::t('user', 'Cập nhật tài khoản')?></a></li>
                    <li class="<?=$action == 'user_manage-login' ? 'active' : ''?>"><a href="<?=Url::toRoute('/user/user/manage-login')?>"><i class="fa fa-envelope-o fa-lg fa-fw small"></i></i> <?=Yii::t('user', 'Quản lý đăng nhập')?></a></li>
                    <li class="<?=$action == 'user_password' ? 'active' : ''?>"><a href="<?=Url::toRoute('/user/user/password')?>"><i class="fa fa-ellipsis-h fa-lg fa-fw small"></i></i> <?=Yii::t('user', $user->password_hash ? 'Đổi mật khẩu' : 'Tạo mật khẩu')?></a></li>
                </ul>
            </div>

            <div class="hidden-sm hidden-xs">
                <a class="btn btn-sm btn-<?=$action == 'user_update' ? 'primary' : 'default'?>" href="<?=Url::toRoute('/user/user/update')?>"><i class="fa fa-edit fa-lg fa-fw small <?=$action == 'user_update' ? '' : 'text-primary'?>"></i> <?=Yii::t('user', 'Cập nhật tài khoản')?></a>
                <a class="btn btn-sm btn-<?=$action == 'user_manage-login' ? 'primary' : 'default'?>" href="<?=Url::toRoute('/user/user/manage-login')?>"><i class="fa fa-envelope-o fa-lg fa-fw small <?=$action == 'user_manage-login' ? '' : 'text-primary'?>"></i></i> <?=Yii::t('user', 'Quản lý đăng nhập')?></a>
                <a class="btn btn-sm btn-<?=$action == 'user_password' ? 'primary' : 'default'?>" href="<?=Url::toRoute('/user/user/password')?>"><i class="fa fa-ellipsis-h fa-lg fa-fw small <?=$action == 'user_password' ? '' : 'text-primary'?>"></i></i> <?=Yii::t('user', $user->password_hash ? 'Đổi mật khẩu' : 'Tạo mật khẩu')?></a>
            </div>
        </div>
        <?php else:?>

            <div class="user-feature">

                <a class="btn btn-sm btn-default" href="<?=$user->getUserRouter('/user/conversation/create')?>">
                    <i class="fa fa-comments-o"></i>
                    <?=Yii::t('user', 'Gửi tin nhắn')?>
                </a>


                <?php
                $this->registerJs('
                $("#pjax-user-follow").on("pjax:start", function(event, container, options) {
                    $("#user-follow").removeClass("pointer").addClass("pointer-disable");
                });

                $("#pjax-user-follow").on("pjax:end", function(event, container, options) {
                    var type = $("#user-follow").attr("data-type");
                    var message = $("#user-follow").attr("data-message");
                    growl(message, type);
                    $("#user-follow").removeClass("pointer-disable").addClass("pointer");
                });
            ', View::POS_READY, 'pjax-user-follow');


                Pjax::begin([
                    'id' => 'pjax-user-follow',
                    'options' => ['class' => 'display-inline'],
                    'linkSelector' => '#user-follow',
                    'enablePushState' => false,
                    'enableReplaceState' => false,
                ]);
                ?>

                <a id="user-follow" class="btn btn-sm btn-default" data-type="<?=$follow == 0 ? 'warning' : 'success'?>" data-message="<?=$follow == 0 ? Yii::t('user','Unfollowed') : Yii::t('user','Followed')?>" href="<?=$user->getUrl(['action' => 'follow'])?>">
                    <?php if($follow == 0):?>
                        <i class="ion-person-add"></i>
                        <?=Yii::t('user', 'Theo dõi')?>
                    <?php elseif($follow == 1):?>
                        <i class="fa fa-check fa-lg text-blue"></i>
                        <?=Yii::t('user', 'Đang theo dõi')?>
                    <?php else:?>
                        <i class="fa fa-check fa-lg text-green"></i>
                        <?=Yii::t('user', 'Đã là bạn')?>
                    <?php endif?>
                </a>

                <?php Pjax::end(); ?>

        </div>
        <?php endif?>

    </div>
</div>