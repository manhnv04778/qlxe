<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\components\assets\AppAsset;

/* @var $this app\components\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>


<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'list-view row gutter margintop20'],
    'itemOptions' => ['class' => 'col col-lg-6 col-md-8 col-sm-8 col-xs-12'],
    'summary' => '',
    'itemView' => '_view_tag',
    'viewParams' => ['pageItemcount' => $dataProvider->count],
    'emptyTextOptions' => ['class' => ''],
    'pager' => [
        'maxButtonCount' => 5,
        'options' => [
            'class' => 'pagination pagination-lg pull-right',
        ]
    ]
]) ?>
