<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\components\assets\AppAsset;

/* @var $this app\components\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'list-view timeline', 'tag' => 'ul'],
    'itemOptions' => ['tag' => false],
    'summary' => '',
    'itemView' => '_view_answer',
    'viewParams' => ['pageItemcount' => $dataProvider->count, 'user' => $user],
    'emptyTextOptions' => ['class' => ''],
    'pager' => [
        'maxButtonCount' => 5,
        'options' => [
            'class' => 'pagination pagination-lg pull-right',
        ]
    ]
]) ?>