<?php

use yii\helpers\Html;
use app\helpers\DateTimeHelper;
use app\modules\question\models\QuestionAnswer;
use app\modules\user\models\User;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\modules\user\models\UserFeed;
use app\modules\user\models\UserActivity;

/* @var $this app\components\View */

/* @var $userFeed app\modules\user\models\UserFeed */
/* @var $activityUser app\modules\user\models\UserActivity */

/* @var $key integer, key of model */
/* @var $index integer, index of array */
/* @var $widget yii\widgets\ListView */
/* @var $pageItemcount integer, item count in page */

/* @var $currentUser app\modules\user\models\User*/

$currentUser = & Yii::$app->controller->user;

$userFeed = &$model;

$isAnswer = preg_match('/^answer\..+/', $userFeed->activity_activity);
$isQuestion = preg_match('/^question\..+/', $userFeed->activity_activity);

$theLabel = '';
if($isAnswer){
    $theLabel = 'Bài giải';
}
if($isQuestion){
    $theLabel = 'Câu hỏi';
}

$activityLabel = '';
if($userFeed->activity_activity == 'answer.rating'){
    $activityLabel = 'gửi ' . UserActivity::getActivityLabel($userFeed->activity_activity);
}
if($userFeed->activity_activity == 'answer.create'){
    $activityLabel = 'gửi câu trả lời';
}
//$activityLabel = $userFeed->activity_activity;

$user = Yii::$app->controller->user;

$feedUrl = $userFeed->activityQuestion->getUrl();
//if($userFeed->activityQuestion->user_id != $user->id){
//    $feedUrl = $userFeed->activityAnswer->getUrl();
//}

//echo "<pre>"; print_r($feedUrl); echo "</pre>\n\n";

if($user->type == 'user'){
    $feedContent = $userFeed->getUserActivityContent('long');
} else {
    $feedContent = $userFeed->activityData;
}


?>


<li class="">
    <div class="feed-item marginbottom15">

        <a class="text-333" href="<?=$feedUrl; ?>">

            <p class="text-14">
                <?=$feedContent; ?>
            </p>
            <p class="white-space-normal text-muted text-small">
                <i class="fa fa-clock-o text-11"></i> <?=DateTimeHelper::relative($userFeed->created)?>
            </p>
        </a>

    </div>
</li>