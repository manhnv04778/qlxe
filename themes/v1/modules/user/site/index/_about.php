<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\StringHelper;

/* @var $this app\components\View */
/* @var $currentUser app\modules\user\models\User*/
/* @var $user app\modules\user\models\User*/

$currentUser = & Yii::$app->controller->user;

?>

<?php if($user->about):?>
    <div class="margintop10">
        <div class="callout callout-info margin0">
            <?=StringHelper::parsedown($user->about);?>

            <?php if($currentUser && $user->id == $currentUser->id):?>
                <p class="margintop10">
                    <a href="<?=Url::toRoute(['/user/user/update', '#' => 'about'])?>"><i class="fa fa-edit"></i> <?=Yii::t('user', 'Sửa giới thiệu')?></a>
                </p>
            <?php endif?>
        </div>
    </div>

<?php elseif($currentUser && $user->id == $currentUser->id):?>
    <div class="margintop10">
        <div class="callout callout-info margin0">
            <p>
                <span><?=Yii::t('user', 'Bạn chưa giới thiệu bản thân')?></span>

                <a href="<?=Url::toRoute(['/user/user/update', '#' => 'about'])?>"><i class="fa fa-edit"></i> <?=Yii::t('user', 'Cập nhật ngay')?></a>
            </p>
        </div>
    </div>
<?php endif?>