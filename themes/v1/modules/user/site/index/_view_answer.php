<?php

use yii\helpers\Html;
use app\helpers\DateTimeHelper;
use app\modules\question\models\QuestionAnswer;
use app\modules\user\models\User;
use app\modules\question\models\QuestionUserVote;
use app\modules\question\models\QuestionUserFavourite;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\modules\user\models\UserActivity;

/* @var $this app\components\View */

/* @var $answer app\modules\question\models\QuestionAnswer */

/* @var $key integer, key of model */
/* @var $index integer, index of array */
/* @var $widget yii\widgets\ListView */
/* @var $pageItemcount integer, item count in page */

/* @var $user app\modules\user\models\User action user*/
/* @var $currentUser app\modules\user\models\User */

$currentUser = & Yii::$app->controller->user;

$answer = &$model;

$printDate = false;
$date = DateTimeHelper::formatDatetimeByLang($answer->created);
if(empty($this->variables['feedDate'][$date])){
    $this->variables['feedDate'][$date] = $date;
    $printDate = true;
}
?>

<?php if($printDate):?>
<li class="time-label">
    <span>
        <?=$date?>
    </span>
</li>
<?php endif?>

<li class="timeline-group">
    
    <i class="fa fa-question <?=$answer->status == 'enable' ? 'bg-blue' : ''?> hidden-xs"></i>


    <div class="timeline-item <?=$answer->status == 'disable' ? 'bg-deleted' : ''?>">

        <div class="media">
            <a class="media-left" href="<?=$user->url?>">
                <img class="img-circle img-responsive" src="<?=$user->getImageUrl()?>">
            </a>
            <div class="media-body">
                <span class="icon-inline">
                    <i class="fa fa-question text-blue visible-xs-inline img-circle"></i>
                </span>

                <a href="<?=$user->url?>"><?=$user->name?></a>
                <?=UserActivity::getActivityLabel('answer.create')?>

                <a href="<?=$answer->url?>"><?=$answer->question->title?></a>
                <?php if($answer->status == 'disable'):?>
                <span class="text-small text-danger"><?=Yii::t('question', 'Deleted')?></span>
                <?php endif?>
                <div class="text-small text-muted"><i class="fa fa-clock-o"></i> <?=DateTimeHelper::relative($answer->created)?></div>

            </div>
        </div>

        <div class="timeline-content">
            <p><?=StringHelper::parsedown($answer->content)?></p>
        </div>
    </div>
</li>
<!-- END timeline item -->

<?php if($index + 1 == $pageItemcount):?>
<li>
    <i class="fa fa-clock-o"></i>
</li>
<?php endif?>