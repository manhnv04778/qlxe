<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\components\assets\AppAsset;

/* @var $this app\components\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $subTypes [] */
/* @var $subType friend|follower|following */

?>


<div class="row margintop10 marginbottom10">
    <div class="col-lg-24 text-right">
        <div id="sub-type" class="btn-group btn-group-sm" role="group">
            <?php foreach($subTypes as $k => $v):?>
                <a title="<?=$v['desc']?>" href="<?=$user->getUrl(['type' => $type, 'subType' => $k])?>" class="btn btn-default <?=$subType == $k ? 'active' : ''?>">
                    <?=$v['name']?>
                    <?php if(!empty($v['count'])):?>
                        <span class="label label-default"><?=$v['count']?></span>
                    <?php endif?>
                </a>
            <?php endforeach?>
        </div>
    </div>
</div>


<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'list-view row gutter users'],
    'itemOptions' => ['class' => 'col col-lg-6 col-md-8 col-sm-12 col-xs-24'],
    'summary' => '',
    'itemView' => '_view_user',
    'viewParams' => ['pageItemcount' => $dataProvider->count, 'user' => $user],
    'emptyTextOptions' => ['class' => ''],
    'pager' => [
        'maxButtonCount' => 5,
        'options' => [
            'class' => 'pagination pagination-lg pull-right',
        ]
    ]
]) ?>
