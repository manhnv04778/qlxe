<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\components\assets\AppAsset;

/* @var $this app\components\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<div class="user-page question">
    <div class="page-header">
        <h4 class="margin0 font-weight-600">Câu hỏi đã đăng</h4>
    </div>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'multiple-list questions', 'tag' => 'ul'],
        'itemOptions' => ['tag' => false],
        'summary' => '',
        'emptyText' => 'Bạn chưa đăng câu hỏi nào',
        'itemView' => '_view_question',
        'viewParams' => ['pageItemcount' => $dataProvider->count, 'user' => $user],
        'emptyTextOptions' => ['class' => 'text-muted'],
        'pager' => [
            'maxButtonCount' => 5,
            'options' => [
                'class' => 'pagination pagination-lg pull-right',
            ]
        ]
    ]) ?>
</div>