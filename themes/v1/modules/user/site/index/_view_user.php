<?php

use yii\helpers\Html;
use app\helpers\DateTimeHelper;
use app\modules\question\models\QuestionAnswer;
use app\modules\user\models\User;
use app\modules\question\models\QuestionUserVote;
use app\modules\question\models\QuestionUserFavourite;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\modules\user\models\UserActivity;
use app\modules\user\models\ConversationNotice;


/* @var $this app\components\View */

/* @var $key integer, key of model */
/* @var $index integer, index of array */
/* @var $widget yii\widgets\ListView */
/* @var $pageItemcount integer, item count in page */

/* @var $user app\modules\user\models\User */
/* @var $currentUser app\modules\user\models\User */

$currentUser = & Yii::$app->controller->user;

$user = &$model;
$userExtra = &$model->userExtra;
?>


<div class="media-user gutter clearfix">
    <a class="col-xs-8 col-sm-9" href="<?=$user->getUrl()?>">
        <img class="img-circle"  src="<?=$user->getImageUrl()?>">
    </a>
    <div class="col-xs-16 col-sm-15">

        <h4 class="media-heading"><a href="<?=$user->getUrl()?>"><?=$user->name?></a></h4>
        <?php if($user->username):?>
            <div><a href="<?=$user->getUrl()?>"><?=$user->username?></a></div>
        <?php endif?>

        <div class="text-large text-bold text-danger help" title="<?=Yii::t('question', "Reputation score")?>"><?=$userExtra->reputation?></div>

        <div class="text-small">
            <span class="text-primary help" title="<?=Yii::t('question', 'Question count')?>"><i class="fa fa-circle"></i> <?=$userExtra->question_count?></span>
            <span class="text-success help" title="<?=Yii::t('question', 'Accepted answer count')?>"><i class="fa fa-circle"></i> <?=$userExtra->answer_accepted_count?></span>
            <span class="text-warning help" title="<?=Yii::t('question', 'Answer count')?>"><i class="fa fa-circle"></i> <?=$userExtra->answer_count?></span>
        </div>

    </div>
</div>