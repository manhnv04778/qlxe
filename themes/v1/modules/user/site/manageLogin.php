<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\components\assets\AppAsset;


/* @var $this app\components\View */
/* @var $user app\modules\user\models\User */

$this->registerCssFile('/files/js/bootstrap-sweetalert/lib/sweet-alert.css', ['depends' => [AppAsset::className()]]);
$this->registerJsFile('/files/js/bootstrap-sweetalert/lib/sweet-alert.min.js', ['depends' => [AppAsset::className()]]);


$this->registerJsFile($this->theme->baseUrl . '/files/js/modules/user/user_manage-login.js', ['depends' => [AppAsset::className()]]);
$this->title = Yii::t('user', 'Manage login') . ' ' . $user->name;

?>

<div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12">

        <div class="row">

            <div class="col-md-6">
                <div class="box box-widget email-list">
                    <div class="box-header with-border_">
                        <h3 class="box-title"><i class="fa fa-envelope-o"></i> <?= Yii::t('user', 'Email') ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">


                        <ul class="list-group list-group-unbordered">
                            <?php foreach($user->userEmails as $userEmail):
                                $email = $userEmail->email ? $userEmail->email : $userEmail->social_id;

                                $iconClass = 'fa-envelope-o';

                                if($userEmail->social_type == 'facebook'){
                                    $iconClass = 'fa-facebook-square';
                                }
                                if($userEmail->social_type == 'google'){
                                    $iconClass = 'fa-google-plus-square';
                                }
                                ?>

                                <li class="list-group-item <?= $userEmail->is_main ? 'text-primary' : ''?>" title="<?= $userEmail->is_main ? Yii::t('user','Main Email') : ''?>">
                                    <i class="fa fa-fw <?=$iconClass?>"></i>
                                    <?=$email?>
                                    <?php if($userEmail->verified):?><i class="fa fa-thumbs-o-up" title="<?=Yii::t('user', 'Verified Email')?>"></i><?php endif?>
                                    <?php if($userEmail->is_main):?><i class="fa fa-key" title="<?=Yii::t('user', 'Main Email')?>"></i><?php endif?>



                                    <div class="pull-right">

                                        <?php if(!$userEmail->is_main && $userEmail->verified):?>
                                            <a href="<?=Url::toRoute(['/user/user/manage-login', 'action' => 'main', 'id' => $userEmail->id])?>"
                                               class="email-main fa fa-fw fa-key text-primary" title="<?=Yii::t('user', 'Make {email} is main email', ['email' => $userEmail->email])?>"></a>
                                        <?php endif?>

                                        <?php if(!$userEmail->verified):?>
                                            <a href="<?=Url::toRoute(['/user/user/manage-login', 'action' => 'request', 'id' => $userEmail->id])?>"
                                               class="email-request fa fa-fw fa-thumbs-o-up text-success" title="<?=Yii::t('user', 'Request new verified email token')?>"></a>
                                        <?php endif?>

                                        <?php if(!$userEmail->is_main):?>
                                            <a href="<?=Url::toRoute(['/user/user/manage-login', 'action' => 'remove', 'id' => $userEmail->id])?>"
                                               class="email-remove fa fa-remove text-danger" title="<?=Yii::t('user', 'Remove this email')?>"></a>
                                        <?php endif?>

                                    </div>
                                </li>


                            <?php endforeach?>

                        </ul>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col (left) -->
            <div class="col-md-6">

                <div class="box box-widget">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-dot-circle-o"></i> <?= Yii::t('user', 'Add Facebook, Google account') ?></h3>
                    </div>
                    <div class="box-body authchoice">
                        <a href="<?php echo Url::toRoute(['/user/user/auth', 'authclient' => 'facebook']) ?>" title="<?=Yii::t('user', 'Sign in with Facebook')?>" class="btn btn-block btn-social btn-facebook" data-popup-width="450" data-popup-height="380">
                            <i class="fa fa-facebook"></i> <?=Yii::t('user', 'Add Facebook account')?>
                        </a>
                        <br/>
                        <a href="<?php echo Url::toRoute(['/user/user/auth', 'authclient' => 'google']) ?>" title="<?=Yii::t('user', 'Sign in with Google')?>" class="btn btn-block btn-social btn-google" data-popup-width="450" data-popup-height="380">
                            <i class="fa fa-google-plus"></i> <?=Yii::t('user', 'Add Google account')?>
                        </a>
                    </div>
                </div>

                <div class="box box-widget">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-plus-circle"></i> <?= Yii::t('user', 'Add email') ?></h3>
                    </div>
                    <div class="box-body">
                        <?php $form = ActiveForm::begin([
                            //        'layout' => 'horizontal',
                            'id' => 'form-email',
                            'enableAjaxValidation' => true,
                            'fieldConfig' => [
                                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                'horizontalCssClasses' => [
                                    'label' => 'col-md-2',
                                    'offset' => 'col-md-offset-2',
                                    'wrapper' => 'col-md-8',
                                    'error' => '',
                                    'hint' => '',
                                ],
                            ],
                            'options' => [
                                'data-action' => Yii::$app->controller->action->id
                            ]
                        ]); ?>


                        <?=$form->field($addEmailForm, 'email')->begin();?>
                        <div class="input-group input-group-sm">
                            <?=Html::activeTextInput($addEmailForm, 'email', ['class' => 'form-control', 'placeholder' => Yii::t('user','Email')])?>
                            <span class="input-group-btn">
                                        <?= Html::submitButton('<i class="fa fa-plus"></i> '.Yii::t('user', 'Add'), ['class' => 'btn btn-success btn-flat']) ?>
                                    </span>
                        </div>
                        <?=Html::error($addEmailForm, 'email', ['class' => 'help-block help-block-error'])?>

                        <?=$form->field($addEmailForm, 'email')->end();?>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>


            </div>
        </div>

    </div>

    <div class="col-lg-12 col-md-12 col-sm-12">

        <div id="help-block" class="alert bg-gray">
            <h3><i class="icon fa fa-lightbulb-o"></i> Tips!</h3>
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li>You can add multiple google, facebook account and email.</li>
                        <li>All emails can use login to your account.</li>
                        <li>Click <i class="fa fa-fw fa-key text-primary"></i> to set main email.</li>
                    </ul>
                </div>
            </div>
        </div>


    </div>


</div>
