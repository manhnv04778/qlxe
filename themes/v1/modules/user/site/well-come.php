<?php
/**
 * Created by PhpStorm.
 * User: van
 * Date: 1/27/2016
 * Time: 9:54 AM
 */
use yii\helpers\Url;

?>

<div id="well-come">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="bg-white text-center page-content">
                    <h1 class="text-thin marginbottom30">Bạn là học sinh hay chuyên gia?</h1>
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="join right-box">
                                <img src="/themes/v1/files/img/front/hoc-sinh.png">
                                <a class="btn btn-join btn-border green" href="<?=Url::to(['/user/user/verify-phone', 'type' => 'pupil'])?>">Học sinh</a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="join left-box">
                                <img src="/themes/v1/files/img/front/chuyen-gia.png">
                                <a class="btn btn-join btn-border green" href="<?=Url::to(['/user/user/verify-phone', 'type' => 'teacher'])?>">Chuyên gia</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>