<?php
use app\components\assets\AppAsset;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\DateTimeHelper;

use app\modules\frontend\models\Exercise;
use app\modules\frontend\widgets\UserInfo;
use app\modules\user\widgets\UserActionMenu;
use app\modules\user\models\UserTransaction;




$this->registerJsFile('/files/js/moment/min/moment.min.js', ['depends' => [AppAsset::className()]]);

$this->registerCssFile('/files/js/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css', ['depends' => [AppAsset::className()]]);

$this->registerJsFile('/files/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js', ['depends' => [AppAsset::className()]]);

//$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/user/site_transaction.js', ['depends' => [AppAsset::className()]]);

$user = Yii::$app->controller->user;

$this->title = 'Quản lý xu';


$columns = [

//    [
//        'class' => 'kartik\grid\DataColumn',
//        'attribute' => 'id',
//        'hAlign' => 'left',
//        'vAlign' => 'middle',
//        'width' => '90px',
//        'headerOptions' => ['class' => 'text-center'],
//    ],

    [
        'class' => 'kartik\grid\DataColumn',
        'enableSorting' => false,
        'attribute' => 'created',
//        'filter' => false,
        'label' => 'Ngày phát sinh',
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'format' => 'html',
        'filterType'=>GridView::FILTER_DATE,
        'filterWidgetOptions'=>[
            'pluginOptions'=>['format'=>'yyyy-mm-dd']
        ],
//        'mergeHeader'=>true,
        'value' => function($data){
            return DateTimeHelper::getDateTime($data->created, 'd-m-Y H:i:s');
        },
        'width'=>'200px',
    ],

    [
        'class' => 'kartik\grid\DataColumn',
        'enableSorting' => false,
        'attribute' => 'action',
        'filter' => [
            'in' => 'Cộng xu',
            'out' => 'Trừ xu'
        ],
        'label' => 'Loại giao dịch',
        'format' => 'html',
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'value' => function($model){
            if($model->action == 'in')
                return 'Cộng xu';
            return 'Trừ xu';
        },

        'width'=>'150px',
    ],


    [
        'class' => 'kartik\grid\DataColumn',
        'enableSorting' => false,
        'attribute' => 'action_type',
        'filter' => false,
        'label' => 'Giao dịch',
        'format' => 'html',
        'vAlign' => 'middle',
        'value' => function($model){


            if($model->action_type == 'in-gift'){
                return UserTransaction::getTypeEventLabel($model->action_type) . ': <b>' . UserTransaction::getGiftLabel($model->gift_event) . '</b>';
            }

            if($model->action_type == 'in-charge' || $model->action_type == 'in-refund' || $model->action_type == 'out-cashout'){
                return UserTransaction::getTypeEventLabel($model->action_type);
            }

            if($model->action_type == 'in-answer'){
                return UserTransaction::getTypeEventLabel($model->action_type) . ': <b>' . $model->question->title . '</b>';
            }

            if($model->action_type == 'in-refund-bid'){
                return UserTransaction::getTypeEventLabel($model->action_type) . ': <b>' . $model->question->title . '</b>';
            }

            if($model->action_type == 'out-question'){
                return UserTransaction::getTypeEventLabel($model->action_type) . ': <b>' . $model->question->title . '</b>';
            }

        },

        'mergeHeader'=>true,
    ],

    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'amount',
        'filter' => false,
        'label' => 'Thay đổi(xu)',
        'hAlign' => 'left',
        'vAlign' => 'middle',
        'format' => 'html',
        'headerOptions' => ['class' => 'text-center'],
        'pageSummary'=>true,
        'mergeHeader'=>true,
        'width'=>'100px',

    ],

    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'money_before',
        'filter' => false,
        'label' => 'Trước giao dịch(xu)',
        'hAlign' => 'left',
        'vAlign' => 'middle',
        'format' => 'html',
        'headerOptions' => ['class' => 'text-center'],
        'mergeHeader'=>true,
        'width'=>'100px',

    ],


    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'money_after',
        'filter' => false,
        'label' => 'Sau giao dịch(xu)',
        'hAlign' => 'left',
        'vAlign' => 'middle',
        'format' => 'html',
        'headerOptions' => ['class' => 'text-center'],
        'mergeHeader'=>true,
        'width'=>'100px',

    ],


];

?>

<div id="history" class="user-site-index paddingtop15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 hidden-xs">

                <?= UserInfo::widget(['user' => $user]); ?>
                <?= UserActionMenu::widget(['user' => $user]); ?>


            </div>
            <div class="col-sm-9">
                <div class="">

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-usd"></i></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Số dư hiện tại</span>
                                    <span class="info-box-number"><?=$user->userExtra->money?> xu</span>
                                </div><!-- /.info-box-content -->
                            </div><!-- /.info-box -->
                        </div>

                    </div>

                    <?php
                    echo DynaGrid::widget([
                    'columns'=>$columns,
                    'storage'=>DynaGrid::TYPE_COOKIE,
                    'enableMultiSort' => false,
                    //                'theme'=>'panel-default',
                    'options'=>['id'=>'dynagrid-reviews-admin'], // a unique identifier is important
                    'gridOptions'=>[
                    'dataProvider'=>$dataProvider,
                    'filterModel'=>$searchModel,
                    'showPageSummary'=>true,
                    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
                    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
                    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
                    'resizableColumns'=> true,
                    'resizeStorageKey'=> 'reviews-'. Yii::$app->user->id.'-'. date("m"),
                    //                    'floatHeader'=>true,
                    'pjax' => true, // pjax is set to always true for this demo
                    //                'beforeHeader'=>[
                    //                    [
                    //                        'columns'=>[
                    //                            ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']],
                    //                            ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']],
                    //                            ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']],
                    //                        ],
                    //                        'options'=>['class'=>'skip-export'] // remove this row from export
                    //                    ]
                    //                ],
                    // set your toolbar
                    'toolbar' =>  [

                    ['content'=>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/'.Yii::$app->request->pathInfo], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>Yii::t('app', 'Bỏ lọc')])
                    ],
                    //                        ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
                    //                        ['content'=>'{dynagrid}'],
                    '{export}',
                    //                    '{toggleData}',
                    ],
                    // export: http://demos.krajee.com/grid#grid-export
                    'export' => [
                    'fontAwesome' => true
                    ],
                    'exportConfig' => [
                    GridView::CSV => [],
                    GridView::EXCEL => [],
                    GridView::HTML => [],
                    GridView::JSON => [],
                    GridView::TEXT => [],
                    ],
                    'responsiveWrap'=>false,
                    'responsive'=>true,
                    'hover'=>true,
                    'bordered' => true,
                    'striped' => true,
                    'condensed' => true,
                    //                'showPageSummary' => true,
                    'panel' => [
                    'type'=>GridView::TYPE_PRIMARY,
                    'heading' => '<h3 class="panel-title"><i class="fa fa-list"></i> '.Yii::t('review', 'Sao kê giao dịch').'</h3>',
                    //                        'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
                    //                        'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
                    //                        'footer'=>false
                    ],
                    //                'exportConfig' => $exportConfig,
                    'emptyText' => Yii::t('review', 'Không có kết quả nào.'),
                    'summary' => Yii::t('review', 'Đang hiển thị từ {begin} -> {end} trong tổng số '.$dataProvider->totalCount)
                    ],

                    ]);
                    ?>


                    <?php
                    /*
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-info margintop15">
                                <div class="box-header">
                                    <h3 class="box-title">Sao kê giao dịch</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <th>Ngày</th>
                                                <th>Giao dịch</th>
                                                <th>Số dư đầu(xu)</th>
                                                <th>Thay đổi(xu)</th>
                                                <th>Số dư cuối(xu)</th>
                                            </tr>

                                            <?php foreach($transactions as $transaction): ?>
                                                <?php
                                                    $prefix = '+ ';
                                                    if($transaction->type == 'out')
                                                        $prefix = '- ';
                                                    $changeIcon = 'fa-caret-down text-danger';
                                                    if($transaction->type == 'in')
                                                        $changeIcon = 'fa-caret-up text-aqua';
                                                ?>
                                                <tr>
                                                    <td><?=DateTimeHelper::getDateTime($transaction->created, 'd-m-Y')?></td>
                                                    <td><?=UserTransaction::getTypeEventLabel($transaction->type_event)?></td>
                                                    <td><?=$transaction->money_before?></td>
                                                    <td><?=$prefix?><?=$transaction->amount?></td>
                                                    <td><i class="fa <?=$changeIcon?>"></i> <?=$transaction->money_after?></td>
                                                </tr>
                                            <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>

                        </div>
                    </div>

                    */
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>