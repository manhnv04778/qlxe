<?php
use app\components\assets\AppAsset;
use app\modules\frontend\widgets\UserInfo;
use yii\helpers\Url;
use app\modules\user\widgets\UserActionMenu;

$this->registerJsFile('/files/js/moment/min/moment.min.js', ['depends' => [AppAsset::className()]]);

$this->registerCssFile('/files/js/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css', ['depends' => [AppAsset::className()]]);

$this->registerJsFile('/files/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js', ['depends' => [AppAsset::className()]]);

$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/user/site_index.js', ['depends' => [AppAsset::className()]]);

$this->title = 'Thông báo của bạn';

?>

<div id="history" class="user-site-index paddingtop15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 hidden-xs">

                <?= UserInfo::widget(['user' => $user]); ?>
                <?= UserActionMenu::widget(['user' => $user]); ?>


            </div>
            <div class="col-sm-9 bg-white">
                <div class="bg-white">
                    <div class="row">
                        <div class="col-xs-12">

                            <?=$this->render('index/_list_view_'.$type, compact('user', 'dataProvider','type'))?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>