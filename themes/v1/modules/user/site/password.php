<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\components\assets\AppAsset;


$user = Yii::$app->user->identity;

/* @var $this app\components\View */
/* @var $user app\modules\user\models\User */
/* @var $passwordForm app\modules\user\models\forms\PasswordForm */


$actionName = $passwordForm->scenario == 'update' ? 'Change password' : 'Create password';
$this->title = Yii::t('user', $actionName);

?>

<div class="row">

    <div class="col-lg-8 col-md-8 col-sm-7">

        <?php
        $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'id' => 'form-password',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-md-3',
                    'offset' => 'col-md-offset-3',
                    'wrapper' => 'col-md-9',
                    'error' => '',
                    'hint' => '',
                ],
            ],
            'options' => [
                'data-action' => Yii::$app->controller->action->id
            ]
        ]); ?>

        <?php if($passwordForm->scenario == 'update'):?>

        <?=$form->field($passwordForm, 'passwordOld')->begin();?>
        <?=Html::activeLabel($passwordForm, 'passwordOld', ['class' => 'control-label col-md-4'])?>
        <div class="col-md-6">
            <?=Html::activePasswordInput($passwordForm, 'passwordOld', ['class' => 'form-control']) ?>
            <?=Html::error($passwordForm, 'passwordOld', ['class' => 'help-block help-block-error'])?>
        </div>
        <?=$form->field($passwordForm, 'passwordOld')->end();?>

        <?php endif?>

        <?=$form->field($passwordForm, 'passwordNew')->begin();?>
        <?=Html::activeLabel($passwordForm, 'passwordNew', ['class' => 'control-label col-md-4'])?>
        <div class="col-md-6">
            <?=Html::activePasswordInput($passwordForm, 'passwordNew', ['class' => 'form-control']) ?>
            <?=Html::error($passwordForm, 'passwordNew', ['class' => 'help-block help-block-error'])?>
        </div>
        <?=$form->field($passwordForm, 'passwordNew')->end();?>


        <?=$form->field($passwordForm, 'passwordNewReType')->begin();?>
        <?=Html::activeLabel($passwordForm, 'passwordNewReType', ['class' => 'control-label col-md-4'])?>
        <div class="col-md-6">
            <?=Html::activePasswordInput($passwordForm, 'passwordNewReType', ['class' => 'form-control']) ?>
            <?=Html::error($passwordForm, 'passwordNewReType', ['class' => 'help-block help-block-error'])?>
        </div>
        <?=$form->field($passwordForm, 'passwordNewReType')->end();?>

        <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
                <?= Html::submitButton(Yii::t('user', $actionName), ['class' => 'btn btn-success btn-flat btn-lg']) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
                <?php if($passwordForm->scenario == 'update'):?>
                    <hr />
                    <?=Yii::t('user', '{resetLink}', [
                        'resetLink' => Html::a(Yii::t('user','Reset password '), Url::toRoute('/user/user/reset-password'), ['target' => '_blank'])
                    ])?>
                <?php endif?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

    <div class="col-lg-4 col-md-4 col-sm-5">
        <div id="help-block" class="alert bg-gray">
            <h3><i class="icon fa fa-lightbulb-o"></i> Tips!</h3>
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <?php if($passwordForm->scenario == 'update'):?>
                            <li>If you forget your password. You can <a href="<?=Url::toRoute('/user/user/reset-password')?>">reset password</a></li>
                        <?php else:?>
                            <li>After password was created successful, you can use can't login by password with all email.<i>(You still can login by Facebook or Google accounts)</i></li>
                        <?php endif?>
                    </ul>
                </div>
            </div>
        </div>

    </div>


</div>
