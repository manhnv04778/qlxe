<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\components\assets\AppAsset;

use app\models\geo\City;
use app\models\geo\District;
use app\components\rbac\AuthManager;


use app\helpers\DateTimeHelper;
use app\modules\user\models\UserExtra;

$user = &Yii::$app->controller->user;
$userExtra = &Yii::$app->controller->userExtra;

/* @var $this app\components\View */
/* @var $user app\modules\user\models\User */

$sidebarSelected = &Yii::$app->controller->sidebarSelected;
$sidebarChildSelected = &Yii::$app->controller->sidebarChildSelected;

?>

<?php $this->beginContent('@theme/layouts/sidebar.php'); ?>
<section class="content-header">
    <h1>
        <?=$this->title?>
    </h1>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <a href="<?=$user->url?>">
                        <img class="profile-user-img img-responsive img-circle" src="<?=$user->getImageUrl()?>" alt="User profile picture">
                    </a>
                    <h3 class="profile-username text-center"><?=$user->name?></h3>

                    <p class="text-muted text-center">Member since <?=DateTimeHelper::formatDatetimeByLang($user->created)?></p>

                    <ul class="list-group list-group-unbordered">
                        <?php if($userExtra->package):?>
                        <li class="list-group-item">
                            <b>Package</b> <span class="pull-right"><?=UserExtra::getPackageData($userExtra->package)?></span>
                        </li>
                        <?php if($userExtra->package != 'free'):?>
                        <li class="list-group-item">
                            <b>Expire</b> <a class="pull-right"><?=DateTimeHelper::formatDateTime($userExtra->package_expire)?></a>
                        </li>
                        <?php endif?>
                        <?php endif?>
                    </ul>

                    <?php if(!$userExtra->package || $userExtra->package == 'free'):?>
                        <a href="<?=Url::to(['/biz/site/pricing'])?>" class="btn btn-primary btn-block"><b>Upgrade</b></a>
                    <?php else:?>
                        <a href="<?=Url::to(['/biz/site/payment', 'p' => $userExtra->package])?>" class="btn btn-primary btn-block"><b>Renew</b></a>
                    <?php endif?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="<?=$sidebarChildSelected == 'user-login' ? 'active': '' ?>"><a href="<?=Url::toRoute('/user/user/manage-login')?>"><i class="fa fa-sign-in"></i> Manage login</a></li>
                    <li class="<?=$sidebarChildSelected == 'user-password' ? 'active': '' ?>"><a href="<?=Url::toRoute('/user/user/password')?>"><i class="fa fa-ellipsis-h"></i> <?=Yii::t('app', $user->password_hash ? 'Change password' : 'Create password')?></a></li>
                    <li class="<?=$sidebarChildSelected == 'user-profile' ? 'active': '' ?>"><a href="<?=Url::toRoute('/user/user/update')?>"><i class="fa fa-edit"></i> <?=Yii::t('app', 'Update profile')?></a></li>
                </ul>
                <div class="tab-content">
                    <?=$content?>
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>

</section>
<?php $this->endContent(); ?>
