<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$model = new \app\modules\user\models\forms\LoginForm();

/* @var $user app\modules\user\models\User */
/* @var $this app\components\View */
/* @var $form yii\bootstrap\ActiveForm */

$user = Yii::$app->controller->user;

?>

<div id="alert-expert-result" class="modal fade modal-confirm" role="dialog" data-toggle="modal">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thông báo</h4>
            </div>

            <div class="modal-footer text-center">
                    <?php if($user->expert_status == 'done'): ?>
                        <div class="padding30 text-success">
                        <?='Bạn đã được chấp nhận là chuyên gia của hệ thống. Khi là chuyên gia bạn có thể tham gia giải bài cho học sinh và nhiều chính sách khác'?>
                        </div>
                    <?php elseif($user->expert_status == 'fail'): ?>
                        <div class="padding30 text-danger">
                        <?='Rất tiếc bài kiểm tra chuyên môn của bạn không đạt yêu cầu. '?>
                        </div>
                    <?php endif?>

                </div>
            </div>
        </div>
    </div>
</div>
