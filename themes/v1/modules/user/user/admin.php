<?php
use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\modules\user\models\User;
use app\components\assets\AppAsset;

/* @var $this app\components\View */
/* @var $searchModel app\modules\user\models\elasticsearch\UserElasticsearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

AppAsset::register($this);

$this->title = Yii::t('user', 'Quản lý thành viên');
?>
<section class="content-header">

    <?= $this->render('//common/breadcrumbs', [
        'data' => [
            $this->title => Url::to(['/'.Yii::$app->request->pathInfo]),
        ]
    ]) ?>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-lg-24">

            <?php
            $columns = [
//                [
//                    'class' => 'kartik\grid\SerialColumn',
//                    'order'=>DynaGrid::ORDER_FIX_LEFT,
//                    'contentOptions'=>['class'=>'kartik-sheet-style'],
//                    'width'=>'40px',
//                    'header'=>'STT',
//                    'headerOptions'=>['class'=>'kartik-sheet-style']
//                ],
                [
                    // http://demos.krajee.com/grid#expand-row-column
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detail' => function ($model, $key, $index, $column) {
                        return Yii::$app->controller->renderPartial('_view_expand', ['model'=>$model]);
                    },
                    'headerOptions'=>['class'=>'kartik-sheet-style'],
                    'detailAnimationDuration' => 200,
                    'order'=>DynaGrid::ORDER_FIX_LEFT,
                    //'disabled'=>true,
                    //'detailUrl' => Url::to(['/site/test-expand'])
                ],

                [
                    'class' => 'kartik\grid\DataColumn',
                    'format' => 'html',
                    'filter' => false,
                    'label' => 'Avatar',
                    'mergeHeader'=>true,
                    'pageSummary' => true,
//                    'hAlign' => 'center',
                    'headerOptions' => ['class' => 'text-center'],
                    'order'=>DynaGrid::ORDER_FIX_LEFT,
                    'footer' => true,
                    'value' => function ($data) {
                        return Html::img($data->imageUrl, ['class' => 'img-circle']);
                    },
                ],
                [
//                        'class' => 'yii\grid\DataColumn', // http://www.yiiframework.com/doc-2.0/yii-grid-datacolumn.html
                    'class' => 'kartik\grid\DataColumn', // http://www.yiiframework.com/doc-2.0/yii-grid-datacolumn.html
                    'attribute' => 'id',
                    'label' => 'ID',
                    'format' => 'html',
                    'vAlign'=>'middle',
//                    'filter' => false,
                    'width'=>'50px',
                    'headerOptions' => ['class' => 'text-center'],
                    'order'=>DynaGrid::ORDER_FIX_LEFT,
                    'value' => function ($data) {
                        return $data->id;
                    },
                ],
                [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'name',
                    'label' => 'Tên',
                    'format' => 'raw',
                    'vAlign'=>'middle',
                    'width'=>'220px',
                    'order'=>DynaGrid::ORDER_FIX_LEFT,
                    'value' => function ($data) {
                        return Html::a('<div class="text-bold">'.$data->name. '</div><div>'.$data->username.'</div>', $data->url, ['target' => '_blank']);
                    },
                ],

                [
                    'attribute' => 'email',
                    'label' => 'Email',
                    'vAlign'=>'middle',
                    'value' => function ($data) {
                        return $data->email;
                    },
                ],
                [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'reputation',
                    'format' => 'html',
                    'filter' => false,
                    'label' => 'Điểm danh vọng',
                    'mergeHeader'=>true,
                    'pageSummary' => true,
                    'hAlign' => 'center',
                    'headerOptions' => ['class' => 'text-center'],
                    'footer' => true,
                    'value' => function ($data) {
                        return $data->reputation;
                    },
                ],
                [
                    'class' => 'kartik\grid\DataColumn',
                    'attribute' => 'view',
                    'format' => 'html',
                    'filter' => false,
                    'label' => 'Lượt thăm',
                    'mergeHeader'=>true,
                    'pageSummary' => true,
                    'hAlign' => 'center',
                    'headerOptions' => ['class' => 'text-center'],
                    'footer' => true,
                    'value' => function ($data) {
                        return $data->view;
                    },
                ],
                [
                    'attribute' => 'question_count',
                    'filter' => false,
                    'label' => 'Question count',
                    'mergeHeader'=>true,
                    'pageSummary' => true,
//                    'hAlign' => 'center',
                    'headerOptions' => ['class' => 'text-center'],
                    'visible'=>false,
                ],
                [
                    'attribute' => 'answer_count',
                    'filter' => false,
                    'label' => 'Answer count',
                    'mergeHeader'=>true,
                    'pageSummary' => true,
//                    'hAlign' => 'center',
                    'headerOptions' => ['class' => 'text-center'],
                    'visible'=>false,
                ],
                [
                    'attribute' => 'answer_accepted_count',
                    'filter' => false,
                    'label' => 'Answer accepted count',
                    'mergeHeader'=>true,
                    'pageSummary' => true,
//                    'hAlign' => 'center',
                    'headerOptions' => ['class' => 'text-center'],
                    'visible'=>false,
                ],
                [
                    'attribute' => 'comment_count',
                    'filter' => false,
                    'label' => 'Comment count',
                    'mergeHeader'=>true,
                    'pageSummary' => true,
//                    'hAlign' => 'center',
                    'headerOptions' => ['class' => 'text-center'],
                    'visible'=>false,
                ],
                [
                    'attribute'=>'created',
                    'label' => 'Ngày gia nhập',
                    'filterType'=>GridView::FILTER_DATE,
                    'format'=>'raw',
                    'vAlign'=>'middle',
                    'width'=>'150px',
                    'filterWidgetOptions'=>[
                        'pluginOptions'=>['format'=>'yyyy-mm-dd']
                    ],
                ],

                [
                    'class' => 'kartik\grid\DataColumn',
                    'enableSorting' => false,
                    'attribute' => 'status',
                    'label' => 'Trạng thái',
                    'filter' => User::getStatusData(),
                    'format' => 'html',
                    'vAlign'=>'middle',
                    'hAlign' => 'center',
                    'width'=>'90px',
                    'value' => function ($data) {
                        $html = '';
                        if($data->status == 'enable'){
                            $html = '<span class="glyphicon glyphicon-ok text-success"></span>';
                        }elseif($data->status == 'disable'){
                            $html = '<span class="glyphicon glyphicon-remove text-danger"></span>';
                        }

                        return $html;
                    },
                ],
                [
                    'class' => 'kartik\grid\DataColumn',
                    'enableSorting' => false,
                    'attribute' => 'role',
                    'label' => 'Vai trò',
                    'vAlign'=>'middle',
                    'filter' => User::getRoleData(),
                    'format' => 'html',
                    'hAlign' => 'center',
                    'width'=>'90px',
                    'value' => function ($data) {
                        return $data->roleValue;
                    },
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'order'=>DynaGrid::ORDER_FIX_RIGHT,
                    'header' => Yii::t('app', 'Hành động'),
                    'width'=>'100px',
                    'template' => '{update}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open text-primary"></span>', $model->url, [
                                'title' => Yii::t('yii', 'Xem'),
                                'data-pjax' => 0,
                            ]);
                        },

                        'update' =>  function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-pencil text-primary"></span>', $model->getUserRouter('/user/user/update'), [
                                'title' => Yii::t('yii', 'Cập nhật'),
                                'data-pjax' => 0,
                                'class' => 'btn btn-default btn-sm'
                            ]);
                        },
                    ],
                ],
//                ['class'=>'kartik\grid\CheckboxColumn',  'order'=>DynaGrid::ORDER_FIX_RIGHT],
            ];


            // http://demos.krajee.com/dynagrid#dynagrid
            echo DynaGrid::widget([
                'columns'=>$columns,
                'storage'=>DynaGrid::TYPE_COOKIE,
                'enableMultiSort' => false,
//                'theme'=>'panel-default',
                'options'=>['id'=>'dynagrid-users-admin'], // a unique identifier is important
                'gridOptions'=>[
                    'dataProvider'=>$dataProvider,
                    'filterModel'=>$searchModel,
                    'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false
                    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
                    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
                    'resizableColumns'=> true,
                    'resizeStorageKey'=> 'users-'. Yii::$app->user->id.'-'. date("m"),
//                    'floatHeader'=>true,
                    'pjax' => true, // pjax is set to always true for this demo
//                'beforeHeader'=>[
//                    [
//                        'columns'=>[
//                            ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']],
//                            ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']],
//                            ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']],
//                        ],
//                        'options'=>['class'=>'skip-export'] // remove this row from export
//                    ]
//                ],
                    // set your toolbar
                    'toolbar' =>  [
                        ['content'=>
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/'.Yii::$app->request->pathInfo], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>Yii::t('app', 'Bỏ lọc')])
                        ],
//                        ['content'=>'{dynagridFilter}{dynagridSort}{dynagrid}'],
//                        ['content'=>'{dynagrid}'],
                        '{export}',
//                    '{toggleData}',
                    ],
                    // export: http://demos.krajee.com/grid#grid-export
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'exportConfig' => [
                        GridView::PDF => [
                            'label' => Yii::t('app', 'PDF'),
                            'iconOptions' => ['class' => 'text-danger'],
                            'showHeader' => true,
                            'showPageSummary' => true,
                            'showFooter' => true,
                            'showCaption' => true,
                            'filename' => Yii::t('app', 'grid-export'),
                            'alertMsg' => Yii::t('app', 'The PDF export file will be generated for download.'),
                            'options' => ['title' => Yii::t('app', 'Portable Document Format')],
                            'mime' => 'application/pdf',
                            'config' => [
                                'mode' => 'c',
                                'format' => 'A4-L',
                                'destination' => 'D',
                                'marginTop' => 10,
                                'marginBottom' => 10,
                                'cssInline' => '.kv-wrap{padding:20px;}' .
                                    '.kv-align-center{text-align:center;}' .
                                    '.kv-align-left{text-align:left;}' .
                                    '.kv-align-right{text-align:right;}' .
                                    '.kv-align-top{vertical-align:top!important;}' .
                                    '.kv-align-bottom{vertical-align:bottom!important;}' .
                                    '.kv-align-middle{vertical-align:middle!important;}' .
                                    '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
                                    '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
                                    '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
                                'methods' => [
                                    'SetHeader' => [
                                        ['odd' => '', 'even' => '']
                                    ],
                                    'SetFooter' => [
                                        ['odd' => '', 'even' => '']
                                    ],
                                ],
                                'options' => [
                                    'title' => '',
                                    'subject' => '',
                                    'keywords' => ''
                                ],
                                'contentBefore'=>'',
                                'contentAfter'=>''
                            ]
                        ],
                        GridView::CSV => [],
                        GridView::EXCEL => [],
                        GridView::HTML => [],
                        GridView::JSON => [],
                        GridView::TEXT => [],
                    ],
                    'responsiveWrap'=>false,
                    'responsive'=>true,
                    'hover'=>true,
                    'bordered' => true,
                    'striped' => true,
                    'condensed' => true,
//                'showPageSummary' => true,
                    'panel' => [
                        'type'=>GridView::TYPE_DEFAULT,
//                        'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Questions</h3>',
//                        'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
//                        'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
//                        'footer'=>false
                    ],
//                'exportConfig' => $exportConfig,
                ],

            ]); ?>

        </div>
    </div>

</section><!-- /.content -->