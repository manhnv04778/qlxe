<?php
use yii\helpers\Html;
use yii\helpers\Url;

$user = &Yii::$app->controller->user;

/* @var $user app\modules\user\models\User */

/* @var $this app\components\View */

$action = Yii::$app->controller->id.'-'.Yii::$app->controller->action->id;
?>

<div id="box-user-menu" class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title">
            <a class="text-white" href="<?=$user->url?>">
                <img class="avatar img-circle" src="<?=$user->imageUrl?>">
                <?=$user->name?></a>
        </h3>
    </div>
    <ul class="list-group">

        <li><a class="list-group-item <?=$action == 'user-update' ? 'active' : ''?>" href="<?=Url::toRoute('/user/user/update')?>"><i class="fa fa-edit fa-lg fa-fw small"></i> <?=Yii::t('user', 'Cập nhật tài khoản')?></a></li>
        <li><a class="list-group-item <?=$action == 'user-manage-login' ? 'active' : ''?>" href="<?=Url::toRoute('/user/user/manage-login')?>"><i class="fa fa-envelope-o fa-lg fa-fw small"></i></i> <?=Yii::t('user', 'Quản lý đăng nhập')?></a></li>
        <li><a class="list-group-item <?=$action == 'user-password' ? 'active' : ''?>" href="<?=Url::toRoute('/user/user/password')?>"><i class="fa fa-ellipsis-h fa-lg fa-fw small"></i></i> <?=Yii::t('user', $user->password_hash ? 'Đổi mật khẩu' : 'Tạo mật khẩu')?></a></li>



        <li class="text-right">
            <a class="list-group-item" href="<?php echo Url::toRoute('/user/user/logout')?>" data-method="post">
                 Thoát <i class="fa fa-sign-out text-danger"></i>
            </a>
        </li>
    </ul>
</div>
