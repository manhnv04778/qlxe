<?php
use app\components\assets\AppAsset;
use app\models\City;
use app\models\District;
use app\modules\user\models\User;
use kartik\date\DatePicker;
use kartik\datecontrol\DateControl;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/user/check_forgot.js?v='.Yii::$app->controller->version, ['depends' => [AppAsset::className()]]);

?>

<div id="update_profile" class="margintop20 paddingtop20">
    <div class="container paddingtop20">
        <div class="row">

            <div class="col-xs-12 col-md-8 padding0 col-md-offset-2">
                <div class="box-header width-100 bg-blue paddingtop15 paddingbottom5">
                    <h5 class="paddingleft15 text-upper text-18 text-white">Quên mật khẩu</h5>
                </div>
                <div class="content bg-white paddingtop30 paddingbottom30">

                    <div class="row header margin0">
                        <div class="col-md-10 col-md-offset-2">
                            <?php
                            $form = ActiveForm::begin([
                                'layout' => 'horizontal',
                                'id' => 'form-changePass',
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-md-2',
                                        'offset' => 'col-md-offset-2',
                                        'wrapper' => 'col-md-8',
                                        'error' => '',
                                        'hint' => '',
                                    ],
                                ],
                                'options' => [
                                    'data-action' => Yii::$app->controller->action->id,
                                    'class' => 'form-changePass'
                                ]
                            ]);
                            ?>
                            <div class="row header margin0">
                                <div class="col-md-10 col-md-offset-1">
                                    <h4 class=" text-upper margintop20 marginbottom20"><span>Tạo mật khẩu mới</span></h4>
                                </div>
                            </div>
                            <div class="row margin0-15">
                                <?= $form->field($forgotPassForm, 'user_name')->begin(); ?>
                                <?= Html::activeLabel($forgotPassForm, 'user_name', ['class' => 'col-md-4 text-right']) ?>
                                <div class="col-md-5">
                                    <?= Html::activeTextInput($forgotPassForm,'user_name',['class' => 'form-control'])?>
                                    <?= Html::error($forgotPassForm, 'user_name', ['class' => 'help-block help-block-error']) ?>
                                </div>
                                <?= $form->field($forgotPassForm, 'user_name')->end(); ?>
                            </div>
                            <div class="row margin0-15">
                                <?= $form->field($forgotPassForm, 'code')->begin(); ?>
                                <?= Html::activeLabel($forgotPassForm, 'code', ['class' => 'col-md-4 text-right']) ?>
                                <div class="col-md-5">
                                    <?= Html::activeTextInput($forgotPassForm,'code',['class' => 'form-control'])?>
                                    <i>Mã xác nhận đã được gửi vào số điện thoại <b><?=substr_replace($model->phone,"***",-3)?></b></i>
                                    <?= Html::error($forgotPassForm, 'code', ['class' => 'help-block help-block-error']) ?>
                                </div>
                                <?= $form->field($forgotPassForm, 'code')->end(); ?>
                            </div>
                            <div class="row margin0-15">
                                <?= $form->field($forgotPassForm, 'passwordNew')->begin(); ?>
                                <?= Html::activeLabel($forgotPassForm, 'passwordNew', ['class' => 'col-md-4 text-right']) ?>
                                <div class="col-md-5">
                                    <?= Html::activePasswordInput($forgotPassForm,'passwordNew',['class' => 'form-control'])?>
                                    <?= Html::error($forgotPassForm, 'passwordNew', ['class' => 'help-block help-block-error']) ?>
                                </div>
                                <?= $form->field($forgotPassForm, 'passwordNew')->end(); ?>
                            </div>
                            <div class="row margin0-15">
                                <?= $form->field($forgotPassForm, 'passwordNewReType')->begin(); ?>
                                <?= Html::activeLabel($forgotPassForm, 'passwordNewReType', ['class' => 'col-md-4 text-right']) ?>
                                <div class="col-md-5">
                                    <?= Html::activePasswordInput($forgotPassForm,'passwordNewReType',['class' => 'form-control'])?>
                                    <?= Html::error($forgotPassForm, 'passwordNewReType', ['class' => 'help-block help-block-error']) ?>
                                </div>
                                <?= $form->field($forgotPassForm, 'passwordNewReType')->end(); ?>
                            </div>

                            <div class="row margin0-15 margintop20 marginbottom10">
                                <div class="col-xs-12 col-md-12 text-center">
                                    <?= Html::submitButton('Hoàn tất', ['class' => 'btn bg-orange paddingright20 paddingleft20 text-white']) ?>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>

            </div>


        </div>

    </div>
</div>

<div id="modal-verify" class="modal fade modal-confirm" role="dialog" data-toggle="modal">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content radius0">
            <div class="modal-header padding0">
                <h4 class="modal-title bg-blue text-upper text-white padding10">Thông báo</h4>
            </div>
            <div class="">
                <h4 class="margintop20 marginbottom20 paddingleft20">Xác thực tài khoản thành công!</h4>
            </div>
            <div class="modal-footer">

                    <h3 class="text-16 text-center marginbottom0">
                        <a class="btn btn-primary" href="<?=Url::base(true)?>">Tiếp tục</a>
                    </h3>
            </div>
        </div>
    </div>
</div>