<?php
    use app\components\assets\AppAsset;
    use app\helpers\DateTimeHelper;
    use app\modules\user\widgets\AppRecent;
    use app\modules\user\models\User;
    use app\modules\exam\models\Clas;
    use app\modules\exam\models\School;
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ListView;

    $this->registerJsFile($this->theme->baseUrl . '/files/js/modules/user/profile.js?v='.Yii::$app->controller->version, ['depends' => [AppAsset::className()]]);
    $percent = ($user->userExtra->point * 100) / Yii::$app->params['maxpoint'];
    $user_sess = Yii::$app->controller->user;


    if($user_sess){
        $this->registerJs("var user_id = ".$user_sess->id."; var follow_user_id = ".$user->id."", $this::POS_BEGIN);
    }
    
?>

<div id="profile" class="margintop20 paddingtop20">
    <div class="play-main paddingtop20">
        <div class="container">
            <div class="row">
                <div class="col-xs-8">
                    <div class="info bg-white padding15 float-left width-100">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="media-block info-game">
                                    <div class="media-image">
                                        <img width="150" class="img-responsive img-circle" src="<?=$user->imageUrl?>"/>
                                    </div>

                                    <div class="media-content marginleft15 paddingtop5">
                                        <h1 class="text-18 text-blue"><?=$user->name?></h1>
                                        <p class="text-13 text-muted marginbottom0">Thành viên từ: <?=DateTimeHelper::getDateTime($user->created,'d/m/Y')?></span></p>
                                        <p class="float-left width-100 marginbottom0">
                                            <span class="font-500 text-blue float-left">Level: <?=$user->userExtra->level?></span>
                                            <span class="float-right text-13">/<?=Yii::$app->params['maxpoint']?></span>
                                            <span class="float-right text-blue text-13"><?=$user->userExtra->point?></span>
                                        </p>
                                        <div class="progress float-left width-100">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?=$user->userExtra->point?>" aria-valuemin="0" aria-valuemax="<?=Yii::$app->params['maxpoint']?>" style="width:<?=$percent?>%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <?php
                                if(isset($user_sess) && $user_sess->id == $user->id){
                                    ?>
                                    <div class="group-btn float-left width-100">
                                        <div class="float-left">
                                            <p class="margin0 text-upper">Tài khoản</p>
                                            <p class="margin0">
                                                <i class="icon-exu"></i><span class="text-orange font-500 paddingleft40"><?=$user->userExtra->coin?></span>
                                                <span class="text-13 text-bold">Exu</span>
                                            </p>
                                        </div>
                                        <a class="float-right pay" href="<?=Url::toRoute(['/user/charge/charge-card'])?>">Nạp</a>
                                    </div>

                                    <div class="float-right group-tooltip margintop10 text-white">
                                        <a class="text-22 text-white h-tooltip bg-yellow growl" title="" href="#"><i class="fa fa-usd" aria-hidden="true"></i></a>
                                        <a class="icon-edit text-22 text-white bg-blue h-tooltip  growl" title="" href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a class="text-19 text-white bg-green h-tooltip  growl" title="" href="#"><i class="fa fa-cogs" aria-hidden="true"></i></a>
                                        <a class="text-19 text-white bg-purple h-tooltip  growl" title="" href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                                        <a class="text-20 text-white bg-red h-tooltip  growl" title="" href="#"><i class="fa fa-calendar-times-o" aria-hidden="true"></i></a>
                                    </div>
                                <?php }else{ ?>
                                    <div class="user-info">
                                        <div class="user-info-list">
                                            <?php if($user->userExtra->class_id):?>
                                                <p><i class="fa fa-briefcase"></i> Đang là Học sinh <span><?= $user->userExtra->class->name?></span></p>
                                            <?php endif;?>
                                            <?php if($user->userExtra->school_id):?>
                                                <p><i class="fa fa-university"></i> Trường <span><?=$user->userExtra->school->name?></span></p>
                                            <?php endif;?>
                                            <?php if($user->userExtra->school_id):?>
                                                    <p><i class="fa fa-map-marker"></i> Sống tại <span><?= $user->city->name; ?></span></p>
                                            <?php endif;?>
                                            <?php if($user->userExtra->school_id):?>
                                                <p><i class="fa fa-gift"></i> Sinh nhật <span><?=DateTimeHelper::getDateTime($user->birthday,'d/m/Y'); ?></span></p>
                                            <?php endif;?>
                                            <?php if($user->gender):?>
                                                <p><i class="fa fa-transgender"></i> Giới tính <span><?= User::getGenderData($user->gender); ?></span></p>
                                            <?php endif;?>
                                        </div>
                                        <div class="friend-btn">

                                        <?php if(!$user_sess):?>
                                            <button class="friend-add require-login"><i class="fa fa-plus"></i> Kết bạn</button>
                                        <?php else:?>
                                            <?php
                                                if(!empty($friend_check['process_user'])){
                                            ?>
                                                <button class="friend-process">Chờ trả lời</button>
                                            <?php }else if(!empty($friend_check['process_user_follow'])){ ?>
                                                <div class="process-user-follow">
                                                    <button class="friend-process user-friend-accept" data-id="<?= $friend_check['process_user_follow']->id; ?>"><i class="fa fa-check"></i> Đồng ý</button>
                                                    <button class="friend-process user-friend-cancel" data-id="<?= $friend_check['process_user_follow']->id; ?>"><i class="fa fa-remove"></i> Hủy bỏ</button>
                                                </div>
                                            <?php }else if($friend_check['done_user'] || $friend_check['done_user_follow']){ ?>
                                                <button class="friend-done"><i class="fa fa-check"></i> Bạn bè</button>
                                            <?php }else{ ?>
                                                <button class="friend-add <?= !$user_sess ? 'require-login' : ''; ?>"><i class="fa fa-plus"></i> Kết bạn</button>
                                            <?php } ?>
                                        <?php endif;?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                    </div>
                    <div class="list-friend float-left width-100 margintop20 bg-white box-shadow">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="text-orange margin0 text-left text-upper padding15 paddingtop30">Danh sách bạn bè</h4>
                                </div>
                                <div class="col-md-6 padding15">
<!--                                    <form name = 'search-friend' id ='search-friend' method="post">-->
                                    <?php
                                    $form = ActiveForm::begin([
                                        'layout' => 'horizontal',
                                        'id' => 'form-doctor',
                                        'fieldConfig' => [
                                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                            'horizontalCssClasses' => [
                                                'label' => 'col-md-2',
                                                'offset' => 'col-md-offset-2',
                                                'wrapper' => 'col-md-8',
                                                'error' => '',
                                                'hint' => '',
                                            ],
                                        ],
                                        'options' => [
                                            'data-action' => Yii::$app->controller->action->id
                                        ]
                                    ]);
                                    ?>
                                        <div id="custom-search-input">
                                            <div class="input-group col-md-12">
<!--                                                <input type="text" class="search-query form-control" name="txt_name" placeholder="Tìm kiếm bạn bè" />-->
                                                <?=Html::textInput('name_friend', '',['class'=>'search-query form-control', 'placeholder' => 'Tìm kiếm bạn bè']) ?>
                                                <span class="input-group-btn">
                                                    <button type ="submit" class="btn btn-danger" name="btn_search" type="button">
                                                        <span class=" glyphicon glyphicon-search text-16"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
<!--                                    </form>-->
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div id="friend" class="">
                                    <div class="list-friend">
                                        <div class="container">
                                            <div class="row no-padding">
                                                <div class="col-xs-8">
                                                    <div class="friends ">
                                                        <ul class="nav nav-tabs tab-cm" role="tablist">
                                                            <li role="presentation" class="text-upper text-orange active"> <a href="#list"  class="float-left padding15" aria-controls="list" role="tab" data-toggle="tab">Danh sách bạn bè (<span class="count-friend" value=""><?=$dataProvider_friend->totalCount; ?></span>)</a></li>
                                                            <?php if($user_sess && $user_sess->id == $id):?>
                                                            <li role="presentation" class="text-upper text-mute"><a  class="padding15" href="#waiting" aria-controls="waiting" role="tab" data-toggle="tab">Chờ kết bạn (<span class="count-waiting"><?=$dataProvider_waiting->totalCount; ?></span>)</a></li>
                                                            <li role="presentation" class="text-upper text-mute"><a href="#search" class="padding15" aria-controls="search" role="tab" data-toggle="tab">Yêu cầu kết bạn(<span class="count-request"><?=$dataProvider_request->totalCount; ?></span>)</a></li>
                                                            <?php endif;?>
                                                        </ul>
                                                        <!-- Tab panes -->
                                                        <div class="tab-content paddingtop20 bg-white">
                                                            <div role="tabpanel" class="tab-pane active" id="list">
                                                                <div class="row">
                                                                    <?php
                                                                    \yii\widgets\Pjax::begin([
                                                                        'id' => 'pjax-post1',
                                                                        'linkSelector' => '#pagination-post a',
                                                                        'enableReplaceState' => false,
                                                                        'timeout' => 10000
                                                                    ]);
                                                                    ?>

                                                                    <?= ListView::widget([
                                                                        'dataProvider' => $dataProvider_friend,
                                                                        'id' => 'post-list1',
                                                                        'itemOptions' => ['class' => 'item'],
                                                                        'summary' => '',
                                                                        'emptyText' => '<div class="paddingleft40 text-16 text-highlight paddingbottom30">Bạn chưa có bạn bè nào</div>',
                                                                        'itemView' => '_item_list',
                                                                        'viewParams' => ['id' => $id , 'is_search' => $is_search],
                                                                        'pager' => [
                                                                            'class' => \kop\y2sp\ScrollPager::className(),
                                                                            'triggerTemplate' => '<div class="margintop20 text-center paddingbottom30">
                                                                    <div class="row"><a class="btn text-center border1 margin0 padding10 radius0">{text}</a></div>
                                                                </div>',
                                                                            'triggerText' => '<div class="col-xs-12 text-center color-orange ">Xem thêm</div>',
                                                                            'noneLeftText' => '',
                                                                        ],
                                                                    ]);
                                                                    ?>
                                                                    <?php \yii\widgets\Pjax::end(); ?>
                                                                </div>
                                                            </div>

                                                            <?php if(!empty($dataProvider_waiting)):?>

                                                            <div role="tabpanel" class="tab-pane bg-white" id="waiting">
                                                                <div class="row">
                                                                    <?php
                                                                    \yii\widgets\Pjax::begin([
                                                                        'id' => 'pjax-post2',
                                                                        'linkSelector' => '#pagination-post a',
                                                                        'enableReplaceState' => false,
                                                                        'timeout' => 10000
                                                                    ]);
                                                                    ?>

                                                                    <?= ListView::widget([
                                                                        'dataProvider' => $dataProvider_waiting,
                                                                        'id' => 'post-list2',
                                                                        'itemOptions' => ['class' => 'item2'],
                                                                        'summary' => '',
                                                                        'emptyText' => '<div class="paddingleft40 text-16 text-highlight paddingbottom30">Bạn chưa gửi yêu cầu kết bạn cho bạn nào!!!</div>',
                                                                        'itemView' => '_item_list_waiting',
                                                                        'pager' => [
                                                                            'class' => \kop\y2sp\ScrollPager::className(),
                                                                            'triggerTemplate' => '<div class="margintop20 text-center paddingbottom20">
                                                                    <a class="btn text-center border1 margin0 padding10 radius0">{text}</a>
                                                                </div>',
                                                                            'triggerText' => '<div class="col-xs-12 text-center color-orange ">Xem thêm</div>',
                                                                            'noneLeftText' => '',
                                                                        ],
                                                                    ]);
                                                                    ?>
                                                                    <?php \yii\widgets\Pjax::end(); ?>
                                                                </div>
                                                            </div>
                                                          <?php endif; ?>

                                                            <?php if(!empty($dataProvider_request)):?>
                                                            <div role="tabpanel" class="tab-pane" id="search">
                                                                <div class="row">
                                                                    <?php
                                                                    \yii\widgets\Pjax::begin([
                                                                        'id' => 'pjax-post3',
                                                                        'linkSelector' => '#pagination-post a',
                                                                        'enableReplaceState' => false,
                                                                        'timeout' => 10000
                                                                    ]);
                                                                    ?>

                                                                    <?= ListView::widget([
                                                                        'dataProvider' => $dataProvider_request,
                                                                        'id' => 'post-list3',
                                                                        'itemOptions' => ['class' => 'item3'],
                                                                        'summary' => '',
                                                                        'emptyText' => '<div class="paddingleft40 text-16 text-highlight paddingbottom30">Bạn chưa có bạn bè nào gửi yêu cầu kết bạn!</div>',
                                                                        'itemView' => '_item_list_request',
                                                                        'pager' => [
                                                                            'class' => \kop\y2sp\ScrollPager::className(),
                                                                            'triggerTemplate' => '<div class="margintop20 text-center paddingbottom20">
                                                                    <a class="btn text-center border1 margin0 padding10 radius0">{text}</a>
                                                                </div>',
                                                                            'triggerText' => '<div class="col-xs-12 text-center color-orange ">Xem thêm</div>',
                                                                            'noneLeftText' => '',
                                                                        ],
                                                                    ]);
                                                                    ?>
                                                                    <?php \yii\widgets\Pjax::end(); ?>
                                                                </div>
                                                            </div>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="news-event float-left width-100 margintop20 bg-white box-shadow">
                        <div class="box-header">
                            <h4 class="text-orange margin0 text-left text-upper padding15">Ứng dụng có thể bạn quan tâm</h4>
                        </div>

                        <div class="box-content bg-white padding15">
                            <div class="row">
                                <?php foreach($apps as $app):?>
                                    <div class="col-xs-3 marginbottom20">
                                        <div class="item-game">
                                            <a href="<?=$app->getUrl('frontend','play')?>"><img src="<?= $app->imageUrl?>" class="img-responsive"/></a>
                                            <i></i>
                                            <a class="text-666 no-hover" href="<?=$app->getUrl('frontend','play')?>"><p class="desc text-bold margintop10 marginbottom5"><?= $app->name?></p></a>
                                            <div class="">
                                                <span class="display-block float-left text-orange text-13"><i class="fa fa-star text-15" aria-hidden="true"></i> <?=$app->appExtra->rating_avg; ?></span>
                                                <span class="display-block float-right text-blue text-13"><i class="fa fa-gamepad text-15" aria-hidden="true"></i> <?=$app->user_plays?></span>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach?>
                            </div>
                        </div>
                    </div>



                </div>
                <div class="col-xs-4">
                    <div class="margintop-20">
                        <?=AppRecent::widget(); ?>
                    </div>

                    <!--                <div class="box-extra box-shadow">-->
                    <!--                    <div class="box-header bg-green text-white padding10">-->
                    <!--                        <h4 class="margin0 text-upper">Ứng dụng hay dùng</h4>-->
                    <!--                    </div>-->
                    <!--                    <div class="box-content bg-white padding15">-->
                    <!--                        <div class="row">-->
                    <!--                            --><?php //for ($i = 1; $i <= 3; $i++): ?>
                    <!--                                <div class="col-xs-4 paddingleft5 paddingright5">-->
                    <!--                                    <img class="img-responsive" src="http://static.appvn.com/i/uploads/thumbnails/012016/9d2257cb5431bd8866b2d1946a6efae9-0-icon.jpg"/>-->
                    <!--                                    <p>Pokemon mới nhất</p>-->
                    <!--                                </div>-->
                    <!--                            --><?php //endfor; ?>
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </div>-->

                    <div class="box-extra margintop20 box-shadow">
                        <div class="box-header bg-purple text-white padding15">
                            <h4 class="margin0 text-upper">Các cuộc thi</h4>
                        </div>
                        <div class="box-content bg-white paddingtop15 paddingbottom40">
                            <div class="media-block item paddingbottom20 marginbottom20 border-bottom-ddd">
                                <div class="media-image paddingleft15">
                                    <img class="img-responsive" src="/files/image/logo_cpvm.png"/>
                                </div>

                                <div class="media-content float-right text-right marginright15">
                                    <p class="name margin0"><a  href="http://chinhphucvumon.vn/" class="text-bold text-15 text-666 no-hover">Chinh phục vũ môn</a></p>
                                    <p><a class="btn bg-blue text-white radius20 text-upper font-500" href="http://chinhphucvumon.vn/">Đăng ký ngay</a></p>
                                </div>
                            </div>
                            <div class="media-block item paddingbottom20 marginbottom20 border-bottom-ddd">
                                <div class="media-image paddingleft15">
                                    <img class="img-responsive" width="60" src="http://giaothonghocduong.com.vn/images/logo-gthd.png"/>
                                </div>

                                <div class="media-content float-right text-right marginright15">
                                    <p class="name margin0"><a href="http://giaothonghocduong.com.vn/" class="text-bold text-15 text-666 no-hover">Thủ lĩnh sinh viên</a></p>
                                    <p><a class="btn bg-orange text-white no-hover radius20" href="http://giaothonghocduong.com.vn/"><i class="icon-cup"></i><span class="marginleft25">213 Điểm </span></a></p>
                                </div>
                            </div>
                            <div class="media-block item">
                                <div class="media-image paddingleft15">
                                    <img class="img-responsive" src="http://anhsangsoiduong.cpvm.vn/images/logo.png"/>
                                </div>

                                <div class="media-content text-right marginright15">
                                    <p class="name margin0"><a  href="http://anhsangsoiduong.cpvm.vn/" class="text-bold text-666 text-15 no-hover">Học và làm theo lời bác</a></p>
                                    <p><a class="btn bg-green text-white text-upper no-hover radius20" href="http://anhsangsoiduong.cpvm.vn/">Vào thi</a></p>
                                </div>
                            </div>
                            <!--                        <div class="media-block item marginbottom20 float-right paddingbottom40">-->
                            <!--                            <p class="name margin0 text-right"><a  href="#" class="text-bold no-hover paddingright30">Xem thêm</a></p>-->
                            <!--                        </div>-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
