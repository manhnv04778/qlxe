<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$user = Yii::$app->user->identity;
/* @var $this app\components\View */
/* @var $user app\modules\user\models\User */
/* @var $passwordForm app\modules\user\models\forms\PasswordForm */


$this->title = Yii::t('user', 'Lấy lại mật khẩu');
?>

<section class="content-header">
    <h1>
        <?=$this->title?>
    </h1>
</section>


<!-- Main content -->
<section class="content">
    <div class="box box-solid">
        <div class="box-header with-border">
            <h4 class="box-title"><?=Yii::t('user', 'Hệ thống sẽ gửi email xác nhận và giúp bạn lấy lại mật khẩu')?></h4>
        </div>
        <div class="box-body">
            <div class="row">

                <div class="col-lg-3 col-lg-push-5 col-sm-5 col-sm-push-7 authchoice">
                    <a href="<?php echo Url::toRoute(['/user/user/auth', 'authclient' => 'facebook']) ?>" class="btn btn-block btn-social btn-facebook" data-popup-width="450" data-popup-height="380">
                        <i class="fa fa-facebook"></i> <?=Yii::t('user', 'Đăng nhập với Facebook')?>
                    </a>
                    <br/>
                    <a href="<?php echo Url::toRoute(['/user/user/auth', 'authclient' => 'google']) ?>" class="btn btn-block btn-social btn-google" data-popup-width="450" data-popup-height="380">
                        <i class="fa fa-google-plus"></i> <?=Yii::t('user', 'Đăng nhập với Google')?>
                    </a>
                </div>


                <div class="col-lg-5 col-lg-pull-3 col-sm-7 col-sm-pull-5">
                    <hr class="visible-xs" />

                    <?php $form = ActiveForm::begin([
                        'layout' => 'horizontal',
                        'id' => 'form-forgot-password',
                        'fieldConfig' => [
                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                            'horizontalCssClasses' => [
                                'label' => 'col-md-3',
                                'offset' => 'col-md-offset-3',
                                'wrapper' => 'col-md-9',
                                'error' => '',
                                'hint' => '',
                            ],
                        ],
                        'options' => [
                            'data-action' => Yii::$app->controller->action->id
                        ]
                    ]); ?>

                    <?=$form->field($resetForm, 'email')->textInput(['placeholder' => Yii::t('user','Email của bạn')]);?>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button class="btn btn-warning" type="submit">
                                <i class="fa fa-retweet"></i> <?=Yii::t('user', 'Gửi')?>
                            </button>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>


                </div>

            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer text-right">
            <a href="<?=Url::toRoute('/user/user/register')?>" class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o"></i> <?=Yii::t('user', 'Đăng ký')?></a>
            <a href="<?=Url::toRoute('/user/user/login')?>" class="btn btn-primary btn-xs"><i class="fa fa-sign-in"></i> <?=Yii::t('user', 'Đăng nhập')?></a>

        </div>
    </div>
</section><!-- /.content -->

