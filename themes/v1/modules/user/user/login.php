<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this app\components\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $registerForm \app\modules\user\models\forms\RegisterForm */

$this->title = Yii::t('user', 'Đăng nhập');

?>


<div id="login" class="paddingtop20">
    <section class="content">
        <div class="container padding20">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h4 class="title marginbottom20 bg-blue text-upper text-white text-20"><?=Yii::t('user', 'Đăng nhập')?></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h5 class="title-body text-orange text-upper text-20">Egame Id</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <?php $form = ActiveForm::begin([
                        'layout' => 'horizontal',
                        'id' => 'form-login',
                        'action' => Url::toRoute('/user/user/login'),
                        'fieldConfig' => [
                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-3',
                                'offset' => 'col-sm-offset-3',
                                'wrapper' => 'col-sm-9',
                                'error' => '',
                                'hint' => '',
                            ],
                        ],
                        'options' => [
                            //'data-action' => Yii::$app->controller->action->id
                        ]
                    ]); ?>

                    <?=$form->field($model, 'username')->begin();?>
                    <?=Html::activeTextInput($model, 'username', ['class' => 'form-control','placeholder' => Yii::t('user','Email/Username')])?>
                    <?=Html::error($model, 'username', ['class' => 'help-block help-block-error float-left margintop5'])?>
                    <?=$form->field($model, 'username')->end();?>


                    <?=$form->field($model, 'password')->begin();?>
                    <?=Html::activePasswordInput($model, 'password', ['class' => 'form-control','placeholder' => Yii::t('user','Password')])?>
                    <?=Html::error($model, 'password', ['class' => 'help-block help-block-error float-left margintop5'])?>
                    <?=$form->field($model, 'password')->end();?>

                    <div class="row">
                        <div class="col-xs-6 loginform-rememberme">
                            <?= Html::activeCheckbox($model, 'rememberMe',['label' => 'Ghi nhớ đăng nhập','for' => 'loginform-rememberme']) ?>
                        </div>

                        <div class="col-xs-6 text-right">
                            <a href="<?=Url::toRoute('/user/user/check-forgot-pass')?>" class="text-orange"> <?=Yii::t('user', 'Quên mật khẩu')?></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <button class="padding10 width-100 margintop15 text-center text-white text-upper text-18 bg-orange border-none" type="submit">
                                <?=Yii::t('user', 'Đăng nhập')?>
                            </button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>

            </div>

            <div class="padding20 paddingtop5 social-login margintop15">
                <div class="row">
                    <h5 class="or-social text-muted text-italic text-15 font-weight-400 text-center">Hoặc đăng nhập bằng</h5>

                    <div class="col-xs-6">
                        <a href="<?php echo Url::toRoute(['/user/user/auth', 'authclient' => 'facebook']) ?>" title="<?=Yii::t('user', 'Đăng nhập bằng tài khoản Facebook')?>" class="btn-facebook text-white">
                            <i class="fa fa-facebook"></i> <span><?=Yii::t('user', 'Facebook')?></span>
                        </a>
                    </div>
                    <div class="col-xs-6">
                        <a href="<?php echo Url::toRoute(['/user/user/auth', 'authclient' => 'google']) ?>" title="<?=Yii::t('user', 'Đăng nhập bằng tài khoản Google')?>" class="btn-google text-white">
                            <i class="fa fa-google-plus"></i> <span><?=Yii::t('user', 'Google')?></span>
                        </a>
                    </div>
                </div>
            </div>


            <div class="row margintop15">
                <div class="col-sm-12"><h4 class="text-upper font-weight-400 text-center">Bạn chưa có tài khoản?</h4></div>
            </div>
            <div class="row marginbottom20">
                <div class="col-sm-6 col-sm-offset-3">
                    <a href="<?=Url::toRoute('/user/user/register')?>" class="btn-register"><?=Yii::t('user', 'Đăng ký tài khoản')?><span class="text-orange text-upper"> Egame</span></a>
                </div>
            </div>

        </div>

    </section><!-- /.content -->
</div>
<!-- Main content -->
