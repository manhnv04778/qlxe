<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this app\components\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $registerForm \app\modules\user\models\forms\RegisterForm */

$this->title = Yii::t('user', 'Đăng ký');

?>


<div id="register">
    <section class="content">
        <div class="container paddingtop20">
            <div class="row">
<!--                <div class="col-xs-12 col-md-3 col-md-push-6 col-md-offset-2 hidden-xs hidden-sm">-->
<!--                    <div class="rown step-box">-->
<!--                        <div class="col col-xs-4 col-sm-12">-->
<!--                            <div class="box-header bg-blue">-->
<!--                                <h4 class="text-17 text-white margin0 text-left text-upper padding15 text-center">tạo tài khoản</h4>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col col-xs-4 col-sm-12">-->
<!--                            <div class="box-header bg-gray2">-->
<!--                                <h4 class="text-17 text-white margin0 text-left text-upper padding15 text-center">Thông tin chung</h4>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col col-xs-4 col-sm-12">-->
<!--                            <div class="box-header bg-gray2">-->
<!--                                <h4 class="text-17 text-white margin0 text-left text-upper padding15 text-center">thông tin riêng</h4>-->
<!--                        </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="col-xs-12 col-md-1 col-md-offset-2 padding0 margintop40">
                    <div class="padding0 active margintop10">
                        <div class="box-header-pro bg-orange" data-toggle="tooltip" data-placement="left" title="Đăng ký tài khoản">
                            <h4 class="text-30 text-white text-center height50">
                                <i class="fa fa-user-plus" aria-hidden="true"></i>
                            </h4>
                        </div>
                    </div>

                    <div class="box-header-pro bg-gray" data-toggle="tooltip" data-placement="left" title="Thông tin chung">
                        <h4 class="text-17 text-white height50">
                            <div class="icon-notice">
                            </div>
                        </h4>
                    </div>

                    <div class="box-header-pro bg-gray"  data-toggle="tooltip" data-placement="left" title="Thông tin thêm">
                        <h4 class="text-17 text-white height50">
                            <div class="icon-lock">
                            </div>
                        </h4>
                    </div>
                    <div class="box-header-pro bg-gray"  data-toggle="tooltip" data-placement="left" title="Bảo vệ tài khoản">
                        <h4 class="text-17 text-white height50">
                            <div class="icon-protect">
                            </div>
                        </h4>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6 bg-white padding0">
                    <h4 class="marginbottom20 bg-blue text-upper text-center text-white text-20"><?=Yii::t('user', 'Đăng ký')?></h4>
                    </div>
                    <div class="col-xs-12 col-md-6 bg-white">

                        <h5 class="title-body text-orange text-upper text-20">Thành viên mới</h5>
                        <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'id' => 'form-login',
                            'fieldConfig' => [
                                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                'horizontalCssClasses' => [
                                    'label' => 'col-sm-3',
                                    'offset' => 'col-sm-offset-3',
                                    'wrapper' => 'col-sm-9',
                                    'error' => '',
                                    'hint' => '',
                                ],
                            ],
                            'options' => [
                                'data-action' => Yii::$app->controller->action->id
                            ]
                        ]); ?>

                        <?=$form->field($registerForm, 'username')->begin();?>
                        <?=Html::activeTextInput($registerForm, 'username', ['class' => 'form-control border-gray','placeholder' => Yii::t('user','Tên đăng nhập')])?>
                        <?=Html::error($registerForm, 'username', ['class' => 'help-block help-block-error float-left margintop5'])?>
                        <?=$form->field($registerForm, 'username')->end();?>


                        <?=$form->field($registerForm, 'password')->begin();?>
                        <?=Html::activePasswordInput($registerForm, 'password', ['class' => 'form-control border-gray','placeholder' => Yii::t('user','Mật khẩu')])?>
                        <span class="text-12"><i>(Mật khẩu có độ dài từ 6-20 ký tự, viết liền không dấu)</i></span>
                        <?=Html::error($registerForm, 'password', ['class' => 'help-block help-block-error float-left margintop5'])?>
                        <?=$form->field($registerForm, 'password')->end();?>

                        <?=$form->field($registerForm, 'passwordReType')->begin();?>
                        <?=Html::activePasswordInput($registerForm, 'passwordReType', ['class' => 'form-control border-gray','placeholder' => Yii::t('user','Xác nhận mật khẩu')])?>
                        <?=Html::error($registerForm, 'passwordReType', ['class' => 'help-block help-block-error float-left margintop5'])?>
                        <?=$form->field($registerForm, 'passwordReType')->end();?>

                        <?=$form->field($registerForm, 'phone')->begin();?>
                        <?=Html::activeTextInput($registerForm, 'phone', ['class' => 'form-control border-gray','placeholder' => Yii::t('user','Số điện thoại')])?>
                        <?=Html::error($registerForm, 'phone', ['class' => 'help-block help-block-error float-left margintop5'])?>
                        <?=$form->field($registerForm, 'phone')->end();?>


                        <div class="form-group margintop30">
                            <button class="bg-orange text-white padding10 width-100 border-none text-upper text-18" type="submit"><?=Yii::t('user', 'Đăng ký')?></button>
                        </div>
                        <?php ActiveForm::end(); ?>

                        <div class="padding20 paddingtop5 social-login">
                            <div class="row">
                                <h5 class="or-social text-muted text-italic text-15 font-weight-400 text-center">Hoặc đăng ký bằng</h5>

                                <div class="col-xs-6">
                                    <a href="<?php echo Url::toRoute(['/user/user/auth', 'authclient' => 'facebook']) ?>" title="<?=Yii::t('user', 'Đăng nhập bằng tài khoản Facebook')?>" class="btn-facebook text-white">
                                        <i class="fa fa-facebook"></i> <span><?=Yii::t('user', 'Facebook')?></span>
                                    </a>
                                </div>
                                <div class="col-xs-6">
                                    <a href="<?php echo Url::toRoute(['/user/user/auth', 'authclient' => 'google']) ?>" title="<?=Yii::t('user', 'Đăng nhập bằng tài khoản Google')?>" class="btn-google text-white">
                                        <i class="fa fa-google-plus"></i> <span><?=Yii::t('user', 'Google')?></span>
                                    </a>
                                </div>
                            </div>
                        </div>

                </div>
<!--                <div class="col-xs-12 col-md-6 col-md-pull-3">-->
<!--                    <h4 class="marginbottom20 bg-blue text-upper text-center text-white text-20">--><?//=Yii::t('user', 'Đăng ký')?><!--</h4>-->
<!--                    <h5 class="title-body text-orange text-upper text-20">Thành viên mới</h5>-->
<!--                    --><?php //$form = ActiveForm::begin([
//                        'layout' => 'horizontal',
//                        'id' => 'form-login',
//                        'fieldConfig' => [
//                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
//                            'horizontalCssClasses' => [
//                                'label' => 'col-sm-3',
//                                'offset' => 'col-sm-offset-3',
//                                'wrapper' => 'col-sm-9',
//                                'error' => '',
//                                'hint' => '',
//                            ],
//                        ],
//                        'options' => [
//                            'data-action' => Yii::$app->controller->action->id
//                        ]
//                    ]); ?>
<!---->
<!--                    --><?//=$form->field($registerForm, 'username')->begin();?>
<!--                    --><?//=Html::activeTextInput($registerForm, 'username', ['class' => 'form-control','placeholder' => Yii::t('user','Tên đăng nhập')])?>
<!--                    --><?//=Html::error($registerForm, 'username', ['class' => 'help-block help-block-error float-left margintop5'])?>
<!--                    --><?//=$form->field($registerForm, 'username')->end();?>
<!---->
<!---->
<!--                    --><?//=$form->field($registerForm, 'password')->begin();?>
<!--                    --><?//=Html::activePasswordInput($registerForm, 'password', ['class' => 'form-control','placeholder' => Yii::t('user','Mật khẩu')])?>
<!--                    --><?//=Html::error($registerForm, 'password', ['class' => 'help-block help-block-error float-left margintop5'])?>
<!--                    --><?//=$form->field($registerForm, 'password')->end();?>
<!---->
<!--                    --><?//=$form->field($registerForm, 'passwordReType')->begin();?>
<!--                    --><?//=Html::activePasswordInput($registerForm, 'passwordReType', ['class' => 'form-control','placeholder' => Yii::t('user','Xác nhận mật khẩu')])?>
<!--                    --><?//=Html::error($registerForm, 'passwordReType', ['class' => 'help-block help-block-error float-left margintop5'])?>
<!--                    --><?//=$form->field($registerForm, 'passwordReType')->end();?>
<!---->
<!--                    --><?//=$form->field($registerForm, 'phone')->begin();?>
<!--                    --><?//=Html::activeTextInput($registerForm, 'phone', ['class' => 'form-control','placeholder' => Yii::t('user','Số điện thoại')])?>
<!--                    --><?//=Html::error($registerForm, 'phone', ['class' => 'help-block help-block-error float-left margintop5'])?>
<!--                    --><?//=$form->field($registerForm, 'phone')->end();?>
<!---->
<!---->
<!--                    <div class="form-group margintop30">-->
<!--                        <button class="bg-orange text-white padding10 width-100 border-none text-upper text-18" type="submit">--><?//=Yii::t('user', 'Đăng ký')?><!--</button>-->
<!--                    </div>-->
<!--                    --><?php //ActiveForm::end(); ?>
<!---->
<!--                    <div class="padding20 paddingtop5 social-login">-->
<!--                        <div class="row">-->
<!--                            <h5 class="or-social text-muted text-italic text-15 font-weight-400 text-center">Hoặc đăng ký bằng</h5>-->
<!---->
<!--                            <div class="col-xs-6">-->
<!--                                <a href="--><?php //echo Url::toRoute(['/user/user/auth', 'authclient' => 'facebook']) ?><!--" title="--><?//=Yii::t('user', 'Đăng nhập bằng tài khoản Facebook')?><!--" class="btn-facebook text-white">-->
<!--                                    <i class="fa fa-facebook"></i> <span>--><?//=Yii::t('user', 'Facebook')?><!--</span>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                            <div class="col-xs-6">-->
<!--                                <a href="--><?php //echo Url::toRoute(['/user/user/auth', 'authclient' => 'google']) ?><!--" title="--><?//=Yii::t('user', 'Đăng nhập bằng tài khoản Google')?><!--" class="btn-google text-white">-->
<!--                                    <i class="fa fa-google-plus"></i> <span>--><?//=Yii::t('user', 'Google')?><!--</span>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                </div>-->

            </div>
        </div>

    </section><!-- /.content -->
</div>
<!-- Main content -->
