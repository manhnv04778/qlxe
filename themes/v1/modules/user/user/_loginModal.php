<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$model = new \app\modules\user\models\forms\LoginForm();


/* @var $this app\components\View */
/* @var $form yii\bootstrap\ActiveForm */

$requireLoginMessage = Yii::t('user','Bạn cần đăng nhập để sử dụng chức năng này.');
$this->registerJs("var loginMessage = '{$requireLoginMessage}'", $this::POS_END);

?>
<div id="modal-login" class="modal fade" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header padding0">
                <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i></button>

				<h4 class="modal-title bg-blue text-upper text-center text-white text-20"><?=Yii::t('user', 'Đăng nhập')?></h4>
			</div>
			<div class="modal-body">
                <h5 class="title-body text-orange text-upper text-20">Egame Id</h5>
                <div class="row">
                    <div class="col-sm-12">
                        <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'id' => 'form-login',
                            'action' => Url::toRoute('/user/user/login'),
                            'fieldConfig' => [
                                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                'horizontalCssClasses' => [
                                    'label' => 'col-sm-3',
                                    'offset' => 'col-sm-offset-3',
                                    'wrapper' => 'col-sm-9',
                                    'error' => '',
                                    'hint' => '',
                                ],
                            ],
                            'options' => [
                                //'data-action' => Yii::$app->controller->action->id
                                'class' => 'padding10'
                            ]
                        ]); ?>

                        <?=$form->field($model, 'username')->begin();?>
                        <?=Html::activeTextInput($model, 'username', ['class' => 'form-control','placeholder' => Yii::t('user','Email/Username')])?>
                        <?=Html::error($model, 'username', ['class' => 'help-block help-block-error float-left margintop5'])?>
                        <?=$form->field($model, 'username')->end();?>


                        <?=$form->field($model, 'password')->begin();?>
                        <?=Html::activePasswordInput($model, 'password', ['class' => 'form-control','placeholder' => Yii::t('user','Password')])?>
                        <?=Html::error($model, 'password', ['class' => 'help-block help-block-error float-left margintop5'])?>
                        <?=$form->field($model, 'password')->end();?>



                        <div class="row">
                            <div class="col-xs-6 loginform-rememberme">
                                <?= Html::activeCheckbox($model, 'rememberMe',['label' => 'Ghi nhớ đăng nhập','for' => 'loginform-rememberme']) ?>
                            </div>

                            <div class="col-xs-6 text-right">
                                <a href="<?=Url::toRoute('/user/user/check-forgot-pass')?>" class="text-orange"> <?=Yii::t('user', 'Quên mật khẩu')?></a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <button class="padding15 width-100 margintop15 text-center text-white text-upper text-18 bg-blue border-none" type="submit">
                                    <?=Yii::t('user', 'Đăng nhập')?>
                                </button>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>

                </div>

                <div class="padding20 paddingtop5 social-login">
                    <div class="row">
                        <h5 class="or-social text-muted text-italic text-15 font-weight-400 text-center">Hoặc đăng nhập bằng</h5>

                        <div class="col-xs-6">
                            <a href="<?php echo Url::toRoute(['/user/user/auth', 'authclient' => 'facebook']) ?>" title="<?=Yii::t('user', 'Đăng nhập bằng tài khoản Facebook')?>" class="btn-facebook text-white">
                                <i class="fa fa-facebook"></i> <span><?=Yii::t('user', 'Facebook')?></span>
                            </a>
                        </div>
                        <div class="col-xs-6">
                            <a href="<?php echo Url::toRoute(['/user/user/auth', 'authclient' => 'google']) ?>" title="<?=Yii::t('user', 'Đăng nhập bằng tài khoản Google')?>" class="btn-google text-white">
                                <i class="fa fa-google-plus"></i> <span><?=Yii::t('user', 'Google')?></span>
                            </a>
                        </div>
                    </div>
                </div>


                <div class="row margintop15">
                    <div class="col-xs-12"><h4 class="text-upper font-weight-400 text-center">Bạn chưa có tài khoản?</h4></div>
                </div>

			</div>

            <div class="row paddingright25 paddingleft25 marginbottom20">
                <div class="col-xs-12">
                    <a href="" class="btn-register-modal"><?=Yii::t('user', 'Đăng ký tài khoản')?><span class="text-orange text-upper"> Egame</span></a>
                </div>
            </div>

		</div>
		<!-- /.modal-dalog -->
	</div>
</div>