<?php
use app\components\assets\AppAsset;
use app\models\City;
use app\models\District;
use app\modules\user\models\User;
use kartik\date\DatePicker;
use kartik\datecontrol\DateControl;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;


?>

<div id="update_profile" class="margintop20 paddingtop20">
    <div class="container paddingtop20">
        <div class="row">

            <div class="col-xs-12">
                <div class="box-header width-100 bg-blue paddingtop15 paddingbottom5">
                    <h5 class="paddingleft15 text-upper text-18 text-white">Cập nhật thông tin</h5>
                </div>
                <div class="content bg-white paddingtop30 paddingbottom30">
                    <div class="row header margin0">
                        <div class="col-md-10 col-md-offset-2">
                            <h3 class="title text-upper text-orange"><span>Thay đổi password</span></h3>
                        </div>
                    </div>

                    <?php
                    $form = ActiveForm::begin([
                        'layout' => 'horizontal',
                        'id' => 'form-doctor',
                        'fieldConfig' => [
                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                            'horizontalCssClasses' => [
                                'label' => 'col-md-2',
                                'offset' => 'col-md-offset-2',
                                'wrapper' => 'col-md-8',
                                'error' => '',
                                'hint' => '',
                            ],
                        ],
                        'options' => [
                            'data-action' => Yii::$app->controller->action->id,
                            'class' => 'margintop30'
                        ]
                    ]);
                    ?>
                    <div class="row margin0-15">
                        <?= $form->field($changePassForm, 'passOld')->begin(); ?>
                        <?= Html::activeLabel($changePassForm, 'passOld', ['class' => 'col-md-3 col-md-offset-2 text-right']) ?>
                        <div class="col-md-5">
                            <?= Html::activePasswordInput($changePassForm,'passOld',['class' => 'form-control'])?>
                            <?= Html::error($changePassForm, 'passOld', ['class' => 'help-block help-block-error']) ?>
                        </div>
                        <?= $form->field($changePassForm, 'passOld')->end(); ?>
                    </div>
                    <div class="row margin0-15">
                        <?= $form->field($changePassForm, 'passNew')->begin(); ?>
                        <?= Html::activeLabel($changePassForm, 'passNew', ['class' => 'col-md-3 col-md-offset-2 text-right']) ?>
                        <div class="col-md-5">
                            <?= Html::activePasswordInput($changePassForm,'passNew',['class' => 'form-control'])?>
                            <?= Html::error($changePassForm, 'passNew', ['class' => 'help-block help-block-error']) ?>
                        </div>
                        <?= $form->field($changePassForm, 'passNew')->end(); ?>
                    </div>
                    <div class="row margin0-15">
                        <?= $form->field($changePassForm, 'passNewReType')->begin(); ?>
                        <?= Html::activeLabel($changePassForm, 'passNewReType', ['class' => 'col-md-3 col-md-offset-2 text-right']) ?>
                        <div class="col-md-5">
                            <?= Html::activePasswordInput($changePassForm,'passNewReType',['class' => 'form-control'])?>
                            <?= Html::error($changePassForm, 'passNewReType', ['class' => 'help-block help-block-error']) ?>
                        </div>
                        <?= $form->field($changePassForm, 'passNewReType')->end(); ?>
                    </div>

                    <div class="row margin0-15 margintop20 marginbottom10">
                        <div class="col-xs-12 col-md-9 col-md-offset-3 text-center">
                            <?= Html::submitButton('Hoàn thành', ['class' => 'btn bg-orange paddingright20 paddingleft20 text-white']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>


        </div>

    </div>
</div>

<div id="modal-verify" class="modal fade modal-confirm" role="dialog" data-toggle="modal">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content radius0">
            <div class="modal-header padding0">
                <h4 class="modal-title bg-blue text-upper text-white padding10">Thông báo</h4>
            </div>
            <div class="">
                <h4 class="margintop20 marginbottom20 paddingleft20">Xác thực tài khoản thành công!</h4>
            </div>
            <div class="modal-footer">

                <h3 class="text-16 text-center marginbottom0">
                    <a class="btn btn-primary" href="<?=Url::base(true)?>">Tiếp tục</a>
                </h3>
            </div>
        </div>
    </div>
</div>