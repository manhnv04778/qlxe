<?php
use app\components\assets\AppAsset;
use app\helpers\DateTimeHelper;
use app\helpers\AppHelper;
use app\models\App;
use app\modules\frontend\widgets\Point;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use app\modules\frontend\widgets\AppCat;
use app\modules\frontend\widgets\AppSuggest;
$this->registerJsFile($this->theme->baseUrl.'/files/js/modules/exam/exam.js', ['depends' => [AppAsset::className()]]);
$user = Yii::$app->controller->user;
?>

<div id="modal-accept-info" class="modal fade modal-confirm" role="dialog" data-toggle="modal">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content radius0">
            <div class="modal-header padding0">
                <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i></button>
                <h4 class="modal-title bg-blue text-upper text-white padding10">Thông tin cá nhân</h4>
            </div>

            <div class="modal-body">
                <div class="row margin0 marginbottom10 border-bottom-ddd">
                    <div class="col-sm-4"><label>Họ tên</label></div><div class="col-sm-7"><label><?=$user->full_name?></label></div>
                </div>
                <div class="row margin0 marginbottom10 border-bottom-ddd">
                    <div class="col-sm-4"><label>Ngày sinh: </label></div><div class="col-sm-7"><label><?=DateTimeHelper::getDateTime($user->birthday,'d-m-Y')?></label></div>                   </div>
                <div class="row margin0 marginbottom10 border-bottom-ddd">
                    <div class="col-sm-4"><label>Giới tính: </label></div><div class="col-sm-7"><label><?=$user->gender == 'male' ? 'Nam' : 'Nữ'?></label></div>
                </div>
                <div class="row margin0 marginbottom10 border-bottom-ddd">
                    <div class="col-sm-4"><label>Lớp: </label></div><div class="col-sm-7"><label><?=!empty($user->userExtra->class_id) ? $user->userExtra->class->name : '' ?></label></div>
                </div>
                <div class="row margin0 marginbottom10 border-bottom-ddd">
                    <div class="col-sm-4"><label>Trường: </label></div><div class="col-sm-7"><label><?=!empty($user->userExtra->school_id) ? $user->userExtra->school->name : '' ?></label></div>
                </div>
                <div class="row margin0 marginbottom10 border-bottom-ddd">
                    <div class="col-sm-4"><label>Quận/huyện:</label></div><div class="col-sm-7"><label><?=!empty($user->userExtra->district_id) ? $user->userExtra->district->name : '' ?></label></div>
                </div>
                <div class="row margin0 marginbottom10 border-bottom-ddd">
                    <div class="col-sm-4"><label>Tỉnh/thành:</label></div><div class="col-sm-7"><label><?=!empty($user->userExtra->city_id) ? $user->userExtra->city->name : '' ?></label></div>
                </div>

                <div class="row">
                    <div class="col-xs-12 text-muted text-italic">Toàn bộ thông tin thi của bạn phải chính xác nếu không sẽ ảnh hưởng tới thành tích thi của bạn.</div>
                    <div class="col-xs-12 text-muted text-italic">Bạn có muốn cập nhập lại thông tin của mình không?</div>
                </div>
            </div>
            <div class="modal-footer border-none">
                <h3 class="text-16 text-center marginbottom0">
                    <a class="btn btn-primary btn-update" href="<?=Url::toRoute(['/user/user/update-profile'])?>">Cập nhật</a>
                    <a class="btn btn-default paddingleft20 paddingright20 btn-accept" href="<?=$url?>">Tiếp tục</a>
                </h3>
            </div>
        </div>
    </div>
</div>