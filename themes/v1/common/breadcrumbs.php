<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $data array */
?>
<div class="breadcrumbs">
    <span><a href="<?= Url::toRoute(['/'])?>"><i class="home ion-ios-home"></i></a></span>
    <?php if(!empty($data)):?>

        <?php if(is_array($data)):?>
            <?php foreach($data as $label => $url):?>
                <span>
                    <i class="arrow ion-ios-arrow-forward"></i>
                    <?php if($url):?>
                        <a href="<?=$url?>"><?=$label?></a>
                    <?php else:?>
                        <?=$label?>
                    <?php endif?>
                </span>
            <?php endforeach?>
        <?php else:?>

            <?=$data?>

        <?php endif?>

    <?php endif?>
</div>
