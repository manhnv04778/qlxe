<?php

use yii\helpers\Html;

/* @var $this app\components\View */
?>

<div class="container-fluid">
    <div class="page-header">
        <h1>Sticky footer with fixed navbar</h1>
    </div>


    <div class="card">

        <div class="card-height-indicator"></div>

        <div class="card-content">

            <div class="card-image">
                <img src="./image.jpg" alt="Loading image...">
                <h3 class="card-image-headline">Lorem Ipsum Dolor</h3>
            </div>

            <div class="card-body">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>

            <footer class="card-footer">
                <button class="btn btn-flat">Share</button>
                <button class="btn btn-flat btn-warning">Learn More</button>
            </footer>

        </div>

    </div>


</div>