<?php

use yii\helpers\Html;

/* @var $this app\components\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
//echo "<pre>"; print_r($exception); echo "</pre>";

?>

<section class="content-header">
    <h1>
        <?= Html::encode($this->title) ?>
        <small><?=$message?></small>
    </h1>
</section>

<!-- Main content -->

