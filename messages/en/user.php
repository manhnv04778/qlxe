<?php
return [

    'Đăng nhập' => 'Login',
    'Đăng nhập bằng tài khoản Facebook' => 'Login by Facebook account',
    'Đăng nhập bằng tài khoản Google' => 'Login by Facebook account',
    'Đăng nhập với Facebook' => 'Login with Facebook',
    'Đăng nhập với Google' => 'Login with Facebook',

    'Thoát' => 'Logout',
    'Đăng ký' => 'Register',
    'Tạo mật khẩu' => 'Create password',
    'Đổi mật khẩu' => 'Change password',
    'Bạn cần đăng nhập để sử dụng chức năng này.' => 'You must be signed in to use this feature.',


    'Lấy lại mật khẩu' => 'Reset password',
    'Khuyến khích bạn đăng nhập nhanh bằng tài khoản Facebook hay Google' => 'Quick login by Google or Facebook account are recommended',
    'Hệ thống sẽ gửi email để giúp bạn lấy lại mật khẩu' => 'We will send an email to help you reset your password',
    'Email của bạn' => 'Your email',
    'Kiểm tra hòm thư của bạn và tiến hành làm mới mật khẩu' => 'Check mail and reset your password',
    'Lỗi! Hệ thống không gửi email tới bạn được. Bạn hãy kiểm tra lại email hoặc liên hệ lại với chúng tôi' => ' Sorry, we are unable to reset password for email provided',
    'Xin chào {0}, Kích vào liên kết bên dưới để làm mới mật khẩu' => 'Hi! {0}, Click to link below to reset your password',
    'Hãy đặt lại mật khẩu mới cho tài khoản của bạn' => 'Create new your password now',

    'Xác nhận email thành công' => 'Verify email successful',
    'Mật khẩu của bạn đã được cập nhật thành công' => 'Your password has been updated successful',
    'Mật khẩu của bạn đã được tạo thành công' => 'Your password has been created successful',
    'Thêm email thành công. Hãy kiểm tra email và xác nhận để hoàn thành' => 'Add new email successful. Please check mail to verify account',
    'Gửi yêu cầu xác nhận email thành công. Hãy kiểm tra email và xác nhận để hoàn thành' => 'Send request successful. Please check mail and verify it',
    'Đăng ký và đăng nhập nhanh thành công. Hãy kiểm tra email và xác nhận để hoàn thành' => 'Register and Login successful. Please check mail to verify account',
    'Thêm tài khoản {social}: {email} thành công' => 'Add {social} account: {email} successful',
    'Tài khoản {social}: {email} đã được thêm rồi' => '{social} account {email} was added',
    'Tài khoản {social}: {email} đã được sử dụng bởi tài khoản khác' => '{social} account: {email} was added by another account',

    'Quản lý đăng nhập' => 'Manage login',
    'Tham gia từ' => 'Member since',
    'Cập nhật tài khoản' => 'Update profile',
    'Nam' => 'Male',
    'Nữ' => 'Female',
    '' => '',
];