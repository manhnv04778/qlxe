<?php
/**
 * Created by PhpStorm.
 * User: ABC
 * Date: 10/17/14
 * Time: 17:46
 */

namespace app\helpers;

use \DateTime;
use Yii;

class DateTimeHelper {

	/**
	 * Parse time to relative
	 * @param string|numeric $inputTime
	 * @param bool $type long or short
     * @param string $lang vi|en. default lang by Yii::$app->language
     * @return bool|string
	 */
	public static function relative($inputTime, $type = 'long', $lang = null, $debug = false)
	{
        $lang = in_array($lang, ['en', 'vi']) ? $lang : substr(Yii::$app->language, 0, 2);

		if(!is_numeric($inputTime)) $inputTime = strtotime($inputTime);

		$langConfig = array(
			'vi' =>[
                'format' => 'd/m/Y',
                'at' => 'lúc',
                'moment' => 'vừa mới đây',
                'minute_ago' => 'phút trước',
                'hour' => 'giờ',
                'today' => 'hôm nay',
                'yesterday' => 'hôm qua',
                'before_yesterday' => 'hôm kia'
            ],
			'en' =>[
                'format' => 'm/d/Y',
                'at' => 'at',
                'moment' => 'just a moment',
                'minute_ago' => 'minutes ago',
                'hour' => 'hours',
                'today' => 'today',
                'yesterday' => 'yesterday'
            ]
		);

		$dateInput = date('Y-m-d', $inputTime);

		$currentDatetime = self::getDateTime();

		$currentTime = strtotime($currentDatetime);

		$seconds = $currentTime - $inputTime; // Calculates time difference in seconds.
		if($seconds < 180) {
			$s = $langConfig[$lang]['moment'];
		} elseif($seconds > 180 && $seconds < 3600) {
			$minutes = floor($seconds / 60);
			$s = $minutes . ' ' . $langConfig[$lang]['minute_ago'];
		} elseif($seconds > 3600 && $seconds < 3600 * 5) {
			$minutes = floor($seconds / 60);
			$hours = floor($minutes / 60);
			$minutes_diff = $minutes - ($hours * 60);
			$s = $hours . ' ' . $langConfig[$lang]['hour'] . ' ' . $minutes_diff . ' ' . $langConfig[$lang]['minute_ago'];
		} else {
			$today = date('Y-m-d', $currentTime);
			$yesterday = date('Y-m-d', strtotime('yesterday'));
			$before_yesterday = date('Y-m-d', strtotime('yesterday - 1 day'));

			$timestamp_before_week = strtotime('today - 1 week');

			$s = date($langConfig[$lang]['format'], $inputTime);
			if($dateInput == $today) {
				$s = $langConfig[$lang]['today'];
			} elseif($dateInput == $yesterday) {
				$s = $langConfig[$lang]['yesterday'];
			} elseif($dateInput == $before_yesterday && !empty($langConfig[$lang]['before_yesterday'])) {
				$s = $langConfig[$lang]['before_yesterday'];
			}

			if($type == 'long' && $inputTime > $timestamp_before_week) {
				$hour = date('H:i', $inputTime);
				$hour = $hour == '00:00' ? '' : "{$langConfig[$lang]['at']} {$hour}";
				$s = "{$s} {$hour}";

			}
		}



		return $s;
	}

	/**
	 * @param strimg|numeric $date
	 * @param bool $dateOnly
	 * @param string $lang bool|null|string vi|en|schema:2011-05-08T19:30
	 * @return
	 */
	public static function formatDateTime($dateTime = null, $dateOnly = false, $lang = null)
	{
		$dateTime = $dateTime ? $dateTime : time();

		$lang = in_array($lang, ['en', 'vi']) ? $lang : substr(Yii::$app->language, 0, 2);

		if($dateOnly) {
			$format = $lang == 'schema' ? 'Y-m-d' : ($lang == 'vi' ? 'd/m/Y' : 'm/d/Y');
		} else {
			$format = $lang == 'schema' ? 'c' : ($lang == 'vi' ? 'd/m/Y H:i' : 'm/d/Y H:i');
		}
		return date($format, is_numeric($dateTime) ? $dateTime : strtotime($dateTime));
	}

    /**
     * format date time by language
     * @param $datetime mysql-datetime or unix-time
     * @return string
     */
    public static function formatDatetimeByLang($datetime, $lang = null)
    {
        $lang = in_array($lang, ['en', 'vi']) ? $lang : substr(Yii::$app->language, 0, 2);

        $datetime = is_numeric($datetime) ? $datetime : strtotime($datetime);


        return self::getDateTime($datetime, $lang == 'en' ? 'M d, Y' : 'd-m-Y');
    }

	/**
	 * get DateTime
	 * @link http://php.net/manual/en/datetime.construct.php
	 * 		 http://php.net/manual/en/datetime.formats.php
	 * @param string|numeric $time
	 * @param bool|string $format
	 * @return DateTime
	 */
		public static function getDateTime($time = 'now', $format = 'Y-m-d H:i:s')
	{
        if(!$time) return null;

        if($time != 'now'){
            $time = is_numeric($time) ? $time : strtotime($time);
            $time = date('Y-m-d H:i:s', $time);
        }

//		$dateTime = new DateTime($time);
		$dateTime = new DateTime($time, new \DateTimeZone('Asia/Ho_Chi_Minh'));
//		$dateTime = new DateTime("now", new DateTimeZone('America/Los_Angeles'));
		if($time == 'now')  $dateTime->modify('-13 minute');


		if(!$format) return $dateTime;

		return $dateTime->format($format);
	}


    public static function modifyDateTime($time = 'now', $modify = '+0 hour', $format = 'Y-m-d H:i:s'){
        $dateTime = self::getDateTime($time, false);
        $dateTime->modify($modify);

        if(!$format) return $dateTime;

        return $dateTime->format($format);
    }

} 