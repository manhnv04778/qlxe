<?php
/**
 * Created by PhpStorm.
 * User: ABC
 * Date: 10/17/14
 * Time: 17:46
 */

namespace app\helpers;

use Curl\Curl;
use \DateTime;
use Yii;

class SmsHelper {

	public static function sendSMS($content, $phone, $guid = '', $sBranchName = '')
	{
		if (empty ($guid)) {
			$guid = uniqid();
		}
		if (empty ($phone)) {
			return 0;
		}

		//$api = "http://center.fibosms.com/Service.asmx/SendSMS?clientNo=Cl8896&clientPass=7A5!4Cghv&smsGUID={GUID}&serviceType=6105&phoneNumber={PHONE}&smsMessage={CONTENT}";

		$api = 'http://103.48.194.60/SendMT/Service.asmx/SendMaskedSMS?clientNo=CL1610070003 &clientPass=1qaz2wsx&senderName=LONGCODEOTP&phoneNumber={PHONE}&smsMessage={CONTENT}&smsGUID=0&serviceType=1';

		$api = str_replace('{GUID}', $guid, $api);
		$content = urlencode($content);
		$api = str_replace('{CONTENT}', $content, $api);
		$api = str_replace('{PHONE}', $phone, $api);

		$return = self::open_https_url($api);

		$return = htmlspecialchars_decode($return);
		$start = strpos($return, '<Code>') + strlen('<Code>');
		$stop = strpos($return, '</Code>');
		$code = substr($return, $start, $stop - $start);
		return $code;
	}

	public static function open_https_url($url, $refer = "", $usecookie = false)
	{
		if ($usecookie) {
			if (file_exists($usecookie)) {
				if (!is_writable($usecookie)) {
					return "Can't write to $usecookie cookie file, change file permission to 777 or remove read only for windows.";
				}
			} else {
				$usecookie = "cookie.txt";
				if (!is_writable($usecookie)) {
					return "Can't write to $usecookie cookie file, change file permission to 777 or remove read only for windows.";
				}
			}
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

		curl_setopt($ch, CURLOPT_HEADER, 1);

		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");

		if ($usecookie) {
			curl_setopt($ch, CURLOPT_COOKIEJAR, $usecookie);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $usecookie);
		}

		if ($refer != "") {
			curl_setopt($ch, CURLOPT_REFERER, $refer);
		}

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		$iHeaderSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$result = substr($result, $iHeaderSize);

		curl_close($ch);

		return $result;
	}

} 