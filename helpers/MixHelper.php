<?php
/**
 * Created by PhpStorm.
 * User: ABCnet
 * Date: 10/27/14
 * Time: 12:57 AM
 */

namespace app\helpers;

use Yii;

class MixHelper
{

	public static function cookieGet($key, $value = null)
	{
		$cookies = Yii::$app->request->cookies;
		return $cookies->getValue($key);
	}

	public static function cookieSet($key, $value = null, $expire = null, $httpOnly = true)
	{
		$cookies = Yii::$app->response->cookies;

        $expire = $expire ? $expire : 3600*24*365;
        $expire += time();

		if (is_null($value)) $cookies->remove($key);

        $cookie = new \yii\web\Cookie([
            'name' => $key,
            'value' => $value,
            'expire' => $expire,
            'httpOnly' => $httpOnly,
        ]);

		$cookies->add($cookie);
	}

	/**
	 * Lấy chiều dài và rộng của external image url
	 * @param $img_url
	 * @param int $rang bytes
	 * @param bool $show_time xem tổng thời gian
	 * @return null
	 */
	public static function getWidthHeightImage($img_url, $rang = 666, $show_time = FALSE)
	{
		if ($show_time) $start = microtime(true);

		$curl = curl_init($img_url);
		if ($rang) curl_setopt($curl, CURLOPT_HTTPHEADER, array("Range: bytes=0-{$rang}"));

		curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$img = curl_exec($curl);
		curl_close($curl);
		$im = @imagecreatefromstring($img);
		if ($show_time) {
			$stop = round(microtime(true) - $start, 5);
			$data['time'] = $stop;
		}
		$data['width'] = @imagesx($im);
		$data['height'] = @imagesy($im);
		return $data['width'] ? $data : NULL;
	}



	/**
	 * Tính khoảng cách từ 2 toạ độ
	 * @param $latlng1
	 * @param $latlng2
	 * @param string $unit K: km, M: met
	 * @return float
     *
     *  $a = MixHelper::getDistanceFromLatLng('20.997684, 105.802034', '20.998586, 105.810370', "k");
     * $b = MixHelper::getDistanceFromLatLng('20.997684, 105.802034', '20.998586, 105.810370', "m");
	 */
	public static function getDistanceFromLatLng($latlng1, $latlng2, $unit = 'K')
	{
		list($lat1, $lng1) = explode(',', $latlng1);
		list($lat2, $lng2) = explode(',', $latlng2);

		$theta = $lng1 - $lng2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

        $m = $miles * 1609.344;
        $km = $m/1000;

        $m = sprintf('%d', $m);
        $km = sprintf('%.3f', $km);

		if($unit == "K") {
			return $km;
		}else {
			return $m;
		}
	}


	public static function detectBot($ua = null)
	{
		$ua = $ua ? $ua : $_SERVER['HTTP_USER_AGENT'];

		$bots = array(
			"Google" => "Googlebot",
			"Yahoo" => "Slurp",
			"Bing" => "bingbot"
		);
		if (!preg_match('/' . implode("|", $bots) . '/i', $ua, $m)) return false;
		return array_search($m[0], $bots);
	}

} 