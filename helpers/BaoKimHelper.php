<?php
/**
 * Created by PhpStorm.
 * User: van
 * Date: 3/3/2016
 * Time: 2:25 PM
 */

namespace app\helpers;


use Yii;
use Curl\Curl;

/*
 * @var $this app\helper\SmsHelper;
 */

class BaoKimHelper {


    public static function getConfig()
    {
        return Yii::$app->params['baokim'];
    }


    public static function makeSignature($method, $url, $getArgs=array(), $postArgs=array(), $priKeyFile){
        if(strpos($url,'?') !== false)
        {
            list($url,$get) = explode('?', $url);
            parse_str($get, $get);
            $getArgs=array_merge($get, $getArgs);
        }

        ksort($getArgs);
        ksort($postArgs);
        $method=strtoupper($method);

        $data = $method.'&'.urlencode($url).'&'.urlencode(http_build_query($getArgs)).'&'.urlencode(http_build_query($postArgs));

        $priKey = openssl_get_privatekey($priKeyFile);

        openssl_sign($data, $signature, $priKey, OPENSSL_ALGO_SHA1);

        return urlencode(base64_encode($signature));
    }

    public static function sendSMS($phone_no, $content, &$msg = ''){

        $config = self::getConfig();
        $httpUser = $config['user'];
        $httpPsw = $config['pasw'];


        $url = '/services/smsmt_api/send';

        $arrayPost = array(
            'phone'=>$phone_no,
            'content'=>$content,
            'branch_name'=>'123doc.vn'
        );

        //ksort($arrayPost);
        //	Khi có Signature thì thêm vào $url
        $signature = self::makeSignature('POST',$url, array(), $arrayPost,$config['privateKey']);

//        echo "<pre>";print_r($signature);echo "</pre>"; die;

        $url_signature = 'http://sms.x.baokim.vn'.$url.'?signature='.$signature;
        // Sandbox no signature
        $curl = curl_init($url_signature);


        $curl = new Curl();
        $curl->setOpt(CURLOPT_HEADER, false);
        $curl->setOpt(CURLOPT_POST, true);
        $curl->setOpt(CURLINFO_HEADER_OUT, true);
        $curl->setOpt(CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        $curl->setOpt(CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        $curl->setOpt(CURLOPT_USERPWD, $httpUser.':'.$httpPsw);
        $curl->setOpt(CURLOPT_TIMEOUT, 30);
        $curl->setOpt(CURLOPT_RETURNTRANSFER, true);

        $curl->post($url_signature, $arrayPost);

        $status = $curl->httpStatusCode;

        if($status == 200){
            $msg = 'Gửi thành công';
            return true;
        } elseif($status == 403){
            $msg = 'api account is not loaded';
            return true;
        }

        return false;



//        curl_setopt_array($curl, array(
//            CURLOPT_HEADER=>false,
//            CURLOPT_POST=>true,
//            CURLINFO_HEADER_OUT=>true,
//            CURLOPT_HTTP_VERSION=>CURL_HTTP_VERSION_1_0,
//            CURLOPT_HTTPAUTH=>CURLAUTH_DIGEST,
//            CURLOPT_USERPWD=>$httpUser.':'.$httpPsw,
//            CURLOPT_TIMEOUT=>30,
//            CURLOPT_RETURNTRANSFER=>true,
//            CURLOPT_POSTFIELDS=>$arrayPost,
//        ));
//
//        // Lấy Data
//        $data = curl_exec($curl);
//        print_r($data);
//        $httpStatus = curl_getinfo($curl);
//        echo '<pre>',print_r($httpStatus),'</pre>';
//        echo '<pre>',print_r($data, false),'</pre>';
//
//        die;


    }

}