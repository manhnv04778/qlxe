<?php
namespace yii\helpers;

/**
 * StringHelper
 */
class FileHelper extends BaseFileHelper
{

	/**
	 * @param $dir
	 * @param bool $removeDir Có xoá $dir hay không.
	 */
	public static function deleteDirectory($dir, $removeDir = false)
	{
		$dir = trim($dir, '/');
		if(file_exists($dir) && is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if($object != "." && $object != "..") {
					if(filetype($dir . "/" . $object) == "dir") {
						self::deleteDirectory($dir . "/" . $object);
					} else {
						self::deleteFile($dir . "/" . $object);
					}
				}
			}
			reset($objects);

			if($removeDir) {
				rmdir($dir);
			}
		}
	}

	/**
	 * Xoá file
	 * @param $file_path
	 */
	public static function deleteFile($file_path)
	{
		if (file_exists($file_path) && !is_dir($file_path)) {
			unlink($file_path);
		}
	}


	//$file = "/folder/filename.ext";D
	/**
	 * Download file
	 * @param $file
	 */
	public static function forceDownload($file, $name = null)
	{
		$name = $name ? $name : basename($file);

		$ext = explode(".", $file);
		switch ($ext[sizeof($ext) - 1]) {
			case 'jar':
				$mime = "application/java-archive";
				break;
			case 'zip':
				$mime = "application/zip";
				break;
			case 'jpeg':
				$mime = "image/jpeg";
				break;
			case 'jpg':
				$mime = "image/jpg";
				break;
			case 'jad':
				$mime = "text/vnd.sun.j2me.app-descriptor";
				break;
			case "gif":
				$mime = "image/gif";
				break;
			case "png":
				$mime = "image/png";
				break;
			case "pdf":
				$mime = "application/pdf";
				break;
			case "txt":
				$mime = "text/plain";
				break;
			case "doc":
				$mime = "application/msword";
				break;
			case "ppt":
				$mime = "application/vnd.ms-powerpoint";
				break;
			case "wbmp":
				$mime = "image/vnd.wap.wbmp";
				break;
			case "wmlc":
				$mime = "application/vnd.wap.wmlc";
				break;
			case "mp4s":
				$mime = "application/mp4";
				break;
			case "ogg":
				$mime = "application/ogg";
				break;
			case "pls":
				$mime = "application/pls+xml";
				break;
			case "asf":
				$mime = "application/vnd.ms-asf";
				break;
			case "swf":
				$mime = "application/x-shockwave-flash";
				break;
			case "mp4":
				$mime = "video/mp4";
				break;
			case "m4a":
				$mime = "audio/mp4";
				break;
			case "m4p":
				$mime = "audio/mp4";
				break;
			case "mp4a":
				$mime = "audio/mp4";
				break;
			case "mp3":
				$mime = "audio/mpeg";
				break;
			case "m3a":
				$mime = "audio/mpeg";
				break;
			case "m2a":
				$mime = "audio/mpeg";
				break;
			case "mp2a":
				$mime = "audio/mpeg";
				break;
			case "mp2":
				$mime = "audio/mpeg";
				break;
			case "mpga":
				$mime = "audio/mpeg";
				break;
			case "wav":
				$mime = "audio/wav";
				break;
			case "m3u":
				$mime = "audio/x-mpegurl";
				break;
			case "bmp":
				$mime = "image/bmp";
				break;
			case "ico":
				$mime = "image/x-icon";
				break;
			case "3gp":
				$mime = "video/3gpp";
				break;
			case "3g2":
				$mime = "video/3gpp2";
				break;
			case "mp4v":
				$mime = "video/mp4";
				break;
			case "mpg4":
				$mime = "video/mp4";
				break;
			case "m2v":
				$mime = "video/mpeg";
				break;
			case "m1v":
				$mime = "video/mpeg";
				break;
			case "mpe":
				$mime = "video/mpeg";
				break;
			case "mpeg":
				$mime = "video/mpeg";
				break;
			case "mpg":
				$mime = "video/mpeg";
				break;
			case "mov":
				$mime = "video/quicktime";
				break;
			case "qt":
				$mime = "video/quicktime";
				break;
			case "avi":
				$mime = "video/x-msvideo";
				break;
			case "midi":
				$mime = "audio/midi";
				break;
			case "mid":
				$mime = "audio/mid";
				break;
			case "amr":
				$mime = "audio/amr";
				break;
			default:
				$mime = "application/force-download";
		}

		header('Content-Description: File Transfer');
		header('Content-Type: ' . $mime);
		header('Content-Disposition: attachment; filename=' . $name);
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		ob_clean();
		flush();
		readfile($file);
	}


	public static function getFileType($filePath)
	{
		$filename = realpath($filePath);
		$extension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

		if (class_exists('finfo', false)) {
			if ($info = new \finfo(defined('FILEINFO_MIME_TYPE') ? FILEINFO_MIME_TYPE : FILEINFO_MIME)) {
				return $info->file($filename);
			}
		}
		else if (ini_get('mime_magic.magicfile') && function_exists('mime_content_type')) {
			return mime_content_type($filename);
		}
		elseif (preg_match('/^(?:jpe?g|png|[gt]if|bmp|swf)$/', $extension)) {
			$file = getimagesize($filename);

			if (isset($file['mime'])) {
				return $file['mime'];
			}
		}


		return false;
	}


	public static function getFileData($filePath)
	{
		$data = '';
		if (file_exists($filePath)) {
			ob_start();
			readfile($filePath);
			$data = ob_get_clean();
		}

		return $data;
	}



	public static function getExternalFileInfo($fileUrl){

		$ch = curl_init();
//		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept:image/jpeg']);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_NOBODY, true); // make it a HEAD request
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36');
		curl_setopt($ch, CURLOPT_URL, $fileUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$head = curl_exec($ch);


		$mimeType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
		$size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
		$info = pathinfo($fileUrl);

		curl_close($ch);

		$data = [
//			'head' => $head,
			'mimeType' => $mimeType,
			'size' => $size,
			'fileName' => !empty($info['filename']) ? $info['filename'] : null,
			'extension' => !empty($info['extension']) ? strtolower($info['extension']) : null,
		];

		if(empty($data['extension'])){
			$data['extension'] = self::getExtensionFromMimeType($data['mimeType']);
		}

		return $data;

	}

	public static function getInternalFileInfo($file){

		$info = pathinfo($file['name']);
		$data = [
			'mimeType' => $file['type'],
			'size' => $file['size'],
			'fileName' => $info['filename'],
			'extension' => isset($info['extension']) ? strtolower($info['extension']) : null,
		];

		if(empty($data['extension'])){
			$data['extension'] = self::getExtensionFromMimeType($data['mimeType']);
		}

		return $data;

	}

	/**
	 * @param $data binary or base64
	 */
	public static function getDataFileInfo($data){

		// base64
		if(ctype_print($data)){
			preg_match('`^data:([^;]+);base64,(.+)`', $data, $m);
			list(, $mimeType, $data) = $m;
			$data = base64_decode($data);
		}
		// binary
		else{
			$finfo = new \finfo(FILEINFO_MIME_TYPE);
			$mimeType = $finfo->buffer($data);
		}

		$data = [
			'mimeType' => $mimeType,
			'size' => strlen($data),
			'fileName' => null,
			'extension' => null,
			'data' => $data,
		];

		if(empty($data['extension'])){
			$data['extension'] = self::getExtensionFromMimeType($data['mimeType']);
		}

		return $data;
	}

	/**
	 * @param $mimeType
	 * @return int|null|string
	 */
	public static function getExtensionFromMimeType($mimeType){
		$data = array(
			'hqx'	=>	array('application/mac-binhex40', 'application/mac-binhex', 'application/x-binhex40', 'application/x-mac-binhex40'),
			'cpt'	=>	'application/mac-compactpro',
			'csv'	=>	array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain'),
			'bin'	=>	array('application/macbinary', 'application/mac-binary', 'application/octet-stream', 'application/x-binary', 'application/x-macbinary'),
			'dms'	=>	'application/octet-stream',
			'lha'	=>	'application/octet-stream',
			'lzh'	=>	'application/octet-stream',
			'exe'	=>	array('application/octet-stream', 'application/x-msdownload'),
			'class'	=>	'application/octet-stream',
			'psd'	=>	array('application/x-photoshop', 'image/vnd.adobe.photoshop'),
			'so'	=>	'application/octet-stream',
			'sea'	=>	'application/octet-stream',
			'dll'	=>	'application/octet-stream',
			'oda'	=>	'application/oda',
			'pdf'	=>	array('application/pdf', 'application/force-download', 'application/x-download', 'binary/octet-stream'),
			'ai'	=>	array('application/pdf', 'application/postscript'),
			'eps'	=>	'application/postscript',
			'ps'	=>	'application/postscript',
			'smi'	=>	'application/smil',
			'smil'	=>	'application/smil',
			'mif'	=>	'application/vnd.mif',
			'xls'	=>	array('application/vnd.ms-excel', 'application/msexcel', 'application/x-msexcel', 'application/x-ms-excel', 'application/x-excel', 'application/x-dos_ms_excel', 'application/xls', 'application/x-xls', 'application/excel', 'application/download', 'application/vnd.ms-office', 'application/msword'),
			'ppt'	=>	array('application/powerpoint', 'application/vnd.ms-powerpoint', 'application/vnd.ms-office', 'application/msword'),
			'pptx'	=> 	array('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/x-zip', 'application/zip'),
			'wbxml'	=>	'application/wbxml',
			'wmlc'	=>	'application/wmlc',
			'dcr'	=>	'application/x-director',
			'dir'	=>	'application/x-director',
			'dxr'	=>	'application/x-director',
			'dvi'	=>	'application/x-dvi',
			'gtar'	=>	'application/x-gtar',
			'gz'	=>	'application/x-gzip',
			'gzip'  =>	'application/x-gzip',
			'php'	=>	array('application/x-httpd-php', 'application/php', 'application/x-php', 'text/php', 'text/x-php', 'application/x-httpd-php-source'),
			'php4'	=>	'application/x-httpd-php',
			'php3'	=>	'application/x-httpd-php',
			'phtml'	=>	'application/x-httpd-php',
			'phps'	=>	'application/x-httpd-php-source',
			'js'	=>	array('application/x-javascript', 'text/plain'),
			'swf'	=>	'application/x-shockwave-flash',
			'sit'	=>	'application/x-stuffit',
			'tar'	=>	'application/x-tar',
			'tgz'	=>	array('application/x-tar', 'application/x-gzip-compressed'),
			'z'	=>	'application/x-compress',
			'xhtml'	=>	'application/xhtml+xml',
			'xht'	=>	'application/xhtml+xml',
			'zip'	=>	array('application/x-zip', 'application/zip', 'application/x-zip-compressed', 'application/s-compressed', 'multipart/x-zip'),
			'rar'	=>	array('application/x-rar', 'application/rar', 'application/x-rar-compressed'),
			'mid'	=>	'audio/midi',
			'midi'	=>	'audio/midi',
			'mpga'	=>	'audio/mpeg',
			'mp2'	=>	'audio/mpeg',
			'mp3'	=>	array('audio/mpeg', 'audio/mpg', 'audio/mpeg3', 'audio/mp3'),
			'aif'	=>	array('audio/x-aiff', 'audio/aiff'),
			'aiff'	=>	array('audio/x-aiff', 'audio/aiff'),
			'aifc'	=>	'audio/x-aiff',
			'ram'	=>	'audio/x-pn-realaudio',
			'rm'	=>	'audio/x-pn-realaudio',
			'rpm'	=>	'audio/x-pn-realaudio-plugin',
			'ra'	=>	'audio/x-realaudio',
			'rv'	=>	'video/vnd.rn-realvideo',
			'wav'	=>	array('audio/x-wav', 'audio/wave', 'audio/wav'),
			'bmp'	=>	array('image/bmp', 'image/x-bmp', 'image/x-bitmap', 'image/x-xbitmap', 'image/x-win-bitmap', 'image/x-windows-bmp', 'image/ms-bmp', 'image/x-ms-bmp', 'application/bmp', 'application/x-bmp', 'application/x-win-bitmap'),
			'gif'	=>	'image/gif',
			'jpeg'	=>	array('image/jpeg', 'image/pjpeg'),
			'jpg'	=>	array('image/jpeg', 'image/pjpeg'),
			'jpe'	=>	array('image/jpeg', 'image/pjpeg'),
			'png'	=>	array('image/png',  'image/x-png'),
			'tiff'	=>	'image/tiff',
			'tif'	=>	'image/tiff',
			'css'	=>	array('text/css', 'text/plain'),
			'html'	=>	array('text/html', 'text/plain'),
			'htm'	=>	array('text/html', 'text/plain'),
			'shtml'	=>	array('text/html', 'text/plain'),
			'txt'	=>	'text/plain',
			'text'	=>	'text/plain',
			'log'	=>	array('text/plain', 'text/x-log'),
			'rtx'	=>	'text/richtext',
			'rtf'	=>	'text/rtf',
			'xml'	=>	array('application/xml', 'text/xml', 'text/plain'),
			'xsl'	=>	array('application/xml', 'text/xsl', 'text/xml'),
			'mpeg'	=>	'video/mpeg',
			'mpg'	=>	'video/mpeg',
			'mpe'	=>	'video/mpeg',
			'qt'	=>	'video/quicktime',
			'mov'	=>	'video/quicktime',
			'avi'	=>	array('video/x-msvideo', 'video/msvideo', 'video/avi', 'application/x-troff-msvideo'),
			'movie'	=>	'video/x-sgi-movie',
			'doc'	=>	array('application/msword', 'application/vnd.ms-office'),
			'docx'	=>	array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip', 'application/msword', 'application/x-zip'),
			'dot'	=>	array('application/msword', 'application/vnd.ms-office'),
			'dotx'	=>	array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip', 'application/msword'),
			'xlsx'	=>	array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/zip', 'application/vnd.ms-excel', 'application/msword', 'application/x-zip'),
			'word'	=>	array('application/msword', 'application/octet-stream'),
			'xl'	=>	'application/excel',
			'eml'	=>	'message/rfc822',
			'json'  =>	array('application/json', 'text/json'),
			'pem'   =>	array('application/x-x509-user-cert', 'application/x-pem-file', 'application/octet-stream'),
			'p10'   =>	array('application/x-pkcs10', 'application/pkcs10'),
			'p12'   =>	'application/x-pkcs12',
			'p7a'   =>	'application/x-pkcs7-signature',
			'p7c'   =>	array('application/pkcs7-mime', 'application/x-pkcs7-mime'),
			'p7m'   =>	array('application/pkcs7-mime', 'application/x-pkcs7-mime'),
			'p7r'   =>	'application/x-pkcs7-certreqresp',
			'p7s'   =>	'application/pkcs7-signature',
			'crt'   =>	array('application/x-x509-ca-cert', 'application/x-x509-user-cert', 'application/pkix-cert'),
			'crl'   =>	array('application/pkix-crl', 'application/pkcs-crl'),
			'der'   =>	'application/x-x509-ca-cert',
			'kdb'   =>	'application/octet-stream',
			'pgp'   =>	'application/pgp',
			'gpg'   =>	'application/gpg-keys',
			'sst'   =>	'application/octet-stream',
			'csr'   =>	'application/octet-stream',
			'rsa'   =>	'application/x-pkcs7',
			'cer'   =>	array('application/pkix-cert', 'application/x-x509-ca-cert'),
			'3g2'   =>	'video/3gpp2',
			'3gp'   =>	array('video/3gp', 'video/3gpp'),
			'mp4'   =>	'video/mp4',
			'm4a'   =>	'audio/x-m4a',
			'f4v'   =>	'video/mp4',
			'webm'	=>	'video/webm',
			'aac'   =>	'audio/x-acc',
			'm4u'   =>	'application/vnd.mpegurl',
			'm3u'   =>	'text/plain',
			'xspf'  =>	'application/xspf+xml',
			'vlc'   =>	'application/videolan',
			'wmv'   =>	array('video/x-ms-wmv', 'video/x-ms-asf'),
			'au'    =>	'audio/x-au',
			'ac3'   =>	'audio/ac3',
			'flac'  =>	'audio/x-flac',
			'ogg'   =>	'audio/ogg',
			'kmz'	=>	array('application/vnd.google-earth.kmz', 'application/zip', 'application/x-zip'),
			'kml'	=>	array('application/vnd.google-earth.kml+xml', 'application/xml', 'text/xml'),
			'ics'	=>	'text/calendar',
			'ical'	=>	'text/calendar',
			'zsh'	=>	'text/x-scriptzsh',
			'7zip'	=>	array('application/x-compressed', 'application/x-zip-compressed', 'application/zip', 'multipart/x-zip'),
			'cdr'	=>	array('application/cdr', 'application/coreldraw', 'application/x-cdr', 'application/x-coreldraw', 'image/cdr', 'image/x-cdr', 'zz-application/zz-winassoc-cdr'),
			'wma'	=>	array('audio/x-ms-wma', 'video/x-ms-asf'),
			'jar'	=>	array('application/java-archive', 'application/x-java-application', 'application/x-jar', 'application/x-compressed'),
			'svg'	=>	array('image/svg+xml', 'application/xml', 'text/xml'),
			'vcf'	=>	'text/x-vcard',
			'srt'	=>	array('text/srt', 'text/plain'),
			'vtt'	=>	array('text/vtt', 'text/plain'),
			'ico'	=>	array('image/x-icon', 'image/x-ico', 'image/vnd.microsoft.icon')
		);

		foreach($data as $key => $value){
			$value = !is_array($value) ? [$value] : $value;

			if(in_array($mimeType, $value)){
				return $key;
			}
		}

		return null;
	}
}
