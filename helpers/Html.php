<?php
namespace yii\helpers;

use app\libraries\simple_html_dom;
use app\libraries\Curl;
use tidy;

/**
 * StringHelper
 */
class Html extends BaseHtml
{
    public static function decode($content){
        $content = parent::decode($content);
        $content = html_entity_decode($content);
        $content = trim($content);
        return $content;
    }

    public static function removeEmptyTags($s)
    {
        return preg_replace('/<([^<\/>]*)>([\s]*?|(?R)|&nbsp;)<\/\1>/imsU', '', $s);
    }

    public static function removeCommentTags($s)
    {
        return preg_replace('/<!\-\-.*\-\->/', '', $s);
    }


    public static function getHtmlObj($s, $useCUrl = true, $loopFix = 0)
    {
        $urlValidator = new \yii\validators\UrlValidator();
        if ($useCUrl && $urlValidator->validateValue($s)) {
            $loopCount = 0;

            loopFix:
            $url = $s;
            $curl = new Curl($url);
            $curl->gzdecode = 1;
            $data = $curl->run();
            $loopCount++;
            $loopFix = $loopFix ? (intval($loopFix) ? $loopFix : 5) : 0;

            if (!$data && $loopFix && $url && $loopCount <= 5) goto loopFix;

            $s = $data;
        }
        $html = new simple_html_dom($s);
        return $html->root ? $html : false;
    }

    public static function stripAttributes($html, $blackAttrs = array(), $whiteAttrs = array())
    {
        if (!$blackAttrs && !$whiteAttrs) return $html;

        $allAttrs = array('accept', 'accept-charset', 'accesskey', 'action', 'align', 'alt', 'async', 'autocomplete', 'autofocus', 'autoplay', 'autosave', 'bgcolor', 'buffered', 'challenge', 'charset', 'checked', 'cite', 'class', 'code', 'codebase', 'color', 'cols', 'colspan', 'content', 'contenteditable', 'contextmenu', 'controls', 'coords', 'data', 'data-*', 'datetime', 'default', 'defer', 'dir', 'dirname', 'disabled', 'download', 'draggable', 'dropzone', 'enctype', 'for', 'form', 'formaction', 'headers', 'height', 'hidden', 'high', 'href', 'hreflang', 'http-equiv', 'icon', 'id', 'ismap', 'itemprop', 'keytype', 'kind', 'label', 'lang', 'language', 'list', 'loop', 'low', 'manifest', 'max', 'maxlength', 'media', 'method', 'min', 'multiple', 'name', 'novalidate', 'open', 'optimum', 'pattern', 'ping', 'placeholder', 'poster', 'preload', 'pubdate', 'radiogroup', 'readonly', 'rel', 'required', 'reversed', 'rows', 'rowspan', 'sandbox', 'spellcheck', 'scope', 'scoped', 'seamless', 'selected', 'shape', 'size', 'sizes', 'span', 'src', 'srcdoc', 'srclang', 'start', 'step', 'style', 'summary', 'tabindex', 'target', 'title', 'type', 'usemap', 'value', 'width', 'wrap',);

        $removeAttrs = $blackAttrs ? $blackAttrs : array_diff($allAttrs, $whiteAttrs);

        $dom = new simple_html_dom();
        $dom->load($html);

        foreach ($removeAttrs as $attr) {
            foreach ($dom->find("*[$attr]") as $e) {
                $e->$attr = null;
            }
        }

        $dom->load($dom->save());
        return $dom->save();
    }


    /**
     * Xóa rác HTML khi paste vào từ MsOffice
     *
     * @param string $value
     * @return string
     */
    public static function removeMsOfficeHtml($value)
    {
        $value = str_replace('<o:p>&nbsp;</o:p>', '', $value);
        $value = str_replace('<o:p></o:p>', '', $value);
        $value = str_replace('style="margin: 0cm 0cm 0pt;"', "", $value);
        $value = str_replace('>&nbsp;<', "><", $value);
        return $value;
    }

    /**
     * Tidy HTML để đảm bảo đoạn HTML không bị vỡ
     *
     * @param string $value
     * @return string
     */
    public static function tidyHtml($value)
    {
        $value = self::removeMsOfficeHtml($value);
        $options = array("output-html" => true, "show-body-only" => true, "hide-comments" => true, "word-2000" => true, "output-encoding" => "utf8", "input-encoding" => "utf8",);
        $value = str_replace('&nbsp;', "(olala)", $value);
        $tidy = new \tidy();
        $tidy->parseString($value, $options);
        $tidy->cleanRepair();
        $value = $tidy;
        $value = str_replace("(olala)", "&nbsp;", $value);
        $value = str_replace("\n", " ", $value);
        return $value;
    }
}
