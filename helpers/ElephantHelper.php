<?php
/**
 * Created by PhpStorm.
 * User: ABCnet
 * Date: 10/27/14
 * Time: 12:57 AM
 */

namespace app\helpers;

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

use yii\helpers\Url;
use Yii;
use app\modules\user\models\UserFeed;
use app\modules\user\models\ConversationNotice;
use app\modules\question\models\Question;

class ElephantHelper
{
    public static $port = 8080;

    /**
     * @param UserFeed $userFeed
     * @param $receiverIds [user_id => user_feed_id]
     */
    public static function notification(UserFeed $userFeed, $receiverIds)
    {
        $data = [
            'html' => Yii::$app->view->render('//layouts/_navbar_notify_notification_row', compact('userFeed')),
            'receiverId' => $receiverIds
        ];

        $uri = Url::base(true) . ':' . self::$port;
        $client = new Client(new Version1X($uri));

        $client->initialize();
        $client->emit('notification', $data);
        $client->close();

    }


    /**
     * @param ConversationNotice $conversationNotice
     * @param $receiverIds [user_id => conversation_notice_id]
     */
    public static function message(ConversationNotice $conversationNotice, $receiverIds)
    {
        $data = [
            'html' => Yii::$app->view->render('//layouts/_navbar_notify_message_row', compact('conversationNotice')),
            'receiverIds' => $receiverIds
        ];

        $uri = Url::base(true) . ':' . self::$port;
        $client = new Client(new Version1X($uri));

        $client->initialize();
        $client->emit('message', $data);
        $client->close();
    }

    /**
     * Khi 1 question mới được tạo thì bắn sang nodejs
     * @param $question Question
     */
    public static function newQuestion($question)
    {

        $data = [
            'question' => [
                'id' => $question->id,
                'user_id' => $question->user_id,
                'title' => $question->title,
                'price' => $question->price,
                'created' => $question->created,
            ],
            'html' => Yii::$app->view->render('//modules/expert/question/_bid_content', compact('question')),
        ];

        $uri = Url::base(true) . ':' . self::$port;
        $client = new Client(new Version1X($uri));

        $client->initialize();
        $client->emit('newQuestion', $data);
        $client->close();
    }


}