<?php
/**
 * Created by PhpStorm.
 * User: ABCnet
 * Date: 10/27/14
 * Time: 12:57 AM
 */

namespace app\helpers;


/**

BaseOAuth line 505
// haidm: refresh token
try{
return $this->apiInternal($accessToken, $url, $method, $params, $headers);
}catch (Exception $e){
$userFacebook = UserFacebook::find()->where(['fb_token' => $accessToken]);
$userFacebook->fb_token = $accessToken = FacebookHelper::refreshToken($userFacebook->fb_cookie);
$userFacebook->update();

return $this->apiInternal($accessToken, $url, $method, $params, $headers);
}
 */
use app\components\FacebookAuth;
use app\helpers\DateTimeHelper;
use app\libraries\simple_html_dom;
use app\models\Setting;
use app\models\Vars;
use app\modules\facebook\models\UserFacebook;
use app\modules\facebook\models\UserFacebookPage;
use Curl\Curl;
use Yii;
use yii\authclient\clients\Facebook;
use yii\authclient\OAuthToken;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;

class FacebookHelper
{


	public static function getAvatar($fb_id, $w = 50, $h = 50){
		return "http://graph.facebook.com/{$fb_id}/picture?width={$w}&height={$h}";
	}

	public static function getFbUrl($fb_id){
		return "https://facebook.com/{$fb_id}";
	}

	public static function getFbGroupUrl($fb_id){
		return "https://facebook.com/groups/{$fb_id}";

	}

	public static function getGenderIcon($gender = 'male'){
		return $gender == 'male' ? '♂' : '♀';
	}


	public static function buildUrl($url, $data = array()) {
		return $url . (empty($data) ? '' : '?' . http_build_query($data));
	}

	public static function getScopes(){
		$scopes = [
			'public_profile',
			'user_friends',
			'email',
			'user_about_me',
			'user_actions.books',
			'user_actions.fitness',
			'user_actions.music',
			'user_actions.news',
			'user_actions.video',
			'user_birthday',
			'user_education_history',
			'user_events',
			'user_games_activity',
			'user_groups',
			'user_hometown',
			'user_likes',
			'user_location',
			'user_managed_groups',
			'user_photos',
			'user_posts',
			'user_relationships',
			'user_relationship_details',
			'user_religion_politics',
			'user_status',
			'user_tagged_places',
			'user_videos',
			'user_website',
			'user_work_history',
			'read_custom_friendlists',
			'read_insights',
			'read_audience_network_insights',
			'read_mailbox',
			'read_page_mailboxes',
			'read_stream',
			'manage_notifications',
			'manage_pages',
			'publish_pages',
			'publish_actions',
			'rsvp_event',
			'ads_read',
			'ads_management',
		];
		return $scopes;
	}

	/**
	 * @param null $app_id
	 * @param string $sub www|m
	 * @param string $v
	 * @return string
	 * https://developers.facebook.com/docs/apps/changelog
	 * https://developers.facebook.com/docs/facebook-login/permissions#reference
	 */
	public static function getFacebookLoginUrl($sub = 'www', $v = '', $partnerApp = 'htc', $displayPopup = true){

		$app_id = Yii::$app->params['facebook_extension']['partner_app_ids'][$partnerApp];
		$v = $v ? '/v'.$v : '';
		$authUrl = "https://{$sub}.facebook.com{$v}/dialog/oauth";

		$params = [
			'redirect_uri' => 'https://m.facebook.com/connect/login_success.html?site=fber',
			// scope v2.4
			'scope' => implode(',', self::getScopes()),
			// v1.0
//            'scope' => 'user_about_me,user_groups,photo_upload,publish_stream,status_update,user_events,user_groups,user_likes,user_subscriptions,user_photos,friends_events,friends_groups,friends_subscriptions,friends_photos,user_notes,friends_notes,friends_likes,read_mailbox,manage_pages,read_page_mailboxes,friends_status,read_stream',
			'response_type' => 'token',
			'client_id' => $app_id,
		];
		if($displayPopup){
			$params['display'] = 'popup';
		}
		// add scope prev version
//		$params['scope'] .= ',photo_upload,publish_stream,status_update,user_subscriptions,user_notes,friends_events,friends_groups,friends_subscriptions,friends_photos,friends_notes,friends_likes,friends_status';
		$url = self::buildUrl($authUrl, $params);
//        echo "<pre>"; print_r($url); echo "</pre>\n\n"; die;
		return $url;
	}


	/**
	 * get facebook client by token
	 * @param $token
	 * @param string $partnerApp
	 * @return Facebook
	 */
	public static function getFacebookClientByAccessToken($token, $partnerApp = 'htc')
	{
//		$app_id = Yii::$app->params['facebook_extension']['partner_app_ids'][$partnerApp];

		// find friends
		$facebook = new FacebookAuth();
//		$facebook->clientId = $app_id;

		$oAuthToken = new OAuthToken();
		$oAuthToken->setToken($token);

		$facebook->setAccessToken($oAuthToken);

		return $facebook;
	}


	/**
	 * get curl
	 * @param $cookieJson $userFacebook->fb_cookie
	 * @return Curl
	 */
	public static function getCurlByCookie($cookieJson, $userAgent = 'mobile'){

		$userAgents = [
			'iphone' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3',
			'mobile' => 'Mozilla/4.0 (compatible; MSIE 7.0; Windows Phone OS 7.0; Trident/3.1; IEMobile/7.0; LG; GW910)',
			'web' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36',
		];

		$curl = new Curl();
		$curl->setUserAgent($userAgents[$userAgent]);
		$headers = [
			'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
			'accept-language' => 'en-US,en;q=0.8,vi;q=0.6',
			'cache-control' => 'max-age=0',
			'upgrade-insecure-requests' => '1',
		];
		foreach($headers as $k => $v){
			$curl->setHeader($k, $v);

		}
		// set cookies
		$cookies = json_decode($cookieJson, true);
		if($cookies){
			foreach($cookies as $k => $v){
				$curl->setCookie($k, $v);
			}
		}


		return $curl;
	}


	public static function refreshToken($cookieJson, $partnerApp = 'htc'){
		$url = FacebookHelper::getFacebookLoginUrl('www', '', $partnerApp, false);
//		$url = FacebookHelper::getFacebookLoginUrl('m', '1.0', $partnerApp, false);

//		echo "<pre>"; print_r($url); echo "</pre>\n\n";

		$curl = FacebookHelper::getCurlByCookie($cookieJson);
		$curl->setOpt(CURLOPT_HEADER, true);
		$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
		$result = $curl->head($url);

//		echo "<pre>"; print_r($result); echo "</pre>\n\n";

		$tokenParten = '/^.+access_token=([^&]+).+$/is';
		$token = preg_replace($tokenParten, '$1', $result);

//		echo "<pre>"; print_r($token); echo "</pre>\n\n";
//		die;

		return $token;
	}


	/**
	 * Lấy thời gian cua token
	 * @param $cookieJson user cookie to login.
	 * @param $token token can lay info
	 * @return array|falsey
	 */
	public static function getTokenInfo($cookieJson, $token){

		$curl = FacebookHelper::getCurlByCookie($cookieJson, 'mobile');
		$result = $curl->get("https://developers.facebook.com/tools/debug/accesstoken?q={$token}");
		$ss = explode('Issued', $result);
		if(empty($ss[1])) return false;

		$ss = explode('(', $ss[1]);
		$start = trim(strip_tags($ss[0]));
		if(!is_numeric($start)) return false;
		$start = DateTimeHelper::getDateTime($start);

		$ss = explode('Expires', $result);
		if(empty($ss[1])) return false;

		$ss = explode('(', $ss[1]);
		$end = trim(strip_tags($ss[0]));
		if(!is_numeric($end)) return false;
		$end = DateTimeHelper::getDateTime($end);

		$data = [
			'start' => $start,
			'end' => $end,
		];

		return $data;
	}



	/**
	 * @param $cookieJson
	 * @param array $fbIds send to account account fb ids
	 * @param $message
	 * @param array $photos array of local file path
	 */
	public static function sendMessage($cookieJson, $fbIds, $message, $photos = []){
		$fbIds = is_array($fbIds) ? $fbIds : [$fbIds];
		$photos = $photos ? (is_array($photos) ? $photos : [$photos]) : null;

		$curl = FacebookHelper::getCurlByCookie($cookieJson);
		$result = $curl->get('https://m.facebook.com/messages/photo/', [
			'ids' => implode(',', $fbIds),
			'cancel' => 'https://m.facebook.com',
		]);

		$html = new simple_html_dom();
		$html->load($result);

		$form = $html->find('#root form', 0);
		$formAction = $form->action;

		$postData = [];
		foreach($form->find('input[type=hidden], textarea') as $input){
			$postData[$input->name] = $input->value;
		}
		$postData['body'] = $message;

		if($photos){
			$i = 1;
			foreach($photos as $filePath){
				$mimeType = FileHelper::getFileType(realpath($filePath));
				$postData['file'.$i] = new \CURLFile($filePath, $mimeType);
				$i++;
			}
		}
		$result = $curl->post($formAction, $postData);

	}

	/**
	 * post to group by cookie
	 * @param $cookieJson
	 * @param $groupId
	 * @param $message
	 * @param array $tags
	 * @param null $at location: page id
	 */
	public static function postToGroup($cookieJson, $groupId, $message, $tagFriendIds = [], $at = null){

		$curl = FacebookHelper::getCurlByCookie($cookieJson);
		$result = $curl->get("https://m.facebook.com/groups/{$groupId}");

		// get from
		$html = new simple_html_dom();
		$html->load($result);

		$form = $html->find('#root form', 0);
//        echo "<pre>"; print_r($form->outertext); echo "</pre>\n\n";

		$formAction = 'https://m.facebook.com'.$form->action;
		$postData = [];
		foreach($form->find('input[type=hidden]') as $input){
			$postData[$input->name] = $input->value;
		}
		$postData['view_overview'] = 'More';
		$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
		$result = $curl->post($formAction, $postData);

		$html->clear();
		unset($html);


		// post with add tags
		$html = new simple_html_dom();
		$html->load($result);

		$form = $html->find('#root form', 0);
		$formAction = 'https://m.facebook.com'.$form->action;

		$postData = [];
		foreach($form->find('input[type=hidden]') as $input){
			$postData[$input->name] = $input->value;
		}
		if($tagFriendIds) $postData['users_with'] = implode(',', $tagFriendIds);
		if($at) $postData['at'] = $at;
		$postData['xc_message'] = $message;
		$postData['view_post'] = 'Post';

		$curl->post($formAction, $postData);
		$html->clear();
		unset($html);

	}



	/**
	 * @param $cookieJson
	 * @param $groupId
	 * @param $friendIds array
	 */
	public static function inviteToGroup($cookieJson, $groupId, $friendIds){
		$friendIds = is_array($friendIds) ? $friendIds : [$friendIds];

		$curl = FacebookHelper::getCurlByCookie($cookieJson);
		$result = $curl->get('https://m.facebook.com/groups/members/search/', ['group_id' => $groupId]);

		$html = new simple_html_dom();
		$html->load($result);
		$form = $html->find('#root form[method=post]', 0);
		$posts = [];
		foreach($form->find('input[type=hidden]') as $input){
			$posts[$input->getAttribute('name')] = $input->getAttribute('value');
		}
		foreach($friendIds as $friendId){
			$posts["addees[{$friendId}]"] = $friendId;
		}

		$uri = 'https://m.facebook.com'.$form->getAttribute('action');

		$html->clear();
		unset($html);
		unset($form);

		$curl->post($uri, $posts);
	}


	/**
	 * Leave|Join group
	 * @param $cookieJson
	 * @param array $group_id
	 */
	public static function leaveGroup($cookieJson, $group_id, $leave = true){
		$curl = FacebookHelper::getCurlByCookie($cookieJson);

		// leave group
		if($leave){
			$result = $curl->get("https://m.facebook.com/group/leave/?group_id={$group_id}");
			$html = new simple_html_dom();
			$html->load($result);
			$form = $html->find('#root form', 0);

			$formAction = 'https://m.facebook.com'.$form->action;

			$postData = [];
			foreach($form->find('input[type=hidden], textarea') as $input){
				$postData[$input->name] = $input->value;
			}

			$result = $curl->post($formAction, $postData);
		}

		// request join group
		else{
			$result = $curl->get("https://m.facebook.com/groups/{$group_id}");
			$html = new simple_html_dom();
			$html->load($result);
			$form = $html->find('#root form', 0);

			$formAction = 'https://m.facebook.com'.$form->action;

			$postData = [];
			foreach($form->find('input[type=hidden], textarea') as $input){
				$postData[$input->name] = $input->value;
			}

			$result = $curl->post($formAction, $postData);
		}


	}


	public static function postToPageApiLink($pageToken, $fbPageId, $message,
		$linkUrl = null,
		$linkTitle = null,
		$linkDesc = null,
		$linkBy = null,
		$publish = null, // Time when this post should go live, this can be any date between ten minutes and six months
		$at = null) {

		// post
		$facebookClient = FacebookHelper::getFacebookClientByAccessToken($pageToken);
		$postData = [
			'message' => $message,
//			'place' => $at,
//			'link' => $linkUrl, // photo url
//			'name' => $linkTitle, // Overwrites the title of the link preview.
//			'caption' => $linkDesc, // Overwrites the caption under the title in the link preview.
//			'description' => $linkBy, // Overwrites the caption under the title in the link preview.
//			'scheduled_publish_time' => $publish, // Overwrites the caption under the title in the link preview.
		];
		if($linkUrl) $postData['link'] = $linkUrl;
		if($linkTitle) $postData['name'] = $linkTitle;
		if($linkDesc) $postData['description'] = $linkDesc;
		if($linkBy) $postData['caption'] = $linkBy;
		if($publish) $postData['scheduled_publish_time'] = $publish;
		if($at) $postData['place'] = $at;

		echo "<pre>"; print_r($postData); echo "</pre>\n\n";
		$data = $facebookClient->api("/{$fbPageId}/feed", 'POST', $postData);

		return $data;

	}

	public static function postToPageApiPhoto($pageToken, $fbPageId, $message,
		$photo, // path or url photo
		$publish = null, // Time when this post should go live, this can be any date between ten minutes and six months
		$at = null) {


//		$pageToken = 'CAAAACZAVC6ygBAOmVh5H69waHczzpW0mXadoC0v3HAZCjP8ZBkFlZBpMAmaegs2KnxD6LVpmSvQdGNRw05fSDp5nhUu4Ihu8UkME1pr0EnpZCZBtgW8zZBau7ZBF8o991Un2fZBxTdNnW3ZAVePDbGWuYNEfEbIUyahWbixvZCRUrey9rSZBTX5ps8xmn9kmV89ofuIZD';

		// post
		$facebookClient = FacebookHelper::getFacebookClientByAccessToken($pageToken);
		$postData = [
			'caption' => $message,
		];


		if(substr($photo, 0, 4) == 'http'){
			$postData['url'] = $photo;
		}else{

			// TODO fix upload from local
			$mimeType = FileHelper::getFileType(realpath($photo));
			$postData['source'] = new \CURLFile($photo, $mimeType);
		}



		if($publish) $postData['scheduled_publish_time'] = $publish;
		if($at) $postData['place'] = $at;

		$uri = "{$fbPageId}/photos";

//		echo "<pre>"; print_r($pageToken); echo "</pre>\n\n";
//		echo "<pre>"; print_r($postData); echo "</pre>\n\n";
//		echo "<pre>"; print_r($uri); echo "</pre>\n\n";
//		die;

		$data = $facebookClient->api($uri, 'POST', $postData);

		return $data;

	}

		/**
	 * post to page by cookie
	 * @param $cookieJson
	 * @param $pageId
	 * @param $message
	 * @param array $tags
	 * @param null $at location: page id
	 */
	public static function postToPageLiked($cookieJson, $pageId, $message, $tagFriendIds = [], $at = null){

		$curl = FacebookHelper::getCurlByCookie($cookieJson);
		$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
		$result = $curl->get("https://m.facebook.com/{$pageId}");

		// get from
		$html = new simple_html_dom();
		$html->load($result);

		$form = $html->find('#root form', 0);
		$formAction = 'https://m.facebook.com'.$form->action;
		$postData = [];
		foreach($form->find('input[type=hidden]') as $input){
			$postData[$input->name] = $input->value;
		}
		$postData['view_overview'] = 'More';
		$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
		$result = $curl->post($formAction, $postData);

		$html->clear();
		unset($html);


		// post with add tags
		$html = new simple_html_dom();
		$html->load($result);

		$form = $html->find('#root form', 0);
		$formAction = 'https://m.facebook.com'.$form->action;

		$postData = [];
		foreach($form->find('input[type=hidden]') as $input){
			$postData[$input->name] = $input->value;
		}
		if($tagFriendIds) $postData['users_with'] = implode(',', $tagFriendIds);
		if($at) $postData['at'] = $at;
		$postData['xc_message'] = $message;
		$postData['view_post'] = 'Post';
		$postData['waterfall_source'] = 'advanced_composer_page';
		$postData['id'] = $pageId;

		if(isset($postData['r2a'])){
			unset($postData['r2a']);
		}
		$postData['c_src'] = 'page_other';

		ksort($postData);

//		echo "<pre>"; print_r($formAction); echo "</pre>\n\n";
//		echo "<pre>"; print_r($postData); echo "</pre>\n\n";
//		die;

		$curl->setHeader('Content-Type', 'multipart/form-data');
		$curl->post($formAction, $postData);
		$html->clear();
		unset($html);

	}


	/**
	 * @param $cookieJson
	 * @param $groupId
	 * @param $friendIds array
	 */
	public static function inviteToPage($cookieJson, $pageId, $friendId, $userId){
		$curl = FacebookHelper::getCurlByCookie($cookieJson, 'web');

		$uri = "https://www.facebook.com/{$pageId}";

		$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
		$result = $curl->get($uri);

		$postData = [];
		$postData['fb_dtsg'] = StringHelper::explodeString('fb_dtsg" value="', '"', $result);
		$postData['page_id'] = $pageId;
		$postData['invitee'] = $friendId;
		$postData['action'] = 'send';
		$postData['ref'] = 'context_row_dialog';
		$postData['__user'] = $userId;
		$postData['__a'] = '1';

		$uri = "https://www.facebook.com/ajax/pages/invite/send_single/?__pc=EXP1%3ADEFAULT";

//		echo "<pre>"; print_r($postData); echo "</pre>\n\n";

		$result = $curl->post($uri, $postData);

//		die($result);
	}

	/**
	 * Unlike page
	 * @param $cookieJson
	 * @param array $page_id
	 */
	public static function likePage($userFacebook, $page_id, $unlike = false){
		$curl = FacebookHelper::getCurlByCookie($userFacebook->fb_cookie, 'web');

		$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
		$result = $curl->get("https://www.facebook.com/{$page_id}");
//		die($result);

		// like
		$postData = [];
		$postData['fb_dtsg'] = StringHelper::explodeString('fb_dtsg" value="', '"', $result);
		$postData['fbpage_id'] = $page_id;
		$postData['add'] = 'true';
		$postData['reload'] = 'false';
		$postData['fan_origin'] = 'page_profile';
		$postData['__user'] = $userFacebook->fb_id;
		$postData['__a'] = '1';
//		$postData['__rev'] = '2041018';

//		echo "<pre>"; print_r($postData); echo "</pre>\n\n"; die;
		// unlike
		if($unlike){
			$postData['add'] = 'false';
			$postData['fan_origin'] = 'liked_menu';
		}

//		$curl = FacebookHelper::getCurlByCookie($userFacebook->fb_cookie, 'web');
//		$curl->setReferer("https://www.facebook.com/thanhnha.fashion");
//		$curl->setHeader('content-type', 'application/x-www-form-urlencoded');


		$uri = "https://www.facebook.com/ajax/pages/fan_status.php?__pc=EXP1%3ADEFAULT";

		$result = $curl->post($uri, $postData);

//		echo "<pre>"; print_r($uri); echo "</pre>\n\n";
//		echo "<pre>"; print_r($postData); echo "</pre>\n\n";
//		echo "<pre>"; print_r($result); echo "</pre>\n\n";
//		die;

	}



	/**
	 * Unfriend
	 * @param $cookieJson
	 * @param array $user_id
	 */
	public static function unfriend($userFacebook, $user_id, $unfriend = true){


		if($unfriend){
			$curl = FacebookHelper::getCurlByCookie($userFacebook->fb_cookie);
			$result = $curl->get("https://m.facebook.com/removefriend.php?friend_id={$user_id}");
			$html = new simple_html_dom();
			$html->load($result);
			$form = $html->find('#root form', 0);
			if(!$form) return false;

			$formAction = 'https://m.facebook.com'.$form->action;

			$postData = [];
			foreach($form->find('input[type=hidden], textarea') as $input){
				$postData[$input->name] = $input->value;
			}

			$curl->setOpt(CURLOPT_NOBODY, true);
			$curl->post($formAction, $postData);
		}else{

			$curl = FacebookHelper::getCurlByCookie($userFacebook->fb_cookie, 'web');

			$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
			$result = $curl->get("https://www.facebook.com/{$user_id}");

			// add friend
			$postData = [];
			$postData['fb_dtsg'] = StringHelper::explodeString('fb_dtsg" value="', '"', $result);
			$postData['to_friend'] = $user_id;
			$postData['action'] = 'add_friend';
			$postData['how_found'] = 'profile_button';
			$postData['ref_param'] = 'none';
			$postData['link_data[gt][profile_owner]'] = $user_id;
			$postData['link_data[gt][ref]'] = 'timeline:timeline';
			$postData['no_flyout_on_click'] = 'true';
			$postData['floc'] = 'profile_button';


			$postData['__user'] = $userFacebook->fb_id;
			$postData['__a'] = '1';
			$postData['__req'] = 'f';

			if($unfriend){

			}

			$uri = "https://www.facebook.com/ajax/add_friend/action.php?__pc=EXP1%3ADEFAULT";

			$result = $curl->post($uri, $postData);

		}


	}

	/**
	 * @param string $username
	 */
	public static function getFbID($username = 'eva365.com.vns', $getIDOnly = true, $getScoped = true){
		$client = FacebookHelper::getFacebookClientByAccessToken(Setting::get('fb_token')->value);

		try{
			$result = $client->api('/'.$username);
			unset($client);

			$result['url'] = "https://www.facebook.com/{$result['id']}";
			$result['photo'] = "http://graph.facebook.com/{$result['id']}/picture";

			if(!empty($result['owner']['id'])){
				$result['owner']['url'] = "https://www.facebook.com/{$result['owner']['id']}";
				$result['owner']['photo'] = "http://graph.facebook.com/{$result['owner']['id']}/picture";
			}

			$result['scoped_id'] = null;
			$result['scoped_link'] = null;

			// get app scoped user id
			if(!$getIDOnly && $getScoped && !isset($result['can_post'])){
				$client = FacebookHelper::getFacebookClientByAccessToken(Setting::get('fb_token_graph')->value);
				$res = $client->api('/'.$result['id'].'?fields=link');
				if(!empty($res['link'])){
					$result['scoped_id'] = preg_replace('/^.+app_scoped_user_id\/(\d+).+$/i', '$1', $res['link']);
					$result['scoped_link'] = $res['link'];
				}
			}


		}catch (Exception $e){
			return null;
		}

		return !empty($result['id']) ? ($getIDOnly ? $result['id'] : $result) : null;
	}


	/**
	 * nếu cookie hết hạn hoặc invalid => cập nhật cookie, token mới từ user gần nhất
	 * @param null $uri
	 * @return mixed|string
	 */
	private static function updateSettingFbCookie(){

		$userFacebookAttrs = UserFacebook::find()->select(['fb_cookie', 'fb_token'])->orderBy(['updated' => SORT_DESC])->limit(1)->asArray()->one();
		$cookieJson = $userFacebookAttrs['fb_cookie'];

		Setting::set('fb_cookie', $cookieJson);
		Setting::set('fb_token', $userFacebookAttrs['fb_token']);

		return $cookieJson;
	}


	/**
	 * @param string $appFbID
	 */
	public static function getRealFbID($appFbID = '1714612068760421', $getIDOnly = true){

		$uri = "https://www.facebook.com/app_scoped_user_id/{$appFbID}/";
//		echo "<pre>"; print_r($uri); echo "</pre>\n\n"; die;

		$cookieJson = Setting::get('fb_cookie')->value;

		$curl = FacebookHelper::getCurlByCookie($cookieJson);
		$curl->setOpt(CURLOPT_HEADER, true);
		$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
		$result = $curl->head($uri);

		// nếu cookie hết hạn hoặc invalid => lấy cookie mới từ user gần nhất
		if(!$result || strpos($result, 'login.php') !== false){
			$cookieJson = self::updateSettingFbCookie();
			$curl = FacebookHelper::getCurlByCookie($cookieJson);
			$curl->setOpt(CURLOPT_HEADER, true);
			$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
			$result = $curl->head($uri);
		}


		preg_match("/^.+Location:([^\n]+).+$/is", $result, $m);
		if(empty($m[1])) return null;


		if(strpos($m[1], '?id=') !== false){
			$id = preg_replace('/^.+\?id=(\d+).+/', '$1', $m[1]);
			if($getIDOnly) return $id;
			return self::getFbID($id, $getIDOnly);
		}else{
			$username = preg_replace('/^.+\.com\/([^\?]+).*/', '$1', $m[1]);
			$res = self::getFbID($username, $getIDOnly, false);
			if(!$res) return null;

			if($getIDOnly) return $res;

			$res['scoped_id'] = $appFbID;
			$res['scoped_link'] = "https://www.facebook.com/app_scoped_user_id/{$appFbID}/";

			return $res;
		}

		return null;
	}

	/**
	 * @param string $group_username
	 */
	public static function getGroupIDByUsername($group_username = 'phutungoto', $getIDOnly = true){

		$uri = "https://www.facebook.com/groups/{$group_username}";
//		echo "<pre>"; print_r($uri); echo "</pre>\n\n"; die;

		$cookieJson = Setting::get('fb_cookie')->value;
		$curl = FacebookHelper::getCurlByCookie($cookieJson);
		$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
		$result = $curl->get($uri);

		// nếu cookie hết hạn hoặc invalid => lấy cookie mới từ user gần nhất
		if(!$result || strpos($result, 'login.php') !== false){
			$cookieJson = self::updateSettingFbCookie();
			$curl = FacebookHelper::getCurlByCookie($cookieJson);
			$curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
			$result = $curl->get($uri);
		}

		$html = new simple_html_dom();
		$html->load($result);
		$form = $html->find('#root form', 0);

		$group_id = null;
		if(strpos($form, '?group_id=') !== false){
			$group_id = preg_replace('/^.+\?group_id=(\d+).+/', '$1', $form);
		}
		if(!$group_id) return null;

		if($getIDOnly){
			return $group_id;
		}

		// get more info
		$client = FacebookHelper::getFacebookClientByAccessToken(Setting::get('fb_token')->value);
		try{
			$result = $client->api("{$group_id}?fields=id,name,privacy,description,owner,updated_time");
			$result['url'] = "https://www.facebook.com/{$result['id']}";
			$result['photo'] = "http://graph.facebook.com/{$result['id']}/picture";

			if(!empty($result['owner']['id'])){
				$result['owner']['url'] = "https://www.facebook.com/{$result['owner']['id']}";
				$result['owner']['photo'] = "http://graph.facebook.com/{$result['owner']['id']}/picture";
			}

			return $result;

		}catch (Exception $e){
			return null;
		}

	}

} 