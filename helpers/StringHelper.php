<?php
namespace yii\helpers;

use app\libraries\simple_html_dom;
use Yii;
use app\libraries\Parsedown;

/**
 * StringHelper
 */
class StringHelper extends BaseStringHelper
{
    /**
     * Đổi những ký tự là tiếng Việt thành nguyên âm tiếng Anh
     *
     * @param string $text
     * @return string
     */
    public static function convertVietnameseToNon($text)
    {
        $vietnameseMappingTable = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");

        $nonVietnameseMappingTable = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "d", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y", "D");

        return str_replace($vietnameseMappingTable, $nonVietnameseMappingTable, $text);
    }

    /**
     * Biến 1 xâu thông thường thành 1 xâu hiển thị trên URL
     *
     * @param string $str
     *
     * @return string
     */
    public static function toSEOString($str, $separator = '-', $strLower = true)
    {
        $str = strip_tags($str);
        $str = self::convertVietnameseToNon($str);
        if ($strLower) $str = strtolower($str);

        $str = trim(preg_replace("{[^\s\da-z]+}i", ' ', $str));
        $str = preg_replace("{\s+}", $separator, $str);
        return $str;
    }


    /**
     * Chuyển thẻ <br>|<br/> thành \n. decode của hàm ln2br
     * @param $html
     * @return mixed
     */
    public static function br2ln($html)
    {
        return preg_replace('#<br\s*/?>#i', "\n", $html);
    }

    public static function mb_ucfirst_utf8($str, $lowerAfter = true)
    {
        $str = trim($str);
        $fc = mb_substr($str, 0, 1, 'UTF-8');
        $fcUpper = mb_convert_case($fc, MB_CASE_UPPER, 'UTF-8');
        $strWithoutFc = mb_substr($str, 1, mb_strlen($str, 'UTF-8') - 1, 'UTF-8');
        if ($lowerAfter) {
            $strWithoutFc = mb_convert_case($strWithoutFc, MB_CASE_LOWER, 'UTF-8');
        }
        return $fcUpper . $strWithoutFc;
    }

    /**
     * Lấy ký tự ngẫu nhiên
     * @param int $num
     * @return string
     */
    public static function getRandomChar($num = 6)
    {
        $charNumArr = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        );
        $n = count($charNumArr);
        $str = "";
        for ($i = 0; $i < $num; $i++) {
            $str .= $charNumArr[rand(0, $n - 1)];
        }
        return $str;
    }

    /**
     * Làm sáng một đoạn text những từ khớp với từ đang tìm
     *
     * @param string $text text nhập vào
     * @param string $highlight từ cần làm sáng
     * @return string
     * @author IPB 3.0.1
     */
    public static function highLight($text, $highlight)
    {
        $highlight = self::cleanValue(urldecode($highlight));
        $loosematch = strstr($highlight, '*') ? 1 : 0;
        $keywords = str_replace('*', '', str_replace("+", " ", str_replace("++", "+", str_replace('-', '', trim($highlight)))));
        $keywords = str_replace('&quot;', '', str_replace('\\', '&#092;', $keywords));
        $word_array = array();
        $endmatch = "(.)?";
        $beginmatch = "(.)?";

        if ($keywords) {
            if (preg_match("/,(and|or),/i", $keywords)) {
                while (preg_match("/\s+(and|or)\s+/i", $keywords, $match)) {
                    $word_array = explode(" " . $match[1] . " ", $keywords);
                    $keywords = str_replace($match[0], '', $keywords);
                }
            } else if (strstr($keywords, ' ')) {
                $word_array = explode(' ', str_replace('  ', ' ', $keywords));
            } else {
                $word_array[] = $keywords;
            }

            if (!$loosematch) {
                $beginmatch = "(^|\s|\>|;)";
                $endmatch = "(\s|,|\.|!|<br|&|$)";
            }

            if (is_array($word_array)) {
                foreach ($word_array as $keywords) {
                    /* Make sure we're not trying to process an empty keyword */
                    if (!$keywords) {
                        continue;
                    }

                    preg_match_all("/{$beginmatch}(" . preg_quote($keywords, '/') . "){$endmatch}/is", $text, $matches);

                    for ($i = 0; $i < count($matches[0]); $i++) {
                        $text = str_replace($matches[0][$i], $matches[1][$i] . "<span class='text-highlight'>" . $matches[2][$i] . "</span>" . $matches[3][$i], $text);
                    }
                }
            }
        }

        return $text;
    }

    /**
     * Kiểm tra xem xâu có phải UTF8 không
     *
     * @param string $str
     */
    public static function isUTF8($str)
    {
        $c = 0;
        $b = 0;
        $bits = 0;
        $len = strlen($str);
        for ($i = 0; $i < $len; $i++) {
            $c = ord($str[$i]);

            if ($c > 128) {
                if (($c >= 254)) return false; elseif ($c >= 252) $bits = 6;
                elseif ($c >= 248) $bits = 5;
                elseif ($c >= 240) $bits = 4;
                elseif ($c >= 224) $bits = 3;
                elseif ($c >= 192) $bits = 2;
                else return false;

                if (($i + $bits) > $len) return false;

                while ($bits > 1) {
                    $i++;
                    $b = ord($str[$i]);
                    if ($b < 128 || $b > 191) return false;
                    $bits--;
                }
            }
        }

        return true;
    }


    /**
     * @param $text
     * @param $length
     * @param string $ending
     * @return string
     */
    public static function htmlLimit($text, $length, $ending = '...')
    {
        // if the plain text is shorter than the maximum length, return the whole text
        if (mb_strlen(preg_replace('/<.*?>/s', '', $text)) <= $length) {
            return $text;
        }

        // splits all html-tags to scanable lines
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);

        $total_length = mb_strlen($ending);
        $open_tags = array();
        $truncate = '';

        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
            if (!empty($line_matchings[1])) {
                // if it's an "empty element" with or without xhtml-conform closing slash (f.e. <br/>)
                if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                    // if tag is a closing tag (f.e. </b>)
                } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list
                    $pos = array_search($tag_matchings[1], $open_tags);
                    if ($pos !== false) {
                        unset($open_tags[$pos]);
                    }
                    // if tag is an opening tag (f.e. <b>)
                } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list
                    array_unshift($open_tags, strtolower($tag_matchings[1]));
                }
                // add html-tag to $truncate'd text
                $truncate .= $line_matchings[1];
            }

            // calculate the length of the plain text part of the line; handle entities as one character
            $content_length = mb_strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
            if ($total_length + $content_length > $length) {
                // the number of characters which are left
                $left = $length - $total_length;
                $entities_length = 0;
                // search for html entities
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range
                    foreach ($entities[0] as $entity) {
                        if ($entity[1] + 1 - $entities_length <= $left) {
                            $left--;
                            $entities_length += mb_strlen($entity[0]);
                        } else {
                            // no more characters left
                            break;
                        }
                    }
                }
                $truncate .= mb_substr($line_matchings[2], 0, $left + $entities_length);

                // maximum lenght is reached, so get off the loop
                break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }

            // if the maximum length is reached, get off the loop
            if ($total_length >= $length) {
                break;
            }
        }

        // ...search the last occurance of a space...
        $spacepos = mb_strrpos($truncate, ' ');
        if (isset($spacepos)) {
            // ...and cut the text in this position
            $truncate = mb_substr($truncate, 0, $spacepos);
        }

        // add the defined ending to the text
        $truncate .= $ending;

        // close all unclosed html-tags
        foreach ($open_tags as $tag) {
            $truncate .= '</' . $tag . '>';
        }

        return $truncate;
    }

    public static function charLimit($string, $limit = 100, $ellipsis = '...', &$isTruncate = false)
    {
        $str_sub = mb_substr($string, 0, $limit, 'UTF-8');
        if (mb_strlen($string, 'UTF-8') > $limit) {
            $isTruncate = true;
            return $str_sub . $ellipsis;
        } else {
            return $string;
        }
    }

    public static function wordLimit($string, $limit = 50, $ellipsis = "...", &$isTruncate = false)
    {
        $string = trim(preg_replace('/[\s\n\r]+/u', ' ', $string));
        $words = explode(' ', $string);
        $wordCount = count($words);
        $isTruncate = $wordCount > $limit;

        return $isTruncate ? implode(' ', array_slice($words, 0, $limit)) . $ellipsis : $string;
    }

    public static function numberFormat($number, $int = FALSE)
    {
        if ($int) return str_replace('.', '', $number);
        return number_format($number, 0, '', '.');
    }


    public static function removeSpecialChars($string)
    {
        $string = str_replace(explode(' ', '` ~ ! @ # $ % ^ & * ( ) - _ = + [ { ] } \ | ; : \' " , < . > / ?'), ' ', $string);
        $string = preg_replace('/\s+/', ' ', $string);
        $string = trim($string);
        return $string;
    }

    public static function parseTextarea($content)
    {

//        $content = preg_replace("/#([\w\-]+)/i", '<a class="hashtag" href="/hashtag/$1">#$1</a>', $content);

        return self::parsedown($content);
    }


    /**
     * convert content markdown to html
     * @return string
     * @url https://github.com/erusev/parsedown/wiki/Usage
     */
    public static function parsedown($content)
    {
//        $content = strip_tags($content, '<strong><em><blockquote><code><ul><ol><li><h2><br><hr><a><img><pre>');
        $content = Parsedown::instance()
            ->setBreaksEnabled(true)// line to <br>
            ->setMarkupEscaped(true)// ignore tag markup
            ->text($content);

//		$content = Markdown::process($content);


        $content = str_replace('<h2>', '<h3>', $content);
        $content = str_replace("<\/h2>", "<\/h3>", $content);
        $content = str_replace('<h1>', '<h2>', $content);
        $content = str_replace('</h1>', '</h2>', $content);

        $html = new simple_html_dom();
        $html->load($content, true, false);

        foreach ($html->find('a') as $a) {
            if (preg_match('{^https?.+$}six', $a->href) && strpos($a->href, Url::base(true)) === FALSE) {
                $a->rel = 'nofollow';
                $a->target = '_blank';
            }
        }


        $content = $html->save();
		$html->clear();
		unset($html);

//      $content = str_replace('<pre>', '<pre class="prettyprint">', $content);
//      	$content = trim($content, '<p></p>');
        return $content;
    }


    /**
     * Parse number to number plus: 23567 => 20k+
     * @param string $num
     * @param bool $type long or short
     * @param string $lang vi|en. default lang by Yii::$app->language
     * @return bool|string
     */
    public static function numberPlus($number, $type = 'long', $lang = null)
    {
        $lang = in_array($lang, ['en', 'vi']) ? $lang : substr(Yii::$app->language, 0, 2);

        $k = floor($number / 1000);
        if ($k) {
            return $k . 'k+';
        }

        return $number;
    }

    public static function diff($old, $new)
    {
        $old = str_split($old);
        $new = str_split($new);

        $matrix = array();
        $maxlen = 0;
        foreach ($old as $oindex => $ovalue) {
            $nkeys = array_keys($new, $ovalue);
            foreach ($nkeys as $nindex) {
                $matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ?
                    $matrix[$oindex - 1][$nindex - 1] + 1 : 1;
                if ($matrix[$oindex][$nindex] > $maxlen) {
                    $maxlen = $matrix[$oindex][$nindex];
                    $omax = $oindex + 1 - $maxlen;
                    $nmax = $nindex + 1 - $maxlen;
                }
            }
        }
        if ($maxlen == 0) return array(array('d' => $old, 'i' => $new));
        return array_merge(
            self::diff(array_slice($old, 0, $omax), array_slice($new, 0, $nmax)),
            array_slice($new, $nmax, $maxlen),
            self::diff(array_slice($old, $omax + $maxlen), array_slice($new, $nmax + $maxlen)));
    }

    public static function htmlDiff($old, $new)
    {
        $ret = '';
        $diff = diff(preg_split("/[\s]+/", $old), preg_split("/[\s]+/", $new));
        foreach ($diff as $k) {
            if (is_array($k))
                $ret .= (!empty($k['d']) ? "<del>" . implode(' ', $k['d']) . "</del> " : '') .
                    (!empty($k['i']) ? "<ins>" . implode(' ', $k['i']) . "</ins> " : '');
            else $ret .= $k . ' ';
        }
        return $ret;
    }

    public static function explodeString($start, $end, $string, $join = false){
        $ss = explode($start, $string);
        if(empty($ss[1])) return null;
        $ss = explode($end, $ss[1]);

        $s = $ss[0];
        if($join){
            $s = $start.$s.$end;
        }
        return $s;
    }

    public static function getIdVideoYoutube($url){
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $matches);
        if (!empty($matches)) {
            return $matches[1];
        }
    }

    public static function chuyenChuoi($str) {
// In thường
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
// In đậm
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        return $str; // Trả về chuỗi đã chuyển
    }

}