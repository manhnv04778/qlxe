<?php
/**
 * Created by PhpStorm.
 * User: ABCnet
 * Date: 10/27/14
 * Time: 12:57 AM
 */

namespace app\helpers;

use app\libraries\PicasaClient;
use app\models\App;
use app\modules\location\models\Location;
use app\modules\question\models\QuestionAnswer;
use Curl\Curl;
use Yii;

/* @var $this \app\helpers\AppHelper */

class AppHelper
{
	/**
	 *
	 * @param $realFbId facebook id thật. Lưu trong user_email.fb_id
	 * @param $message nội dung tin nhắn
	 */
	public static function sendMessageToFbId($realFbId, $message){
		$cookieJson = Setting::get('fb_cookie')->value;
		FacebookHelper::sendMessage($cookieJson, $realFbId, $message);
	}

	/**
	 * @param $longUrl
	 * @return mixed
	 */
	public static function shortenUrl($longUrl){
		$apiKey = Yii::$app->params['google']['apiKey']['server'];
		$curl = new Curl("https://www.googleapis.com/urlshortener/v1/url?key={$apiKey}");
		$curl->setHeader('Content-Type', 'application/json');
		$result = $curl->post(['longUrl' => $longUrl]);
		return $result->id;
	}


	/**
	 * @param string|array $fileDrive [] {email, file_id} or file_id
	 * @param null $fileName tên file ảnh. vd: ten-file-anh.jpg
	 * @param string $host
	 * @return string
	 */
	public static function getFileDrive($fileDrive, $type = 'direct'){

		if(is_array($fileDrive)){
			$fileId = $fileDrive['file_id'];
		}else{

			$temp = json_decode($fileDrive, true);
			if(!empty($temp['file_id'])){
				$fileId = $temp['file_id'];
			}else{
				$fileId = $fileDrive;
			}
		}

		$data = [];
		$data['direct'] = "https://googledrive.com/host/{$fileId}";
		$data['view'] = "https://drive.google.com/file/d/{$fileId}/view?usp=drivesdk";
		$data['download'] = "https://docs.google.com/uc?id={$fileId}&export=download";

		return !empty($data[$type]) ? $data[$type] : null;

	}

	/**
	 * @param float $star number selected stars
	 * @param string $size large | small
	 * <?=\app\helpers\AppHelper::getRatingHtml(3, 'small')?>
	 */
	public static function getRatingHtml($star = 3, $size = 'small', $showLabel = true){
		$star = round($star, 0);

		$data = App::getStarData();
		$starHtml = '';
		foreach($data as $num => $text){
			$class = "";
			if($num <= $star && $star > 0){
				$class.= " br-selected";
				if($num == $star){
					$class.= " br-current";
				}
			}
			$starHtml .= "<i title=\"{$text}\" class=\"{$class}\"></i>";
		}

		$ratingLabel = $star > 0 ? $data[$star] : 'Chưa đánh giá';
        if($showLabel){
		    $starHtml.= "<div class=\"br-current-rating\">{$ratingLabel}</div>";
        }

		$html = <<<HTML
		<div class="br-wrapper br-theme-stars">
			<div class="br-widget {$size}">
				{$starHtml}
			</div>
		</div>
HTML;

		return $html;
	}

	public static function ratingConvert($number){
	    $convert_to_int=intval($number);//Chuyển đổi sang số nguyên

	    if($number >= $convert_to_int+0.1 && $number <= $convert_to_int+0.2){
	        $number = $convert_to_int;
	    }else if($number >= $convert_to_int+0.3 && $number <= $convert_to_int+0.4){
	    	$number= $convert_to_int + 0.5;
	    }else if($number >= $convert_to_int+0.6 && $number <= $convert_to_int+0.7){
	    	$number= $convert_to_int + 0.5;
	    }else if($number >= $convert_to_int+0.8 && $number <= $convert_to_int+0.9){
	    	$number= $convert_to_int + 1;
	    }
	    
	    return $number;
	}



}