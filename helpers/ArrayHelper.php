<?php
namespace yii\helpers;

/**
 * StringHelper
 */
class ArrayHelper extends BaseArrayHelper
{
	/**
	 * Chuyển mảng phẳng thành dạng tree
	 * @param array $rows
	 * @param string $id_field
	 * @param string $parent_id_field
	 * @param string $childs_field
	 * @param bool $ref
	 * @return array
	 */
	public static function fetchRowsToTree(array $rows, $id_field = 'id', $parent_id_field = 'parent_id', $childs_field = 'childs', $ref = FALSE)
	{
		$data = array();
		$ref = array();
		foreach ($rows as & $row) {
			$ref[$row[$id_field]] = &$row;
			$row[$childs_field] = array();
		}

		foreach ($rows as $index => & $row) {
			if (empty($row[$parent_id_field])) {
				if ($ref)
					$data[$index] = &$row;
				else
					$data[] = &$row;
			} else {
				if ($ref)
					$ref[$row[$parent_id_field]][$childs_field][$index] = &$row;
				else
					$ref[$row[$parent_id_field]][$childs_field][] = &$row;
			}
		}
		return $data;
	}
	/**
	 * Chuyển mảng phẳng thành dạng trtree
	 * @param $data
	 * @param string $id_key
	 * @param string $parent_id_key
	 * @param string $children_key
	 * @param null $parent_id_value
	 * @param bool $ref
	 * @return mixed
	 */
//	public static function arrayToTree($data, $id_key = 'id', $parent_id_key = 'parent_id', $children_key = 'children', $parent_id_value = NULl, $ref = FALSE)
//	{
//		if (empty($data)) return;
//
//		$fetch = function(&$tree, $index, $item, $id_key, $parent_id_key, $children_key, $ref = FALSE){
//			$i = $item[$id_key];
//			$p = $item[$parent_id_key];
//			$tree[$i] = isset($tree[$i]) ? $item + $tree[$i] : $item;
//			if ($ref) {
//				$tree[$p][$children_key][$index] = &$tree[$i];
//			} else {
//				$tree[$p][$children_key][] = &$tree[$i];
//			}
//		};
//
//		$tree = array();
//		foreach ($data as $index => $item) {
//			$fetch($tree, $index, $item, $id_key, $parent_id_key, $children_key, $ref);
//		}
//		return $tree[$parent_id_value][$children_key];
//	}


    /**
     * @param $array
     * @param int $number
     * @return array
     */
	public static function getRandomArray($array, $number = 1)
	{
        $count = count($array);
        if(!is_numeric($number)) $number = $count;

        $number = ($number > $count || $number < 1) ? $count : $number;

		$rand_keys = array_rand($array, $number);
		if ($number > 1) {
			$randomArray = array();
			foreach ($rand_keys as $i => $key) {
				if ($number == $i) break;
				$randomArray[$key] = $array[$key];
			}
			return $randomArray;
		} else {
			return $array[$rand_keys];
		}
	}

	public static function printArray($arr)
	{
		$str = '<ul>';
		if (is_array($arr)) {
			foreach ($arr as $key => $val) {
				$key = is_numeric($key) ? $key : "'{$key}'";
				if (is_array($val)) {
					$str .= "<li>{$key} => array({self::printArray($val)}),</li>";
				} else {
					$val = is_numeric($val) ? $val : "'{$val}'";
					$str .= "<li>{$key} => {$val},</li>";
				}
			}
		}
		$str .= '</ul>';
		return $str;

	}


	public static function addslashes(&$array){
		foreach($array as &$v){
			$v = addslashes($v);
		}
	}

}
