<?php
namespace app\components;

use yii\web\Response;
use Yii;

class AuthAction extends \yii\authclient\AuthAction
{
    /**
     * Trước khi auth thì lưu lại url hiện tại để sau khi auth thì redirect
     * @param mixed $client auth client instance.
     * @return Response response instance.
     * @throws \yii\base\NotSupportedException on invalid client.
     */
    protected function auth($client)
    {
        //$client->authUrl = 'https://www.facebook.com/dialog/oauth?';
        if($client->apiBaseUrl == 'https://graph.facebook.com')
            $client->authUrl = 'https://www.facebook.com/dialog/oauth?';
        $redirect = Yii::$app->request->referrer;
        if(strpos($redirect, 'facebook.com') === false){
            Yii::$app->user->setReturnUrl($redirect);
        }
        parent::auth($client);
    }

}
