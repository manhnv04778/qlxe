<?php
/**
 * Created by PhpStorm.
 * Date: 10/17/14
 * Time: 20:36
 */

namespace app\components;

use yii\bootstrap\Html;

class Alert extends \yii\bootstrap\Alert{

	public function init()
	{
		parent::init();
	}

	/**
	 * Renders the widget.
	 */
	public function run()
	{
		echo "\n" . $this->renderBodyEnd();
		echo "\n" . Html::endTag('div');
	}
} 