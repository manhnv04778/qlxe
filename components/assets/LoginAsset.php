<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components\assets;

use Yii;
use yii\web\AssetBundle;

class LoginAsset extends AssetBundle
{
//	public $sourcePath = '@common/css';
//	public $basePath = '@webroot'; // @webroot
//	public $baseUrl = '@web'; // @web
	public $css = [
	];
	public $js = [
	];
	public $depends = [
			'yii\web\YiiAsset',
			'yii\jui\JuiAsset',
			'yii\bootstrap\BootstrapAsset',
			'yii\authclient\widgets\AuthChoiceAsset',
			'app\components\assets\IEAsset',
	];

	public function init()
	{

		parent::init();
		$theme = Yii::$app->view->theme->baseUrl;


		// load theme
		$this->css[] = 'files/css/Font-Awesome/css/font-awesome.min.css';
		$this->css[] = 'files/css/ionicons/css/ionicons.min.css';
		$this->js[] = $theme.'/files/js/theme.options.js';

		// custom
		$this->css[] = 'files/css/animate.css'; // animations
		$this->css[] = $theme.'/files/css/admin_style.css?v='.Yii::$app->controller->version;

		// load init & common
		$this->js[] = $theme.'/files/js/common_ct.js';

	}
}
