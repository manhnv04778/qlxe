<?php
/**
 * cmd controller: yii\console\controllers\AssetController.php
 * cmd compress: ./yii asset components/assets/compression/config.php config/assets_compressed.php
 */
return [
    // Adjust command/callback for JavaScript files compressing:
    'jsCompressor' => 'java -jar components/assets/compression/compiler.jar --js {from} --js_output_file {to}',
    // Adjust command/callback for CSS files compressing:
    'cssCompressor' => 'java -jar components/assets/compression/yuicompressor.jar --type css {from} -o {to}',
    // The list of asset bundles to compress:
    'bundles' => [
//        'app\components\assets\AppAsset',
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ],
    // Asset bundle for compression output:
    'targets' => [
        'app\components\assets\AppAsset' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => realpath(__DIR__ . '/../../..'),
            'baseUrl' => STATIC_FILE.'/',
            'js' => 'assets-compiled/all-{hash}.js',
            'css' => 'assets-compiled/all-{hash}.css',
        ],
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => STATIC_FILE.realpath(__DIR__ . '/../../../assets'),
        'baseUrl' => STATIC_FILE.'/assets',
    ],
];
