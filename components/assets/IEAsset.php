<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components\assets;

use yii\web\AssetBundle;

class IEAsset extends AssetBundle
{
//	public $sourcePath = '@common/css';
	public $basePath = '@webroot'; // @webroot
	public $baseUrl = '@web'; // @web
	public $css = [
	];
    public $cssOptions = [];
	public $js = [
        'https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js',
        'https://oss.maxcdn.com/respond/1.4.2/respond.min.js'
	];
    public $jsOptions = [
        'condition' => 'lt IE 9',
        'position' => \yii\web\View::POS_HEAD
    ];
	public $depends = [
	];

	public function init()
	{
		parent::init();
	}
}
