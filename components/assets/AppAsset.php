<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components\assets;

use Yii;
use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
	public $css = [
	];
	public $js = [
	];
	public $depends = [
			'yii\web\YiiAsset',
			'yii\jui\JuiAsset',
			'yii\bootstrap\BootstrapAsset',
			'yii\authclient\widgets\AuthChoiceAsset',
			'app\components\assets\IEAsset',
	];

	public function init()
	{

		parent::init();
		$theme = Yii::$app->view->theme->baseUrl;


		// load theme
		//$this->css[] = 'files/css/Font-Awesome/css/font-awesome.min.css';
		$this->js[] = '/files/css/bootstrap/dist/js/bootstrap.min.js';
		$this->js[] = '/files/css/bootstrap/dist/js/bootstrap-waitingfor.js';

		$this->css[] = '/files/css/bootstrap/css/bootstrap.css';
		$this->css[] = 'files/css/Font-Awesome-master/css/font-awesome.min.css';
		$this->css[] = 'https://fonts.googleapis.com/css?family=Roboto:400,300,100,100italic,300italic,400italic,500,500italic,700,700italic&subset=latin,vietnamese';
		$this->css[] = '/files/css/ionicons/css/ionicons.min.css';
		$this->js[] = $theme.'/files/js/theme.options.js';
		$this->js[] = '/files/js/bootstrap-growl/bootstrap-growl.min.js';

		$this->js[] = '/files/js/typeahead/dist/typeahead.bundle.min.js';
		$this->js[] = '/files/js/typeahead/dist/handlebars-v2.0.0.js';

		//       import headroom
		$this->js[] = '/files/js/headroom.js/dist/headroom.min.js';
		$this->js[] ='/files/js/headroom.js/dist/jQuery.headroom.min.js';

		$this->js[] ='/files/js/jquery.confirm.js';

		// custom
		$this->css[] ='/files/css/animate.css'; // animations
		$this->css[] = $theme.'/files/css/style.css?v='.Yii::$app->controller->version;

		// load init & common
		$this->js[] = $theme.'/files/js/functions.js';
		$this->js[] = $theme.'/files/js/init.js';
		$this->js[] = $theme.'/files/js/common.js';

	}
}
