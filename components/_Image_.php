<?php
namespace yii\imagine;
use Yii;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;


class Image extends BaseImage
{
    /**
     * Creates a thumbnail ZoomIn image. The function differs from `\Imagine\Image\ImageInterface::thumbnail()` function that
     * it keeps the aspect ratio of the image.
     * @param string $filename the image file path or path alias.
     * @param integer $width the width in pixels to create the thumbnail
     * @param integer $height the height in pixels to create the thumbnail
     * @param string $mode
     * @return ImageInterface
     */
    public static function thumbnailZoomIn($filename, $width, $height, $mode = ManipulatorInterface::THUMBNAIL_OUTBOUND)
    {
        $img = static::getImagine()->open(Yii::getAlias($filename));
        $box = new Box($width, $height);

        if ((!$box->getWidth() && !$box->getHeight())) {
            return $img->copy();
        }

        $size = $img->getSize();

        $sizeMax = $height < $width ? $width : $height;
        $widthOut = $sizeMax;
        $heightOut = $size->getHeight()*$sizeMax/$size->getWidth();

        $boxOut = new Box($widthOut, $heightOut);
        $img->resize(new Box($widthOut, $heightOut));
        $img = $img->thumbnail($box, $mode);

//        echo "<pre>"; print_r($box); echo "</pre>";
//        echo "<pre>"; print_r($boxOut); echo "</pre>"; die;

        return $img;
    }
}