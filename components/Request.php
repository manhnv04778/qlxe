<?php
/**
 * Created by PhpStorm.
 * User: ABCnet
 * Date: 11/18/14
 * Time: 7:21 PM
 */

namespace app\components;

use Yii;

class Request extends \yii\web\Request
{
    public $noCsrfRoutes = [];

    public function validateCsrfToken($token = null)
    {
        if(
            $this->enableCsrfValidation &&
            in_array(Yii::$app->getUrlManager()->parseRequest($this)[0], $this->noCsrfRoutes)
        ){
            return true;
        }
        return parent::validateCsrfToken($token);
    }
}
