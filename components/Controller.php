<?php
/**
 * Created by PhpStorm.
 * Date: 10/17/14
 * Time: 20:36
 */

namespace app\components;

use app\helpers\MixHelper;
use app\models\Setting;
use app\modules\ticket\models\Ticket;
use app\modules\user\models\ConversationNotice;
use app\modules\user\models\UserFeed;
use app\modules\user\models\UserRequest;
use app\modules\user\models\UserSetting;
use Yii;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;


class Controller extends \yii\web\Controller{

    public $user;
    public $user_add_friend;
    public $countNoty;
    public $notyIds = [];
    public $alias = '';
    public $keywordSearch = '';

    public $version = 5.4;

	public function init()
	{
        parent::init();

        $this->user = Yii::$app->user->identity;
    }

    public function render($view, $params = [])
    {
        return parent::render($view, $params);
    }

} 