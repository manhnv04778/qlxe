<?php
/**
 * Created by PhpStorm.
 * Date: 10/17/14
 * Time: 20:36
 */

namespace app\components;

use yii\helpers\FileHelper;
use yii\web\HttpException;
use Yii;

class Model extends \yii\db\ActiveRecord{

	public function init()
	{
		parent::init();
        $this->loadDefaultValues();
	}

    public function clearDefaultValues(){
        foreach($this->attributes as $k => $v){
            $this->{$k} = null;
        }
    }


    /* EVENTS
    -------------------------------------------------- */
    public function beforeValidate(){
        if (!parent::beforeValidate()) return false;
        // code here


        return true;
    }

    public function afterValidate(){
        // code here

//        if($this->errors){
//            echo "<pre>"; print_r($this->className()); echo "</pre>\n\n";
//            echo "<pre>"; print_r($this->errors); echo "</pre>\n\n";
//            echo "<pre>"; print_r($this->attributes); echo "</pre>\n\n"; die;
//        }



        parent::afterValidate();
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        // code here

        if($this->errors){
            echo "<pre>"; print_r($this->className()); echo "</pre>\n\n";
            echo "<pre>"; print_r($this->errors); echo "</pre>\n\n";
            echo "<pre>"; print_r($this->attributes); echo "</pre>\n\n"; die;
        }


        // before insert
        if($insert){

        }
        // before update
        else{

        }
        return true;
    }

    public function afterSave($insert, $changedAttributes){
        // after insert
        if($insert){

        }
        // after update
        else{

        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete(){
        if(!parent::beforeDelete()) return false;
        // code here

        return true;
    }


    public function afterDelete(){
        // code here

        parent::afterDelete();
    }


    public static function getShortClassName(){
        $function = new \ReflectionClass(static::className());
        return $function->getShortName();
    }

    /**
     * Khởi tạo 1 đối tượng Model
     * @param array $attributes
     * @return static
     */
    public static function getInstance($attributes = []){
        $modelReference = self::className();

        /* @var $model static */
        $model = new $modelReference();
        if($attributes) $model->setAttributes($attributes, false);

        return $model;
    }


    protected static $_models;

    /**
     * Lấy ra đối tượng và cache lại trong request đó
     * @param $condition
     * @param bool|codeNumber $throwException Có throw ra exception hay không ?
     * @return static
     * @throws HttpException
     */
    public static function findOneCache($condition, $throwException = false)
    {
        $conditionHash = md5(json_encode($condition));

        if(isset(self::$_models[self::className()][$conditionHash])) return self::$_models[self::className()][$conditionHash];

        $model = self::findOne($condition);

        if(!$model) {
            if($throwException) throw new HttpException(is_numeric($throwException) ? $throwException : 404);
            return;
        }

        return self::$_models[self::className()][$conditionHash] = $model;
    }




    protected static $_caches = array();

    /**
     * Cache data
     * @param string $method format: namespace\Model::methodName
     * @param int $expire 0 expire forever, n: cache in seconds, -1: clear cache,
     */
    public static function cacheData($method, $expire = 0, $cacheKey = null)
    {

        $cacheKey = $cacheKey ? $cacheKey : $method;
        if($expire == -1) {
            Yii::$app->cache->delete($cacheKey);
            if(isset(self::$_caches[$cacheKey])) unset(self::$_caches[$cacheKey]);
            return false;
        }

        if(!empty(self::$_caches[$cacheKey])) return self::$_caches[$cacheKey];

        self::$_caches[$cacheKey] = Yii::$app->cache->get($cacheKey);


        if(self::$_caches[$cacheKey] === false) {

            list($class, $method) = explode('::', $method);
            $data = $class::$method(false);

            self::$_caches[$cacheKey] = $class::$method(false);
            Yii::$app->cache->set($cacheKey, self::$_caches[$cacheKey], $expire);
        }

        return self::$_caches[$cacheKey];
    }

    /**
     * Clear all Cache: cache engine, assets files
     */
    public static function clearCache()
    {
//        FileHelper::removeDirectory(APP_PATH.'/runtime/cache/');
        Yii::$app->cache->flush();
        FileHelper::deleteDirectory(APP_PATH.'/assets');
    }
} 