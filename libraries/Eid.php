<?php
/**
 * Mobile Detect Library
 * =====================
 *
 * Motto: "Every business should have a mobile detection script to detect mobile readers"
 *
 * Mobile_Detect is a lightweight PHP class for detecting mobile devices (including tablets).
 * It uses the User-Agent string combined with specific HTTP headers to detect the mobile environment.
 *
 * @author      Current authors: Serban Ghita <serbanghita@gmail.com>, Nick Ilyin <nick.ilyin@gmail.com>
 *              Original author: Victor Stanciu <vic.stanciu@gmail.com>
 *
 * @license     Code and contributions have 'MIT License'
 *              More details: https://github.com/serbanghita/Mobile-Detect/blob/master/LICENSE.txt
 *
 * @link        Homepage:     http://mobiledetect.net
 *              GitHub Repo:  https://github.com/serbanghita/Mobile-Detect
 *              Google Code:  http://code.google.com/p/php-mobile-detect/
 *              README:       https://github.com/serbanghita/Mobile-Detect/blob/master/README.md
 *              HOWTO:        https://github.com/serbanghita/Mobile-Detect/wiki/Code-examples
 *
 * @version     2.8.5
 */

namespace app\libraries;

use Curl\Curl;

class Eid{

    const CLIENT_ID     = 'egame_287162';
    const SECRET_ID     = 'bQfa5esKwDXCHJvs5nktUU2lwDlOqR4D';
    const URL_REQUEST_LOGIN = 'http://eid.dev/user/api/login';
    const URL_REQUEST_REGISTER = 'http://eid.dev/user/api/register';
    public function __construct() {

    }

    public function login($params = [])
    {
        $params['hash_key'] = $this->encryptIt(self::CLIENT_ID,self::SECRET_ID);
        $params['secret'] = self::SECRET_ID;
        if(!empty($params['p']))
            $params['p'] = $this->encryptIt($params['p'],self::SECRET_ID);

        $curl = new Curl();
        $data = $curl->post('http://eid.dev/api/user/login',$params);

        return $data;

    }


    public function register($params = [])
    {
        $params['hash_key'] = $this->encryptIt(self::CLIENT_ID,self::SECRET_ID);
        $params['secret'] = self::SECRET_ID;
        if(!empty($params['p']))
            $params['p'] = $this->encryptIt($params['p'],self::SECRET_ID);

        $url = self::URL_REQUEST_REGISTER . "?" . http_build_query($params);
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

        $output=curl_exec($ch);

        curl_close($ch);
        return $output;

    }

    public function encryptIt($q, $cryptKey) {
        $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
        return( $qEncoded );
    }

    public function decryptIt($q, $cryptKey) {
        $qDecoded  = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }
}
