<?php
namespace app\libraries;

class Crypt extends \yii\base\Component
{
	public $key = 'haidm';
	public $iv = MCRYPT_RIJNDAEL_256;


	public function safe_b64encode($string)
	{
		$data = base64_encode($string);
		$data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
		return $data;
	}

	public function safe_b64decode($string)
	{
		$data = str_replace(array('-', '_'), array('+', '/'), $string);
		$mod4 = strlen($data) % 4;
		if($mod4) {
			$data .= substr('====', $mod4);
		}
		return base64_decode($data);
	}

	public function encode($value)
	{
		if(!$value) {
			return false;
		}
		$text = $value;
		$iv_size = mcrypt_get_iv_size($this->iv, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$crypttext = mcrypt_encrypt($this->iv, $this->key, $text, MCRYPT_MODE_ECB, $iv);
		return trim($this->safe_b64encode($crypttext));
	}

	public function decode($value)
	{
		if(!$value) {
			return false;
		}
		$crypttext = $this->safe_b64decode($value);
		$iv_size = mcrypt_get_iv_size($this->iv, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$decrypttext = mcrypt_decrypt($this->iv, $this->key, $crypttext, MCRYPT_MODE_ECB, $iv);
		return trim($decrypttext);
	}

	public function encodeJson(array $value)
	{
		return $this->encode(json_encode($value));
	}

	public function decodeJson($value)
	{
		if(!$value) return null;
		$text = $this->decode($value);
		return $text ? json_decode($text, TRUE) : null;
	}
}