<?php
namespace app\libraries;

/**
 *
$bm = new app\libraries\Benmark();
$bm->getTime();
 *
 */
class Benmark
{

	public $start;
	public $end;
	public $time;

	public function __construct()
	{
		$this->start = microtime(true);

	}

	public function getTime($echo = false)
	{
		$this->end = microtime(true);
		$this->time = round($this->end - $this->start, 3);
		if($echo) {
			echo "<br>\n {$this->time}s <br>\n";
		}
		return $this->time;
	}

}

?>
