<?php

namespace app\libraries;
use Curl\Curl;

class ChargeCardTelco{

    const KEY    = '920130506!@#123';
    const PARTNER_ID    = '920130506';
    const URL_SEND    = 'http://sandbox2.vtcebank.vn/WSCard2010/card.asmx?wsdl';
    const USE_CARD    = 'UseCard';
    const PORT    = '80';

    public function __construct() {

    }

    public function Encrypt($input, $key_seed){
        $input = trim($input);
        $block = mcrypt_get_block_size('tripledes', 'ecb');
        $len = strlen($input);
        $padding = $block - ($len % $block);
        $input .= str_repeat(chr($padding),$padding);
        // generate a 24 byte key from the md5 of the seed
        $key = substr(md5($key_seed),0,24);
        $iv_size = mcrypt_get_iv_size(MCRYPT_TRIPLEDES, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        // encrypt
        $encrypted_data = mcrypt_encrypt(MCRYPT_TRIPLEDES, $key,
            $input, MCRYPT_MODE_ECB, $iv);
        // clean up output and return base64 encoded
        return base64_encode($encrypted_data);
    }

    public function splid_xml($xml,$tab){
        $pieces = explode("<".$tab.">", $xml);
        $pieces1 = explode("</".$tab.">", $pieces[1]);
        return $pieces1[0];
    }

    public function Decrypt($input, $key_seed)
    {
        $input = base64_decode($input);
        $key = substr(md5($key_seed),0,24);
        $text=mcrypt_decrypt(MCRYPT_TRIPLEDES, $key, $input, MCRYPT_MODE_ECB,'12345678');
        $block = mcrypt_get_block_size('tripledes', 'ecb');
        $packing = ord($text{strlen($text) - 1});
        if($packing and ($packing < $block)){
            for($P = strlen($text) - 1; $P >= strlen($text) - $packing; $P--){
                if(ord($text{$P}) != $packing){
                    $packing = 0;
                } } }
        $text = substr($text,0,strlen($text) - $packing);
        return $text;
    }

    public function cardexcute($cardid,$cardcode,$des){
        //$urlpost="http://sandbox2.vtcebank.vn/WSCard2010/card.asmx?wsdl";

        $cardfun="<?xml version=\"1.0\" encoding=\"utf-16\"?>\n" .
            "<CardRequest>\n" .
            "<Function>".$this::USE_CARD."</Function>\n" .
            "<CardID>".$cardid."</CardID>\n" .
            "<CardCode>".$cardcode."</CardCode>\n" .
            "<Description>".$des."</Description>\n" .
            "</CardRequest>";
        $RequestData=$this->Encrypt($cardfun,$this::KEY);
        $urlpar=parse_url($this::URL_SEND);

        $xml_data="<?xml version=\"1.0\" encoding=\"utf-8\"?>"
            ."<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"
            . "<soap:Body>"
            . "<Request xmlns=\"VTCOnline.Card.WebAPI\">"
            . "<PartnerID>".$this::PARTNER_ID."</PartnerID>"
            . "<RequestData>".$RequestData."</RequestData>"
            . "</Request>"
            . "</soap:Body>"
            . "</soap:Envelope>";

        $headers = array(
            "POST ".$urlpar['path']." HTTP/1.1",
            "Host: ".$urlpar['host']."",
            "Content-Type: text/xml; charset=utf-8",
            "SOAPAction: VTCOnline.Card.WebAPI/Request",
            "Content-Length: ".strlen($xml_data)
        );
        $ch = curl_init(); // initialize curl handle
        curl_setopt($ch, CURLOPT_VERBOSE, 1); // set url to post to
        curl_setopt($ch, CURLOPT_PORT, $this::PORT);
        curl_setopt($ch, CURLOPT_URL, $this::URL_SEND); // set url to post to
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40); // times out after 4s
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data); // add POST fields
        curl_setopt($ch, CURLOPT_POST, 1);
        $result = curl_exec($ch); // run the whole process
        return $result;
    }


}
