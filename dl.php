<?php
function forceDownload($file, $name = null)
{
    $name = $name ? $name : basename($file);

    $ext = explode(".", $file);
    switch ($ext[sizeof($ext) - 1]) {
        case 'jar':
            $mime = "application/java-archive";
            break;
        case 'zip':
            $mime = "application/zip";
            break;
        case 'jpeg':
            $mime = "image/jpeg";
            break;
        case 'jpg':
            $mime = "image/jpg";
            break;
        case 'jad':
            $mime = "text/vnd.sun.j2me.app-descriptor";
            break;
        case "gif":
            $mime = "image/gif";
            break;
        case "png":
            $mime = "image/png";
            break;
        case "pdf":
            $mime = "application/pdf";
            break;
        case "txt":
            $mime = "text/plain";
            break;
        case "doc":
            $mime = "application/msword";
            break;
        case "ppt":
            $mime = "application/vnd.ms-powerpoint";
            break;
        case "wbmp":
            $mime = "image/vnd.wap.wbmp";
            break;
        case "wmlc":
            $mime = "application/vnd.wap.wmlc";
            break;
        case "mp4s":
            $mime = "application/mp4";
            break;
        case "ogg":
            $mime = "application/ogg";
            break;
        case "pls":
            $mime = "application/pls+xml";
            break;
        case "asf":
            $mime = "application/vnd.ms-asf";
            break;
        case "swf":
            $mime = "application/x-shockwave-flash";
            break;
        case "mp4":
            $mime = "video/mp4";
            break;
        case "m4a":
            $mime = "audio/mp4";
            break;
        case "m4p":
            $mime = "audio/mp4";
            break;
        case "mp4a":
            $mime = "audio/mp4";
            break;
        case "mp3":
            $mime = "audio/mpeg";
            break;
        case "m3a":
            $mime = "audio/mpeg";
            break;
        case "m2a":
            $mime = "audio/mpeg";
            break;
        case "mp2a":
            $mime = "audio/mpeg";
            break;
        case "mp2":
            $mime = "audio/mpeg";
            break;
        case "mpga":
            $mime = "audio/mpeg";
            break;
        case "wav":
            $mime = "audio/wav";
            break;
        case "m3u":
            $mime = "audio/x-mpegurl";
            break;
        case "bmp":
            $mime = "image/bmp";
            break;
        case "ico":
            $mime = "image/x-icon";
            break;
        case "3gp":
            $mime = "video/3gpp";
            break;
        case "3g2":
            $mime = "video/3gpp2";
            break;
        case "mp4v":
            $mime = "video/mp4";
            break;
        case "mpg4":
            $mime = "video/mp4";
            break;
        case "m2v":
            $mime = "video/mpeg";
            break;
        case "m1v":
            $mime = "video/mpeg";
            break;
        case "mpe":
            $mime = "video/mpeg";
            break;
        case "mpeg":
            $mime = "video/mpeg";
            break;
        case "mpg":
            $mime = "video/mpeg";
            break;
        case "mov":
            $mime = "video/quicktime";
            break;
        case "qt":
            $mime = "video/quicktime";
            break;
        case "avi":
            $mime = "video/x-msvideo";
            break;
        case "midi":
            $mime = "audio/midi";
            break;
        case "mid":
            $mime = "audio/mid";
            break;
        case "amr":
            $mime = "audio/amr";
            break;
        default:
            $mime = "application/force-download";
    }

    header('Content-Description: File Transfer');
    header('Content-Type: ' . $mime);
    header('Content-Disposition: attachment; filename=' . $name);
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
}

$file = 'http://wsodprogrf.bbc.co.uk/vietnamese/dps/2012/10/witn_viet_naked_exhibition_16x9_hi.mp4';
$name = 'video - 0001 - Triển lãm ảnh khỏa thân nam tại Áo.mp4';

$file = $_GET['f'];
$name = $_GET['n'];

forceDownload($file, $name);
