<?php

namespace app\controllers;


use app\helpers\DateTimeHelper;
use app\helpers\MixHelper;
use app\helpers\SmsHelper;
use app\libraries\Eid;
use app\libraries\SendSms;
use app\libraries\simple_html_dom;
use app\models\App;
use app\models\AppExtra;
use app\models\AppHasTag;
use app\models\AuthItem;
use app\models\City;
//use app\models\CityB;

use app\models\College;
use app\models\CtNews;
use app\models\District;
//use app\models\DistrictB;
use app\models\Tag;
use app\modules\exam\models\Clas;
use app\modules\exam\models\Rank;
use app\modules\exam\models\Round;
use app\modules\exam\models\School;
//use app\modules\exam\models\Schools;
use app\modules\exam\models\Statistic;
use app\modules\exam\models\UserExam;
use app\modules\exam\models\UserRegister;
use app\modules\user\models\forms\RegisterForm;
use app\modules\user\models\User;
use app\modules\user\models\UserExtra;
use app\modules\user\models\UserFollowUser;
use app\modules\user\models\Wallet;
use Curl\Curl;
use WideImage\WideImage;
use Yii;
use app\components\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Cookie;

class DevController extends Controller
{


    public function actionAddPermission(){

        $params = Yii::$app->params['permission'];
        foreach($params as $index => $p){
            $authItem = new AuthItem();
            $authItem->name = $p['name'];
            $authItem->type = 2;
            $authItem->description = $p['description'];
            $authItem->group = $p['group'];
            $authItem->group_name = $p['group_name'];
            $authItem->created_at = time();
            $authItem->insert();
        }

    }

    public function actionUpdateDistrict(){
        $districts = District::findAll(['name_unicode' => null]);
        if($districts){
            foreach($districts as $item){
                $item->name_unicode = str_replace('-',' ',$item->alias);
                $item->update();
            }
        }
    }

}
