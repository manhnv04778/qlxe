<?php
namespace app\widgets;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * \Yii::$app->getSession()->setFlash('error', 'This is the message');
 * \Yii::$app->getSession()->setFlash('success', 'This is the message');
 * \Yii::$app->getSession()->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * \Yii::$app->getSession()->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 */
class Growl extends \yii\base\Widget
{

    public $alertTypes = [
        'danger',
        'success',
        'info',
        'warning'
    ];

    public function init()
    {
        parent::init();

        $session = \Yii::$app->getSession();
        $flashes = $session->getAllFlashes();

        foreach ($flashes as $type => $data) {

            if (in_array($type, $this->alertTypes)) {

                $data = (array) $data;
                foreach ($data as $message) {
                    \Yii::$app->view->registerJs("growl('{$message}', '{$type}');");
                }
                $session->removeFlash($type);
            }
        }
    }
}
