<?php
namespace app\widgets;
use yii\web\View;

/**
 * Thực hiện các hành động tương ứng khi setFlash
 * Các hành động
 * clearLocalStorage (require store.js): xoá local storage ở client. VD: \Yii::$app->getSession()->setFlash('clearLocalStorage', 'storageKey');
 *
 *
 * @package app\widgets
 */
class FlashAction extends \yii\base\Widget
{
    public function init()
    {
        parent::init();

        $session = \Yii::$app->getSession();
        $flashes = $session->getAllFlashes();

        foreach ($flashes as $type => $data) {

            if ($type == 'clearLocalStorage') {
                \Yii::$app->view->registerJs("
                        var clearLocalStorage = true;
                    ", View::POS_BEGIN);

                $data = (array) $data;
                foreach ($data as $value) {
                    \Yii::$app->view->registerJs("

                            console.log('Flash action')
                        if(store.enabled){
                            console.log('clear store')
                            store.remove('{$value}');
                        }
                    ");
                }
            }


//            if ($type == 'updateAvatar') {
//                \Yii::$app->view->registerJs("
//                        $.post('/user/user/ajax-update-avatar', {avatar: '{$data}'});
//                    ", View::POS_READY);
//            }

            $session->removeFlash($type);
        }
    }
}
