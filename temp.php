<?php

namespace app\modules\post\controllers;

use app\helpers\AppHelper;
use app\libraries\PicasaClient;
use app\models\Temp;
use Yii;
use app\modules\post\models\Post;
use app\modules\post\models\PostSearch;
use app\modules\post\components\Controller;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\rbac\AuthManager;
use yii\filters\AccessControl;
use app\helpers\DateTimeHelper;
use app\modules\post\models\elasticsearch\PostElasticsearch;
use yii\helpers\StringHelper;
use yii\helpers\FileHelper;
use WideImage\WideImage;
use app\helpers\MixHelper;
use yii\helpers\Url;
use app\models\Image;
use app\modules\post\models\PostTag;
use app\modules\post\models\PostHasTag;
use app\libraries\simple_html_dom;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            // http://www.yiiframework.com/doc-2.0/yii-filters-accesscontrol.html
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin', 'create', 'update', 'index', 'view'], // chỉ những action này mới được check access, những actions khác sẽ bị bỏ qua.
                'rules' => [
//                    [
//                        'actions' => ['register'],
//                        'allow' => true,
//                        'roles' => ['?'], // chỉ dành cho user chưa đăng nhập
//                    ],
//                    [
//                        'actions' => ['create', 'update'],
//                        'allow' => true,
//                        'roles' => ['@'], // chỉ dành cho user đã đăng nhậpdeny. nếu chưa đăng nhập thì redirect sang trang login.
//                    ],
//                    [
//                        'actions' => ['re-index'],
//                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
//                        'matchCallback' => function ($rule, $action){
//                            return Yii::$app->user->can(AuthManager::ROLE_ADMIN);
//                        }
//                    ],
                    [
                        'actions' => ['create', 'admin', 'update'],
                        'allow' => true, // nếu là true thì mới áp dụng rule. còn nếu là false hoặc ko set thì rule trả về true vẫn
                        'matchCallback' => function ($rule, $action){
                            return Yii::$app->user->can(AuthManager::ROLE_AUTHOR);
                        }
                    ],
                ],
            ],
        ];
    }


    public function init(){
        parent::init();
    }



    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $this->catChildSelected = 'admin';

        $searchModel = new PostElasticsearch();

        // Check if there is an Editable ajax request
        if (Yii::$app->request->post('hasEditable')) {
            $data = current($_POST['GrammarElasticsearch']);
            $id = Yii::$app->request->post('editableKey');

            Post::updateAll($data, ['id' => $id]);
            PostElasticsearch::updateAll($data, ['id' => $id]);
            $out = Json::encode(['output'=> current($data), 'message'=>'']);

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            echo $out;
            return;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $editor html5|markdown
     * @return mixed
     */
    public function actionCreate($editor = 'markdown')
    {
//        $postImageTemp = MixHelper::cookieGet('img-temp-post');
//        echo "<pre>"; print_r($postImageTemp); echo "</pre>"; die;
        $this->catChildSelected = 'create';

        /* @var $model Post */
        $model = new Post();
        $model->editor = $editor;
        $model->scenario = 'create';

        // load from temp
        $temp = Temp::find()->where([
            'user_id' => $this->user->id,
            'object' => 'post',
            'action' => 'create',
        ])->one();
        if($temp && $temp->data){
            $data = json_decode($temp->data, true);
            $model->load($data, '');

            if($model->image_upload){
                $imageView = $model->image_upload;
            }
        }


        // image for view: null or post data
        $imageView = !empty($imageView) ? $imageView : null;

        if($post = Yii::$app->request->post()){
            $model->load($post);

            $model->user_id = Yii::$app->user->id;
            $model->alias = StringHelper::toSEOString($model->title);

            $imageView = $model->image_upload; // imageView = post data

            if($model->validate()){


                $picasaConf = AppHelper::getPicasaConfig();
                $pc = new PicasaClient($picasaConf['accessToken'], $picasaConf['account']);


                // save source image
                if($model->image_upload) {
                    if (substr($model->image_upload, 0, 4) == 'http') {
                        $model->image_source = $model->image_upload;
                    }

                    $imageName = $picasaConf['fileNamePrefix'] . $model->alias;
                    $data = $pc->uploadPhoto($model->image_upload, $imageName);
                    $imagePicasa = [
                        'email' => $data['userEmail'],
                        'album_id' => $data['albumId'],
                        'photo_id' => $data['photoId'],
                        'photo_url' => $data['photoUrl'],
                    ];

                    $model->image_picasa = json_encode($imagePicasa);
                }

                $model->insert();




                // save image in content
                if($model->editor == 'html5'){
                    $html = new simple_html_dom($model->content);
                    $baseUrl = Url::base(true);

                    $imageSourceContent = []; // source image in content
                    foreach($html->find('img') as $img){
                        $imageSource = $img->src;

                        // ignore save internal images
                        if(strpos($img->src, $baseUrl) !== false || substr($img->src, 0, 1) == '/') continue;

                        // check duplicate image url
                        if(in_array($img->src, $imageSourceContent)){
                            $savedPath = $model->getImageContentPath('image');

                            $imageId = array_search($img->src, $imageSourceContent);
                            $imageName = Image::find()->select(['image'])->where(['id' => $imageId])->scalar();
                            if($imageName){
                                $img->src = $baseUrl.'/'.$savedPath."/{$imageName}.jpg";
                                continue;
                            }
                        }

                        $imageName = $model->alias.'_'.uniqid();

                        $wideImageLoad = WideImage::load($img->src);
                        foreach($postConf['content']['thumbs'] as $thumbType => $conf){

                            $savedPath = $model->getImageContentPath($thumbType);
                            FileHelper::createDirectory($savedPath);


                            $imagePath = $savedPath."/{$imageName}.jpg";

                            list($w, $h) = explode('x', $conf['size']);
                            $wideImage = $wideImageLoad;
                            $wideImage = $wideImage->resize($w, $h, 'outside', $conf['scale']);
                            $wideImage = $wideImage->resizeCanvas($w, $h,'center','center', null, 'down');

                            // add watermark
                            if(!empty($conf['watermark'])){
                                $watermark = WideImage::load($conf['watermark']['file']);
                                $wideImage = $wideImage->merge($watermark, $conf['watermark']['left'], $conf['watermark']['top']);
                            }

                            $wideImage->saveToFile($imagePath, $conf['quality']);

                            // gán ảnh đã lưu thay cho ảnh lúc đầu
                            if($thumbType == 'image'){
                                $img->src = $baseUrl.'/'.$imagePath;
                            }
                        }

                        // insert to DB
                        $image = new Image();
                        $image->post_id = $model->id;
                        $image->user_id = $this->user->id;
                        $image->image = $imageName;
                        $image->source = $imageSource;
                        $image->insert();

                        if(substr($img->src, 0, 4) == 'http') {
                            $imageSourceContent[$image->id] = $imageSource;
                        }

                    }

                    if($imageSourceContent) $model->image_source_content = json_encode($imageSourceContent);


                    $model->content = $html->save();
                    $html->clear();

                    $model->content = Post::formatContentHtml($model->content);
                    $model->update();
                }
                elseif($model->editor == 'markdown'){

                    $contentHtml = StringHelper::parsedown($model->content);

                    $html = new simple_html_dom($contentHtml);
                    $baseUrl = Url::base(true);

                    $imageSourceContent = []; // source image in content
                    foreach($html->find('img') as $img){
                        // ignore save internal images
                        if(strpos($img->src, $baseUrl) !== false || substr($img->src, 0, 1) == '/' || in_array($img->src, $imageSourceContent)) continue;

                        $imageName = $model->alias.'_'.uniqid();

                        $wideImageLoad = WideImage::load($img->src);
                        foreach($postConf['content']['thumbs'] as $thumbType => $conf){

                            $savedPath = $model->getImageContentPath($thumbType);
                            FileHelper::createDirectory($savedPath);


                            $imagePath = $savedPath."/{$imageName}.jpg";

                            list($w, $h) = explode('x', $conf['size']);
                            $wideImage = $wideImageLoad;
                            $wideImage = $wideImage->resize($w, $h, 'outside', $conf['scale']);
                            $wideImage = $wideImage->resizeCanvas($w, $h,'center','center', null, 'down');

                            // add watermark
                            if(!empty($conf['watermark'])){
                                $watermark = WideImage::load($conf['watermark']['file']);
                                $wideImage = $wideImage->merge($watermark, $conf['watermark']['left'], $conf['watermark']['top']);
                            }

                            $wideImage->saveToFile($imagePath, $conf['quality']);

                            // gán ảnh đã lưu thay cho ảnh lúc đầu
                            if($thumbType == 'image'){
                                $model->content = str_replace($img->src, $baseUrl.'/'.$imagePath, $model->content);
                            }
                        }

                        // insert to DB
                        $image = new Image();
                        $image->post_id = $model->id;
                        $image->user_id = $this->user->id;
                        $image->image = $imageName;
                        $image->source = $img->src;
                        $image->insert();

                        if(substr($img->src, 0, 4) == 'http') {
                            $imageSourceContent[$image->id] = $img->src;
                        }

                    }

                    if($imageSourceContent) $model->image_source_content = json_encode($imageSourceContent);

                    $html->clear();
                    $model->update();

                }



                // save image by file ajax upload
                if($postImageTemp = MixHelper::cookieGet('img-temp-post')){

                    // to replace image temp => posted image
                    $postImageTempMapping = [];

                    foreach($postImageTemp as $i => $tempPath){
                        if(!file_exists($tempPath)) continue;

                        $imageName = $model->alias.'_'.uniqid();
                        $wideImageLoad = WideImage::load($tempPath);
                        foreach($postConf['content']['thumbs'] as $size => $conf) {

                            $savedPath = $model->getImageContentPath($size);
                            FileHelper::createDirectory($savedPath);

                            $imagePath = $savedPath . "/{$imageName}.jpg";

                            list($w, $h) = explode('x', $conf['size']);
                            $wideImage = $wideImageLoad;
                            $wideImage = $wideImage->resize($w, $h, 'outside', $conf['scale']);
                            $wideImage = $wideImage->resizeCanvas($w, $h, 'center', 'center', null, 'down');

                            // add watermark
                            if (!empty($conf['watermark'])) {
                                $watermark = WideImage::load($conf['watermark']['file']);
                                $wideImage = $wideImage->merge($watermark, $conf['watermark']['left'], $conf['watermark']['top']);
                            }

                            $wideImage->saveToFile($imagePath, $conf['quality']);
                        }
                        $postImageTempMapping[Url::home().$tempPath] = Url::home().$imagePath;

                        // insert to DB
                        $image = new Image();
                        $image->post_id = $model->id;
                        $image->user_id = $this->user->id;
                        $image->image = $imageName;
                        $image->insert();

                        // delete image in temp
                        FileHelper::deleteFile($tempPath);

                    }

                    MixHelper::cookieSet('img-temp-post');


                    // replace image temp => posted image
                    $model->content = str_replace(array_keys($postImageTempMapping), array_values($postImageTempMapping), $model->content);
                    $model->updateAttributes(['content' => $model->content]);

                }




                // save tag
                //PostHasTag::deleteAll(['post_id' => $model->id]);

                if($model->tag){
                    $tagInsertData = [];

                    $tagAliases = [];
                    $tagIds = [];
                    foreach(explode(',', $model->tag) as $sTag){
                        $sTag = StringHelper::mb_ucfirst_utf8($sTag);
                        $sTagAlias = StringHelper::toSEOString($sTag);

                        if(in_array($sTagAlias, $tagAliases)) continue;
                        $tagAliases[] = $sTagAlias;

                        $postTag = PostTag::find()->where(['alias' => $sTagAlias])->one();
                        if(!$postTag){
                            $postTag = new PostTag();
                            $postTag->name = $sTag;
                            $postTag->alias = $sTagAlias;
                            $postTag->verified = $this->user->isMod ? 1 : 0;
                            $postTag->save();
                        }else{
                            if(!$postTag->verified && $this->user->isMod){
                                $postTag->verified = 1;
                                $postTag->update();
                            }
                        }

                        $tagInsertData[] = [$model->id, $postTag->id];
                        $tagIds[] = $postTag->id;
                    }
                    Yii::$app->db->createCommand()->batchInsert('post_has_tag', ['post_id', 'tag_id'], $tagInsertData)->execute();
                    PostTag::updatePostCount($tagIds);
                    $model->updateAttributes(['tag_count' => count($tagIds)]);

                }

                Yii::$app->session->setFlash('success', Yii::t('post','Create Post successful'));
                Yii::$app->session->setFlash('clearLocalStorage', 'formPost');

                PostElasticsearch::saveDocument($model->id);

                return $this->redirect(['update', 'id' => $model->id]);

            }

        }


        return $this->render('create', [
            'model' => $model,
            'imageView' => $imageView,
        ]);

    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param $editor null|html5|markdown
     * @return mixed
     */
    public function actionUpdate($id, $editor = null)
    {
        $this->catChildSelected = 'create';

        $model = Post::findOneCache($id);

        $model->editor = in_array($editor, ['html5','markdown']) ? $editor : $model->editor;

        // init form fields
        $model->tag = implode(',', PostTag::getAllTagsByPostId($model->id));

        // image for view: saved image url or post data
        $imageView = $model->getImageUrl().'?'.uniqid(); // imageView = saved image url


        if($post = Yii::$app->request->post()) {
            $model->load($post);

            $model->alias = StringHelper::toSEOString($model->title);

            if($model->image_upload) $imageView = $model->image_upload; // imageView = post data

            if($model->validate()){

                $postConf = \Yii::$app->params['post'];

                // update cover
                if($model->image_upload){
                    // store source image
                    if(substr($model->image_upload, 0, 4) == 'http'){
                        $model->image_source = $model->image_upload;
                    }
                    $model->image = $model->alias; // reset image name = alias

                    // save cover thumbs
                    $savedPath = $model->getImagePath();

                    // delete image cover directory
                    FileHelper::removeDirectory($savedPath);

                    FileHelper::createDirectory($savedPath);

                    $wideImageLoad = WideImage::load($model->image_upload);
                    foreach($postConf['image']['thumbs'] as $size => $conf){
                        $imagePath = $savedPath."/{$model->image}_{$size}.jpg";

                        list($w, $h) = explode('x', $size);
                        $wideImage = $wideImageLoad;
                        $wideImage = $wideImage->resize($w, $h, 'outside', $conf['scale']);
                        $wideImage = $wideImage->resizeCanvas($w, $h,'center','center', null, 'down');
                        $wideImage->saveToFile($imagePath, $conf['quality']);
                    }
                }


                // save image in content
                if($model->editor == 'html5') {
                    $html = new simple_html_dom();
                    $html->load($model->content, true, false);
                    $baseUrl = Url::base(true);

                    $imageSourceContent = $model->image_source_content ? json_decode($model->image_source_content, true) : []; // source image in content

                    foreach ($html->find('img') as $img) {
                        $imageSource = $img->src;

                        // ignore save internal images
                        if (strpos($img->src, $baseUrl) !== false || substr($img->src, 0, 1) == '/') continue;

                        // check duplicate image url
                        if(in_array($img->src, $imageSourceContent)){
                            $savedPath = $model->getImageContentPath('image');

                            $imageId = array_search($img->src, $imageSourceContent);
                            $imageName = Image::find()->select(['image'])->where(['id' => $imageId])->scalar();
                            if($imageName){
                                $img->src = $baseUrl.'/'.$savedPath."/{$imageName}.jpg";
                                continue;
                            }
                        }


                        $imageName = $model->alias . '_' . uniqid();

                        $wideImageLoad = WideImage::load($img->src);
                        foreach ($postConf['content']['thumbs'] as $thumbType => $conf) {

                            $savedPath = $model->getImageContentPath($thumbType);
                            FileHelper::createDirectory($savedPath);

                            $imagePath = $savedPath . "/{$imageName}.jpg";

                            list($w, $h) = explode('x', $conf['size']);
                            $wideImage = $wideImageLoad;
                            $wideImage = $wideImage->resize($w, $h, 'outside', $conf['scale']);
                            $wideImage = $wideImage->resizeCanvas($w, $h, 'center', 'center', null, 'down');

                            // add watermark
                            if (!empty($conf['watermark'])) {
                                $watermark = WideImage::load($conf['watermark']['file']);
                                $wideImage = $wideImage->merge($watermark, $conf['watermark']['left'], $conf['watermark']['top']);
                            }

                            $wideImage->saveToFile($imagePath, $conf['quality']);

                            // gán ảnh đã lưu thay cho ảnh lúc đầu
                            if ($thumbType == 'image') {
                                $img->src = $baseUrl.'/'.$imagePath;
                                $img->style = null;
                                $img->setAttribute('data-filename', null);
                            }

                        }

                        // insert to DB
                        $image = new Image();
                        $image->post_id = $model->id;
                        $image->user_id = $this->user->id;
                        $image->image = $imageName;
                        $image->source = $imageSource;
                        $image->insert();

                        if(substr($img->src, 0, 4) == 'http') {
                            $imageSourceContent[$image->id] = $imageSource;
                        }

                    }

                    if ($imageSourceContent) $model->image_source_content = json_encode($imageSourceContent);


                    $model->content = $html->save();
                    $html->clear();

                    $model->content = Post::formatContentHtml($model->content);
                    $model->update();                }

                elseif($model->editor == 'markdown'){

                    $contentHtml = StringHelper::parsedown($model->content);

                    $html = new simple_html_dom($contentHtml);
                    $baseUrl = Url::base(true);

                    $imageSourceContent = $model->image_source_content ? json_decode($model->image_source_content, true) : []; // source image in content

                    foreach($html->find('img') as $img){
                        // ignore save internal images
                        if(strpos($img->src, $baseUrl) !== false || substr($img->src, 0, 1) == '/') continue;

                        // check duplicate image url
                        if(in_array($img->src, $imageSourceContent)){
                            $savedPath = $model->getImageContentPath('image');

                            $imageId = array_search($img->src, $imageSourceContent);
                            $imageName = Image::find()->select(['image'])->where(['id' => $imageId])->scalar();
                            if($imageName){
                                $model->content = str_replace($img->src, $baseUrl.'/'.$savedPath."/{$imageName}.jpg", $model->content);
                                continue;
                            }
                        }


                        $imageName = $model->image.'_'.uniqid();

                        $wideImageLoad = WideImage::load($img->src);
                        foreach($postConf['content']['thumbs'] as $thumbType => $conf){

                            $savedPath = $model->getImageContentPath($thumbType);
                            FileHelper::createDirectory($savedPath);


                            $imagePath = $savedPath."/{$imageName}.jpg";

                            list($w, $h) = explode('x', $conf['size']);
                            $wideImage = $wideImageLoad;
                            $wideImage = $wideImage->resize($w, $h, 'outside', $conf['scale']);
                            $wideImage = $wideImage->resizeCanvas($w, $h,'center','center', null, 'down');

                            // add watermark
                            if(!empty($conf['watermark'])){
                                $watermark = WideImage::load($conf['watermark']['file']);
                                $wideImage = $wideImage->merge($watermark, $conf['watermark']['left'], $conf['watermark']['top']);
                            }

                            $wideImage->saveToFile($imagePath, $conf['quality']);

                            // gán ảnh đã lưu thay cho ảnh lúc đầu
                            if($thumbType == 'image'){
                                $model->content = str_replace($img->src, $baseUrl.'/'.$imagePath, $model->content);
                            }
                        }

                        // insert to DB
                        $image = new Image();
                        $image->post_id = $model->id;
                        $image->user_id = $this->user->id;
                        $image->image = $imageName;
                        $image->source = $img->src;
                        $image->insert();

                        if(substr($img->src, 0, 4) == 'http') {
                            $imageSourceContent[$image->id] = $img->src;
                        }

                    }

                    if($imageSourceContent) $model->image_source_content = json_encode($imageSourceContent);

                    $html->clear();
                    $model->update();

                }

//                echo "<pre>"; print_r($model->attributes); echo "</pre>"; die;
                // save tag
                PostHasTag::deleteAll(['post_id' => $model->id]);
                if($model->tag){
                    $tagInsertData = [];

                    $tagAliases = [];
                    $tagIds = [];
                    foreach(explode(',', $model->tag) as $sTag){
                        $sTag = StringHelper::mb_ucfirst_utf8($sTag);
                        $sTagAlias = StringHelper::toSEOString($sTag);

                        if(in_array($sTagAlias, $tagAliases)) continue;
                        $tagAliases[] = $sTagAlias;

                        $postTag = PostTag::find()->where(['alias' => $sTagAlias])->one();
                        if(!$postTag){
                            $postTag = new PostTag();
                            $postTag->name = $sTag;
                            $postTag->alias = $sTagAlias;
                            $postTag->verified = $this->user->isMod ? 1 : 0;
                            $postTag->save();
                        }else{
                            if(!$postTag->verified && $this->user->isMod){
                                $postTag->verified = 1;
                                $postTag->update();
                            }
                        }


                        $tagInsertData[] = [$model->id, $postTag->id];
                        $tagIds[] = $postTag->id;
                    }
                    Yii::$app->db->createCommand()->batchInsert('post_has_tag', ['post_id', 'tag_id'], $tagInsertData)->execute();
                    PostTag::updatePostCount($tagIds);
                    $model->updateAttributes(['tag_count' => count($tagIds)]);

                }


                Yii::$app->session->setFlash('success', Yii::t('post','Update Post successful'));

                $model->update();
                PostElasticsearch::saveDocument($model->id);


                return $this->refresh();

            }
        }


        return $this->render('update', [
            'model' => $model,
            'imageView' => $imageView,
        ]);

    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        $post = Post::findOneCache($id, true);

        return $this->render('view', [
            'post' => $post,
        ]);
    }


    /**
     * Delete post.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $redirect = null)
    {
        $redirect = $redirect ? $redirect : Url::to(['/post/post/admin']);

        PostElasticsearch::deleteAll(['id' => $id]);


        $post = Post::findOne($id);
        if(!$post) throw new BadRequestHttpException();

        $post->delete();

        FileHelper::deleteDirectory($post->getImagePath(), true);

        Yii::$app->session->setFlash('success', Yii::t('post','Delete post done'));

        return $this->redirect($redirect);
    }

    /**
     * @return string
     */
    public function actionIndex($cat = null)
    {
        $dataProvider = PostElasticsearch::getInstance()->search([
            'status' => 'enable',
            'limit' => Yii::$app->params['post']['pageSize']
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @param string $type relevance|votes|answers|newest|unanswered
     * @return string
     */
    public function actionSearch($keyword = null, $type = 'relevance', $accepted = null)
    {
        $this->navMainSelected = 'search';

        //relevance|votes|answers|newest
        $types = [
            'relevance' => [
                'name' => Yii::t('question','Relevance'),
                'desc' => Yii::t('question','Search results with best match to search terms'),
            ],
            'votes' => [
                'name' => Yii::t('question','Votes'),
                'desc' => Yii::t('question','Highest voted search results'),
            ],
            'answers' => [
                'name' => Yii::t('question','Answers'),
                'desc' => Yii::t('question','Highest answer count search results'),
            ],
            'newest' => [
                'name' => Yii::t('question','Newest'),
                'desc' => Yii::t('question','Newest search results'),
            ],
            'unanswered' => [
                'name' => Yii::t('question','Unanswered'),
                'desc' => Yii::t('question','Questions that have no answers'),
            ],
        ];

        // validate type && $interval
        if(!in_array($type, array_keys($types))){
            return $this->redirect(['/question/site/search', 'keyword' => $keyword]);
        }


        $dataProvider = null;

        if($keyword){
            $query = QuestionElasticsearch::find();
            $query->where(['status' => 'enable']);

            $query->query([
                "match" => [
                    "title" => $keyword
                ]
            ]);

            if(!is_null($accepted)){
                if($accepted == 1){
                    $query->filter([
                        'range' => [
                            'answer_id' => ['gt' => 0]
                        ]
                    ]);
                }elseif($accepted == 0){
                    $query->andWhere(['answer_id' => null]);
                }
            }

            // relevance|votes|answers|newest|unanswered
            if ($type == 'votes') {
                $query->orderBy(['vote_point' => SORT_DESC]);
            }
            elseif ($type == 'answers') {
                $query->orderBy(['answer_count' => SORT_DESC]);

            } elseif ($type == 'newest') {
                $query->orderBy(['created' => SORT_DESC]);

            } elseif ($type == 'unanswered') {
                $query->andWhere(['answer_count' => 0]);
                $query->orderBy(['created' => SORT_DESC]);
            }

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'defaultPageSize' => Yii::$app->params['question']['pageSize'],
                ],
            ]);
        }



        return $this->render('search', [
            'type' => $type,
            'types' => $types,
            'keyword' => $keyword,
            'accepted' => $accepted,
            'dataProvider' => $dataProvider,
        ]);
    }


}
