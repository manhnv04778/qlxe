<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
/* @var $userEmail app\modules\user\models\UserEmail */

//$verifiedLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/user/email-verified-token', 'token' => $userEmail->verified_token]);
$verifiedLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/user/email-verified-token', 'token' => $user->verify_token]);
//if(YII_ENV_DEV) echo "<pre>"; print_r($verifiedLink); echo "</pre>"; die;
?>

<b style="font-size: 15px;">Xin chào <?= Html::encode($user->name) ?></b>,

<h4>Cám ơn bạn đã đăng ký tài khoản tại Chinhphucvumon.vn</h4>

Click vào link dưới đây để xác thực tài khoản của bạn:

<?= Html::a(Html::encode($verifiedLink), $verifiedLink) ?>
