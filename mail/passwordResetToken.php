<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/user/reset-password-token']);
//if(YII_ENV_DEV) echo "<pre>"; print_r($resetLink); echo "</pre>"; die;
?>

<?=Yii::t('app', 'Xin chào , Kích vào liên kết bên dưới để làm mới mật khẩu');?>
<br/>
<?= Html::a(Html::encode($resetLink), $resetLink) ?>
