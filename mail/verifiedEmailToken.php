<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
/* @var $userEmail app\modules\user\models\UserEmail */

$verifiedLink = Yii::$app->urlManager->createAbsoluteUrl(['/user/user/email-verified-token', 'token' => $userEmail->verified_token]);
//if(YII_ENV_DEV) echo "<pre>"; print_r($verifiedLink); echo "</pre>"; die;
?>

Hello <?= Html::encode($user->name) ?>,

Follow the link below to verify email <?=$userEmail->email?>:

<?= Html::a(Html::encode($verifiedLink), $verifiedLink) ?>
