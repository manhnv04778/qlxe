<?php
use app\helpers\DateTimeHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
/* @var $userEmail app\modules\user\models\UserEmail */

?>

Xin chào <span style="color: #3767B9"><?=$full_name?></span>,
<br/ >Bạn đã nhận được lịch đưa đón cán bộ đi công tác.
Mời bạn đến nhận xe vào lúc <?=!empty($document->date_from_car) ? DateTimeHelper::getDateTime($model->date_from_car, 'H:i d/m/Y') : ''?>
Tên xe: <?=$car->name ?>, Biển kiểm soát :<?=$car->code?>
<br/ >Đơn vị công tác: <?=$document->city->name;?>
<br />Địa điểm đón tại: <?=$document->place_user?>
<br/ >Thời gian xe đón cán bộ: <?=(!empty($document->date_from_user)) ? DateTimeHelper::getDateTime($document->date_from_user, 'H:i d/m/Y') : ''?>
<br/ >Thời gian xe trả cán bộ: <?=(!empty($document->date_to_user)) ? DateTimeHelper::getDateTime($document->date_to_user, 'H:i d/m/Y') : ''?>
<br/ >Danh sách cán bộ đi công tác:<br /><br/>
<?php if(!empty($list_user)):?>
<table border="1" style="border-collapse: collapse; width: 100%;">
    <tr style="background-color: #3767B9; color: #ffffff">
        <th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd; text-align:center">STT</th>
        <th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd; text-align:center">Họ và tên</th>
        <th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd; text-align:center">Chức danh</th>
        <th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd; text-align:center">Đơn vị</th>
        <th style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd; text-align:center">Điện thoại</th>
    </tr>
    <?php
        $count = 1;
        foreach($list_user as $item):
        ?>
            <tr>
                <td style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd; text-align: center"><?=$count++?></td>
                <td style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;"><?=$item->user_name; ?></td>
                <td style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;"><?=$item->position ?></td>
                <td style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;"><?=$item->department ?></td>
                <td style="padding: 8px; text-align: left; border-bottom: 1px solid #ddd;"><?=$item->phone ?></td>
            </tr>
    <?php
        endforeach;
    ?>
</table>
<?php endif;?>
<br/>
Thông báo để các cán bộ được biết và thu xếp công việc tới đúng thời gian, địa điểm đã nêu ở trên.
<br />
Xin cám ơn!

