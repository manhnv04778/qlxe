<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */

$homeLink = Url::home(true);
?>

Hello <?= Html::encode($user->name) ?>,

You have registered successfully!

Goto <?= Html::a(Html::encode($homeLink), $homeLink) ?> now.

