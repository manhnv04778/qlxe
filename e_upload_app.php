<?php
/**
 *
 * Handle upload app ajax
 * @author LeeIT
 * @package Upload
 * @version 1.0
 *
 */

// Path folder upload.
$rootpath ='upload/file_app/';

if( !file_exists( $rootpath ) ){
	mkdir( $rootpath );
}

// Rule file upload.
$valid_exts = array( 'zip' );

$result = array();

if ( 'POST' == $_SERVER['REQUEST_METHOD']  ) {  

	if( ! empty( $_FILES['file'] ) ) {

            //upload multi file
            $count = count( $_FILES['file']['name'] );

            for($i = 0; $i < $count; $i++){

				// get uploaded file extension
				$ext = strtolower( pathinfo( $_FILES['file']['name'][$i], PATHINFO_EXTENSION ) );
				
				$filename = str_replace('.zip', '', $_FILES['file']['name'][$i] );
				
				// looking for format and size validity
				if ( in_array( $ext, $valid_exts ) ) {
					$path = $rootpath . uniqid(). '.' .$ext;

					// move uploaded file from temp to uploads directory
					if ( move_uploaded_file( $_FILES['file']['tmp_name'][$i], $path ) ) {
	                    $result['error'][$i] = 'no';
	                    $result['data'][$i] = $path;
	              
	                    $unZip = new ZipArchive;
	                    $zipOpen = $unZip->open( $path );
	                    $result['extra'][$i] =  $rootpath . trim( $unZip->getNameIndex(0), '/' );
	                    if( true == $zipOpen){
	                    	$unZip->extractTo( $rootpath );
  							$unZip->close();
	                    }
					}

				} else {
						$result['error'] = 'Invalid file!';
				}	
            }
	} else {
		$result['error'] = 'File not uploaded!';
	}
} else {
	$result['error'] = 'Bad request!';
}

echo json_encode( $result );

