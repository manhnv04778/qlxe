<?php
/**
 * http://www.yiiframework.com/doc-2.0/guide-runtime-routing.html
 */

return [
//	'defaultRoute' => 'main/index',
//	'catchAll' => 'site/offline',

//	['class' => 'yii\rest\UrlRule', 'controller' => 'user'],
//	'PUT,PATCH users/<id>' => 'user/update',
//	'DELETE users/<id>' => 'user/delete',
//	'GET,HEAD users/<id>' => 'user/view',
//	'POST users' => 'user/create',
//	'GET,HEAD users' => 'user/index',
//	'users/<id>' => 'user/options',
//	'users' => 'user/options',

    'trang-chu' => 'frontend/document/index',
    'dang-nhap' => 'frontend/site/login',
    'dang-xuat' => 'frontend/site/logout',

];