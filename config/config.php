<?php

// load modules
$modules = [
    'user' => [
        'class' => 'app\modules\user\Module',
    ],
    'admin' => [
        'class' => 'app\modules\admin\Module',
    ],
    'frontend' => [
        'class' => 'app\modules\frontend\Module',
    ],
];

// load params & rewrite from modules
$params = require(__DIR__ . '/params.php');
$rewrite = [];
foreach(array_keys($modules) as $module){
    $params = array_merge($params, require(dirname(__DIR__). "/modules/{$module}/config/params.php"));
    $rewrite = array_merge($rewrite, require(dirname(__DIR__). "/modules/{$module}/config/rewrite.php"));
}
$rewrite = array_merge($rewrite, require(__DIR__ . '/rewrite.php'));

// add more modules
$modules['gridview'] = [
	'class' => '\kartik\grid\Module',
];


$modules['dynagrid'] = [
	'class'=>'\kartik\dynagrid\Module',
	'cookieSettings' => [
		'httpOnly'=>true,
		'expire'=>time() + 3600*24*365
	],
	'defaultTheme' => 'panel-primary',
	'defaultPageSize' => 20,
	'minPageSize' => 1,
	'maxPageSize' => 500,
	'themeConfig' => [
		'panel-default'=>['panel'=>['type'=>'default','before'=>'']],
		'panel-primary'=>['panel'=>['type'=>'primary','before'=>'']],
		'panel-info'=>['panel'=>['type'=>'info','before'=>'']],
		'panel-danger'=>['panel'=>['type'=>'danger','before'=>'']],
		'panel-success'=>['panel'=>['type'=>'success','before'=>'']],
		'panel-warning'=>['panel'=>['type'=>'warning','before'=>'']],
	]
];



$config = [
    'id' => 'egroup',
    'name' => 'egroup.vn',
    'basePath' => dirname(__DIR__),
	'language' => 'vi',
//	'language' => 'en',
	'sourceLanguage' => 'vi',
    'bootstrap' => ['log'],
	//'defaultRoute' => $defaultRoute,
	'defaultRoute' => '/frontend/document/index',
//	'catchAll' => ['site/under-construction'], // set to all routers. used to when turn off website
    'components' => [

        'request' => [
//            'enableCsrfValidation' => false,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'wA18MhTjSBlXySHe2cRz1EHIa6G9SBcv',
            'class' => 'app\components\Request',
            'noCsrfRoutes' => [
                'user/site/manage-login',
                'common/upload',
            ]
        ],
		'redis' => [
				'class' => 'yii\redis\Connection',
				'hostname' => '127.0.0.1',
				'port' => 6379,
				'database' => 0,
		],
       // 'cache' => [
//            'class' => 'yii\caching\FileCache',
//            'cachePath' => '@runtime/cache',

//            'class' => 'yii\redis\Cache',
//            'redis' => [
//				'class' => 'yii\redis\Connection',
//                'hostname' => '127.0.0.1',
//                'port' => 6379,
//                'database' => 0,
//                'password' => '',
//            ],


            //'class' => 'yii\caching\MemCache',
           // 'servers' => [

//				[
//						'host' => '127.0.0.1',
//						'port' => 10013,
//						'weight' => 60,
//				],
//                [
//                    'host' => 'server2',
//                    'port' => 11211,
//                    'weight' => 40,
//                ],
          // ],
		'cache' => [
				'class' => 'yii\caching\MemCache',
				'servers' => [
						[
								'host' => '127.0.0.1',
								'port' => 11211,
								'weight' => 100,
						],
//						[
//								'host' => 'server2',
//								'port' => 11211,
//								'weight' => 50,
//						],
				],
		],

       // ],
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/user/user/login']
        ],
        'errorHandler' => [
            'errorAction' => '/site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'viewPath' => '@app/mail',
//            'useFileTransport' => YII_ENV_DEV ? true : false,
            'useFileTransport' => false,
				'transport' => [
						'class' => 'Swift_SmtpTransport',
						'host' => 'smtp.gmail.com',
						'username' => 'duynv@egame.vn',
						'password' => 'nguyenduy123654',
						'port' => '587',
						'encryption' => 'tls',
				],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'categories' => [
                        'yii\db\*',
//                        'yii\web\HttpException:*',
                    ],
                    'except' => [
//                    'yii\web\HttpException:404',
                        'yii\web\HttpException:500',
                    ],
                ],

            ],
        ],
		
//		'db' => [
//			'class' => 'yii\db\Connection',
//			'dsn' => 'mysql:host=123.30.174.153;port=3307;dbname=egame', //maybe other dbms such as psql,...
//			'username' => 'egame',
//			'password' => '5iowVy316YxZKbr7Fqz',
//			'charset' => 'utf8',
//			'enableQueryCache' => false,
//			'enableSchemaCache' => false,
//			'schemaCacheDuration' => 3600*24*365*10,
//		],
        'db' => require(__DIR__ . '/db.php'),
       /* 'elasticsearch' => [
            'class' => 'yii\elasticsearch\Connection',
            'nodes' => [
                ['http_address' => '127.0.0.1:9200'],
                // configure more hosts if you have a cluster
            ],
        ],*/
	    'urlManager' => [
		    'enablePrettyUrl' => true,
//		    'enableStrictParsing' => true,
		    'showScriptName' => false,
//		    'suffix' => '.html',
		    'rules' => $rewrite,
	    ],
	    'authManager' => [
            'class' => 'app\components\rbac\AuthManager',
		    'itemTable' => 'auth_item',
		    'itemChildTable' => 'auth_item_child',
		    'assignmentTable' => 'auth_assignment',
		    'ruleTable' => 'auth_rule',
	    ],
	    'assetManager' => [
			//'basePath' => STATIC_FILE,
            'converter' => [
                'class' => 'yii\web\AssetConverter',
//                'forceConversion' => true,
                'commands' => [
//					'scss' => ['css', 'minify -css >> {to}']
//                  'less' => ['css', 'lessc {from} {to} --no-color'],
//                  'ts' => ['js', 'tsc --out {to} {from}'],
                ],
            ],
//			'bundles' => (require __DIR__ . '/assets_compressed.php'),
		    'bundles' => [				

			   // 'yii\web\JqueryAsset' => [
//				    'js' => [
//					    'jquery.min.js',
//				    ],
			   // ],
                //'yii\jui\JuiAsset' => [
//                    'js' => [
//                        'jquery-ui.min.js'
//                    ],
//                    'css' => [
//						'themes/flick/jquery-ui.min.css',
//					],
               // ],
			    'yii\bootstrap\BootstrapAsset' => [
				    //'css' => ['css/bootstrap.min.css'],
				    'js' => [
					    'js/bootstrap.min.js',
					    //'js/bootstrap.js',
				    ],
			    ],
			    'yii\authclient\widgets\AuthChoiceAsset' => [
				    'css' => [],
			    ],

		    ]
	    ],
	    'session' => [
		    //'class' => 'yii\web\DbSession',
	    ],
	    'authClientCollection' => [
		    'class' => \yii\authclient\Collection::className(),
		    'clients' => [
			    'facebook' => [
				    'class' => \yii\authclient\clients\Facebook::className(),
				    'clientId' => $params['facebook']['appId'],
				    'clientSecret' => $params['facebook']['appSecret'],
				    // https://developers.facebook.com/docs/facebook-login/permissions/v2.1#reference
				    // https://developers.facebook.com/docs/graph-api/reference/v2.1/status
				    // public_profile, email, user_birthday, user_friends, read_friendlists, user_location, user_location, user_website, user_photos, user_status, read_stream
				    // publish_actions, manage_pages
                    'scope' => 'public_profile, email, user_friends, user_birthday', // user_birthday
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
//                    'returnUrl' => "http://{$_SERVER['SERVER_NAME']}/login/facebook",

                ],
			    'google' => [
				    'class' => 'yii\authclient\clients\GoogleOAuth',
				    'clientId' => $params['google']['clientId'],
				    'clientSecret' => $params['google']['clientSecret'],
//				    'scope' => 'profile email https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.login https://picasaweb.google.com/data/',
//				    'returnUrl' => "http://local.toicode.com/login/google",
//				    'returnUrl' => "http://{$_SERVER['SERVER_NAME']}/login/google",
			    ],
		    ],
	    ],
	    'i18n' => [
		    'translations' => [
			    '*' => [
				    'class' => 'yii\i18n\PhpMessageSource',
//				    'basePath' => '@app/messages',
//				    'sourceLanguage' => 'en-US',
				    'fileMap' => [
//					    'yii' => 'yii.php',
//					    'app' => 'app.php',
//					    'user/common' => 'user/common.php',
				    ],
			    ],
		    ],
	    ],
	    'view' => [
		    'class' => 'app\components\View',
		    'theme' => [ // http://www.yiiframework.com/doc-2.0/guide-output-theming.html
			    'baseUrl' => "@web/themes/".THEME,
			    'basePath' => "@app/themes/".THEME,


			    'pathMap' => [
				    '@app/views' => ["@app/themes/".THEME/*, '@app/themes/v0',*/],
				    '@app/widgets' => "@app/themes/".THEME."/widgets",

				    '@app/modules/user/views' => "@app/themes/".THEME."/modules/user",
                    '@app/modules/user/widgets/views' => "@app/themes/".THEME."/modules/user/widgets",

                    '@app/modules/admin/views' => "@app/themes/".THEME."/modules/admin",

                    '@app/modules/frontend/views' => "@app/themes/".THEME."/modules/frontend",
                    '@app/modules/frontend/widgets/views' => "@app/themes/".THEME."/modules/frontend/widgets",


                ],
		    ],
	    ],
    ],
    'params' => $params,
	'modules' => $modules,
];

	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
			'class' => 'yii\debug\Module',
			'allowedIPs' => ['*'],
			'panels' => [
					'elasticsearch' => [
							'class' => 'yii\elasticsearch\DebugPanel',
					],
			],
	];

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = 'yii\gii\Module';


return $config;
