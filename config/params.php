<?php

$data = [
	'dateFormat' => 'd/m/Y',
	'dateTimeFormat' => 'd/m/Y hh:ii',

	'permission' => [
	    [
			'name' => 'time-use-index',
			'description' => 'Xem danh sách',
			'group' => 'time-use',
			'group_name' => 'Quản lý tình hình sử dụng'
		],
		[
			'name' => 'time-use-create',
			'description' => 'Thêm tình hình sử dụng',
			'group' => 'time-use',
			'group_name' => 'Quản lý tình hình sử dụng'
		],
		[
			'name' => 'time-use-update',
			'description' => 'Cập tình hình sử dụng',
			'group' => 'time-use',
			'group_name' => 'Quản lý tình hình sử dụng'
		],
		[
			'name' => 'time-use-delete',
			'description' => 'Xóa tình hình sử dụng',
			'group' => 'time-use',
			'group_name' => 'Quản lý tình hình sử dụng'
		],
	]

];

return $data;