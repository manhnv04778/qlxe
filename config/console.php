<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        // http://www.yiiframework.com/doc-2.0/yii-rbac-dbmanager.html
        // yii migrate --migrationPath=@yii/rbac/migrations/
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'itemTable' => 'user_auth_item',
            'itemChildTable' => 'user_auth_item_child',
            'assignmentTable' => 'user_auth_assignment',
            'ruleTable' => 'user_auth_rule',
//            'itemTable' => 'doctor_auth_item',
//            'itemChildTable' => 'doctor_auth_item_child',
//            'assignmentTable' => 'doctor_auth_assignment',
//            'ruleTable' => 'doctor_auth_rule',
        ],
    ],
    'params' => $params,
];
