<?php
Yii::$aliases['@theme'] = APP_PATH .'/themes/'.THEME;
Yii::$aliases['@theme_app'] = APP_PATH .'/themes/mobile';
Yii::$aliases['@WideImage'] = APP_PATH . '/libraries/WideImage';


Yii::$classMap['yii\helpers\ArrayHelper']   = APP_PATH . '/helpers/ArrayHelper.php';
Yii::$classMap['yii\helpers\FileHelper']    = APP_PATH . '/helpers/FileHelper.php';
Yii::$classMap['yii\helpers\Html']          = APP_PATH . '/helpers/Html.php';
Yii::$classMap['yii\helpers\StringHelper']  = APP_PATH . '/helpers/StringHelper.php';
Yii::$classMap['yii\imagine\Image']         = APP_PATH . '/components/Image.php';


