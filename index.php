<?php
define('IS_LOCAL', strpos($_SERVER['DOCUMENT_ROOT'], '/data/') === false);

define('THEME', 'v1');
define('THEME_MOBILE', 'mobile');

$domain = ($_SERVER['HTTP_HOST'] == 'egame.vn') ? 'http://stc.egame.vn' : 'http://stc.cuocthi.cpvm.vn';

$static = IS_LOCAL ? 'http://'.$_SERVER['HTTP_HOST'] : $domain;
//$static = IS_LOCAL ? 'http://'.$_SERVER['HTTP_HOST'] : 'http://stc.cuocthi.cpvm.vn';
define('STATIC_FILE', $static);
define('STATIC_CP', 'http://stc.cuocthi.cpvm.vn');

$debug = IS_LOCAL || isset($_GET['debug']) || isset($_COOKIE['debug']) || $_SERVER['HTTP_HOST'] = 'chinhphucvumon.vn';

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', $debug ? 'dev' : 'prod'); // prod|dev|test

define('APP_PATH', __DIR__);

require(APP_PATH . '/vendor/autoload.php');
require(APP_PATH . '/vendor/yiisoft/yii2/Yii.php');
require(APP_PATH . '/config/bootstrap.php');
$config = require(APP_PATH . '/config/config.php');

(new yii\web\Application($config))->run();
